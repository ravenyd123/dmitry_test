<?php

include_once $_SERVER['DOCUMENT_ROOT'] . '/assets/DB.php';

$db = DB::Connection();

function GetOnlyMyGroupPlatform ($iduser){
    $firstPlatformUser = GetPlatformsByFirstLevel($iduser);
    $platformList = GetAllFirstPlatform($firstPlatformUser);
    $platforms = GetArrayPlatfornByMyFirst($platformList);
    return $platforms;
}


function GetArrayPlatfornByMyFirst($listplatform)/// получить массив id и названия площадок при моей главной площаке
{
    $data = [];
    $db = DB::Connection();
    $result1 = $db->query("SELECT id, name FROM platform WHERE id IN ({$listplatform})");
    $K=0;
    while ($row = $result1->fetch_assoc()) {
    	$data[$K]['Id'] = $row['id'];
 		$data[$K]['PlatformName'] = $row['name'];
 		$K++;
       // $data[$row['id']] = $row['name'];


    }
    return $data;
}

/* Главная площадка пользователя */
function GetPlatformsByFirstLevel($id)/// Все платформы и цеха в твоей главной платформе
{
    $db = DB::Connection();
    $query = "SELECT `platform` FROM `users` WHERE id = {$id}";
    
    $result = $db->query($query)->fetch_assoc();
    $pl = explode(",",$result['platform']);
    $platforms = intval($pl['0']);

    $idparrent = GetFirstDB($platforms);
    if ($idparrent==0) {
        $idFirstPlatform = $platforms;
    } else {
        $idparrent2 = GetFirstDB($idparrent);
        if ($idparrent2==0) {
            $idFirstPlatform = $idparrent;
        } else {
            $idparrent3 = GetFirstDB($idparrent2);
            if ($idparrent3==0) {
                $idFirstPlatform = $idparrent2;
            } else {
                $idFirstPlatform = $idparrent3;
            }
        }
    }
    return $idFirstPlatform;
}

/* Запрос в базу для фунции выше */
function GetFirstDB($id){
    $db = DB::Connection();
    $query = "SELECT `parent` FROM platform WHERE id = {$id}";
    $result = $db->query($query)->fetch_assoc();
    return $result['parent'];
}

/* Все площажки и цеха у главной площадки пользователя */ 
function GetAllFirstPlatform($id)
{
     $db = DB::Connection();
     $query = "SELECT `id` FROM platform WHERE parent = {$id}";
     $result = $db->query($query);
     $listplatforms = $id;
        foreach ($result as $key => $value){
            $listplatforms .= ",".$value['id'];
            $query = "SELECT `id` FROM platform WHERE parent = {$value['id']}";
            $result2 = $db->query($query);
            foreach ($result2 as $key2 => $value2){
                $listplatforms .= ",".$value2['id'];
                $query = "SELECT `id` FROM platform WHERE parent = {$value2['id']}";
                $result3 = $db->query($query);
                foreach ($result3 as $key3 => $value3){
                    $listplatforms .= ",".$value3['id'];
                }
            }
        }

return $listplatforms;   
}


/* Отправляем виды работ (опасные действия или условия) */
if ($_POST['action'] == "GetJobs"){
    $myArray = array();
    $type_jobs = $db->query("SELECT id, type AS JobsName FROM types_jobs WHERE `type_n` = 1 ORDER BY `type`");
    foreach ($type_jobs as $value){
    	$myArray[] = $value;
    }
    echo json_encode($myArray, JSON_UNESCAPED_UNICODE | JSON_HEX_TAG);
}

/* Отправляем платформы поьзователя */
if ($_POST['action'] == "GetPlatforms"){
    $userid = $_POST['userid'];
    $platforms = GetOnlyMyGroupPlatform($userid);
    echo json_encode($platforms, JSON_UNESCAPED_UNICODE | JSON_HEX_TAG);
}

/* Отправляем список что было сделано (ActionDo) */
if ($_POST['action'] == "GetActionDoList"){
    $Array = array();
    $actions = $db->query("SELECT `id`, `action_name` AS ActionDO FROM `nearmiss_initiator_actions`");
    foreach ($actions as $value){
        $Array[] = $value;
    }
    echo json_encode($Array, JSON_UNESCAPED_UNICODE | JSON_HEX_TAG);
}

/* Отправляем список статусов нарушения */
if ($_POST['action'] == "GetStatusNearMiss"){
    $Array = array();
    $actions = array(
        ["id"=>1, "status"=>"В очереди на отправку"],
        ["id"=>2, "status"=>"Отправлен"],
        ["id"=>3, "status"=>"Просмотрен"],
        ["id"=>4, "status"=>"В работе"],
        ["id"=>5, "status"=>"На согласовании"],
        ["id"=>6, "status"=>"Выполнен"],
        ["id"=>7, "status"=>"Просрочен"],
        ["id"=>8, "status"=>"Архив"]
            );
    //$actions = $db->query("SELECT `id`, `action_name` AS ActionDO FROM `nearmiss_initiator_actions`");
    foreach ($actions as $value){
        $Array[] = $value;
    }
    echo json_encode($Array, JSON_UNESCAPED_UNICODE | JSON_HEX_TAG);
}

/* Получить задачи*/
if ($_POST['action'] == "GetMyTasks"){
    $Array = array();
    $actions = $db->query("SELECT `id`, `action_name` AS ActionDO FROM `nearmiss_initiator_actions`");
    foreach ($actions as $value){
        $Array[] = $value;
    }
    echo json_encode($Array, JSON_UNESCAPED_UNICODE | JSON_HEX_TAG);
}
?>