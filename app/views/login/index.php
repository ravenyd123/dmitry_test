<? require_once $_SERVER['DOCUMENT_ROOT'].'/assets/functions.php';
if( isset($_POST['submit']) ){
  # Вытаскиваем из БД все записи, у которых логин равняеться введенному
  $user = $db->query("SELECT id,password,platform,company,count,status FROM `users` WHERE `login`='".$db->escape_string($_POST['login'])."' LIMIT 1");
  $userData = $user->fetch_assoc();
  $userAmount = $user->num_rows;

  # Если пользователь найден в базе
  if ($userAmount == 1) {

    # Сравниваем введеный пароль с паролем в базе
    if( $userData['password'] === md5(md5($_POST['password'])) ){
      if(($userData['status']<>2) AND ($userData['status']<>1)) {

      # Массив ошибок
      $error = array();

      #Интервал дат
      $now = date("Y-m-d H:i:s");
      $end = date("Y-m-d H:i:s",strtotime("+1 days"));

      # Если лицензия новая генерируем случайное число и шифруем его
      $hash = md5(generateCode(10));

      # Проверяем оставшееся количество активных лицензий данного пользователя за сутки
      $sessionAmount = $db->query(
      "SELECT *  FROM `sessions`
       WHERE 
       `login` = '".$db->escape_string($_POST['login'])."' AND
       `date_begin` <= '$now' AND 
       `date_end` >= '$now'
       ")->num_rows;

      if ( $sessionAmount > 20 ) {
        $error['licenseAmount'] = "Ваш запас лицензий исчерпан($sessionAmount/10). Обратитесь к системному администратору";
      }

      # Проверяем дату и время окончания сессии
      if ( isset($_COOKIE['session']) && empty($error) ) {
        $ending = $db->query(
        "SELECT *  FROM `sessions` 
         WHERE `login`='".$db->escape_string($_POST['login'])."' AND 
         `session`='".$_COOKIE['session']."' 
         ORDER BY date_begin 
         DESC LIMIT 1")->fetch_assoc();

        if ( strtotime($now) < strtotime($ending["date_end"]) ) {
          # Счетчик заходов пользователя
          visitCount($userData['id'], $userData['count'] + 1);
          # Если ошибок нет, редирект на главную страницу
          header("Location: tasks"); exit();
        }
      }

      // var_dump($error);

      if ( empty($error) ) {

        # Считываем user-agent и IP
        $userAgent = $_SERVER['HTTP_USER_AGENT'];
        $userIp = $_SERVER["REMOTE_ADDR"];

        # Создаем сессию
        $db->query("INSERT INTO `sessions` SET session='".$hash."', user_agent='".$userAgent."', ip='".$userIp."' , date_begin='".$now."' , date_end='".$end."' , login='".$_POST['login']."'") or die("MySQL Error: " . $db->error());
        
        # Добавляем пользователю хеш
        $db->query("UPDATE users SET hash='".$hash."' WHERE id='".$userData['id']."'") or die("MySQL Error: " .$db->error());
     
        setcookie('session', $hash, time() + 60*60*24, '/', NULL, NULL, NULL);
        setcookie('login', $_POST['login'], time() + 60*60*24, '/', NULL, NULL, NULL);
        setcookie('id', $userData['id'], time() + 60*60*24, '/', NULL, NULL, NULL);
        setcookie('platform', $userData['platform'], time() + 60*60*24, '/', NULL, NULL, NULL);
        setcookie('company', $userData['company'], time() + 60*60*24, '/', NULL, NULL, NULL);

        # Счетчик заходов пользователя
        visitCount($userData['id'], $userData['count'] + 1);

        # Если ошибок нет, редирект на главную страницу
        header("Location: tasks"); exit(); 
      }

      print_r('test');
    }else{ 
      $error['wrongLogin'] = 'Вы уволены или находитесь в декрете, поэтому не можете войти в систему';
    }
    }else{
      $error['wrongPassword'] = 'Неправильный пароль'; 
    }

  }else{
    $error['wrongLogin'] = 'Данного пользователя не существует'; 
  }
// if ($userData['status'] == 2) {$error['wrongLogin'] = 'Вы были уволены и не можете войти в систему';}
// if ($userData['status'] == 1) {$error['wrongLogin'] = 'Вы в декрете и не можете войти в систему';}

}

?>

<? require_once $_SERVER['DOCUMENT_ROOT'].'/app/views/header.php'; ?>

<body>
  <div class="wrapper-page animated fadeInDown">
      <div class="panel panel-color panel-primary w-100">
          <div class="panel-heading"> 
             <h3 class="text-center m-t-10"> Войти в <strong>EMP</strong> </h3>
          </div>

          <form class="form-horizontal m-t-40" id="auth" method="POST">

              <div class="form-group <?=( isset($error['wrongLogin']) ? 'has-error' : ''); ?>">
                  <div class="col-xs-12">
                      <input class="form-control" type="text" placeholder="Логин" name="login">
                      <div class="help-block">
                        <? if ( isset($error['wrongLogin']) ){ ?>
                          <?=$error['wrongLogin'];?> 
                        <? } ?>
                      </div>
                  </div>
              </div>

              <div class="form-group <?=( isset($error['wrongPassword']) ? 'has-error' : ''); ?>">
                  <div class="col-xs-12">
                      <input class="form-control" type="password" placeholder="Пароль" name="password">
                      <div class="help-block">
                        <? if ( isset($error['wrongPassword']) ){ ?>
                          <?=$error['wrongPassword'];?> 
                        <? } ?>
                      </div>
                  </div>
              </div>

              <div class="form-group ">
                  <div class="col-xs-12">
                      <label class="cr-styled">
                          <input type="checkbox" checked>
                          <i class="fa"></i> 
                          Запомнить меня
                      </label>
                  </div>
              </div>

              <? if ( isset($error['licenseAmount']) ){ ?>
              <p class="text-danger text-center m-b-30">
                <?=$error['licenseAmount'];?> 
              </p>
               <? } ?>

              <div class="form-group text-right">
                  <div class="col-xs-12">
                      <input class="btn btn-primary w-md" type="submit" name="submit" value="Войти" />
                  </div>
              </div>

          </form>

      </div>
  </div>



</body>

</html>