<?php
include_once $_SERVER['DOCUMENT_ROOT'].'/vendor/phpoffice/phpexcel/Classes/PHPExcel.php'; // Подключаем библиотеку
include_once $_SERVER['DOCUMENT_ROOT'].'/assets/excel/config_excel.php'; // Подтягиваем файл настроек екселя
include_once $_SERVER['DOCUMENT_ROOT'].'/assets/functions.php'; // Подтягиваем файл функций 
$new_file_name = generateCode($length=9); //генерируем рандомное новое имя файла с 9 символами

	/* Проверяем загруженный файл */
$firstStr = $_FILES['userfile']['name']; // Переменная = имя файла
$first_name = explode('_', $firstStr, 2); // Выдергиваем тип отчета из имени
$returnVal = preg_match('/\[(.*)\]/s', $firstStr, $matches); // Вытаскиваем из имени файла содержимое квадратных скобок [шифрованный id ]
$query = $db->query("SELECT `unique-key` FROM `calendars` WHERE `unique-key` = '".$matches[1]."'"); // Смотрим есть ли такая самооценка у нас.
$num_rows = $query->num_rows;
// var_dump ($matches);
/* Если номера самооценки нету у нас или в названии файла нету кода, то выводим ошибку */
// if ($num_rows == 0) {
// 	$errors['evaluation']['header'] = 'Ошибка загрузки файла';
// 	$errors['evaluation']['text'] = 'Выбранный файл неверный или поврежден'; }
/* Проверка формат загружаемого файла */	
if (($_FILES['userfile']['type'] <> "application/vnd.ms-excel") && ($_FILES['userfile']['type'] <> "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")) {$errors['type']['header'] = 'Ошибка загрузки файла';
    $errors['type']['text'] = 'Неверный формат файла'; }
/* Проверка размер файла Ограничение до 20 Мб*/	
if ($_FILES['userfile']['size'] > 20971520)  {
	$errors['type']['header'] = 'Ошибка загрузки файла';
    $errors['type']['text'] = 'Файл слишком большой'; }
/* Собираем все ошибки. Если есть ошибки, то выводим их и the end. Если нет ошибок, то продолжаем выполнять скрипт далее */
if( !empty($errors) ){
	print_r ( json_encode($errors, JSON_UNESCAPED_UNICODE) ); 
	exit;
} else { 

move_uploaded_file($_FILES["userfile"]["tmp_name"], $path_file_excel.$new_file_name);// Перемещаем загруженный файл из tmp в свою папку и даем ему рандомное имя
$result = array(); // Создаем массив
	/* PHPExcel */
$inputFileType = PHPExcel_IOFactory::identify($path_file_excel.$new_file_name);  // узнаем тип файла, excel может хранить файлы в разных форматах, xls, xlsx и другие
$objReader = PHPExcel_IOFactory::createReader($inputFileType); // создаем объект для чтения файла
$objPHPExcel = $objReader->load($path_file_excel.$new_file_name); // загружаем данные файла в объект
$result = $objPHPExcel->getActiveSheet()->toArray(); // выгружаем данные из объекта в массив
$nb_string = (count($result)-1) ; // количество строк в массиве
$k=1; // Отступаем строчки, ибо там оглавление.  


$user_numb = $result[0][41];
$us = 2;
while ($us < $user_numb+2) {
	
$users_list[] = $result[$us][42];
$user_login[] = $result[$us][41];
$us++;
}
// var_dump ($users_list);
while ($k++<$nb_string) {
	
$key = array_search ($result[$k][6],$users_list );	
$result[$k][6] = $user_login[$key];
if (is_bool($key) == true) {$result[$k][6] = "0";} 

	
	/* Подменяем Слова на соответствующие цифры Статуса Самооценки */
	 if ($result[$k][12]=="Более 90%") $result[$k][12]=1;
	 if ($result[$k][12]=="От 71% до 90%")  $result[$k][12]=2;
	 if ($result[$k][12]=="От 41% до 70%")  $result[$k][12]=3;
	 if ($result[$k][12]=="Менее 40%")  $result[$k][12]=4;
	 if ($result[$k][12]=="N/A")  $result[$k][12]=5;
		
// var_dump ($nb_string);
// var_dump ($result[2]);
	 /* Загружаем все данные в базу */
    $db->query("UPDATE `collector` SET 
`fio`='".$result[$k][6]."',
`date-plan`='".$result[$k][7]."',
`recommendations`='".$result[$k][9]."',
`fact-date`='".$result[$k][11]."',
`current-status-compliance`='".$result[$k][12]."',
`reason-failure`='".$result[$k][13]."'
 WHERE 
`id`='".$result[$k][14]."'");

$message['ok']['header'] = 'Загрузка файла';
$message['ok']['text'] = 'Файл удачно загружен'; }	
// var_dump ($result[$j][12]);


}
$errors = array(); // Все ошибки в массив
unlink ($path_file_excel.$new_file_name);// Удаляем загруженный файл
print_r ( json_encode($message, JSON_UNESCAPED_UNICODE) ); 
?>