<?php
/* Выгрузка отчетов xls */
/* Версия 1.17 */

include_once $_SERVER['DOCUMENT_ROOT'] .'/assets/functions.php'; // Подтягиваем файл функций (подключение к базе)
include_once $_SERVER['DOCUMENT_ROOT'].'/vendor/phpoffice/phpexcel/Classes/PHPExcel.php'; // Подключаем библиотеку

/* Если отчет открыт из подзадачи (несколько видов работ) */
if (isset($_GET['task_all'])) {
    $db = DB::Connection();
    $query = "SELECT `nreport`, `typen`, `platform` FROM `tasks` WHERE `pid`=".$_GET['task_all'];
    $task = $db->query($query)->fetch_assoc();
    
    $query = "SELECT `type` FROM `name_type_task` WHERE `id`=".$task['typen'];
    var_dump ($query);
    $type_task = $db->query($query)->fetch_assoc();
    
    $id_self_result = $db->query("SELECT `id` FROM `reports` WHERE `bunch`=".$task['bunch']);
    var_dump($id_self_result);
    foreach ($id_self_result as $id_self){
        $id_self_list.=",".$id_self['id'];
    }
    $id_audit  = substr($id_self_list, 1);
    $type = $type_task['type'];
    $platform_audit =$task['platform'];
}

/* Если отчет открыт из подзадачи (Один вид работ) */
if (isset($_GET['task_one'])) {
    $db = DB::Connection();
    $task_data = $db->query("SELECT `platform`, `nreport`, `job` FROM `tasks` WHERE `id` = '".$_GET['task_one']."' LIMIT 1")->fetch_assoc();
  // var_dump($task_data);
    $task_data2 = $db->query("SELECT `typen`,`id` FROM `reports` WHERE `bunch` = '".$task_data['nreport']."' AND `job` IN('".$task_data['job']."') LIMIT 1")->fetch_assoc();

   $query = "SELECT `id`, `type` FROM `name_type_task` WHERE `id` =".$task_data2['typen'];
  $type_task = $db->query($query)->fetch_assoc();
  //var_dump($query);
 // var_dump($type_task);
    $id_audit       = $task_data2['id'];
    $platform_audit = $task_data['platform'];
    $type           = $type_task['type'];
	
} 
/* Забираем всю инфу по отчетам */
if (isset($_GET['id']) and (isset($_GET['platform'])) and (isset($_GET['type']))) {
    $id_audit       = $_GET['id'];
    $platform_audit = $_GET['platform'];
    $type           = $_GET['type'];
}
$login = $_COOKIE['login'];
// $id_audit       = "1,2,3,4,5";
// $platform_audit = 1;
// $type           = "audit";

include_once 'config_xls.php'; // Подтягиваем файл настроек екселя
/* Разделение на 3 вида отчетов: чек-лист, аудит, госорганы */
switch ($type) {
    case "self-evaluation":
        $t_report = array("Создал самооценку","Самооценка","Самооценка");
        break;
    case "audit":
        $t_report = array("Создал аудит","Аудит","Аудит");
        break;
    case "state_inspection":
        $t_report = array("Проверка госорганами","Проверка госорганами","Отчет");
        break;
}

/* Забираем требования из базы */
$query = "SELECT c.*, e.`ehs_numbers`, r.symbol, r.name AS `doc_name`, r.date AS `doc_date`, u.name, u.surname, u.`father_name`, t.type
                                    FROM `collector` as c
                                    LEFT JOIN `ehs` e ON c.ehs = e.id
                                    LEFT JOIN `types_jobs` t ON c.job = t.id
                                    LEFT JOIN `register` r ON c.document = r.nd
                                    LEFT JOIN `users` u ON c.fio = u.login
                                    WHERE `self-evaluation` IN (" . $id_audit . ")";
//var_dump($query);
$result_excel = $db->query($query);
									
							
$platform_name = getPlatformsNames($platform_audit);
// $data          = mysqli_fetch_assoc($result_excel);
// $platform_name = $data['platform_name']; // Название платформы
$date_audit = dateReportCreate($id_audit);

// $date_audit    = date('d.m.Y', $data['date']); // Дата создания отчета
$id_creator = getCreatorById($id_audit);
$fio_creater   = getSmallFioFromId($id_creator); // ФИО кто создал отчет
//var_dump ($fio_creater);
$id_audit_from_bunch = explode(',', $id_audit);
$bunch = getBunchOfId($id_audit_from_bunch['0']); // Номер отчета

/* Шапка */
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
$pos = substr_count($id_audit, ',');
if ($pos >= 1) {
  header('Content-Disposition: attachment;filename="'.$t_report[1].' №'.$bunch.' группа отчетов от '.$date_audit.'.xlsx"');
} else {
    $job_name = getJobNameBySelfId($id_audit);
   header('Content-Disposition: attachment;filename="'.$t_report[1].' №'.$bunch.' ('.$job_name.') от '.$date_audit.'.xlsx"');
}
 header('Cache-Control: max-age=0');

/* Работа с Excel */
$phpexcel = new PHPExcel(); // Создаём объект PHPExcel
/* Подготавливаем будущую таблицу (Стили, формат, печать, рамка при печати и так далее) */
$page = $phpexcel->setActiveSheetIndex(0); // Делаем активной первую страницу и получаем её
$page = $phpexcel->getActiveSheet()->mergeCells('A1:M1'); // Объединяем ячейки на самом верху с инфой	
$page->setCellValue('A1', $t_report[0].": ".$fio_creater." Отчет от ".$date_audit." / Площадка: ".$platform_name['name']." / Дата выгрузки отчета: ".date('d.m.y')." в ".date('H:i:s')." (Мск)");
$page->setTitle($t_report[2]." от ".$date_audit); // Ставим заголовок на странице

//$page->getHeaderFooter()->setOddHeader('&CТД ТИНКО: прайс-лист'); // Устанавливаем хейдер только для печати.
$page->getPageMargins()->setTop(0); //  отступ сверху
$page->getPageMargins()->setRight(0); // отступ справа
$page->getPageMargins()->setLeft(0.6); // отступ слева
$page->getPageMargins()->setBottom(0); // отступ снизу
$page->getPageMargins()->setFooter(0); // Нижний колонтитуль
$page->getPageMargins()->setHeader(0.2); // Верхний колонтитуль
$page->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE); // Альбомная ориентация
$page->getPageSetup()->SetPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4); // Размер листа при печати
$page->getPageSetup()->setFitToWidth(1);
$page->getPageSetup()->setFitToHeight(0);
$page->getHeaderFooter()->setOddFooter('&L&B'.$page->getTitle().'&RСтраница &P из &N'); // устанавливаем подвал только при печати

/* Защищаем определенные ячейки от редактирования */
$phpexcel->getActiveSheet()->getProtection()->setSheet(true); // Защита всего листа
$phpexcel->getActiveSheet()->getProtection()->setFormatCells(true); // Защита от форматирвоания ячейки
$phpexcel->getActiveSheet()->getProtection()->setFormatColumns(true); // Защита от форматирования столбца
$phpexcel->getActiveSheet()->getProtection()->setFormatRows(true); // Защита от форматирвоания строчки
$phpexcel->getActiveSheet()->getProtection()->setInsertColumns(true); //Защита от вставки столбца
$phpexcel->getActiveSheet()->getProtection()->setInsertRows(true); // Защита от вставки строки
$phpexcel->getActiveSheet()->getProtection()->setDeleteColumns(true); //Защита от удаления столбца
$phpexcel->getActiveSheet()->getProtection()->setDeleteRows(true); //Защита от удаления строки
// $phpexcel->getActiveSheet()->getProtection()->setSelectLockedCells(true); //Защита от выделения (и копирования)

$page = $phpexcel->getActiveSheet()->freezePane('A3'); // Закрепляем первую строчку, чтобы не двигалась

/* Записываем и прячем служебную информацию */
/* Если это самооценка: */
if ($type == "self-evaluation") {
	$hide_column = array("O", "P", "Q"); // Столбцы, которые мы прячем для самооценки
    /* Оглавление для служебных записей */
    $page->setCellValue("O2", "Логины сотрудников");
    $page->setCellValue("P2", "ФИО сотрудников");
    $page->setCellValue("Q2", "Количество сотрудников");
	foreach ($hide_column as $value) {$page->getColumnDimension($value)->setWidth("0");}
	/* Достаем список пользователей нашей компании */
	$users_list_arr = getUsersByPlatformFromId($platform_audit);
	foreach ($users_list_arr as $key => $value) {
    	$users_count = $nb_row = $key + 3;
    	$full_fio = $value['surname']." ".$value['name']." ".$value['father_name'];
    	$page->setCellValue("O$nb_row", $value['login']); // логин пользователя
    	$page->setCellValue("P$nb_row", $full_fio); // Полное ФИО
    	$page->setCellValue("Q3", ($key + 1)); // Кол-во пользователей
    	// $page->setCellValue("AR1",$cipher); // Вводим уникальный ключ самооценки.
    }
}
$page->setCellValue("R2", "Тип отчета");
$page->setCellValue("S2", "Номера отчетов");
$page->setCellValue("R3", $type); // Вставляем тип отчета
$page->getColumnDimension("R")->setWidth("0"); // Скрываем его
$page->setCellValue("S3", $id_audit); // Вставляем номер(-а) отчетов 
$page->getColumnDimension("S")->setWidth("0");
/* Настройка стилей в оглавлении */
$arHeadStyle1 = array(
    'bold' => true,
    'font' => array(
        'size' => 10,
        'name' => 'Calibri'
    ),
    'fill' => array(
        'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'rotation' => 0,
        'color' => array(
            'rgb' => "FFC000"
        )
    ), // Заливка ячеек определенным цветом
    'borders' => array(
        'allborders' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN
        )
    ), // Установка рамок (тонких)
    'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER, // Выравнивание текста по верт. и гор. в оглавлении
        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
        'wrap' => true
    )
); // Перенос строк
$arHeadStyle2 = array(
    'bold' => true,
    'font' => array(
        'size' => 10,
        'name' => 'Calibri'
    ),
    'fill' => array(
        'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'rotation' => 0,
        'color' => array(
            'rgb' => "FFFFFF"
        )
    ), // Заливка ячеек определенным цветом
    'borders' => array(
        'allborders' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN
        )
    ), // Установка рамок (тонких)
    'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER, // Выравнивание текста по верт. и гор. в оглавлении
        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
        'wrap' => true
    )
); // Перенос строк
$arHeadStyle3 = array(
    'bold' => true,
    'font' => array(
        'size' => 10,
        'name' => 'Calibri'
    ),
    'fill' => array(
        'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'rotation' => 0,
        'color' => array(
            'rgb' => "FCE4D6"
        )
    ), // Заливка ячеек определенным цветом
    'borders' => array(
        'allborders' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN
        )
    ), // Установка рамок (тонких)
    'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER, // Выравнивание текста по верт. и гор. в оглавлении
        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
        'wrap' => true
    )
); // Перенос строк
/* Стиль шрифтов в самой таблице */
$arBodyStyle  = array(
    'font' => array( // 'color' => array('rgb' => '778899'),       
        'size' => 10,
        'name' => 'Calibri'
    ),
    'borders' => array(
        'allborders' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN
        )
    ), // Установка рамок (тонких)
    'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER, // Выравнивание текста по верт. и гор. в оглавлении
        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
        'wrap' => true
    )
); // Перенос строк
/* Отдельный стиль для столбца НЕОБХОДИМЫЕ МЕРОПРИЯТИЯ (текст не по центру) */
$arBodyStyle2 = array(
    'font' => array( // 'color' => array('rgb' => '778899'),       
        'size' => 10,
        'name' => 'Calibri'
    ),
    'borders' => array(
        'allborders' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN
        )
    ), // Установка рамок (тонких)
    'alignment' => array(
        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER, // Выравнивание текста повертикали.
        'wrap' => true
    )
); // Перенос строк

/* Отдельный стиль для столбца ПУНКТ с гиперссылкой */
$arBodyStyle3 = array(
    'font' => array(
        'color' => array(
            'rgb' => '0000FF'
        ), // Синий шрифт   
        'size' => 10,
        'name' => 'Calibri',
        'underline' => 'true'
    ), // Подчеркивание
    'borders' => array(
        'allborders' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN
        )
    ), // Установка рамок (тонких)
    'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER, // Выравнивание текста по верт. и гор. в оглавлении
        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
        'wrap' => true
    )
); // Перенос строк

foreach (range($start_col, $end_col) as $key => $letter) {

    $page->setCellValue($letter.'2', $name_column[$key]); // Добавляем в ячейки-оглавление  
    $page->getColumnDimension($letter)->setWidth($column_width[$key]); // Устанавливаем ширину всех необходимых столбцов
}
if ($type == "self-evaluation") {
    $head_style = array(
        'E2' => $arHeadStyle1,
        'F2' => $arHeadStyle1,
        'G2' => $arHeadStyle1,
        'H2' => $arHeadStyle1,
        'A2' => $arHeadStyle2,
        'B2' => $arHeadStyle1,
        'C2' => $arHeadStyle2,
        'D2' => $arHeadStyle3,
        'I2' => $arHeadStyle3,
        'J2' => $arHeadStyle3,
        'K2' => $arHeadStyle3,
        'L2' => $arHeadStyle3,
        'M2' => $arHeadStyle3
    );
} else {
    $head_style = array(
        'E2' => $arHeadStyle1,
        'F2' => $arHeadStyle1,
        'A2' => $arHeadStyle2,
        'B2' => $arHeadStyle3,
        'C2' => $arHeadStyle3,
        'D2' => $arHeadStyle3,
        'G2' => $arHeadStyle3,
        'H2' => $arHeadStyle3,
        'I2' => $arHeadStyle3,
        'J2' => $arHeadStyle3,
        'K2' => $arHeadStyle3
    );
}
foreach ($head_style as $key => $value) {
    $page->getStyle($key)->applyFromArray($value);
}
/* Подсказки оглавления + автофильтр*/
if ($type == "self-evaluation") {
	$page->getPageSetup()->setPrintArea('A:H');
	$phpexcel->getActiveSheet(0)->setAutoFilter('A2:M2'); // Автофильтр ячек
    $data_tips = array(
        array('Выберите из списка соответствующий статус', '90', '120', 'E2'),
        array('Выберите из списка ответственного человека', '78', '100', 'F2'),
        array('Укажите дату формата дд.мм.гггг. Пример:30.12.2017', '70', '135', 'G2'),
        array('Укажите необходимые мероприятия', '70', '100', 'H2')
    );
} else {
    $page->getPageSetup()->setPrintArea('A:F');
	$phpexcel->getActiveSheet(0)->setAutoFilter('A2:K2'); // Автофильтр ячеек
    $data_tips = array(
        array('Выберите из списка соответствующий статус', '90', '120', 'E2'),
        array('Укажите выявленные несоответствия', '70', '100', 'F2')
    );
}
foreach ($data_tips as $key => $value_tips) {
    $phpexcel->getActiveSheet()->getComment($value_tips[3])->getText()->createTextRun($value_tips[0]); // текст
    $phpexcel->getActiveSheet()->getComment($value_tips[3])->setHeight($value_tips[1]); // height set to 300
    $phpexcel->getActiveSheet()->getComment($value_tips[3])->setWidth($value_tips[2]); // width set to 400
}
$page = $phpexcel->setActiveSheetIndex(0);
foreach ($result_excel as $key => $data_demands) {
    $k++;
    $str_line = $key+3;

    $number_data = $data_demands['symbol']." ".$data_demands['doc_date']; //Номер и дата актуализации
    $fio_responsive = $data_demands['surname']." ".$data_demands['name']." ".$data_demands['father_name']; // ФИО ответственного

      	/* Замена цифр на соответствующий статус соответствия */
    foreach ($stats_match as $clue => $name_match) {
    	if ($data_demands['status-compliance'] == $clue) {
            $name_status = $name_match;
             }
    	}
    /* Если это самооценка */
    if ($type == "self-evaluation") {
 	
        		/* Создание стили для столбца ФИО + список пользователей */
	$page = $phpexcel->getActiveSheet()->getCell("F$k")->getDataValidation(); // колонка
	$page->setType(PHPExcel_Cell_DataValidation::TYPE_LIST);
	$page->setErrorStyle(PHPExcel_Cell_DataValidation::STYLE_INFORMATION);
	$page->setAllowBlank(true);
	$page->setShowInputMessage(true);
	$page->setShowErrorMessage(true);
	$page->setShowDropDown(true);
	$page->setErrorTitle('Ошибка ввода');
	$page->setError('Не правильное значение');
	$page->setPromptTitle($xml_title1); // Оглавление подсказки
	$page->setPrompt('Список ответственных'); // Всплывающая подсказка(сам текст)	
	$page->setFormula1('P3:P'.$users_count); // элементы выпадающего списка
	
     /* Проверка ячейки с датами на наличие правильно написанной даты (только для самооценки) */ 
    //$page = $phpexcel->setActiveSheetIndex(0);
    $page = $phpexcel->getActiveSheet()->getCell("G$k")->getDataValidation(); // колонка
    $page->setType(PHPExcel_Cell_DataValidation::TYPE_DATE);
    $page->setOperator(PHPExcel_Cell_DataValidation::OPERATOR_BETWEEN); 
    $page->setAllowBlank(true); 
    $page->setShowInputMessage(true); 
    $page->setShowErrorMessage(true); 
    $page->setErrorStyle(PHPExcel_Cell_DataValidation::STYLE_STOP); //'stop' 
    $page->setErrorTitle('Ошибка');
    $page->setError('Не правильно указана дата. Формат даты должен быть: дд.мм.гггг (пример:31.12.2019)');
    // $page->setPromptTitle('Укажите дату');
    // $page->setPrompt('Формат даты:дд.мм.гггг (напрмиер:31.12.2019)');
    $page->setFormula1(20433); // 43433 - ****2018
    $page->setFormula2(73049);
    $page->setAllowBlank(true);
    $phpexcel->getActiveSheet()->getStyle('G')->getNumberFormat()->setFormatCode('dd.mm.yyyy'); 
            
        /* Корректировка дат TimeStamp => data/time Excel */
    if (($data_demands['date-plan'] == NULL) or ($data_demands['date-plan'] == 0)) {
        $data_demands['date-plan'] = "";
    } else { 
        $data_demands['date-plan'] = intval(($data_demands['date-plan']/86400) + 25570);
            // $popo = date('d.m.Y h:i:s', PHPExcel_Shared_Date::ExcelToPHP($data_demands['date-plan']));
    }


        /* Массив из значений */
        $column_data = array(
            array($arBodyStyle, $data_demands["ehs_numbers"]), // номер ehs требования
            array($arBodyStyle, $data_demands["type"]), // виды работ
            array($arBodyStyle2, str_replace(array("&nbsp;", "\r", "\n", "<span>", "</span>"), "", strip_tags($data_demands["demand"]))), // Текст требования + убираем все теги html
            array($arBodyStyle, $data_demands["event"]), // Тип требования по срокам
            array($arBodyStyle, $name_status), // Статус соответсвия
            array($arBodyStyle, $fio_responsive), // ФИО ответственного
            array($arBodyStyle, $data_demands["date-plan"]), // плановый срок приведения в соответствие
            array($arBodyStyle, $data_demands["actions"]), // Мероприятия
            array($arBodyStyle3, $data_demands["paragraph-name"]), // Пункт в документе
          //  array($arBodyStyle, $data_demands["source"]), // Источник информации
            array($arBodyStyle, Sourse_app), // Источник информации
            array($arBodyStyle, $data_demands["doc_name"]), // Наименвание НПА
            array($arBodyStyle, $number_data), // Номер и дата утверждения
            array($arBodyStyle, date("d.m.Y", $data_demands["date"])), // Дата актуализации требования
            array($arBodyStyle, $data_demands["id"]) // ID требования
        );
      
    } else {
        /* Массив из значений */
        $column_data = array(
            array($arBodyStyle, $data_demands["ehs_numbers"]), // номер ehs требования
            array($arBodyStyle, $data_demands["type"]), // виды работ
            array($arBodyStyle2, str_replace(array("&nbsp;", "\r", "\n", "<span>", "</span>"), "", strip_tags($data_demands["demand"]))), // Текст требования + убираем все теги html
            array($arBodyStyle, $data_demands["event"]), // Тип требования по срокам
            array($arBodyStyle, $name_status), // Статус соответсвия
            array($arBodyStyle, $data_demands["actions"]), // Мероприятия
            array($arBodyStyle3, $data_demands["paragraph-name"]), // Пункт в документе
           // array($arBodyStyle, $data_demands["source"]), // Источник информации
            array($arBodyStyle, Sourse_app), // Источник информации
            array($arBodyStyle, $data_demands["doc_name"]), // Наименвание НПА
            array($arBodyStyle, $number_data), // Номер и дата утверждения
            array($arBodyStyle, date("d.m.Y", $data_demands["date"])), // Дата актуализации требования
            array($arBodyStyle, $data_demands["id"]) // ID требования
        );
          /* Замена цифр на соответствующий статус соответствия */
    	foreach ($stats_match as $clue => $name_match) {
        if ($column_data['status-compliance'] == $clue) {
            $name_status = $name_match;
        }
    	}
    }

    $page = $phpexcel->setActiveSheetIndex(0);
    foreach (range($start_col, $end_col) as $key => $letter) {
        $p = $k + 2;
        $page->setCellValue("$letter$p", $column_data[$key][1]); // Вставляем значения в массив
       // $page->getCell("$letter$p")->setValue($column_data[$key][1]); // Вставляем значения в массив
        $page->getStyle("$letter$p")->applyFromArray($column_data[$key][0]);
    }
   
    /* Создание стили для столбца Статус Соответствия */
    $page = $phpexcel->getActiveSheet()->getCell("E$p")->getDataValidation(); // колонка
    $page->setType(PHPExcel_Cell_DataValidation::TYPE_LIST);
    $page->setErrorStyle(PHPExcel_Cell_DataValidation::STYLE_INFORMATION);
    $page->setAllowBlank(false);
    $page->setShowInputMessage(true);
    $page->setShowErrorMessage(true);
    $page->setShowDropDown(true);
    $page->setErrorTitle('Ошибка ввода');
    $page->setError('Значение не в листе');
    $page->setPromptTitle($xml_title1); // Оглавление подсказки
    $page->setPrompt($xml_title2); // Всплывающая подсказка(сам текст)
    $page->setFormula1('"'.$stats_match[0].','.$stats_match[1].','.$stats_match[2].','.$stats_match[3].','.$stats_match[4].','.$stats_match[5].'"'); // элементы выпадающего списка
 
    /* Применяем стили условного форматирования */
   
    /* Цвета заливки, шрифты в статусе соответствия: 'Текст','цв. шрифта','Цв. старт заливки','Цв. конечн заливки','курсив' */
    $style_format_arr  = array(
        array($stats_match[5], 'ffffff', 'ffffff', '808080', 'true'), // При Н/А
        array($stats_match[4], 'ff0000', 'ff0000', '000000', 'false'), // Менее 40%
        array($stats_match[3], 'ffff00', 'ffff00', '000000', 'false'), // 40% - 70%
        array($stats_match[2], '00ff00', '00ff00', '000000', 'false'), // 70% - 90%
        array($stats_match[1], '00ffff', '00ffff', '000000', 'false') // более 90%
    );
    $page = $phpexcel->getActiveSheet()->getCell("E$p")->getDataValidation();
    $conditionalStyles = $phpexcel->getActiveSheet()->getStyle("E$p")->getConditionalStyles();
    foreach ($style_format_arr as $key => $value) {
        ${'page'.$key} = new PHPExcel_Style_Conditional();
        ${'page'.$key}->setConditionType(PHPExcel_Style_Conditional::CONDITION_CELLIS);
        ${'page'.$key}->setOperatorType(PHPExcel_Style_Conditional::OPERATOR_EQUAL);
        ${'page'.$key}->addCondition('"'.$value[0].'"');
        ${'page'.$key}->getStyle()->getFill()->setFillType(PHPExcel_Style_Fill::FILL_GRADIENT_LINEAR);
        ${'page'.$key}->getStyle()->getFill()->getStartColor()->applyFromArray(array('rgb' => $value[1]));
        ${'page'.$key}->getStyle()->getFill()->getEndColor()->applyFromArray(array('rgb' => $value[2]));
        ${'page'.$key}->getStyle()->getFont()->getColor()->applyFromArray(array('rgb' => $value[3]));
        ${'page'.$key}->getStyle()->getFont()->setitalic('"'.$value[4].'"');
        // echo $value[3]." - ".$value[4];
        array_push($conditionalStyles, ${'page'.$key});
    }
   
    $phpexcel->getActiveSheet()->getStyle("E$p")->setConditionalStyles($conditionalStyles);
   
    $phpexcel->getActiveSheet()->getProtection()->setPassword('sis1801');

    /* Убираем защиту с определенных ячеек, которые будут редактироваться пользователями */
   if ($type == "self-evaluation") {
        $page = $phpexcel->getActiveSheet()->getStyle("G$p")->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_UNPROTECTED);
        $page = $phpexcel->getActiveSheet()->getStyle("H$p")->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_UNPROTECTED);
        $page = $phpexcel->getActiveSheet()->getStyle("I$p")->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_UNPROTECTED);
   }
    $page = $phpexcel->getActiveSheet()->getStyle("F$p")->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_UNPROTECTED);
    $page = $phpexcel->getActiveSheet()->getStyle("E$p")->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_UNPROTECTED);
}
 
/* Прячем служебную информацию*/
$page = $phpexcel->getActiveSheet();
//$page->getRowDimension($k+3)->setRowHeight(0); // высота строки 0
$page = $phpexcel->setActiveSheetIndex(0);
$objWriter = PHPExcel_IOFactory::createWriter($phpexcel, 'Excel2007');
$objWriter->save("php://output");
exit();
?>