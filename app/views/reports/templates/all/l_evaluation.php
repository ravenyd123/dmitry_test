<?php
/* Загрузка отчетов на сервер xls */
/* Версия 1.16 */
include_once $_SERVER['DOCUMENT_ROOT'].'/vendor/phpoffice/phpexcel/Classes/PHPExcel.php'; // Подключаем библиотеку
include_once $_SERVER['DOCUMENT_ROOT'].'/assets/excel/config_excel.php'; // Подтягиваем файл настроек екселя
include_once $_SERVER['DOCUMENT_ROOT'].'/assets/functions.php'; // Подтягиваем файл функций 
$new_file_name = generateCode($length=9); //генерируем рандомное новое имя файла с 9 символами

/* Проверяем загруженный файл */
$firstStr = $_FILES['userfile']['name']; // Переменная = имя файла
// $first_name = explode('_', $firstStr, 2); // Выдергиваем из названия тип отчета (чек-лист, аудит или госорганы)

/* Проверка формат загружаемого файла */	
if (($_FILES['userfile']['type'] <> "application/vnd.ms-excel") && ($_FILES['userfile']['type'] <> "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")) {
	$message['type'] = 'error';
	$message['header'] = 'Ошибка загрузки файла';
    $message['text'] = 'Неверный формат файла'; 
    print_r ( json_encode($message, JSON_UNESCAPED_UNICODE) ); 
	exit;
}
/* Проверка размер файла Ограничение до 20 Мб*/	
if ($_FILES['userfile']['size'] > 20971520)  {
	$message['type'] = 'error';
	$message['header'] = 'Ошибка загрузки файла';
    $message['text'] = 'Файл слишком большой';
    print_r ( json_encode($message, JSON_UNESCAPED_UNICODE) ); 
	exit;
}
move_uploaded_file($_FILES["userfile"]["tmp_name"], $path_file_excel.$new_file_name);// Перемещаем загруженный файл из tmp в свою папку и даем ему рандомное имя
$result = array(); // Создаем массив

	/* PHPExcel */
$inputFileType = PHPExcel_IOFactory::identify($path_file_excel.$new_file_name);  // узнаем тип файла, excel может хранить файлы в разных форматах, xls, xlsx и другие
$objReader = PHPExcel_IOFactory::createReader($inputFileType); // создаем объект для чтения файла
$objPHPExcel = $objReader->load($path_file_excel.$new_file_name); // загружаем данные файла в объект
$result = $objPHPExcel->getActiveSheet()->toArray(); // выгружаем данные из объекта в массив
$nb_string = (count($result) - 1); // количество строк в массиве
$id_audit = $result[2][18]; // Получаем кодированный ID самооценки из тела файла.
$r_type = $result[2][17];

$query = $db->query("SELECT `self-evaluation` FROM `collector` WHERE `self-evaluation` = '".$id_audit."'"); // Смотрим есть ли такая самооценка у нас.
$num_rows = $query->num_rows;

/* Если номера самооценки нету у нас или вообще таблица не та */
// var_dump($num_rows);
if ($num_rows == 0) {
	$message['type'] = 'error';
	$message['header'] = 'Ошибка загрузки файла';
    $message['text'] = 'Файл не является отчетом или поврежден';
    print_r ( json_encode($message, JSON_UNESCAPED_UNICODE) ); 
	exit;
}
$k=1; // Отступаем строчки, ибо там оглавление.  
$user_numb = $result[2][16];
$us = 2;
while ($us < $user_numb+2) {
	$users_list[] = $result[$us][15];
	$user_login[] = $result[$us][14];
	$us++;
}

while ($k++<$nb_string) { 
if($result[$k][4]==null ) continue;
	if ($r_type=="self-evaluation") {
		$data_report[$k] = array ('id'=>$result[$k][13],
							 'status-compliance'=>$result[$k][4],
							 'fio' =>$result[$k][5],
							 'date-plan'=> strtotime($result[$k][6]),
							 'actions'=> $result[$k][7]);
	} else {
		$data_report[$k] = array ('id'=>$result[$k][11],
							  'status-compliance'=> $result[$k][4],
							  'actions'=>$result[$k][7]);
	}
}
if ($data_report == NULL) {
	$message['type'] = 'error';
	$message['header'] = 'Отчет пустой';
    $message['text'] = 'Не может быть загружен незаполненный отчет';
    print_r ( json_encode($message, JSON_UNESCAPED_UNICODE) ); 
	exit;
}

$data_id_column = array_column($data_report, 'id');
$data_id_str = implode(",", $data_id_column);

$query_data = $db->query("SELECT `id`, `demand`, `status-compliance`, `fio`, `date-plan`,`actions` 
								  FROM `collector` 
								  WHERE `id` IN (".$data_id_str.") AND `status-compliance` <> '0' ");

while ($k++<$nb_string) {
		
	if ($r_type=="self-evaluation") {
		$key = array_search ($result[$k][5],$users_list );	
		$result[$k][5] = $user_login[$key];
		if (is_bool($key) == true) {$result[$k][5] = "0";}
	}

	/* Подменяем Слова на соответствующие цифры Статуса Самооценки */
	 if ($result[$k][4]=="Более 90%") $result[$k][4]=1;
	 if ($result[$k][4]=="От 71% до 90%")  $result[$k][4]=2;
	 if ($result[$k][4]=="От 41% до 70%")  $result[$k][4]=3;
	 if ($result[$k][4]=="Менее 40%")  $result[$k][4]=4;
	 if ($result[$k][4]=="N/A")  $result[$k][4]=5;
	 
	/* Чек-лист */ 
	if ($r_type=="self-evaluation") {
		 /* Загружаем все данные в базу */
		$query = $db->query("UPDATE `collector` SET 
									`status-compliance`='".$result[$k][4]."',
									`fio`='".$result[$k][5]."',
									`date-plan`='".strtotime($result[$k][6])."',
									`actions`='".$result[$k][7]."' WHERE 
									`id`='".$result[$k][13]."'");}

	/* Аудит */
	if ($r_type=="audit") {
		 /* Загружаем все данные в базу */
		$query = $db->query("UPDATE `collector` SET 
									`status-compliance`='".$result[$k][4]."',
									`actions`='".$result[$k][5]."' WHERE 
									`id`='".$result[$k][11]."'");}

	/* Проверка госорганами */
	if ($r_type=="government") {
	 	/* Загружаем все данные в базу */
		$query = $db->query("UPDATE `collector` SET 
									`status-compliance`='".$result[$k][4]."',
									`actions`='".$result[$k][5]."' WHERE 
									`id`='".$result[$k][11]."'");}
}

unlink ($path_file_excel.$new_file_name);// Удаляем загруженный файл
// var_dump ($query);
if ($query == true) {
	$message['type'] = 'success';
	$message['header'] = 'Загрузка файла';
	$message['text'] = 'Файл удачно загружен';
	
} else {
	$message['type'] = 'error';
	$message['header'] = 'Ошибка загрузки файла';
	$message['text'] = 'Отчет не сохранен в системе'; 
}
print_r(json_encode($message, JSON_UNESCAPED_UNICODE)); 
?>