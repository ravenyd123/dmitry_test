<? 
/* Выгрузка календаря */
/* Версия 1.16 */
//var_dump ($_GET);
include_once $_SERVER['DOCUMENT_ROOT'].'/assets/functions.php';// Подключаем функции
include_once $_SERVER['DOCUMENT_ROOT'].'/vendor/phpoffice/phpexcel/Classes/PHPExcel.php'; // Подключаем библиотеку
$db = DB::Connection();
$date_now = date ('d.m.Y'); // Дата в нормальном формате dd.mm.yy
$date_chms = date ('H:i:s'); // Время в нормальном формате часы:минуты:секунды
$unique_key = md5(generateCode()); //генерируем уникальный код (id выгрузки календаря)
$login_get = $_COOKIE['login'];
$user_name = getUserName($login); // Выдергиваем ФИО специалиста
$user_list = getUsers($initials=false); // Список всех пользователей платформы
$count_users = count ($user_list); // Кол-во пользователей в платформе
$type = $_GET['type'];
$fio_my = getUserName($login_get); // Свое ФИО
$type = "calendar";
//$bunch = 1;
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="Календарь от '.$date_now.' ['.$unique_key.'].xlsx"'); // Придумываем будущее имя файла
header('Cache-Control: max-age=0');
include_once 'config_xls.php'; // Подтягиваем файл настроек екселя

$query = "SELECT c.typechek, p.name AS `platform_name`, r.date, r.bunch, j.type AS `job_type`, c.id, c.demand, c.event, c.fio, c.`status-compliance`, c.`date-plan`, c.actions, c.`fact-date`, c.`current-status-compliance`, c.`reason-failure`, r.type, c.recommendations, u.name, u.surname, u.`father_name` 
                                    	FROM reports r 
                                   		JOIN collector c ON r.id =c.`self-evaluation`
                                    	LEFT JOIN users u ON c.fio =u.login 
                                    	JOIN types_jobs j ON c.job =j.id 
                                    	JOIN platform p ON r.platform = p.id
                                    	WHERE c.`status-compliance` NOT IN (0,1,5) AND ";
                               	
/* Если нет user и это не nearmiss, но есть bunch, то ищем по banch */
if  ((!isset($_GET['mode'])) AND (!isset($_GET['user'])) AND (isset($_GET['bunch']))) {
	$bunch = $_GET['bunch'];
	$query .=" r.bunch IN (".$bunch.") ORDER BY c.`date-plan` ASC ";
	$query_select = $db->query($query); 
	//var_dump($query);

} 

/* Если есть только user и это не Nearmiss */
else if ((!isset($_GET['mode'])) AND (isset($_GET['user'])) AND (!isset($_GET['bunch'])))  {
	$user_get = $_GET['user'];
	$query .=" c.fio = '".$_COOKIE['login']."' ORDER BY c.`date-plan` ASC ";
	$query_select = $db->query($query); 
	//var_dump($query);
}
/* Если это Near Miss по одному ответственному */
else if ((isset($_GET['mode'])) AND (isset($_GET['user'])) AND (!isset($_GET['bunch']))) {
	$query .=" c.typechek = ".$_GET['mode']." AND 
			   c.fio = '".$_COOKIE['login']."'  
			   ORDER BY c.`date-plan` ASC ";
	$query_select = $db->query($query);   
	//var_dump($query); 
} 
/* Если все nearmiss есть только mode */
else if ((isset($_GET['mode'])) AND (!isset($_GET['user'])) AND (!isset($_GET['bunch']))) {
	$query .=" c.typechek = '".$_GET['mode']."' ORDER BY c.`date-plan` ASC ";
	$query_select = $db->query($query);  
//var_dump($query);
} 

/* Подготавливаем будущую таблицу (Стили, формат, печать, рамка при печати и так далее) */
$phpexcel = new PHPExcel(); // Создаём объект PHPExcel
$page = $phpexcel->setActiveSheetIndex(0); // Делаем активной первую страницу и получаем её
$page = $phpexcel->getActiveSheet()->mergeCells('A1:K1'); // Обьединяем ячейки в первой строчке	
$page = $phpexcel->getActiveSheet()->freezePane('A3'); // Закрепляем первую строчку, чтобы не двигалась
$page->setCellValue('A1', "Календарь выгрузил: ".$fio_my." / Дата выгрузки календаря: ".$date_now." в ".$date_chms." (Мск)");
$page->setTitle("Календарь от $date_now"); // Ставим заголовок на странице
// $page->getHeaderFooter()->setOddHeader('&CТД ТИНКО: прайс-лист'); // Устанавливаем хейдер только для печати.
$page->getPageMargins()->setTop(0); //  отступ сверху
$page->getPageMargins()->setRight(0); // отступ справа
$page->getPageMargins()->setLeft(0.6); // отступ слева
$page->getPageMargins()->setBottom(0);// отступ снизу
$page->getPageMargins()->setFooter(0); // Нижний колонтитуль
$page->getPageMargins()->setHeader(0.2); // Верхний колонтитуль
$page->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE); // Альбомная ориентация
$page->getPageSetup()->SetPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4); // Размер листа при печати
$page->getPageSetup()->setFitToWidth(1);
$page->getPageSetup()->setFitToHeight(0);
$page->getHeaderFooter()->setOddFooter('&L&B'.$page->getTitle().'&RСтраница &P из &N'); // устанавливаем подвал только при печати
// $page->getPageSetup()->setPrintArea('A:K'); // Область печати
$phpexcel->getActiveSheet(0)->setAutoFilter('A2:N2'); // Создание автофильтра
/* Прячем служебную информацию (ФИО и логины всех пользователей) */
$page->getColumnDimension("AP")->setWidth("0");
$page->getColumnDimension("AQ")->setWidth("0");	

/* Вставляем комментариии для оглавления, где будет находится краткая информация по работе */
$phpexcel->getActiveSheet()->getComment('G2')->getText()->createTextRun('Выберите из списка ответственного человека');
$phpexcel->getActiveSheet()->getComment('G2')->setHeight (78); // height set to 300
$phpexcel->getActiveSheet()->getComment('G2')->setWidth (120); // width set to 400

$phpexcel->getActiveSheet()->getComment('H2')->getText()->createTextRun('Укажите дату формата дд.мм.гггг. Пример:05.12.2018');
$phpexcel->getActiveSheet()->getComment('H2')->setHeight (70); // height set to 300
$phpexcel->getActiveSheet()->getComment('H2')->setWidth (135); // width set to 400

$phpexcel->getActiveSheet()->getComment('J2')->getText()->createTextRun('Укажите необходимые мероприятия по устранению');
$phpexcel->getActiveSheet()->getComment('J2')->setHeight (78); // height set to 300
$phpexcel->getActiveSheet()->getComment('J2')->setWidth (133); // width set to 400

$phpexcel->getActiveSheet()->getComment('L2')->getText()->createTextRun('Укажите дату формата дд.мм.гггг. Пример:05.12.2018');
$phpexcel->getActiveSheet()->getComment('L2')->setHeight (70); // height set to 300
$phpexcel->getActiveSheet()->getComment('L2')->setWidth (135); // width set to 400

$phpexcel->getActiveSheet()->getComment('M2')->getText()->createTextRun('Выберите текущий статус из списка');
$phpexcel->getActiveSheet()->getComment('M2')->setHeight (50); // height set to 300
$phpexcel->getActiveSheet()->getComment('M2')->setWidth (133); // width set to 400

$phpexcel->getActiveSheet()->getComment('N2')->getText()->createTextRun('Укажите причину не выполнения мероприятия');
$phpexcel->getActiveSheet()->getComment('N2')->setHeight (78); // height set to 300
$phpexcel->getActiveSheet()->getComment('N2')->setWidth (100); // width set to 400
$phpexcel->getActiveSheet()->getComment('N2')->setMarginLeft('1500Pt');

/* Установка стиля для оглавления */
$arHeadStyle1 = array('font' => array('size'  => 10,
									 'name'  => 'Calibri'),
					 'fill' => array ('type' => PHPExcel_Style_Fill::FILL_SOLID,
									  'rotation' => 0,
									  'color' => array('rgb' => "FFC000")),  // Заливка ячеек определенным цветом
					 'borders' => array ('allborders'   => array('style' => PHPExcel_Style_Border::BORDER_THIN )), // Установка рамок (тонких)
				     'alignment' => array ('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,   // Выравнивание текста по верт. и гор. в оглавлении
										   'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
										   'wrap' => true));  // Перенос строк

$arHeadStyle2 = array('font' => array('size'  => 10,
									 'name'  => 'Calibri'),
					 'fill' => array ('type' => PHPExcel_Style_Fill::FILL_SOLID,
									  'rotation' => 0,
									  'color' => array('rgb' => "FFFFFF")),  // Заливка ячеек определенным цветом
					 'borders' => array ('allborders'   => array('style' => PHPExcel_Style_Border::BORDER_THIN )), // Установка рамок (тонких)
				     'alignment' => array ('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,   // Выравнивание текста по верт. и гор. в оглавлении
										   'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
										   'wrap' => true));  // Перенос строк

$arHeadStyle3 = array('font' => array('size'  => 10,
									 'name'  => 'Calibri'),
					 'fill' => array ('type' => PHPExcel_Style_Fill::FILL_SOLID,
									  'rotation' => 0,
									  'color' => array('rgb' => "FCE4D6")),  // Заливка ячеек определенным цветом
					 'borders' => array ('allborders'   => array('style' => PHPExcel_Style_Border::BORDER_THIN )), // Установка рамок (тонких)
				     'alignment' => array ('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,   // Выравнивание текста по верт. и гор. в оглавлении
										   'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
										   'wrap' => true));  // Перенос строк
 /* Стиль шрифтов в самой таблице */						            
$arBodyStyle = array('font'  => array(// 'color' => array('rgb' => '778899'),       
									  'size' => 10,
									  'name' => 'Calibri'),
					 'borders' => array ('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN )),  // Установка рамок (тонких)
					 'alignment' => array ('horizontal'	=> PHPExcel_Style_Alignment::HORIZONTAL_CENTER, // Выравнивание текста по верт. и гор. в оглавлении
										   'vertical'  	=> PHPExcel_Style_Alignment::VERTICAL_CENTER,
										   'wrap'      	=> true ));      // Перенос строк
										   
 /* Отдельный стиль для столбца НЕОБХОДИМЫЕ МЕРОПРИЯТИЯ (текст не по центру) */
$arBodyStyle2 = array('font'  => array(// 'color' => array('rgb' => '778899'),       
									  'size' => 10,
									  'name' => 'Calibri'),
					 'borders' => array ('allborders'   => array('style' => PHPExcel_Style_Border::BORDER_THIN )),  // Установка рамок (тонких)
					 'alignment' => array ('vertical'  	=> PHPExcel_Style_Alignment::VERTICAL_CENTER, // Выравнивание текста повертикали.
										   'wrap'      	=> true ));      // Перенос строк
										   
/* Отдельный стиль для столбца ПУНКТ с гиперссылкой */
$arBodyStyle3 = array('font'  => array('color' => array('rgb' => '0000FF'),// Синий шрифт   
									  'size' => 10,
									  'name' => 'Calibri',
									  'underline' => 'true'), // Подчеркивание
					 'borders' => array ('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN )),  // Установка рамок (тонких)
					 'alignment' => array ('horizontal'	=> PHPExcel_Style_Alignment::HORIZONTAL_CENTER, // Выравнивание текста по верт. и гор. в оглавлении
										   'vertical'  	=> PHPExcel_Style_Alignment::VERTICAL_CENTER,
										   'wrap'      	=> true ));      // Перенос строк
/* Применяем стили к ячейкам оглавления */
$head_style = array('G2'=> $arHeadStyle1,
				    'H2'=> $arHeadStyle1,
	  			    'J2'=> $arHeadStyle1,
			   	 	'L2'=> $arHeadStyle1,
			   	 	'M2'=> $arHeadStyle1,
			   	 	'N2'=> $arHeadStyle1,
			   	 	'A2'=> $arHeadStyle2,
			   	 	'B2'=> $arHeadStyle3,
			   	 	'C2'=> $arHeadStyle3,
			   	 	'D2'=> $arHeadStyle3,
			   	 	'E2'=> $arHeadStyle3,
			   	 	'F2'=> $arHeadStyle3,
			   	 	'I2'=> $arHeadStyle3,
			   	 	'K2'=> $arHeadStyle3);
foreach ($head_style as $key => $value) {$page->getStyle($key)->applyFromArray($value);}
$page->setCellValue("AP1",$count_users); // Вставляем кол-во пользователей в таблицу
$user_line = 3;
/* Вставляем список ползователей платформы в дальний уголок */
foreach ($user_list as $key => $value){
	$page->setCellValue("AP$user_line",$value['login']); // логин пользователя
	$page->setCellValue("AQ$user_line",$value['name']); // Полное ФИО
	$user_line++;
	}
$indent = 3; // Отступаем 3 строчки от верха (оглавление и шапка)
/* Цикл по необходимым требованиям */
foreach ($query_select as $key => $value) {
	// Формируем столбец "Источник" 
	if ($value['typechek']==2) {
		$source = "Near Miss от ".date("d.m.Y",$value['date'])." ".$value['platform_name']; 
	} else {
		$type_self = getReportType($value['type']); // Получаем тип отчета
		$source = $type_self." №".$value['bunch']." от ".date("d.m.Y",$value['date'])." ".$value['platform_name'];
	} 
	$fio = $value['surname']." ".$value['name']." ".$value['father_name'];
	/* Заменяем цифры на текст в статусах соответствия */
	foreach ($stats_match as $clue => $name_match) {
		if ($value['status-compliance']==$clue) {$name_status = $name_match;}
		if ($value['current-status-compliance']==$clue) {$name_current_status = $name_match;}
	}
		/* Убираем NULL и нули из дат */
		if (($value['date-plan']==NULL) OR ($value['date-plan']==0)) {$date_plan='';
	} else {
			$date_plan = intval(($value['date-plan']/86400) + 25570);
		}
		if (($value['fact-date']==NULL) OR ($value['fact-date']==0)) {$date_fact='';
	} else {
			$date_fact = intval(($value['fact-date']/86400) + 25570);
		}
	
	// var_dump ($data_plan);
	$line = $key + $indent; // Корректировка отступа от верха


/* Проверка ячейки с датами на наличие правильно написанной даты (только для самооценки) */ 
    $page = $phpexcel->setActiveSheetIndex(0);
    $page = $phpexcel->getActiveSheet()->getCell("H$line")->getDataValidation(); // колонка
    $page->setType(PHPExcel_Cell_DataValidation::TYPE_DATE);
    $page->setOperator(PHPExcel_Cell_DataValidation::OPERATOR_BETWEEN); 
    $page->setAllowBlank(true); 
    $page->setShowInputMessage(true); 
    $page->setShowErrorMessage(true); 
    $page->setErrorStyle(PHPExcel_Cell_DataValidation::STYLE_STOP); //'stop' 
    $page->setErrorTitle('Ошибка');
    $page->setError('Не правильно указана дата. Формат даты долен быть: дд.мм.гггг (напрмиер:31.12.2019)');
    // $page->setPromptTitle('Укажите дату');
    // $page->setPrompt('Формат даты:дд.мм.гггг (напрмиер:31.12.2019)');
    $page->setFormula1(20433); // 43433 - ****2018
    $page->setFormula2(73049);
    $page->setAllowBlank(true);
    $phpexcel->getActiveSheet()->getStyle('H')->getNumberFormat()->setFormatCode('dd.mm.yyyy');

/* Проверка ячейки с датами на наличие правильно написанной даты Фактический срок */ 
    $page = $phpexcel->setActiveSheetIndex(0);
    $page = $phpexcel->getActiveSheet()->getCell("L$line")->getDataValidation(); // колонка
    $page->setType(PHPExcel_Cell_DataValidation::TYPE_DATE);
    $page->setOperator(PHPExcel_Cell_DataValidation::OPERATOR_BETWEEN); 
    $page->setAllowBlank(true); 
    $page->setShowInputMessage(true); 
    $page->setShowErrorMessage(true); 
    $page->setErrorStyle(PHPExcel_Cell_DataValidation::STYLE_STOP); //'stop' 
    $page->setErrorTitle('Ошибка');
    $page->setError('Не правильно указана дата. Формат даты долен быть: дд.мм.гггг (напрмиер:31.12.2019)');
    // $page->setPromptTitle('Укажите дату');
    // $page->setPrompt('Формат даты:дд.мм.гггг (напрмиер:31.12.2019)');
    $page->setFormula1(20433); // 43433 - ****2018
    $page->setFormula2(73049);
    $page->setAllowBlank(true);
    $phpexcel->getActiveSheet()->getStyle('L')->getNumberFormat()->setFormatCode('dd.mm.yyyy');




	$page = $phpexcel->setActiveSheetIndex(0); // Делаем страницу активной
	$page_style = array ('A' => $arBodyStyle,
		    			 'B' => $arBodyStyle,
						 'C' => $arBodyStyle2,
						 'D' => $arBodyStyle,
						 'E' => $arBodyStyle,
						 'F' => $arBodyStyle,
						 'G' => $arBodyStyle,
						 'H' => $arBodyStyle,
						 'I' => $arBodyStyle,
						 'J' => $arBodyStyle,
					     'K' => $arBodyStyle,
						 'L' => $arBodyStyle,
						 'M' => $arBodyStyle,
						 'N' => $arBodyStyle,
						 'O' => $arBodyStyle);
		foreach ($page_style as $key => $meaning) {
		$coordinates = $key.$line;
		$page->getStyle($coordinates)->applyFromArray($meaning);
		}

		/* Данные которые мы вставляем в таблицу */
		$insert_values = array(
			$value['ehs_numbers'], // Номер EHS
			$value['job_type'], // Название Вида Работ
			str_replace (array("&nbsp;","\r","\n","<span>","</span>"),'', strip_tags($value['demand'])), // Текст требования + убираем html теги
			$value['event'], // Тип требования по срокам
			$type_self, // Тип отчета
			$name_status, // Статус соответствия
			$fio, // ФИО ответственного
			$date_plan, // Плановый строк приведения в соответствие
			$value['actions'], // Выявленные не соответствия
			$value['recommendations'], // Мероприятия по устранению
			$source, // Источник информации
			$date_fact, // Фактический срок приведения в соответствие
			$name_current_status, // Текущий статус соответствия
			$value['reason-failure'], // Причина не выполнения
			$value['id'] // ID требования
		);

	/* Создание стили для столбца Текущий Статус Соответствия */
	$page = $phpexcel->getActiveSheet()->getCell("M$line")->getDataValidation(); // колонка
	$page->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
	$page->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
	$page->setAllowBlank(false);
	$page->setShowInputMessage(true);
	$page->setShowErrorMessage(true);
	$page->setShowDropDown(true);
	$page->setErrorTitle('Ошибка ввода');
	$page->setError('Не правильное значение');
	$page->setPromptTitle($xml_title1); // Оглавление подсказки
	$page->setPrompt($xml_title2); // Всплывающая подсказка(сам текст)
	$page->setFormula1('"'.$stats_match[0].','.$stats_match[1].','.$stats_match[2].','.$stats_match[3].','.$stats_match[4].'"');	// элементы выпадающего списка
	$page = $phpexcel->setActiveSheetIndex(0); // Стиль листа

	/* Создание стили для столбца ФИО + список пользователей */
	$page = $phpexcel->getActiveSheet()->getCell("G$line")->getDataValidation(); // колонка
	$page->setType( PHPExcel_Cell_DataValidation::TYPE_LIST );
	$page->setErrorStyle( PHPExcel_Cell_DataValidation::STYLE_INFORMATION );
	$page->setAllowBlank(true);
	$page->setShowInputMessage(true);
	$page->setShowErrorMessage(true);
	$page->setShowDropDown(true);
	$page->setErrorTitle('Ошибка ввода');
	$page->setError('Не правильное значение');
	$page->setFormula1('AQ3:AQ'.($user_line-1));	// элементы выпадающего списка
	$page->setPrompt('Список ответственных'); // Всплывающая подсказка(сам текст)

	/* Убираем защиту с определенных ячеек, которые будут редактироваться пользователями */
	$protect = array (H,G,J,L,M,N);
	foreach($protect as $key => $value) {  
		$page = $phpexcel->getActiveSheet()->getStyle("$value$line")->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_UNPROTECTED);
	}
	$phpexcel->getActiveSheet()->getProtection()->setPassword('sis1801');
	$key_values = 0; // Начальное значение ключа
		foreach (range($start_col, $end_col) as $letter) {
			$page = $phpexcel->setActiveSheetIndex(0); 
			$page->setCellValue($letter."2", $name_column_cal[$key_values]); // Добавляем в ячейки-оглавление  
			$page->getColumnDimension($letter)->setWidth($column_width[$key_values]); // Устанавливаем ширину всех необходимых столбцов
			$page->setCellValue("$letter$line", $insert_values[$key_values]); // Вставляем значения в таблицу
			$key_values++;
			
		}

		$page = new PHPExcel_Style_Conditional(); // При Н\А
		$page->setConditionType(PHPExcel_Style_Conditional::CONDITION_CELLIS);
		$page->setOperatorType(PHPExcel_Style_Conditional::OPERATOR_EQUAL); // функция РАВНО значению на следующей строке 
		$page->addCondition('"'.$stats_match[5].'"'); 
		$page->getStyle()->getFont()->getColor()->applyFromArray(array('rgb' => '808080')); // серый цвет шрифта
		$page->getStyle()->getFont()->setitalic(true); // курсив

		$page2 = new PHPExcel_Style_Conditional(); // Соответсвие менее 40% 
		$page2->setConditionType(PHPExcel_Style_Conditional::CONDITION_CELLIS);
		$page2->setOperatorType(PHPExcel_Style_Conditional::OPERATOR_EQUAL);
		$page2->addCondition('"'.$stats_match[4].'"');
		$page2->getStyle()->getFill()->setFillType(PHPExcel_Style_Fill::FILL_GRADIENT_LINEAR);
		$page2->getStyle()->getFill()->getStartColor()->applyFromArray(array('rgb' => 'FF7070')); // красный цвет заливки
		$page2->getStyle()->getFill()->getEndColor()->applyFromArray(array('rgb' => 'FF7070'));

		$page3 = new PHPExcel_Style_Conditional(); // соответствие 40-70 %
		$page3->setConditionType(PHPExcel_Style_Conditional::CONDITION_CELLIS);
		$page3->setOperatorType(PHPExcel_Style_Conditional::OPERATOR_EQUAL); 
		$page3->addCondition('"'.$stats_match[3].'"');
		$page3->getStyle()->getFill()->setFillType(PHPExcel_Style_Fill::FILL_GRADIENT_LINEAR);
		$page3->getStyle()->getFill()->getStartColor()->applyFromArray(array('rgb' => 'ffff00'));
		$page3->getStyle()->getFill()->getEndColor()->applyFromArray(array('rgb' => 'ffff00')); // желтый цвет заливки

		$page4 = new PHPExcel_Style_Conditional(); // Соответствие 70-90 % 
		$page4->setConditionType(PHPExcel_Style_Conditional::CONDITION_CELLIS);
		$page4->setOperatorType(PHPExcel_Style_Conditional::OPERATOR_EQUAL);
		$page4->addCondition('"'.$stats_match[2].'"');
		$page4->getStyle()->getFill()->setFillType(PHPExcel_Style_Fill::FILL_GRADIENT_LINEAR);
		$page4->getStyle()->getFill()->getStartColor()->applyFromArray(array('rgb' => '00ff00'));
		$page4->getStyle()->getFill()->getEndColor()->applyFromArray(array('rgb' => '00ff00')); // Зеленый цвет заливки

		$page5 = new PHPExcel_Style_Conditional(); // Более 90%
		$page5->setConditionType(PHPExcel_Style_Conditional::CONDITION_CELLIS);
		$page5->setOperatorType(PHPExcel_Style_Conditional::OPERATOR_EQUAL);
		$page5->addCondition('"'.$stats_match[1].'"');
		$page5->getStyle()->getFill()->setFillType(PHPExcel_Style_Fill::FILL_GRADIENT_LINEAR);
		$page5->getStyle()->getFill()->getStartColor()->applyFromArray(array('rgb' => '00ffff'));
		$page5->getStyle()->getFill()->getEndColor()->applyFromArray(array('rgb' => '00ffff')); //Синий цвет заливки

	 	/* Применяем стили условного форматирования */
		$conditionalStyles = $phpexcel->getActiveSheet()->getStyle("M$k")->getConditionalStyles();
		$conditionalStyles = $phpexcel->getActiveSheet()->getStyle("F$k")->getConditionalStyles();
		array_push($conditionalStyles, $page);
		array_push($conditionalStyles, $page2);
		array_push($conditionalStyles, $page3);
		array_push($conditionalStyles, $page4);
		array_push($conditionalStyles, $page5);
		$phpexcel->getActiveSheet()->getStyle("F$k")->setConditionalStyles($conditionalStyles);
		$phpexcel->getActiveSheet()->getStyle("M$k")->setConditionalStyles($conditionalStyles);
		// $indent = $line + 1;
}
/* Защищаем определенные ячейки от редактирования */
$phpexcel->getActiveSheet()->getProtection()->setSheet(true);// Защита всего листа
$phpexcel->getActiveSheet()->getProtection()->setFormatCells(true);// Защита от форматирвоания ячейки
$phpexcel->getActiveSheet()->getProtection()->setFormatColumns(true);// Защита от форматирования столбца
$phpexcel->getActiveSheet()->getProtection()->setFormatRows(true);// Защита от форматирвоания строчки
$phpexcel->getActiveSheet()->getProtection()->setInsertColumns(true);//Защита от вставки столбца
$phpexcel->getActiveSheet()->getProtection()->setInsertRows(true);// Защита от вставки строки
$phpexcel->getActiveSheet()->getProtection()->setDeleteColumns(true); //Защита от удаления столбца
$phpexcel->getActiveSheet()->getProtection()->setDeleteRows(true); //Защита от удаления строки
// $phpexcel->getActiveSheet()->getProtection()->setSelectLockedCells(true); //Защита от выделения (и копирования)

/* Начинаем готовиться к записи информации в xlsx-файл */
	$objWriter = PHPExcel_IOFactory::createWriter($phpexcel, 'Excel2007');
	$objWriter->save('php://output');
exit();
?>