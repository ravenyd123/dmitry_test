<?php
/* Модуль выгрузки Акта для Газпром */
/* Версия 1.15 */

// include_once $_SERVER['DOCUMENT_ROOT'].'/assets/PhpWord/autoload.php'; // Подключаем PHPWord
include_once $_SERVER['DOCUMENT_ROOT'].'/assets/functions.php';// Подключаем функции
$word = new \PhpOffice\PhpWord\PhpWord(); // Создаем Word объект 

/* Берем нужные нам параметры */
// $platform = $_GET['platform'];
$id = $_GET['id'];
$type = $_GET['type'];
$document = $word->loadTemplate ($_SERVER['DOCUMENT_ROOT'].'/app/views/reports/templates/gazprom/akt.docx'); //шаблон

/* Узнаем общего кол-ва строк + название платформы */
$data_query = $db->query("SELECT COUNT(DISTINCT job) AS count_job, p.name
                                      				 FROM collector c
                                      				 JOIN platform p ON c.platform = p.id
                                     				 WHERE actions <>'' AND `self-evaluation` IN (".$id.")")->fetch_assoc();

$document->cloneBlock('CL', $data_query['count_job']); // Клонируем строки видов работ
$query_select = $db->query("SELECT c.actions, c.`paragraph-name`,c.job, r.symbol, t.type
                                    FROM collector c
                                    LEFT JOIN register r ON c.document = r.nd
                                    LEFT JOIN `types_jobs` t ON c.job = t.id
                                    WHERE actions <>'' AND `self-evaluation` IN (".$id.") 
                                    GROUP BY c.job ,c.id");
// var_dump (count($query_select));
$document->setValue('platform_name',$data_query['name']); // Вставляем название платформы
$number_bunch = getBunchOfId($id); // Получаем номер отчета
// if(mysqli_num_rows($link,$query_select) == null) {
// 	 exit;
// var_dump (mysqli_num_rows($link,$query_select));

// }
/* Алгоритм построения таблицы в цикле */
foreach ($query_select as $value) {
	/* Если вид работ сменился */
	if ($old_job <> $value['job']) {
		/* Если это первая итерация */
		if(empty($n)) {
			$k++; // Ключ строки с actions
			$n++; // Ключ вида работ
			$document->setValue('types_of_jobs_'.$n, $value['type']);
			$data[$k] = array(
				$k,
				$value['actions'],
				$value['paragraph-name'],
				$value['symbol']
			);
			$old_job = $value['job'];
		/* Если это не первая итерация */	
		} else {
			$count_data = count($data);
			$document->cloneRow('rw_'.$n, $count_data);
			foreach ($data as $key => $value_event){
				$document->setValue('rw_'.$n.'#'.$key, $value_event[0]);
				$document->setValue('rowValue_'.$n.'#'.$key, $value_event[1]);
				$document->setValue('paragraph_'.$n.'#'.$key, $value_event[2]);
				$document->setValue('doc_name_'.$n.'#'.$key, $value_event[3]);
			}
			$n++;
			$k=1;
			$document->setValue('types_of_jobs_'.$n, $value['type']);
			$old_job = $value['job'];
			$data = [];
			$data[$k] = array(
				$k,
				$value['actions'],
				$value['paragraph-name'],
				$value['symbol']
		 	);
		}
	/* Если вид работ не сменился */	
	} else {
		$k++;
		$data[$k] = array(
			$k,
			$value['actions'],
			$value['paragraph-name'],
			$value['symbol']
		);
	}
}
/* Завершаем формирование таблицы */
$count_data = count($data);
if ($count_data == 0) { echo "В текущем отчете отсутствуют данные для Акта"; exit;}
$document->cloneRow('rw_'.$n, $count_data);
foreach ($data as $key => $value_event){
	$document->setValue('rw_'.$n.'#'.$key, $value_event[0]);
	$document->setValue('rowValue_'.$n.'#'.$key, $value_event[1]);
	$document->setValue('paragraph_'.$n.'#'.$key, $value_event[2]);
	$document->setValue('doc_name_'.$n.'#'.$key, $value_event[3]);
}
$document->setValue('tx1','ВЫВОДЫ И ПРЕДЛОЖЕНИЯ');
$document->setValue('tx2','В ходе работы комиссии, было установлено:');
$document->setValue('tx3','В области пожарной безопасности и гражданской защиты:');
$document->setValue('tx4','В области экологической безопасности:');
$document->setValue('tx5','В области энергетической безопасности:');
$document->setValue('tx6','В области организации ремонта и реконструкции основных фондов:');
$document->setValue('tx7','Одновременно, комиссия отметила положительные стороны:');
$document->setValue('tx8','По итогам работы комиссии и в целях улучшения организации работы по охране труда и промышленной безопасности необходимо:');
$trans_type = getReportType($type);
$filename = "АКТ ".$trans_type." №".$number_bunch;
header("Content-Description: File Transfer");
header('Content-Disposition: attachment; filename="'.$filename.'.docx"');
header('Content-Type: application/vnd.openxmlformats-officedocument.wordprocessingml.document');
header('Content-Transfer-Encoding: binary');
header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
header('Expires: 0');
ob_clean();
$document->saveAs("php://output");
exit;
?>