<?php
/* Модуль выгрузки Плана КИПД для Газпром */
/* Версия 1.15 */
//include_once $_SERVER['DOCUMENT_ROOT'].'/assets/PhpWord/autoload.php'; // Подключаем PHPWord
include_once $_SERVER['DOCUMENT_ROOT'].'/assets/functions.php';// Подключаем функции
$word = new \PhpOffice\PhpWord\PhpWord(); // Создаем Word объект 

$bunch = $_GET['bunch'];
// $bunch = 34;
$document = $word->loadTemplate ($_SERVER['DOCUMENT_ROOT'].'/app/views/reports/templates/gazprom/Plan_KIPD.docx'); //шаблон

/* Узнаем общего кол-ва строк */
$data_job_query = $db->query("SELECT COUNT(DISTINCT c.job)
                                      AS count_job,r.platform
                                      FROM reports r JOIN collector c 
                                      ON r.id =c.`self-evaluation` 
                                      WHERE r.bunch IN (".$bunch.") AND c.actions <>'' 
                                      GROUP BY r.platform ");

foreach ($data_job_query as $data_unique) { $count_unique_job += $data_unique['count_job']; }
$document->cloneBlock('CLONEME', $count_unique_job); // Клонируем общее кол-во строк
// var_dump ($count_unique_job);
/* Достаем сами данные для таблицы */ 
$query_select = $db->query("SELECT u.name, u.surname, u.father_name, c.id, c.job, r.platform, c.actions,c.fio,c.recommendations,c.`date-plan` 
                                    FROM reports r 
                                    LEFT JOIN collector c ON r.id =c.`self-evaluation`
                                    LEFT JOIN users u ON c.fio =u.login   
                                    WHERE r.bunch IN (".$bunch.") AND c.actions <>'' 
                                    GROUP BY r.platform,c.job ,c.id");

foreach ($query_select as $key => $data_value) {
   
    $platform_name = getPlatformsNames($data_value['platform']); // Узнаем название платформы
    $job_name = getJobById($data_value['job']); // Узнаем вид работ

    /* Алгоритм составления и внесения данных таблицы */

        /* Если нынешняя платформа не равно предыдущей */
        if ($data_value['platform'] <> $old_platform) {
            /* Если это первая итерация */
            $document->setValue('platform', $platform_name['name']);
            if ($id==NULL) { 
                $old_platform = $data_value['platform'];
                $old_job = $data_value['job'];
                $counter_demands++;
                $counter_job++;
                $value_fio = $data_value['surname'].' '.$data_value['name'].' '.$data_value['father_name'];
                $id[$counter_demands] = array(
                    $data_value['actions'],
                    $data_value['recommendations'],
                    $value_fio,
                    $data_value['date-plan']
                ); 
                $document->setValue('job_'.$counter_job, $job_name);
                $document->setValue('platform_'.$counter_job, $platform_name['name']);
            } else {
                /* Если это не первая итерация */
                $document->cloneRow('actions_'.$counter_job, $counter_demands);
                $i=1;
                while ($i <= $counter_demands){
                    if (($id[$i][3] == NULL) or ($id[$i][3] == 0)) {$date_id_platform = '';} else {$date_id_platform = date('d.m.Y',$id[$i][3]);} 
                    $document->setValue('p_'.$counter_job.'#'.$i, $i);
                    $document->setValue('actions_'.$counter_job.'#'.$i, $id[$i][0]);
                    $document->setValue('recommendations_'.$counter_job.'#'.$i, $id[$i][1]);
                    $document->setValue('fio_'.$counter_job.'#'.$i,  $id[$i][2]);
                    $document->setValue('date_'.$counter_job.'#'.$i, $date_id_platform);
                    $i++;
                }
                $counter_demands=0;
                $id = [];
                $old_platform = $data_value['platform'];
                $old_job = $data_value['job'];
                $counter_demands++;
                $counter_job++;
                $value_fio = $data_value['surname'].' '.$data_value['name'].' '.$data_value['father_name'];
                $id[$counter_demands] = array(
                    $data_value['actions'],
                    $data_value['recommendations'],
                    $value_fio,
                    $data_value['date-plan']
                );  
                $document->setValue('job_'.$counter_job, $job_name);
                $document->setValue('platform_'.$counter_job, $platform_name['name']);
            }
        /* Если текущая платформа равна предыдущей */   
        } else {
                /* Если текущий вид работ не равен предыдущему */
                if ($data_value['job'] <> $old_job) {
                    
                    $document->cloneRow('actions_'.$counter_job, $counter_demands);
                    $i=1;
                    while ($i <= $counter_demands){
                        if (($id[$i][3] == NULL) or ($id[$i][3] == 0)) {$date_id_platform = '';} else {$date_id_platform = date('d.m.Y',$id[$i][3]);}  
                        $document->setValue('p_'.$counter_job.'#'.$i, $i);
                        $document->setValue('actions_'.$counter_job.'#'.$i, $id[$i][0]);
                        $document->setValue('recommendations_'.$counter_job.'#'.$i, $id[$i][1]);
                        $document->setValue('fio_'.$counter_job.'#'.$i,  $id[$i][2]);
                        $document->setValue('date_'. $counter_job.'#'.$i,  $date_id_platform);
                        $i++;
                    }
                    $counter_demands=0;
                    $old_job = $data_value['job'];
                    $id = [];
                    $counter_demands++;
                    $counter_job++;
                    $value_fio = $data_value['surname'].' '.$data_value['name'].' '.$data_value['father_name'];
                    $id[$counter_demands] = array(
                    $data_value['actions'],
                    $data_value['recommendations'],
                    $value_fio,
                    $data_value['date-plan']
                    ); 
                    $document->setValue('job_'.$counter_job, $job_name);
                    $document->setValue('platform_'.$counter_job, $platform_name['name']);
                /* Если текущий вид работ равен предыдущему */  
                } else {
                    $counter_demands++;
                    $value_fio = $data_value['surname'].' '.$data_value['name'].' '.$data_value['father_name'];
                    $id[$counter_demands] = array(
                        $data_value['actions'],
                        $data_value['recommendations'],
                        $value_fio,
                        $data_value['date-plan']
                    );  
                }
        }
}
$document->cloneRow('actions_'.$counter_job, $counter_demands);
$i=1;
while ($i <= $counter_demands){
    if (($id[$i][3] == NULL) or ($id[$i][3] == 0)) {$date_id_platform = '';} else {$date_id_platform = date('d.m.Y',$id[$i][3]);}
    $document->setValue('p_'.$counter_job.'#'.$i, $i);
    $document->setValue('actions_'.$counter_job.'#'.$i, $id[$i][0]);
    $document->setValue('recommendations_'.$counter_job.'#'.$i, $id[$i][1]);
    $document->setValue('fio_'.$counter_job.'#'.$i,  $id[$i][2]);
    $document->setValue('date_'. $counter_job.'#'.$i, $date_id_platform);
    $i++;
}
/* Создаем результирующий файл */
$filename = 'План_КиПД_от_'.date('d_m_Y');
header("Content-Description: File Transfer");
header('Content-Disposition: attachment; filename="'.$filename.'.docx"');
header('Content-Type: application/vnd.openxmlformats-officedocument.wordprocessingml.document');
header('Content-Transfer-Encoding: binary');
header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
header('Expires: 0');
$document->saveAs("php://output");
exit;
?>