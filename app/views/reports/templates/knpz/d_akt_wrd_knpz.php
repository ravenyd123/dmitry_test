<?php
include_once $_SERVER['DOCUMENT_ROOT'].'/assets/functions.php';// Подключаем функции
$word = new \PhpOffice\PhpWord\PhpWord(); // Создаем Word объект 
$document = $word->loadTemplate ($_SERVER['DOCUMENT_ROOT'].'/app/views/reports/templates/knpz/akt_knpz.docx'); //шаблон

//$platform_name = getPlatformsNames($link, $_GET['task_all']);
if (isset($_GET['task_all'])) {
	$task_all = $_GET['task_all']; 
}

if (isset($_GET['task_one'])) {
	$task_all = $_GET['task_one']; 
}

$query = $db->query("SELECT `platform`, `name` FROM `tasks` WHERE `id` = '".$task_all."'")->fetch_assoc();
//$query = mysqli_fetch_array(mysqli_query($link,"SELECT `platform`, `name` FROM `tasks` WHERE `id` = '".$task_all."'")); 
$query2 = $db->query("SELECT `id`, `nreport`, `responsible` FROM `tasks` WHERE `id` = '".$task_all."'");
//$query2 = mysqli_query($link,"SELECT `id`, `nreport`, `responsible` FROM `tasks` WHERE `pid` = '".$task_all."'");
 
//$query23 = mysqli_query($link,"SELECT DISTINCT(`responsible`) FROM `tasks` WHERE `pid` = '".$task_all."'");

$query23 = $db->query("SELECT DISTINCT(`responsible`) FROM `tasks` WHERE `id` = '".$task_all."'");

$nreport = mysqli_fetch_array($query2);

//$query3 = mysqli_fetch_array(mysqli_query($link,"SELECT `date` FROM `reports` WHERE `bunch` = '".$nreport['nreport']."'"));
$query3 = $db->query("SELECT `date` FROM `reports` WHERE `bunch` = '".$nreport['nreport']."'")->fetch_assoc();
$platform_name = getPlatformsNames($query['platform']);

foreach($query23 as $key => $value)
{
	$plo = $key+1;
}

$document->cloneBlock('CLONEME1', $plo);
$document->cloneBlock('CL2', $plo);

foreach($query23 as $key => $value)
{
	$op = $key+1;
	$document->setValue('UserOtv_'.$op, getSmallFioFromId($value['responsible']));
}

$arr = [
  'января',
  'февраля',
  'марта',
  'апреля',
  'мая',
  'июня',
  'июля',
  'августа',
  'сентября',
  'октября',
  'ноября',
  'декабря'
];
$month = date('n')-1;
$document->setValue('platform', $platform_name['name']);
$nowdate = date("«j» F  Y");
$document->setValue('NowD', date("j"));
$document->setValue('NowYears', date("Y"));
$document->setValue('NowMouth', $arr[$month]);
$document->setValue('Tema', $query['name']);
$document->setValue('DateCreate', date('d.m.Y', $query3['date']));
$document->setValue('NowDate', date('d.m.Y'));
/* Создаем результирующий файл */
$filename = 'Форма акта комплексной проверки '.date('d_m_Y');
header("Content-Description: File Transfer");
header('Content-Disposition: attachment; filename="'.$filename.'.docx"');
header('Content-Type: application/vnd.openxmlformats-officedocument.wordprocessingml.document');
header('Content-Transfer-Encoding: binary');
header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
header('Expires: 0');
$document->saveAs("php://output");
exit;
?>