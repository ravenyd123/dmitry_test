<?php
include_once $_SERVER['DOCUMENT_ROOT'].'/assets/functions.php';// Подключаем функции
$word = new \PhpOffice\PhpWord\PhpWord(); // Создаем Word объект 

$arr = [
  'января',
  'февраля',
  'марта',
  'апреля',
  'мая',
  'июня',
  'июля',
  'августа',
  'сентября',
  'октября',
  'ноября',
  'декабря'
];
$month = date('n')-1;


$bunch = $_GET['bunch'];
// $bunch = 34;

$document = $word->loadTemplate ($_SERVER['DOCUMENT_ROOT'].'/app/views/reports/templates/knpz/plan_ssk.docx'); //шаблон
//$document = $word->TemplateProcessor($_SERVER['DOCUMENT_ROOT'].'/app/views/reports/templates/knpz/plan_ssk.docx');
$query = "SELECT r.`platform`, c.`actions`, c.`date-plan`, c.`current-status-compliance`, c.`paragraph-name`, c.`document`, c.`fact-date`, c.`recommendations`
                                      FROM reports r JOIN collector c 
                                      ON r.id =c.`self-evaluation` 
                                      WHERE r.bunch IN (".$bunch.") AND c.actions <>''";


$result =  $db->query($query);
									  
//$plstform_q = mysqli_fetch_assoc($result);
//var_dump ($plstform_q);

//$platform = getPlatformsNames($plstform_q['platform']);

$count_string = mysqli_num_rows($result);

$document->cloneBlock('Clone', $count_string, true, true); // Клонируем общее кол-во строк
//$document->setValue('platform', $platform['name']);
$document->setValue('ddc', date('d'));
$document->setValue('yyc', date('Y'));
$document->setValue('mmc', $arr[$month]);
$document->setValue('datanow', date('d.m.Y'));
//var_dump($bunch);

$query = "SELECT `director`, `responsible`, `id` FROM `tasks` WHERE id=(SELECT `pid` FROM `tasks` WHERE `nreport` IN (".$bunch.") LIMIT 1)";
//var_dump ($query);
$query4 =  $db->query($query)->fetch_array();

//var_dump($query4);
$document->setValue('userotv', getSmallFioFromId($query4['director']));
$document->setValue('userpostanov', getSmallFioFromId($query4['responsible']));
//$document->setValue('nnt', 'П000'.$query4['id']);

foreach ($result as $key => $value){

	$compl='';
	$date='';
	$a = $key+1;
	$document->setValue('actions#'.$a, $value['actions']);
	if(($value['date-plan']<>'') AND ($value['date-plan']<>'0')) {
		$date = date('d.m.Y', strtotime($value['date-plan']));
		$date = gmdate("d.m.Y", $value['date-plan']);
		}


	$document->setValue('n#'.$a, $a);	
	$document->setValue('date#'.$a, $date);
	$document->setValue('actionDo#'.$a, $value['recommendations']);
	if($value['current-status-compliance']==1) {$compl = "Устранено";}
	if((2<=$value['current-status-compliance']) AND ($value['current-status-compliance']<=5)) {$compl = "Не устранено";}
	//$document->setValue('compl_'.$a, $compl);
	$document->setValue('punkt#'.$a, $value['paragraph-name']);
	$documentname = html_entity_decode(html_entity_decode(getDocumentByNd($value['document'])));
	$document->setValue('doc#'.$a, $documentname);
	//if ($value['fact-date'] <> 0){
	//$document->setValue('factdate_'.$a, date('d.m.Y',$value['fact-date']));
	//} else {
	//	$document->setValue('factdate_'.$a, '' );
	//}
	
}


/* Создаем результирующий файл */
$filename = 'Акт от '.date('d_m_Y');
header("Content-Description: File Transfer");
header('Content-Disposition: attachment; filename="'.$filename.'.docx"');
header('Content-Type: application/vnd.openxmlformats-officedocument.wordprocessingml.document');
header('Content-Transfer-Encoding: binary');
header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
header('Expires: 0');
$document->saveAs("php://output");
exit;
?>