<? include_once $_SERVER['DOCUMENT_ROOT'] . '/app/views/header.php'; ?>
<? include_once $_SERVER['DOCUMENT_ROOT'] . '/app/views/template.php';
$db = Db::Connection();
//$allfiles = allfiles_on_bunch();
$reportsInfo = $db->query("SELECT r.bunch,c.`self-evaluation`,c.platform,r.date,r.creator,r.responsible,r.status,r.typen as type,r.creator, 
                                                    COUNT(c.id)AS open,c.job,SUM(IF(c.`status-compliance`<>0,1,0)) AS closed 
                                                    FROM collector c 
                                                    LEFT JOIN reports r ON r.id=c.`self-evaluation` 
                                                    WHERE c.platform in({$_COOKIE['platform']})
                                                    GROUP BY r.bunch,c.`self-evaluation` ORDER BY r.`bunch` ");

$t = 0;
foreach ($reportsInfo as $value) {
    if ($t != intval($value["bunch"])) {
        $data[$value["bunch"]] = [
            "platform" => $value["platform"],
            "bunch" => $value["bunch"],
            "date" => $value["date"],
            "status" => $value["status"],
            "type" => $value["type"],
            "creator" =>$value["creator"],
            "self-evaluation" => [
                $value["self-evaluation"] => [
                    "job" => $value["job"],
                    "open" => $value["open"],
                    "closed" => $value["closed"],
                    "responsible" => $value["responsible"],
                ]
            ]
        ];
        $t = intval($value["bunch"]);
    } else {
        $data[$value["bunch"]]["self-evaluation"][$value["self-evaluation"]] = ["job" => $value["job"], "open" => $value["open"], "closed" => $value["closed"],"responsible" => $value["responsible"]];
    }

}

?>
<!--Main Content -->
<section class="content">

    <!-- Page Content -->
    <div class="wraper container-fluid">
        <div class="page-title">
            <h3 class="title">Платформа мониторинга</h3>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title pull-left m-t-10">
                            Чек листы
                        </h3>

                        <?
                        if ($reportsInfo->num_rows != 0) { ?>
                            <div class="top_nav">
                                <? //if ( in_array("U", $access['reports']) ){ ?>
                                <a href="#" data-target="#editReport" data-toggle="modal">
                                    <button class="btn btn-default btn-sm" data-action="editReport"
                                            data-toggle="tooltip"
                                            data-placement="top" title="Подробная информация">
                                        <i class="fa fa-bar-chart-o"></i>
                                    </button>
                                </a><br>
                                <? //} ?>

                                <? if (in_array("R", $access['matrix'])) { ?>
                                    <button class="btn btn-default btn-sm" data-action="editMatrix"
                                            data-toggle="tooltip"
                                            data-placement="top" title="Редактировать отчет">
                                        <i class="fa fa-edit"></i>
                                    </button><br>
                                <? } ?>
                                <button class="btn btn-default btn-sm" data-action="downloadReport"
                                        data-toggle="tooltip"
                                        data-placement="top" title="Скачать в формате Excel">
                                    <i class="fa fa-file-excel-o"></i>
                                </button>
                                <br>
                                <? if (in_array("U", $access['reports'])) { ?>
                                    <button class="btn btn-warning btn-sm" data-action="loadReport"
                                            data-toggle="tooltip"
                                            data-placement="top" title="Загрузить Excel файл на сервер">
                                        <i class="ion-archive"></i>
                                    </button><br>
                                <? } ?>

                                <form class="hidden" name="uploader" enctype="multipart/form-data" method="POST">
                                    <input name="userfile" type="file"/>
                                    <button type="submit" name="submit">Загрузить</button>
                                </form>

                            </div>

                        <? } ?>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-body">
                        <? if ($reportsInfo->num_rows == 0) { ?>
                            <p>У вас еще нет отчетов по самооценке. </p>
                            <? exit; ?>
                        <? } ?>

                        <div class="row">
                            <div class="col-md-12 m-b-15">
                                <div class="row">
                                    <div id="datatable-filter">
                                        <form action="" method="POST">

                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <?

                        // Собираем данные
                        //                        $previousDate = 0;
                        //                        $total = 0;
                        //
                        //                        while ($row = mysqli_fetch_assoc($reports)) {
                        //
                        //                            # Подсчет кол-ва требований
                        //                            if ($previousBunch != $row['bunch']) {
                        //                                $total = 0;
                        //                            }
                        //                            $previousBunch = $row['bunch'];
                        //
                        //                            # Базовая информация по отчету
                        //                            if ($previousReport != $row['self-evaluation']) {
                        //                                $reportsInfo[$row['bunch']]['bunch'] = $row['bunch'];
                        //                                $reportsInfo[$row['bunch']]['platform'] = $row['platform'];
                        //                                $reportsInfo[$row['bunch']]['date'] = $row['date'];
                        //                                $reportsInfo[$row['bunch']]['type'] = $row['type'];
                        //                                $reportsInfo[$row['bunch']]['type-name'] = getReportType($row['type']);//здесь можно подправить
                        //                                $reportsInfo[$row['bunch']]['self-evaluation'][] = $row['self-evaluation'];
                        //                                $reportsInfo[$row['bunch']]['attachment'] = $row['attachment'];
                        //                                $reportsInfo[$row['bunch']]['responsible'] = $row['responsible'];
                        //                                $reportsInfo[$row['bunch']]['creator'] = $row['creator'];
                        //                                $reportsInfo[$row['bunch']]['status'] = $row['status'];
                        //                            }
                        //                            $previousReport = $row['self-evaluation'];
                        //                            # Если требование не удовлетворяет настройкам критичности не выводим
                        //                            if ($row['criticality'] != 4) {
                        //
                        //                                $active = 0;
                        //
                        //                                if ($row['criticality'] == 3) {
                        //                                    if (intval($row['criticality1']) == 3 ||
                        //                                        intval($row['criticality2']) == 3 ||
                        //                                        intval($row['criticality2']) == 3
                        //                                    ) {
                        //                                        $active = 1;
                        //                                    }
                        //                                }
                        //
                        //                                if ($row['criticality'] == 2) {
                        //                                    if (intval($row['criticality1']) == 3 ||
                        //                                        intval($row['criticality2']) == 3 ||
                        //                                        intval($row['criticality2']) == 3 ||
                        //                                        intval($row['criticality1']) == 2 ||
                        //                                        intval($row['criticality2']) == 2 ||
                        //                                        intval($row['criticality2']) == 2
                        //                                    ) {
                        //                                        $active = 1;
                        //                                    }
                        //                                }
                        //
                        //                                if ($row['criticality'] == 1) {
                        //                                    if (intval($row['criticality1']) < 3 &&
                        //                                        intval($row['criticality2']) < 3 &&
                        //                                        intval($row['criticality2']) < 3
                        //                                    ) {
                        //                                        $active = 1;
                        //                                    }
                        //                                }
                        //
                        //                                if ($active == 0) {
                        //                                    continue;
                        //                                }
                        //                            }
                        //
                        //                            // Дата начала отчета(наименьшая дата требования по самооценке)
                        //                            if (!isset($data[$row['self-evaluation']]['date'])) {
                        //                                $data[$row['self-evaluation']]['date'] = $row['date'];
                        //                            }
                        //                            // Площадка
                        //                            if (!isset($data[$row['self-evaluation']]['platform'])) {
                        //                                $data[$row['self-evaluation']]['platform'] = $row['platform'];
                        //                            }
                        //
                        //                            // Создатель
                        //                            if (!isset($data[$row['self-evaluation']]['creator'])) {
                        //                                $data[$row['self-evaluation']]['creator'] = $row['creator'];
                        //                            }
                        //
                        //                            // Ответственный
                        //                            if (!isset($data[$row['self-evaluation']]['responsible'])) {
                        //                                $data[$row['self-evaluation']]['responsible'] = $row['responsible'];
                        //                            }
                        //
                        //                            // Подсчитываем требования
                        //                            if (!isset($data[$row['self-evaluation']]['done']) ||
                        //                                !isset($data[$row['self-evaluation']]['all'])
                        //                            ) {
                        //                                $data[$row['self-evaluation']]['done'] = 0;
                        //                                $data[$row['self-evaluation']]['all'] = 0;
                        //                            }
                        //
                        //                            // Выполненные требования самооценки
                        //                            if ($row['status-compliance'] != 0) {
                        //                                $data[$row['self-evaluation']]['done']++;
                        //                            }
                        //
                        //                            // Все требования самооценки
                        //                            $data[$row['self-evaluation']]['all']++;
                        //
                        //                            // Все требования самооценки
                        //                            $total++;
                        //
                        //                            // Данные для графика
                        //                            if (!isset($data[$row['self-evaluation']]['status']['0']) ||
                        //                                !isset($data[$row['self-evaluation']]['status']['5']) ||
                        //                                !isset($data[$row['self-evaluation']]['status']['1']) ||
                        //                                !isset($data[$row['self-evaluation']]['status']['2']) ||
                        //                                !isset($data[$row['self-evaluation']]['status']['3']) ||
                        //                                !isset($data[$row['self-evaluation']]['status']['4'])
                        //                            ) {
                        //                                $data[$row['self-evaluation']]['status']['0'] = 0;
                        //                                $data[$row['self-evaluation']]['status']['5'] = 0;
                        //                                $data[$row['self-evaluation']]['status']['1'] = 0;
                        //                                $data[$row['self-evaluation']]['status']['2'] = 0;
                        //                                $data[$row['self-evaluation']]['status']['3'] = 0;
                        //                                $data[$row['self-evaluation']]['status']['4'] = 0;
                        //                            }
                        ////                            if ($row['status-compliance'] == 0) {
                        ////                                $data[$row['self-evaluation']]['status']['0']++;
                        ////                            }
                        ////                            if ($row['status-compliance'] == 5) {
                        ////                                $data[$row['self-evaluation']]['status']['5']++;
                        ////                            }
                        ////                            if ($row['status-compliance'] == 1) {
                        ////                                $data[$row['self-evaluation']]['status']['1']++;
                        ////                            }
                        ////                            if ($row['status-compliance'] == 2) {
                        ////                                $data[$row['self-evaluation']]['status']['2']++;
                        ////                            }
                        ////                            if ($row['status-compliance'] == 3) {
                        ////                                $data[$row['self-evaluation']]['status']['3']++;
                        ////                            }
                        ////                            if ($row['status-compliance'] == 4) {
                        ////                                $data[$row['self-evaluation']]['status']['4']++;
                        ////                            }
                        //
                        //                            // Виды работ
                        //                            $job = GetAllvid[$row['job']];
                        //                            if (strpos($data[$row['self-evaluation']]['job'], $job) === false) {
                        //                                $data[$row['self-evaluation']]['job'] .= $job . ",";
                        //                            }
                        //
                        //                        }
                        //                        ?>

                        <?php
                        foreach ($data as $key => $value) {
                            $sum = 0;
                            ?>

                            <h5 class="">
                                <a data-toggle="collapse" data-parent="#accordion"
                                   href="#collapse<?= $value['bunch'] ?>" aria-expanded="false"
                                   class="collapsed font-light">
                                    <?= type_tasks_new[$value['type']] ?> №<?= $value['bunch'] ?>
                                    от <?= date('d.m.Y', $value['date']); ?>
                                    по площадке <?= platforms_user[$value['platform']]; ?>
                                </a>

                                <? if (in_array("D", $access['reports'])) { ?>
                                    <a class="text-danger" onclick="removeBunch(<?= $value['bunch']; ?>)">
                                        (удалить)
                                    </a>
                                <? } ?>

                            </h5>
                            <p>
                                <span>Дата создания отчета: <b><?= date('d.m.Y', $value['date']); ?></b></span>
                                <span class="m-l-30">Создал: <b><?= user_in_platforms[$value['creator']]?></b></span>
                                <span class="m-l-30">Статус чек-листа: <b><?= getReportStatus($value['status']); ?></b></span>

                                <? if (in_array("C", $access['reports'])) { ?>
                                    <a onclick="closeReport(<?= $value['bunch']; ?>)">
                                        <?= ($value['status'] == 0) ? '(закрыть отчет)' : '(открыть отчет)'; ?>
                                    </a>
                                <? } ?>
                            </p>
                            <div id="collapse<?= $value['bunch'] ?>" class="panel-collapse collapse"
                                 style="">
                                <div class="panel-body">

                                    <div class="row">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <table id="datatable" name="uni<?= $value['bunch'] ?>"
                                                   class="table table-hover table-striped table-bordered">
                                                <thead>
                                                <tr>

                                                    <th class="hidden">№</th>
                                                    <th><div style="text-align: left; position:absolute;">
                                                     <input type="checkbox" name="selectAll" Title="Выделить все"></div> Вид работы</th>
                                                    <th>Прогресс</th>
                                                    <th>Ответственный</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php foreach ($value['self-evaluation'] as $rKey => $rValue) {

                                                    switch ($value["status"]) {
                                                        case 0:
                                                            $status = "В работе";
                                                            break;
                                                        case 1:
                                                            $status = "Закрыт";
                                                            break;
                                                        case 2:
                                                            $status = "В архиве";
                                                            break;
                                                    } ?>
                                                    <tr class="<?= ($status == 'В архиве') ? 'archive' : ''; ?><?= ($status == 'Закрыт') ? 'closed' : ''; ?>text-center"
                                                        data-id="<?= $rKey; ?>"
                                                        platform-id="<?= $value['platform']; ?>"
                                                        report-type="<?= Get_all_type_t(true)[$value['type']]; ?>">
                                                        <td class="firstColumn hidden"><input type="checkbox"
                                                                                              name="id[]"></td>
                                                        <td class="hidden"><?= $rKey; ?></td>
                                                        <td class="text-left"><?= GetAllvid[$rValue['job']]; ?></td>
                                                        <td class="report-status <?= ($rValue['closed'] < $rValue['open'] ? 'text-danger' : 'text-success') ?>">
                                                            <?= $rValue['closed']; ?>/<?= $rValue['open']; ?>
                                                        </td>
                                                        <td class="report-responsible text-nowrap">
                                                            <select class="form-control"
                                                                    name='responsible_<?= $rKey; ?>'
                                                                    onchange="setResponsible(<?= $rKey; ?>);"
                                                                    disabled="disabled">
                                                                <option value=""></option>
                                                                <? foreach (user_in_platforms as $uKey => $uValue) { var_dump($rValue['responsible']) ?>
                                                                    <option value="<?= $uKey; ?>"
                                                                        <?= ($uKey == $rValue['responsible']) ? 'selected' : '' ?> >
                                                                        <?= $uValue; ?>
                                                                    </option>
                                                                <? } ?>
                                                            </select>
                                                        </td>

                                                        <input type="hidden" class="status"
                                                               value="<?= $value['status']; ?>">
                                                        <input type="hidden" class="job"
                                                               value="<?= $rValue[$rKey]['job']; ?>">
                                                    </tr>
                                                    <? $sum += $rValue['open'] ?>
                                                <? } ?>
                                                <tr>
                                                    <td class="text-right" colspan="8">Всего
                                                        требований: <?= $sum ?></td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <hr/>
                        <? } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    </div>
    <!-- End Row -->

    </div>

</section>
<?php

?>
<? include 'footer.php'; ?>

<!-- Chart JS -->
<script src="/node_modules/chart.js/dist/Chart.min.js"></script>
<script src="/app/views/reports/js/all.js"></script>
