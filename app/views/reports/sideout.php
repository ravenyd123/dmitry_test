<link rel="stylesheet" href="/app/views/reports/css/ajax.css">
<link rel="stylesheet" href="/app/views/reports/css/matrix.css">
<div id="slideout" class="btn btn-default m-b-5">
    <i class="fa fa-bars"></i>
    <div class="slide_menu">Меню</div>
    <div id="slideout_inner" class="btn btn-default m-b-5">
        <? if ($type == 'calendar') { ?>
            <a href="#" class="btn btn-block btn-sm btn-success" data-action="downloadCalendar">Скачать xls-файл</a>
            <a href="#" class="btn btn-block btn-sm btn-success" data-action="downloadSskPlan">Скачать акт-предписание</a>
            <? if ((($_COOKIE["company"] == "gazprom") OR ($_COOKIE["company"] == "all")) AND (!isset($_GET["user"]))) { ?>
                <a href="#" class="btn btn-block btn-sm btn-pink" data-action="dkipd">Скачать план КиПД</a>
            <? }
                if ((($_COOKIE["company"] == "knpz") OR ($_COOKIE["company"] == "all")) AND (!isset($_GET["user"]))) { ?>
                <a href="#" class="btn btn-block btn-sm btn-pink" data-action="dpred">Скачать предписание</a>
            <? }
        } ?>
        <? if ($type !== 'calendar') { ?>
            <a href="#" class="btn btn-block btn-sm btn-success" data-action="downloadxlsx">Скачать xls-файл</a>
            <? if (($_COOKIE["company"] == "gazprom") OR ($_COOKIE["company"] == "all")) { ?>
                <a href="#" class="btn btn-block btn-sm btn-pink" data-action="devan">Скачать аннотацию</a>
            <? }
                if (($_COOKIE["company"] == "knpz") OR ($_COOKIE["company"] == "all")) { ?>
                <a href="#" class="btn btn-block btn-sm btn-pink" data-action="dakt">Скачать акт</a>
            <? }
        } ?>

    </div>
</div>
<script src="/app/views/reports/js/sideout.js"></script>