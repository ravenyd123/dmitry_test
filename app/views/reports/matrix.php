<?
include_once $_SERVER['DOCUMENT_ROOT'] . '/app/views/header.php';

$db = Db::Connection();
if (!in_array("R", $access['matrix'])) {
    echo "<p>У вас недостаточно прав для просмотра отчета</p>";
    exit;
}

// Определяем тип отчета
$type = $_GET['type'];
$nameTaskType = getNameTask($type);

// Платформа
if (isset($_GET['platform'])) {
    $platform = $_GET['platform'];
}


echo "<title>Well Compliance - ".$nameTaskType."</title>";
// Список видов работ
//if ($type != 'calendar') {
 //   if ($type == 'self-evaluation') {
  //      echo "<title>Well Compliance - Самооценка</title>";
  //  }
 //   if ($type == 'audit') {
 //       echo "<title>Well Compliance - Аудит</title>";
 //   }
 //   if ($type == 'government') {
 //       echo "<title>Well Compliance - Проверка Гос/органов</title>";
  //  }
//}

// Если календарь
if ($type == 'calendar') {
    if ((isset($_GET['mode'])) AND (($_GET['mode'])==2)) {
        
        echo "<title>Well Compliance - Календарь Near Miss</title>";
        $queryk = "SELECT c.*,r.bunch, r.type,r.typen, r.creator, r.responsible, r.status,reg.symbol 
                                  FROM `collector` c
                                  LEFT JOIN `reports` r ON c.`self-evaluation` = r.id 
                                  LEFT JOIN register reg ON c.document=reg.nd 
                                  WHERE c.typechek=2";
        if (isset($_GET['user'])) {
            $queryk .= " AND `fio` LIKE '%" . $_COOKIE['login'] . "%'";
        // Календарь с выборкой
        }

    // }
    // else if  (isset($_GET['self'])) {
    //     $self = $_GET['self'];
    //     echo "<title>Well Compliance - Календарь Near Miss</title>";
    //     $queryk = "SELECT c.*,r.bunch, r.type, r.creator, r.responsible, r.status,reg.symbol 
    //                               FROM `collector` c
    //                               LEFT JOIN `reports` r ON c.`self-evaluation` = r.id 
    //                               LEFT JOIN register reg ON c.document=reg.nd 
    //                               WHERE c.`self-evaluation`=1";
                                  

    } else {
    echo "<title>Well Compliance - Календарь</title>";

    $queryk = "SELECT c.*,r.bunch, r.type, r.typen, r.creator, r.responsible, r.status,reg.symbol 
                                  FROM `collector` c
                                  LEFT JOIN `reports` r ON c.`self-evaluation` = r.id 
                                  LEFT JOIN register reg ON c.document=reg.nd ";
    // Календарь одного пользователя для уведомления
    if (isset($_GET['user'])) {
        $queryk .= "WHERE `fio` LIKE '%" . $_COOKIE['login'] . "%' AND `status-compliance` IN (2,3,4)";
        // Календарь с выборкой
    }
}
//    elseif (isset($_GET['vid'])) {
//        $allusers = GetDistinctUsersfromBunch($_GET['bunch']);/// переработать
//        $queryk .= "WHERE c.`status-compliance` NOT IN (0,1,5) AND r.id IN (" . $_GET['vid'] . ")";
//    } else {
//        $queryk .= "WHERE c.`status-compliance` NOT IN (0,1,5) AND r.bunch IN (" . $_GET['bunch'] . ")";
//        $allusers = GetDistinctUsersfromBunch($_GET['bunch']);/// переработать
//    }

    if(isset($_GET['vid'])){
        $queryk .= "WHERE c.`status-compliance` NOT IN (0,1,5) AND r.id IN (" . $_GET['vid'] . ")";
        $allusers = GetDistinctUsersfromBunch($_GET['bunch']);/// переработать
    }

    if(isset($_GET['bunch'])){
        $queryk .= "WHERE c.`status-compliance` NOT IN (0,1,5) AND r.bunch IN (" . $_GET['bunch'] . ")";
        $allusers = GetDistinctUsersfromBunch($_GET['bunch']);/// переработать
    }

    if(isset($_GET['self'])){
        $queryk .= "WHERE c.`status-compliance` NOT IN (0,1,5) AND c.id IN (" . $_GET['self'] . ")";
        $allusers = GetDistinctUsersfromBunch($_GET['bunch']);/// переработать
    }


    $queryk .= " ORDER BY c.`date-plan` ASC,
                                  `criticality1` DESC,
                                  `criticality2` DESC,
                                 `criticality3` DESC";
    $query = $db->query($queryk);
    $date =  $db->query($queryk)->fetch_assoc();

}


// Если самооценка, аудит или проверка гос/органов
if ($type != 'calendar') {
    $reportsId = $_GET['id'];
    $q = "SELECT 
  c.*, p.criticality,reg.symbol,p.name as pl_name
  FROM `collector` as c
  LEFT JOIN `platform` as p ON c.platform = p.id 
  LEFT JOIN register reg ON c.document=reg.nd ";

    if (isset($_GET['task_one'])) {
        $reportsId = get_task($_GET['task_one'])[0];
        $q .= "WHERE `self-evaluation` IN ({$reportsId}) ORDER BY c.`demand-id`";
    }
    if (isset($_GET['task_all'])) {
        $b="";
        $reportsId = get_task($_GET['task_all'],true);
$max=count($reportsId)-1;
        foreach ($reportsId as $key => $value){
            $b.="(".$q."WHERE `self-evaluation` IN ({$value}))";
            if($key!==$max){
                $b.=" union all ";
            }else{
                $b.="order by `demand-id`";
            }
        }
        $q = $b;
    }
    if (isset($_GET['id'])) {
        $reportsId = $_GET['id'];
        $repotStatus = getFieldByField('reports', 'id', $reportsId, 'status');
        $jobsString = getJobsNamesString($reportsId);
        $q .= "WHERE `self-evaluation` IN ({$reportsId}) ORDER BY c.`demand-id`";
    }
    $query = $db->query($q);
}


//получить данные с task bunch and jobs


// Теперь можно открывать закрытые отчеты.


// if ($repotStatus == 1) {
//     echo "<p>Данный отчет уже закрыт. Вы не можете его редактировать</p>";
//     exit;
// }


include "sideout.php";
?>
<div class="wrapper">
    <table id="<?= $type; ?>" class='m-t-0 report table table-striped table-bordered table-condensed header-static'>
        <thead>
        <tr class="text-center">
            <? if ($type == 'calendar') { ?>
                <th>Источник информации</th>
            <? } ?>
            <th class="orange">Вид работ</th>
            <th class="orange">Под вид </th>
        <? if ($date['typen'] != 7) { ?>
            <th class="w350 orange">Требование</th> <!--Demand actions-->
        <? } else { ?>
             <th class="w350 orange">Необходимые мероприятия</th> <!--Demand actions-->
        <? } ?>
        <? if ($date['typen'] != 7) { ?>
            <th class="orange">Тип требования по срокам</th><!--\ Type of event on time-bound -->
        <? } else { ?>
            <th class="hidden w0 orange">Тип требования по срокам</th><!--\ Type of event on time-bound -->
        <? } ?>
            <th class="w160 orange">Статус соответствия</th><!--\ Status of compliance -->

            <!-- Скрываем ФИО ответственного и Плановый срок в аудитах и гос/проверках -->
            <? if ($type == 'calendar' || $type == 'self-evaluation') { ?>
                <th class="orange">Фамилия, имя ответственного за устранение
                </th><!--\ Name of the responcible for the
                    completion -->
                <th class="orange">Плановый Срок приведения в соответстие</th><!-- \ Completion Date -->
            <? } ?>

            <th class="w350 orange">Выявленные несоответствия</th><!--\ Discrepancy -->

            <? if ($type != 'calendar') { ?>
<!--                <th>Пункт нормативного правового акта</th><!-- \ Item of normative legal act -->
<!--                <th>Источник информации</th><!--\ Source of requirement -->
                <th>Наименование нормативного правового акта</th><!-- \ Name of normative legal act-->
                <th>Номер и дата утверждения</th><!-- \ Number and date of approval-->
                <th>Актуализирован на соответствие оригинальному документу (дата)</th><!--Текст матрицы -->
            <? } ?>

            <? if ($type == 'calendar') { ?>
                <th class="w350 orange">Мероприятия по устранению</th><!--\ Recommendations -->
                                                                      <!--                <th>Источник информации</th>-->
                <th>Фактический срок приведения в соответстие</th>
                <th class="w160">Текущий статус соответствия</th>
                 <? if ($date['typen'] != 7) { ?>
                <th>Причина не выполнения</th>
                <? } ?>
                <th>Утвердить мероприятие</th>
            <? } ?>
        </tr>
        <tr class="filter">
            <!-- EHS № -->
            <? if ($type == 'calendar') { ?>
                <th></th>
            <? } ?>
            <!-- Вид работы -->
            <th class="orange">
                <input class="form-control input-sm" type="text" value="" data-filter="job"
                       onkeydown='matrixFilter(event)'
                       onchange='matrixFilter(event)'
                />
            </th>
            <th class="orange">
                <input class="form-control input-sm" type="text" value="" data-filter="job_child"
                       onkeydown='matrixFilter(event)'
                       onchange='matrixFilter(event)'
                />
            </th>
            <!-- Необходимые мероприятия -->
            <th class="w350 orange">
                <input class="form-control w100 input-sm" type="text" value="" data-filter="demand"
                       onkeydown='matrixFilter(event)'
                       onchange='matrixFilter(event)'
                />
            </th>
            <!-- Тип требования по срокам -->
             <? if ($date['typen'] != 7) { ?>
            <th class="orange"></th>
        <? } ?>
            <!-- Добавляем поле Вид оценки в календарь -->
            <!--            --><? // if ($type == 'calendar') { ?>
            <!--                <th class="orange w160"></th>-->
            <!--            --><? // } ?>

            <!-- Статус соответствия -->
            <th class="w160 orange">
                <? if ($type != 'calendar') { ?>
                    <select class="form-control" onchange='matrixFilter(event)' data-filter="status">
                        <option value="" class="fivet" selected="">Все записи</option>
                        <!-- <option value="" class="fivet" ></option> -->
                        <option value="N/A" class="fivet">N/A</option>
                        <option value="более 90%" class="onet">более 90%</option>
                        <option value="от 71% до 90%" class="twot">от 71% до 90%</option>
                        <option value="от 41% до 70%" class="threet">от 41% до 70%</option>
                        <option value="менее 40%" class="fourt">менее 40%</option>
                    </select>
                <? } ?>
            </th>


            <? if ($type == 'calendar' || $type == 'self-evaluation') { ?>
                <!-- ФИО за устранение -->
                <th class="orange">
                    <input class="form-control w100 input-sm" type="text" value="" data-filter="fio"
                           onkeydown='matrixFilter(event)'
                           onchange='matrixFilter(event)'
                    />
                </th>
                <!-- Плановый срок  -->
                <th class="orange">
                    <input class="form-control w100 input-sm" type="text" value="" data-filter="date-plan"
                           onkeydown='matrixFilter(event)'
                           onchange='matrixFilter(event)'
                    />
                </th>
            <? } ?>


            <? if ($type != 'calendar') { ?>
                <th class="orange"></th>
                <th>
                    <input class="form-control w100 input-sm" type="text" value="" data-filter="document"
                           onkeydown='matrixFilter(event)'
                           onchange='matrixFilter(event)'
                    />
                </th>
                <th></th>
                <th></th>
<!--                <th></th>-->
<!--                <th></th>-->
            <? } ?>

            <? if ($type == 'calendar') { ?>
                <!-- Выявленные несоответствия -->
                <th class="orange"></th>
                <!-- Рекомендации -->
                <th class="orange"></th>
                <!-- Источник информации -->
                <!--                <th></th>-->
                <!-- Фактический срок  -->
                <th></th>
                <!-- Текущий статус  -->
                <th>
                    <a onclick="$('tr.done').toggle();">скрыть выполненные</a>
                </th>
                <!-- Причина не выполнения  -->
                 <? if ($date['typen'] != 7) { ?>
                <th></th>
            <? } ?>
                <!-- Согласованность  -->
                <th class="w200"></th>
            <? } ?>

        </tr>
        </thead>

        <? include_once $_SERVER['DOCUMENT_ROOT'] . '/app/views/reports/matrix-data.php'; ?>

    </table>
</div>

<input type="hidden" value="<?= $repotStatus; ?>" id="repotStatus"/>
<script src="/app/views/reports/js/matrix.js"></script>
