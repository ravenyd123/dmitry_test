<? include_once $_SERVER['DOCUMENT_ROOT'] . '/assets/functions.php'; ?>
<? include_once $_SERVER['DOCUMENT_ROOT'] . '/app/views/header.php'; ?>
<? include_once $_SERVER['DOCUMENT_ROOT'] . '/app/views/template.php';
include_once $_SERVER['DOCUMENT_ROOT'] . '/assets/functions.php'; ?>

<?php
$data = recursion(true);
?>
<!--Main Content -->
<section id="addReport" class="content">

    <!-- Page Content -->

    <div class="wraper container-fluid">
        <div class="col-md-offset-3 col-md-6">
            <div class="panel panel-default report-filter">
                <div class="panel-heading">
                    Создать новый отчет
                </div>
                <form id="tt">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="color">Тип отчета:</label>
                                <select class="select2" name="type">
                                    <option value="self-evaluation">Самооценка</option>
                                    <option value="audit">Аудит</option>
                                    <option value="government">Проверка Гос/органов</option>
                                </select>
                                <div class="help-block"></div>

                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="Crplatform">Предприятие:</label>
                        <select class="select2" id="Crplatform" data-placeholder="Выберите...">
                            <option selected></option>
                            <? foreach ($data as $value) { ?>
                                <option value="<?= $value['id']; ?>"><?= $value['text']; ?></option>
                            <? } ?>

                        </select>
                        <div class="help-block"></div>
                    </div>
                    <div class="form-group hidden">
                        <label for="CrLaw">Наименование организации:</label>
                        <select class="select2" id="CrLaw" data-placeholder="Выберите...">
                            <option selected></option>
                        </select>
                        <div class="help-block"></div>

                    </div>
                    <div class="form-group hidden">
                        <label for="Crdepartment">Подразделении:</label>
                        <select class="select2" id="Crdepartment" data-placeholder="Выберите...">
                            <option selected></option>
                        </select>
                        <div class="help-block"></div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <input type="button" value="Сформировать группу отчетов" class="btn btn-primary m-t-25"
                                   onclick="//submitForm(event);">
                        </div>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>

</section>

<? include 'footer.php'; ?>
<script>

    $(document).ready(function () {
        var data =<?php echo json_encode($data)?>;
        var departments = $('select#Crdepartment');
        var law = $('select#CrLaw');
        var platform_one = $('select#Crplatform');
        var optionszero = '<option value=0 >Выберите...</option>';

        platform_one.on("change", function (e) {
            law.closest('div').addClass('hidden');
            law.empty();
            //  law.removeAttr('name');
            departments.closest('div').addClass('hidden');
            departments.empty();
            // departments.removeAttr('name');

            platform_one.attr("name", "platform");

            if (data[e.val]['children'].length > 0) {
                law.closest('div').removeClass('hidden');
                law.append(optionszero);
                data[e.val]['children'].forEach(function (item, i, arr) {
                    law.append('<option value=' + arr[i].id + '>' + arr[i].text + '</option>');
                    //  if (i==0){law.select2('val',arr[i].id).trigger("change")}
                });
            }
        });


        law.on("change", function (e) {
            if (e.val == 0) {
                platform_one.attr("name", "platform");
                law.removeAttr('name');
            } else {
                platform_one.removeAttr('name');
                law.attr("name", "platform");
            }
            departments.closest('div').addClass('hidden');
            departments.empty();
            var select_platform_id = $('select#Crplatform:enabled option:selected').val();
            var dd = data[select_platform_id]['children'];
            departments.append(optionszero);
            dd.forEach(function (item, i, arr) {
                if (arr[i].id == e.val) {
                    if (dd[i].children.length > 0) {
                        departments.closest('div').removeClass('hidden');
                        dd[i].children.forEach(function (item, i, arr1) {
                            departments.append('<option value=' + arr1[i].id + '>' + arr1[i].text + '</option>');
                            if (i == 0) {
                                //            departments.select2('val',arr1[i].id).trigger("change");
                               // departments.attr("name", "platform");
                                //law.removeAttr('name');
                            }
                        })
                    }
                }
            });

             departments.on('change', function (e) {
                 if (e.val == 0) {
                     law.attr("name", "platform");
                     departments.removeAttr('name');
                 } else {
                     law.removeAttr('name');
                     departments.attr("name", "platform");
                 }

             })

        });
    });

    function submitForm(event) {
        var platform = "";
        $(event.target).val('Идет формирование отчетов...');
        var type = $('select[name="type"]').val();
        $('select[name^="Cr"]:enabled option:selected').each(function (i, arr) {
            platform = "";
            if ($(arr).val() != 0) {
                platform += $(arr).val();
            }
        });
        parseInt(platform);

        setTimeout(function () {
            $.ajax({
                type: 'POST',
                dataType: "json",
                data: {
                    "action": "collector",
                    "platform": parseInt(platform),
                    "type": type
                },
                url: "/assets/functions.php",
                cache: false,
                async: false,
                success: function (data) {
                    if (data['type'] == 'error') {
                        $(event.target).val('Сформировать группу отчетов');
                        autoHideNotify('error', 'top right', data['message'], data['message2']);
                        return false;
                    } else if (data['type'] == 'debug') {
                        console.log(data);
                    } else if (data['type'] == 'success') {
                        window.location.href = "/reports/all";
                    }

                    console.log(data);

                },
                error: function () {
                    console.log('Данные не получены');
                }
            })
        }, 100);

    }
    ;

</script>