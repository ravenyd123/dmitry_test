<? include_once $_SERVER['DOCUMENT_ROOT'].'/app/views/header.php'; ?>
<? include_once $_SERVER['DOCUMENT_ROOT'].'/app/views/template.php'; ?>

<?

// Выбираем все платформы пользователя
$platforms = mysqli_query($link, "SELECT users.login,users.platform FROM `users` AS users
                                  WHERE users.login = '".$_COOKIE['login']."'");
while ($pRow = mysqli_fetch_assoc($platforms)) { 
    $platformsArray .= $pRow['platform'].",";
} 
$platformsArray = trim($platformsArray, ',');

// Выбираем платформу с которой будем работать
if ( isset($_POST['platform']) ) {
	$platform = $_POST['platform'];
}else{
	$platform = explode(',', $platformsArray);
	$platform = $platform[0];
}

// Получаем настройки платформы
$settings = getFieldByField($link,'platform','id',$platform,'settings');
$ej_new = json_decode($settings, true);

// Разбираем информацию по платформе по отдлельным EHS и видам работ в массив $chains
$chains = [];
$i = 0;

// echo "<pre>";
// var_dump($ej_new['chain']);
// echo "</pre>";

foreach ($ej_new['chain'] as $key => $value) {

	if ( strpos($value, ',') ) {

		$job = explode(',',$value);

		foreach ($job as $jKey => $jValue) {
			$chains[$i]['ehs-number'] = $key;
			$chains[$i]['ehs-id'] = getEhsByFilter( $link,$key,$jValue );
			$chains[$i]['job-id'] = $jValue;
			$i++;
		}

	}else{

		$chains[$i]['ehs-number'] = $key;
		$chains[$i]['ehs-id'] = getEhsByFilter( $link,$key,$value );
		$chains[$i]['job-id'] = $value;
		$i++;

	}

}

// echo "<pre>";
// var_dump($chains);
// echo "</pre>";


# Собираем уже используемые в коллекторе виды работ
foreach ($chains as $key => $value) {

$collector = mysqli_num_rows( mysqli_query($link, "SELECT `id` FROM `collector`
												   WHERE 
												   `user` = '".$_COOKIE['login']."' AND
												   `platform` = '".$platform."' AND
												   `calendar` IS NULL AND
												   `job` = ".$chains[$key]['job-id'])
												   );

$columns[$key]['ehs-id'] = $chains[$key]['ehs-id'];
$columns[$key]['ehs-name'] = getFieldByField($link,'ehs','id',$chains[$key]['ehs-id'],'ehs_name');
$columns[$key]['ehs-number'] = $chains[$key]['ehs-number'];
$columns[$key]['job-id'] = $chains[$key]['job-id'];
$columns[$key]['job-name'] = getFieldByField($link,'types_jobs','id',$chains[$key]['job-id'],'type');
// Если запись в коллекторе найдена, ставим виду работы признак disabled
if ($collector > 0) {
	$columns[$key]['disabled'] = '1';
}

}

// Убираем дубликаты номеров EHS
$ehs = $columns;

foreach ($ehs as $key => $value) {
	if ($ehs[$key]['ehs-number'] == $oldValue) {
		unset($ehs[$key]);
		continue;
	}
	unset($ehs[$key]['job-id']);
	unset($ehs[$key]['job-name']);
	$oldValue = $ehs[$key]['ehs-number'];
}

// Массив видов работ
$job = $columns;

?>

<!--Main Content -->
<section id="addReport" class="content">

	<!-- Page Content -->

	<div class="wraper container-fluid">
		<div class="page-title">
			<h3 class="title">Создать новый отчет</h3>
		</div>

		<div class="panel panel-default report-filter">
			<div class="row">
				<div class="col-md-6">

                    <div class="form-group">
                    	<form action="" method="POST">
	                        <label for="color">Выберите предприятие:</label>
	                        <select class="form-control" name="platform" onchange="this.form.submit();">
	                            <?
	                                $result = mysqli_query($link, "SELECT * FROM `platform` WHERE `id` IN (".$platformsArray.")");
	                                while ($row = mysqli_fetch_assoc($result)) { 
	                                    $allPlatforms .= $row['id'].",";
	                                }
	                            ?>

	                            <? mysqli_data_seek($result, 0); ?>

	                            <?
	                                while ($row = mysqli_fetch_assoc($result)) { 
	                                    $allPlatforms .= $row['id'].",";
	                            ?>
	                                <option value="<?=$row['id'];?>" 
	                                <?//=( $platform == $row['id'] ? 'selected=""' : '' );?>
	                                <?
	                                    if (isset($_POST['platform'])) {
	                                    	echo ( $row['id'] == $_POST['platform'] ? 'selected=""' : '' );
	                                    }
	                                ?>
	                                ><?=$row['name'];?></option>

	                            <? } ?>
	                        </select>
	                        <div class="help-block"></div>
                        </form>
                    </div>

				</div>
				<div class="col-md-6">
					<input type="button" value="Сформировать отчет" class="btn btn-default m-t-25" onclick="submitForm();"></input>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-6">




				<div class="panel panel-default">
					<div class="panel-heading b-0">

<?
// echo $_COOKIE['login'];
// echo "Кол-во EHS: ".count($ehs);
// echo "<pre>";
// var_dump($columns);
// echo "</pre>";
?>
						<div class="row">
						   <div class="col-md-11">
								<h3 class="panel-title">Выбор EHS</h3>
						   </div>
						   <div class="col-md-1">
						   	<input class="text-right" type="checkbox" onclick="checkAll(event);">
						   </div>
						</div>
					</div>
					<div class="panel-body p-t-0">
					   <div class="h-700">
	                  <table class='table table-hover pad-5 pointer' id="ehs">
	                  	<? foreach($ehs as $key=>$value){?>
	                       	<tr>
	                            <td>
									<input type="checkbox" name="ehs[]" 
									data-id="<?=$ehs[$key]['ehs-id'];?>" 
									value="<?=$ehs[$key]['ehs-number'];?>" >
									<!-- Не забыть проставить ограничения по пользователям -->
	                            </td>
								<td>
									<p class="text-nowrap"><?=$ehs[$key]['ehs-number'];?></p>
								</td>
								<td>
									<p><?=$ehs[$key]['ehs-name'];?></p>
								</td>
	                       	</tr>
	                      <? } ?>
	                  </table>
	               </div>
					</div>
				</div>
			</div><!-- .col-md-6 -->

			<div class="col-md-6">
				<div class="panel panel-default">
					<div class="panel-heading b-0">

<?
// echo "Кол-во работ: ".count($job);
// echo "<pre>";
// var_dump($job);
// echo "</pre>";
?>

						<div class="row">
						   <div class="col-md-11">
								<h3 class="panel-title">Выбор вида работ</h3>
						   </div>
						   <div class="col-md-1">
						   	<input class="text-right" type="checkbox" onclick="checkAll(event);">
						   </div>
						</div>
					</div>
					<div class="panel-body p-t-0">
						<div class="row">
							<div class="col-md-12">
								<div class="h-700">
								<form id="target" method="post" target="_blank" action="../../../../index.php">
									<table class='table table-hover pad-5 pointer' id='jobs'>
					                  	<? foreach($job as $key=>$value){?>

<tr class="inactive <?=($job[$key]['disabled'] == 1 ? "disabled" : "") ?>" 
	data-ehs="<?=$job[$key]['ehs-number'];?>" 
	data-job="<?=$job[$key]['job-id'];?>">
    <td>
   		<input type="checkbox" name="job[]" 
   		data-ehs="<?= getSimilarEHSid($link, $job[$key]['ehs-id'] );?>"
   		data-job="<?=$job[$key]['job-id'];?>"
		<?=($job[$key]['disabled'] == 1 ? "disabled='disabled'" : "") ?>
   		>
    </td>
	<td>
		<p><?=$columns[$key]['job-name'];?></p>
	</td>
</tr>

											<? $oldValue = $job[$key]['ehs-number']; ?>

					                    <? } ?>		
									</table>
									
								</form>
							   </div>
							</div>

						</div>

					</div>
				</div>
			</div><!-- .col-md-6 -->


		</div>
		<!-- End Row -->

	</div>
<input type="hidden" value="" id="jobs-array">
</section>

<? include 'footer.php'; ?>
<script>

$(document).ready(function () {

	$('#addReport table tr').click(function(){
		var checkbox = $(this).find('input[type="checkbox"]');
		checkbox.trigger('click');
	})

	$('#ehs input[type="checkbox"], #jobs input[type="checkbox"]').change(function() {
		$(this).trigger('click');
		getJob();
	});

});

function getJob(){

	var jobs = [];

	// Ставим все виды работ неактивными
	$('#jobs tr').addClass('inactive');

	// Ставим отмеченные виды работ активными
	$('#ehs input[type="checkbox"]:checked').each(function(index,ehs){

		var job = $('#jobs tr[data-ehs="' + $(this).val() + '"]')

		// Убираем дубли видов работ
		job.each(function(index,item){
			if ( jobs.indexOf( $(item).attr('data-job') ) == -1 ) {
				$(item).removeClass('inactive');
			};
			jobs.push( $(item).attr('data-job') );		
		})



	});

	buttonActivity();

}

function submitForm(){

	if ( checkRowAmount() < 1 ) {
		autoHideNotify('error', 'top right', 'Не выбрано ни одной связки','Необходимо выбрать хотя бы одну связку EHS и вид работы');
		return false;
	}else{

	// Заполнение коллектора
    var chains = [];
    $("tr:not(.inactive) input[name='job[]']:checked").each(function (index,item) {

    	chains.push({ "ehs":$(item).attr('data-ehs'), "job":$(item).attr('data-job') });

    });		

    var platform = $('select[name="platform"]').val();

    var attr = 'action=collector' + '&platform=' + platform + '&data=' + JSON.stringify(chains);
    var matrixID = parseInt(getMaxFieldValue('collector','self-evaluation'))+1

    console.log(chains);

    $.ajax({
        type: 'POST',
        url: "/assets/functions.php",
        cache: false,
        data: attr,
        success: function (msg) {
        	// console.log(msg);
        	window.location.href = "/reports/matrix?type=self-evaluation&id="+matrixID+"&platform="+platform
        }
    });

	};
}

function checkAll(event){
	if ($(event.target).prop("checked")) {
		$(event.target).parents('.panel').find('table tr:not(.inactive,.disabled) input[type="checkbox"]').each(function(index,item){
		      $(this).prop('checked', true);
		});
	}else if (!$(event.target).prop("checked")) {
		$(event.target).parents('.panel').find('table tr:not(.inactive) input[type="checkbox"]').each(function(index,item){
		      $(this).prop('checked', false);
		});	
	};
	getJob();
}

function checkRowAmount(){
	var rowAmount = $('#jobs input[type="checkbox"]:checked').length;
	return rowAmount;
}

function buttonActivity(){
	// Активность кнопки
	if (checkRowAmount() > 0) {
		$('.report-filter').find('input[type="button"]').addClass('btn-primary');
	}else{
		$('.report-filter').find('input[type="button"]').removeClass('btn-primary');
	};
}

// Смена предприятия
function changePlatform(event){

    $.ajax({
        type: 'POST',
        url: "/app/views/settings/ajax-settings.php",
        cache: false,
        data: {
            "platform": $('select[name="platform"] option:selected').val()
        },
        success: function (msg) {
            $("#settings .panel-body").empty();
            $("#settings .panel-body").html(msg);
            // console.log(msg);
            // location.reload();
        }
    });

    // $("#settings .panel-body").load("/app/views/settings/ajax-settings.php",);
}

</script>