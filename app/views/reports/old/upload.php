<?php
include_once $_SERVER['DOCUMENT_ROOT'] . '/assets/functions.php';
$db = db::Connection();
$target_dir = $_SERVER['DOCUMENT_ROOT'] . "/uploads/";
if (isset($_GET["downloads"])) {

    $f_data = file_id($_GET['downloads']);
    $path_to_file=$target_dir .$f_data["f_bunch"]."/". $f_data["f_name"];
    if (file_exists($path_to_file)) {
        $fsize = filesize($path_to_file);

        Header("HTTP/1.1 200 OK");
        Header("Connection: close");
        Header("Content-Type: application/octet-stream");
        Header("Accept-Ranges: bytes");
        Header("Content-Disposition: Attachment; filename=" . $f_data["f_name"]);
        Header("Content-Length: " . $fsize);

// Открыть файл для чтения и отдавать его частями
        $f = fopen($path_to_file, 'r');
        while (!feof($f)) {
            // Если соединение оборвано, то остановить скрипт
            if (connection_aborted()) {
                fclose($f);
                break;
            }
            echo fread($f, 10000);
            // Пазуа в 1 секунду. Скорость отдачи 10000 байт/сек
            sleep(1);
        }
        fclose($f);
    } else {
        header("Location: " . $_SERVER['HTTP_REFERER']);
    }

}

if (isset($_POST["upload"])) {
    // $target_dir = "uploads/";
    $bunch = $_POST["bunch"];
    $dir = $target_dir . $bunch;
    $target_file = $dir . "/". basename($_FILES["fileToUpload"]["name"]);
    $uploadOk = 1;

    if (file_exists($target_file)) {
        echo "К сожалению, файл уже существует";
        $uploadOk = 0;
    }
    if ($_FILES["fileToUpload"]["size"] > 20000000) {
        echo "К сожалению, ваш файл слишком большой";
        $uploadOk = 0;
    }
    if ($uploadOk == 0) {
        echo "";
    } else {
        $name = htmlspecialchars($_FILES["fileToUpload"]["name"]);
        // $blob = file_get_contents($_FILES["fileToUpload"]["tmp_name"]);
        $size = intval($_FILES["fileToUpload"]["size"]);

        ///не работает блоб с docx
        $blob = "ремонт";
        if ($_FILES['fileToUpload']['error'] != 0) {
            echo 'Error: ' . $_FILES['fileToUpload']['error'] . '<br>';
        } else {
            if (is_dir($dir)) {
                rrmdir($dir);
            }

            mkdir($dir, 0755);

            if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
                $result = $db->query("INSERT HIGH_PRIORITY INTO attachments(f_bunch,f_name,f_path,f_data,f_size)
       VALUES('{$bunch}','{$name}','{$dir}','{$blob}','{$size}')");
                if ($result) {
                    echo json_encode('success');
                } else {
                    echo json_encode($result);
                }
            } else {
                echo "уже есть";
            }

        }
    }
}
if (isset($_GET["delete"])) {
    $file = file_id($_POST["key"]);
    $target_file = $file["f_path"] . $file["f_name"];
    if (file_exists($target_file)) {
        if (unlink($target_file)) {
            $result = $db->query("DELETE LOW_PRIORITY QUICK FROM attachments WHERE id ='" . intval($_POST["key"]) . "' LIMIT 1");
            echo $result;
        }
    } else {
        echo " а нету";
    }

}
function file_id($id)
{
    {
        $db = db::Connection();
        $result = $db->query("SELECT a.f_bunch,
       a.f_name,
       a.f_path FROM attachments a WHERE a.id ='{$id}' limit 1");
        $allfiles = $result->fetch_assoc();
        return $allfiles;
    }
}

// Идентификатор группы чек листов
//$bunch = $_POST["attachment-bunch"];
// Директория для загрузки файла
//$type  = $_POST["attachment-report-type"];
//$dir = $_SERVER['DOCUMENT_ROOT']."/uploads/".$type."/".$bunch;
//move_uploaded_file($_FILES['attachment-file']['tmp_name'], $_SERVER['DOCUMENT_ROOT']."/uploads/".$type."/".$bunch."/".$name);
// Тип отчета


//$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
// Check if image file is a actual image or fake image
/*if(isset($_POST["submit"])) {
    $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
    if($check !== false) {
        echo "File is an image - " . $check["mime"] . ".";
        $uploadOk = 1;
    } else {
        echo "File is not an image.";
        $uploadOk = 0;
    }
}*/

// Check file size

/*// Allow certain file formats
if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
    && $imageFileType != "gif" ) {
    echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
    $uploadOk = 0;
}*/
// Check if $uploadOk is set to 0 by an error
$uploadOk = 0;
