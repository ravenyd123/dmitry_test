<? include_once $_SERVER['DOCUMENT_ROOT'].'/assets/functions.php';

// Получаем расширение файла
$extension = explode('.', $_FILES['attachment-file']['name']);
$extension = end($extension);

// Идентификатор группы чек листов
$bunch = $_POST["attachment-bunch"];

// Тип отчета
$type  = $_POST["attachment-report-type"];

// Генерируем новое имя файла
$name = uniqid().".".$extension;

// Директория для загрузки файла
$dir = $_SERVER['DOCUMENT_ROOT']."/uploads/".$type."/".$bunch;


if ( $_FILES['attachment-file']['error'] != 0  ) {
    echo 'Error: ' . $_FILES['file']['error'] . '<br>';
}
else {

	if ( is_dir( $dir ) ){
		rrmdir( $dir );
	}
	
	mkdir( $dir , 0755 );
    move_uploaded_file($_FILES['attachment-file']['tmp_name'], $_SERVER['DOCUMENT_ROOT']."/uploads/".$type."/".$bunch."/".$name);

	// mysqli_query($link, "UPDATE `reports` SET `attachment` = '".$name."' WHERE `bunch`=$bunch");
}

?>