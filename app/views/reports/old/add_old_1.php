<? include_once $_SERVER['DOCUMENT_ROOT'].'/assets/functions.php'; ?>
<? include_once $_SERVER['DOCUMENT_ROOT'].'/app/views/header.php'; ?>
<? include_once $_SERVER['DOCUMENT_ROOT'].'/app/views/template.php'; ?>

<?

// Выбираем все платформы пользователя
$platforms = mysqli_query($link, "SELECT users.login,users.platform FROM `users` AS users
                                  WHERE users.login = '".$_COOKIE['login']."'");
while ($pRow = mysqli_fetch_assoc($platforms)) { 
    $platformsArray .= $pRow['platform'].",";
} 
$platformsArray = trim($platformsArray, ',');

// echo "<pre>";
// var_dump($chains);
// echo "</pre>";

?>

<!--Main Content -->
<section id="addReport" class="content">

	<!-- Page Content -->

	<div class="wraper container-fluid">
		<div class="page-title">
			<h3 class="title">Создать новый отчет</h3>
		</div>

		<div class="panel panel-default report-filter">
			<div class="row">
				<div class="col-md-6">
                    <div class="form-group">
                    	<form action="" method="POST">
	                        <label for="color">Выберите предприятие:</label>
	                        <select class="select2" name="platform">
	                            <?
	                                $result = mysqli_query($link, "SELECT * FROM `platform` WHERE `id` IN (".$platformsArray.")");
	                                while ($row = mysqli_fetch_assoc($result)) { 
	                                    $allPlatforms .= $row['id'].",";
	                                }
	                            ?>

	                            <? mysqli_data_seek($result, 0); ?>

	                            <?
	                                while ($row = mysqli_fetch_assoc($result)) { 
	                                    $allPlatforms .= $row['id'].",";
	                            ?>
	                                <option value="<?=$row['id'];?>"><?=$row['name'];?></option>

	                            <? } ?>
	                        </select>
	                        <div class="help-block"></div>
                        </form>
                    </div>
				</div>

				<div class="col-md-3">
                    <div class="form-group">
                    	<form action="" method="POST">
	                        <label for="color">Тип отчета:</label>
	                        <select class="select2" name="type">
	                            <option value="self-evaluation">Самооценка</option>
	                            <option value="audit">Аудит</option>
	                            <option value="government">Проверка Гос/органов</option>
	                        </select>
	                        <div class="help-block"></div>
                        </form>
                    </div>
				</div>

				<div class="col-md-3">
					<input type="button" value="Сформировать группу отчетов" class="btn btn-primary m-t-25" onclick="submitForm(event);"></input>
				</div>
			</div>
		</div>

	</div>

</section>

<? include 'footer.php'; ?>
<script>

$(document).ready(function () {

});

function submitForm(event){

	$(event.target).val('Идет формирование отчетов...');

    var platform = $('select[name="platform"]').val();
    var type = $('select[name="type"]').val();

    // console.log(platform);
	// return false;

	setTimeout(function() { 
		$.ajax({
			type: 'POST',
			dataType: "json",
			data: {
				"action": "collector",
				"platform": platform,
				"type": type,
			},
			url: "/assets/functions.php",
			cache: false,
			async: false,
			success: function(data){
				if (data['type'] == 'error') {
					$(event.target).val('Сформировать группу отчетов');
					autoHideNotify('error', 'top right', data['message'],data['message2']);	
					return false;
				}else if (data['type'] == 'debug') {
					console.log(data);
				}else if (data['type'] == 'success') {
					window.location.href = "/reports/all";
				}

				console.log(data);

			},
	        error: function() {
	            console.log('Данные не получены');
	        }
		})
	}, 100);

};

</script>