<? include_once $_SERVER['DOCUMENT_ROOT'].'/assets/functions.php'; ?>
<? include_once $_SERVER['DOCUMENT_ROOT'].'/app/views/header.php'; ?>
<? include_once $_SERVER['DOCUMENT_ROOT'].'/app/views/template.php'; ?>

<? $reports = getReportsByPlatform(); ?>

<!--Main Content -->
<section class="content">

	<!-- Page Content -->

	<div class="wraper container-fluid">
		<div class="page-title">
			<h3 class="title">Платформа мониторинга</h3>
			
		</div>

		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title pull-left m-t-10">Календарь мероприятий</h3>
							<div class="pull-right">

<button class="btn btn-success" data-action="loadReport" data-toggle="tooltip" data-placement="top" title="Импорт данных из Excel файла">
	<i class="ion-archive"></i>
</button>
<form class="hidden" name="uploader" enctype="multipart/form-data" method="POST">
    <input name="userfile" type="file" />
    <button type="submit" name="submit">Загрузить</button>
</form>

<button class="btn btn-inverse" data-toggle="tooltip" data-placement="left" title="Вы можете сформировать календарь мероприятий по определенной площадке за указанный период"><i class="ion-information"></i></button>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="panel-body">
						<div class="row">

							<div class="col-md-9">
								<div class="form-group">
									<label>Чек-листы:<sup>*</sup></label>
									<select name="bunch" required multiple class="select2" data-placeholder="Выберите чек-листы по которым строить отчет...">
										<? foreach ($reports as $key => $value) { ?>
										<option value="<?=$reports[$key]['bunch']?>">
                                            <?=type_tasks_new[$reports[$key]['typen']]?>:
											Чек лист №<?=$reports[$key]['bunch']?>
											от <?=date('d.m.Y',$reports[$key]['date']);?>
											по площадке <?=getFieldByField('platform','id',$reports[$key]['platform'],'name');?>
										</option>
										<? } ?>
									</select>
									<div class="help-block"></div>
								</div>
							</div>

							<div class="col-md-3">
								<div class="form-group m-t-25">
									<input type="button" data-action="generateCalendar" class="btn btn-primary w-100" value="Сформировать">
								</div>
							</div>

						</div>
					</div>
				</div>
			</div>

		</div>
		<!-- End Row -->

	</div>

</section>

<? include 'footer.php'; ?>

<script>

// Редактирование самооценки
$('*[data-action="generateCalendar"]').click(function(){

	var bunch = $('select[name="bunch"]').val();

	if ( bunch == '')
	{
		$('select[name="bunch"]').closest('.form-group').addClass('has-error');
		$('select[name="bunch"]').closest('.form-group').find('.help-block').text('Поле обязательно для заполнения');

		$.notify({'type':'error', 'message':'Форма заполнена не верно. Проверьте правильность заполнения полей формы'});
		return false;
	}else{
		window.open(
			'/reports/matrix?type=calendar&bunch=' + bunch,
		  	'_blank' // <- This is what makes it open in a new window.
		);
	}

})

// Загрузить отчет в формате excel
$('*[data-action="loadReport"]').click(function(){
	$('input[name="userfile"]').trigger('click');
})
$('input[name="userfile"]').change(function(){
	
	$("form[name='uploader']").submit();

})

$("form[name='uploader']").submit(function(e) {

    var formData = new FormData($(this)[0]);

    $.ajax({
        url: '/app/views/reports/templates/all/l_calendar.php',
        type: "POST",
        data: formData,
        async: false,
        cache: false,
      //  beforeSend:	$.notify({'type':'error', 'message':'Тестовое сообщение'});
        success: function (data) {
        	console.log(data);
        	if ( typeof $.parseJSON(data) == 'object') {
        		uploadErrors = $.parseJSON(data);
        		for (var key in uploadErrors) {
					//autoHideNotify('error', 'top right', uploadErrors[key]['header'],uploadErrors[key]['text']);
					$.notify({'type':'error', 'message':uploadErrors[key]['text']});
        		}
        	};
        },
        error: function(data) {
            console.log('Ошибка!');
            $.notify({'type':'error', 'message':'Произошла ошибка!'});
        },

        contentType: false,
        processData: false
    });

    e.preventDefault();

});

</script>
