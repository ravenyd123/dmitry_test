/**
 * Created by А д м и н on 09.05.2019.
 */
var table = $('#datatable_matrix').dataTable({
   //"lengthChange": false,
    //stateSave: true,
   // "aaSorting": [[1, 'desc']],
    "sScrollX": "100%",
    "fixedHeader": true,
    "select": true,
    "scrollCollapse": true,
    "paging": true,
    "processing": true,
    "serverSide": true,
    ajax: {
        url: "/app/views/reports/new/matrix_new.php",
        data: {"action": "task_all","type":"calendar"},
        type:"POST",
       // dataType: "json",
        cache: false,
      //  contentType: "application/json; charset=utf-8",
      //   dataSrc: function (json) {
      //       return json.data
      //   },
    },
    // columnDefs: [
    //     {
    //      //   "title": "№",
    //         "targets": [0],
    //         "className": "text-center",
    //         // "data": null,
    //         "orderable": false, "defaultContent": '', "width": "5%"
    //     },
    //     {
    //         // "title": "Виды работ",
    //         "targets": [1],
    //         "className": "text-center",
    //       //  "data": "date",
    //         // "type": "de_datetime",
    //         "width": "5%"
    //     },
    //     {
    //         // "title": "Необходимые мероприятия",
    //         "targets": [2],
    //         "className": "text-justify",
    //         // "data": "name",
    //         "width": "300px",
    //         "orderable": false
    //     },
    //      {
    //          // "title": "Тип требования по срокам",
    //         "targets": [3],
    //         "className": "text-center",
    //         //"data": "type",
    //         "orderable": false,
    //         "width": "5%"},
    //     {"title": "Статус соответствия",
    //         "targets": [4],
    //         "className": "text-center",
    //         //"data": "platform",
    //         "width": "5%"},
    //     {
    //         // "title": "ФИО ответственного за устранение",
    //         "targets": [5],
    //         "className": "text-center",
    //         //"data": "deadline",
    //         "width": "5%"},
    //     {
    //         // "title": "Плановый Срок приведения в соответстие",
    //         "targets": [6],
    //         "className": "text-center",
    //         // "data": null,
    //         //"defaultContent": '',
    //         // "visible": false,
    //         "width": "5%"
    //     },
    //     {
    //         // "title": "Выявленные несоответствия",
    //         "targets": [7],
    //         "className": "text-center",
    //         // "data": "authority",
    //         // "visible": false,
    //         "orderable": false,
    //         "width": "5%"
    //     },
    //     {
    //         // "title": "Пункт нормативного правового акта",
    //         "targets": [8],
    //         "className": "text-justify",
    //        // "data": "responsible",
    //         // "visible": false,
    //         "orderable": false,
    //         "width": "150px"
    //     },
    //     {
    //         // "title": "Источник информации",
    //         "targets": [9],
    //         "className": "text-center",
    //     // "data": "director",
    //      "width": "5%"},
    //     {
    //         // "title": "Наименование нормативного правового акта",
    //         "targets": [10],
    //         "className": "text-center",
    //         // "data": "status",
    //         // "visible": false,
    //         "width": "5%"
    //     },
    //     {
    //         // "title": "Номер и дата утверждения",
    //         "targets": [11],
    //         "className": "text-center",
    //         // "data": "status",
    //         // "visible": false,
    //         "width": "5%"
    //     },
    //     {
    //         // "title": "Номер и дата утверждения",
    //         "targets": [12],
    //         "className": "text-center",
    //         // "data": "status",
    //         // "visible": false,
    //         "width": "5%"
    //     }
    // ],
    "initComplete": function () {
        // this.api().column(3).every(function () {
        //     var column = this;
        //     var select = $('<select class="select3"><option value="">Тип задачи</option></select>')
        //         .appendTo($('#datatable-filter div.eh'))
        //         .on('change', function () {
        //             var val = $.fn.dataTable.util.escapeRegex(
        //                 $(this).val()
        //             );
        //             column
        //                 .search(val ? '^' + val + '$' : '', true, false)
        //                 .draw();
        //         })
        //
        //     column.data().unique().sort().each(function (d, j) {
        //         select.append('<option value="' + d + '">' + d + '</option>')
        //     });
        // });

    },
    "createdRow": function (row, data, dataIndex) {
        // if (data['status'] === "В работе") {
        //     $(row).find("td").eq(0).prepend('<span class="badge bg-warning" title="В работе">' + data['number_text'] + '</span>');
        // }
        // if (data['status'] === "Новая") {
        //     $(row).find("td").eq(0).prepend('<span class="badge bg-primary" title="Новая">' + data['number_text'] + '</span>');
        // }
        // if (data['status'] === "Выполнено") {
        //     $(row).find("td").eq(0).prepend('<span class="badge bg-success" title="Выполнено">' + data['number_text'] + '</span>');
        // }
        // if (data['status'] === "Не выполнено") {
        //     $(row).find("td").eq(0).prepend('<span class="badge bg-danger" title="Не выполнено">' + data['number_text'] + '</span>');
        // }
        // if (data['status'] === "Требует согласования") {
        //     $(row).find("td").eq(0).prepend('<span class="badge bg-info" title="Требует согласования">' + data['number_text'] + '</span>');
        // }
        // $(row).attr("data-id", data['id']);
        //data-plid="<?= $row["platform"] ?>"
        // data-nreport="<?= $row["nreport"] ?>
        // $(row).find("td").eq(6).addClass(data["priority"] == 1 ? 'task-red' : 'task-green').append('<div></div>');
    },
    "language": {
        "processing": "Производиться загрузка данных",
        "lengthMenu": "Выводить по _MENU_ записей",
        "zeroRecords": "Ни одной записи не найдено",
        "info": "Показано страниц _PAGE_ из _PAGES_",
        "infoEmpty": "No records available",
        "infoFiltered": "(отфильтровано из _MAX_ записей)",
        "search": "Поиск:",
        "paginate": {
            "first": "Первая",
            "last": "Последняя",
            "next": "Следующая",
            "previous": "Предыдущая"
        },
    }
}).api()