<? include_once $_SERVER['DOCUMENT_ROOT'] . '/app/views/header.php'; ?>
<?// include_once $_SERVER['DOCUMENT_ROOT'] . '/app/views/template.php'; ?>

<?php $type='caledar' ?>
    <!--Main Content -->
                    <div class="">
                        <div class="">
                            <div class="">
                                <div class="">
                                    <table id="datatable_matrix" class="table table-striped table-bordered table-condensed">
                                        <thead>
                                        <tr class="text-center">
                                            <? if ($type == 'calendar') { ?>
                                                <th>Источник информации</th>
                                            <? } ?>
                                            <th class="orange ">№</th>
                                            <th class="orange ">Виды работ</th>
                                            <th class="orange ">Необходимые мероприятия</th><!--  Demand actions-->
                                            <th class="orange ">Тип требования по срокам </th><!-- Type of event on time-bound-->
                                            <th class="orange ">Статус соответствия </th><!--\\ Status of compliance-->

                                            <!-- Скрываем ФИО ответственного и Плановый срок в аудитах и гос/проверках-->
                                            <? if ($type == 'calendar' || $type == 'self-evaluation') { ?>
                                                <th class="orange">Фамилия, имя ответственного за устранение</th>
                                                <!--\ Name of the responcible for the completion-->
                                                <th class="orange">Плановый Срок приведения в соответстие</th><!-- \ Completion Date-->
                                            <? } ?>

                                            <th class="orange">Выявленные несоответствия </th><!--\ Discrepancy

                                            <? if ($type != 'calendar') { ?>
                                                <th>Пункт нормативного правового акта</th><!-- \ Item of normative legal act-->
                                                <th>Источник информации </th><!--\ Source of requirement-->
                                                <th>Наименование нормативного правового акта </th><!-- \ Name of normative legal act-->
                                                <th>Номер и дата утверждения </th><!-- \ Number and date of approval-->
                                                <th>Актуализирован на соответствие оригинальному документу (дата)</th><!--Текст матрицы-->
                                            <? } ?>

                                            <? if ($type == 'calendar') { ?>
                                                <th class=" orange">Мероприятия по устранению </th><!--\ Recommendations-->
                                                                                                                       <th>Источник информации</th>
                                                <th>Фактический срок приведения в соответстие</th>
                                                <th>Текущий статус соответствия</th>
                                                <th>Причина не выполнения</th>
                                                <th>Согласование</th>
                                            <? } ?>
                                        </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
            <!-- End Row -->

    <script type="text/javascript" src="/assets/admina/js/de_datetime.js"></script>
    <script type="text/javascript" src="/app/views/reports/new/matrix_new.js"></script>
