$(document).ready(function () {
    // Наименование отчета
    report = $('.report').attr('id');

    // Фиксированная шапка в отчетах
    $(".header-static").clone().insertAfter(".header-static");
    $(".header-static:nth-child(2)").addClass("header-fixed").removeClass('header-static');
    $(".header-fixed tbody").remove();

    // Отмена формы
    $('#cancel, .close').click(function () {
        $('input#input-ru').off();
        $('input#input-ru').fileinput('destroy');
        $('#selfModal').modal('hide');
    });

    // Сохранение формы
    $('#save').click(function () {
        var row = parseInt($('#row').text());
        var audit = parseInt($('#audit').text());

        var data = {};

        data["id"] = row;

        data["date-plan"] = $('input[name="date-plan"]').val();
        data["fio"] = $('select[name="fio"]').val();

        if (report != 'calendar') {
            data['status-compliance'] = $('select[name="status"]').val();

            text_actions = $('textarea[name="actions"]');

            if (text_actions.val().length < 5 && text_actions.closest('.form-group').hasClass('hidden') == false) {
                text_actions.closest('.form-group').addClass('has-error');
                text_actions.closest('.form-group').find('.help-block').text('Поле обязательно для заполнения');
                return false;
            }
            data['actions'] = $('textarea[name="actions"]').val();

            if (data["status-compliance"] == 0 || data["status-compliance"] == 5|| data["status-compliance"] == 1) {
                data["reason-failure"] = '';
                data["date-plan"] = '';
                data["fact-date"] = '';
                data["fio"] = "";
                data['actions']="";

                data["recommendations"] = '';
            }
        }

        if (report == 'calendar') {
            data["fact-date"] = $('input[name="fact-date"]').val();//фактическая дата

            data["current-status-compliance"] = $('select[name="status"]').val();//статус

            data["agreed"] = $('select[name="agreed"]').val();

            if ($('select[name="agreed"]').val() == 1) {
                data["agreed"] = get_cookie('login');
            }
            data["reason-failure"] = $('textarea[name="reason-failure"]').val();
            data["recommendations"] = $('textarea[name="recommendations"]').val();
            ;

            if (data["current-status-compliance"] == 0) {
                data["reason-failure"] = '';
                data["fact-date"] = 0;
                $('#' + row).find('td.fact-date').text('');
                $('#' + row).find('td.no_actions').text('');
            }
            ;

        };
        $.ajax({
            type: 'POST',
            url: "/assets/functions.php",
            cache: false,
            data: 'matrixData=' + JSON.stringify(data),
            success: function (msg) {
                // Обновление информации в строке
                $.get("/app/views/reports/matrix-data.php?demand=" + row + '&type=' + report + '&status=status-compliance' + '&actions=actions', function (data) {
                    $(data).insertAfter('.report tbody #' + row);

                }).done(function () {
                    // Удаляем старую строку
                    $('#' + row).remove();
                    $('#selfModal').modal('hide');
                });
                $.notify({'type':'success', 'message':'Запись успешно сохранена'});
            },
            error: function (msg) {
                console.log(msg);
            }
        });
        $('input#input-ru').off();
        $('input#input-ru').fileinput('destroy');
    });

});

// Костыль для фиксированной шапки в отчетах
$(window).scroll(function (event) {
    var x = 0 - $('.header-fixed').scrollLeft();
    $(".header-fixed").offset({
        left: x
    });
});

function matrixFilter(event) {

    setTimeout(function () {
        var phrase = $(event.target).val();
        var field = $(event.target).attr('data-filter');

        // Показываем все записи
        $('.header-static .' + field + ':containsi(' + phrase + ')').parents('tr').show();
        $('.header-static .' + field + ' [value="' + phrase + '"]:selected').parents('tr').show();

        // Скрываем записи не отвечающие критериям
        $('.header-fixed .filter input,.header-fixed .filter select').each(function (index, item) {

            if (item.type == 'text') {

                $('.header-static .' + $(item).data('filter') + ':not(:containsi(' + $(item).val() + '))').parents('tr').hide();

            }
            ;

            if (item.type == 'select-one') {

                $('.header-static .' + $(item).data('filter') + ':not(:containsi(' + $(item).val() + '))').parents('tr').hide();

            }
            ;

        });

    }, 50);

}


