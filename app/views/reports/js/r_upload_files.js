
$(document).ready(function () {
   task_id= $('input[name="task_id"]').val();
    task_id=5;

    var all_data;
$.post('/app/views/tasks/upload.php',{ all_data:task_id }).done(function (data) {
   all_data=JSON.parse(data)

    var el1= $('input#input-ru').fileinput({
        language: "ru",
        theme: 'gly',
        // maxFilePreviewSize:10000,
        uploadUrl: "/app/views/tasks/upload.php",
        uploadAsync: true,
        showUpload: false, // hide upload button
        showRemove: false, // hide remove button
        hideThumbnailContent: false,//предварительный вид контенкта
        initialPreviewAsData: false,
       initialPreview: all_data.initialPreview,// просмотр файла
        initialPreviewConfig: all_data.initialPreviewConfig,// первоначальная конфигурация предварительного просмотра, если вы хотите, чтобы начальный предварительный просмотр отображался с данными загрузки сервера
        uploadExtraData: {upload: task_id},
        //maxFileSize: 100,
        showCaption: true,
        showBrowse: true,
        dropZoneEnabled: false,//показ drop zona
        browseOnZoneClick: true,
        initialPreviewDownloadUrl:'upload?downloads={key}',
        overwriteInitial: false,// добавить файлов к предварительному просмотру
        layoutTemplates:{
            main1: '{preview}\n' +
            '<div class="input-group {class}">\n' +
            '  {caption}\n' +
            '  <div class="input-group-btn">\n' +
            '    {browse}\n' +
            '  </div>\n' +
            '</div>',
        },
        // previewFileIconSettings: { // configure your icon file extensions
        //     'doc': '<i class="fa fa-file-word-o text-primary"></i>',
        //     'xls': '<i class="fa fa-file-excel-o text-success"></i>',
        //     'ppt': '<i class="fa fa-file-powerpoint-o text-danger"></i>',
        //     'pdf': '<i class="fa fa-file-pdf-o text-danger"></i>',
        //     'zip': '<i class="fa fa-file-archive-o text-muted"></i>',
        //     'htm': '<i class="fa fa-file-code-o text-info"></i>',
        //     'txt': '<i class="fa fa-file-text-o text-info"></i>',
        //     'mov': '<i class="fa fa-file-movie-o text-warning"></i>',
        //     'mp3': '<i class="fa fa-file-audio-o text-warning"></i>',
        //     // note for these file types below no extension determination logic
        //     // has been configured (the keys itself will be used as extensions)
        //     'jpg': '<i class="fa fa-file-photo-o text-danger"></i>',
        //     'gif': '<i class="fa fa-file-photo-o text-warning"></i>',
        //     'png': '<i class="fa fa-file-photo-o text-primary"></i>'
        // },
        // previewFileExtSettings: { // configure the logic for determining icon file extensions
        //     'doc': function(ext) {
        //         return ext.match(/(doc|docx)$/i);
        //     },
        //     'xls': function(ext) {
        //         return ext.match(/(xls|xlsx)$/i);
        //     },
        //     'ppt': function(ext) {
        //         return ext.match(/(ppt|pptx)$/i);
        //     },
        //     'zip': function(ext) {
        //         return ext.match(/(zip|rar|tar|gzip|gz|7z)$/i);
        //     },
        //     'htm': function(ext) {
        //         return ext.match(/(htm|html)$/i);
        //     },
        //     'txt': function(ext) {
        //         return ext.match(/(txt|ini|csv|java|php|js|css)$/i);
        //     },
        //     'mov': function(ext) {
        //         return ext.match(/(avi|mpg|mkv|mov|mp4|3gp|webm|wmv)$/i);
        //     },
        //     'mp3': function(ext) {
        //         return ext.match(/(mp3|wav)$/i);
        //     },
        // },
       // frameClass:{width:"70px", height: "70px"},
        previewSettings:{
        image: {width: "50px", height: "50px", 'max-width': "100%", 'max-height': "100%"},
        html: {width: "100%", height: "70px"},
        text: {width: "100%", height: "70px"},
        office: {width: "100%", height: "70px"},
        gdocs: {width: "100%", height: "70px"},
        video: {width: "100%", height: "auto"},
        audio: {width: "100%", height: "30px"},
        flash: {width: "100%", height: "auto"},
        object: {width: "100%", height: "auto"},
        pdf: {width: "100%", height: "70px"},
        other: {width: "100%", height: "70px"}

        }
    }).on("filebatchselected", function(event, files) {
        el1.fileinput("upload");
    }).on("filebeforedelete", function (event, id,index) {
        return new Promise(function(resolve, reject) {
     bootbox.confirm({
            title: "Подтверждение",
            message: "Вы уверены что хотите удалить",
            buttons: {
                cancel: {
                    label: 'Отмена'
                },
                confirm: {
                    label: 'Подтвердить'
                }
            },
            callback: function (result) {
                if (result) {
                   resolve();
                }
            }
        })
        });
    });
})
});