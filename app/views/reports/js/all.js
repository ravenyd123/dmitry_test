// Установить ответственного
function setResponsible(report) {


    var evaluation = $('#datatable tbody  tr.selected').attr('data-id');

    $.ajax({
        type: 'POST',
        dataType: "json",
        data: {
            "action": "setResponsible",
            "report": report,
            "responsible": $(event.currentTarget).val(),
        },
        url: "/assets/functions.php",
        cache: false,
        async: false,
        success: function (data) {
            console.log(data);
           // autoHideNotify('success', 'top right', data['message'], data['message2']);
            $.notify({'type':'success', 'message':data['message2']});
        },
        error: function (data) {
            var error = data.responseJSON;
            console.log(error);
           // autoHideNotify('error', 'top right', error['message'], error['message2']);
            $.notify({'type':'error', 'message':error['message2']});
            return false;
        }
    })

}

$("input[name='selectAll']").click(function (e) {

//alert ($(this).parent().parent().parent().parent().parent().$("tr"));
var idTable = $(this).closest('table').attr('name');
if ($(this).prop('checked') == true) {
    $("table[name="+idTable+"] tbody tr.text-center").addClass('selected info');
}else{
    $("table[name="+idTable+"] tbody tr.text-center").removeClass('selected info');
}

    checkRowAmount_new()
});

// Подробная информация об отчете
$('*[data-action="editReport"]').click(function () {
    if (checkRowAmount_new() != 1) {
        $.notify({'type':'error', 'message':'Можно выбрать только одну запись'});
    }
    ;

    var evaluationID = $('#datatable tbody tr.selected').attr('data-id');

    $('#viewEvaluation').show();

    fillForm(evaluationID);

})

// Редактирование матрицы отчета
$('*[data-action="editMatrix"]').click(function () {

    if (checkRowAmount_new() < 1) {
       // autoHideNotify('error', 'top right', 'Отчет', 'Необходимо выбрать запись');
        $.notify({'type':'error', 'message':'Необходимо выбрать одну запись!'});
        return false;
    }
    ;


    if (checkClosed() == 1) {
        // autoHideNotify('error', 'top right', 'Отчет закрыт', 'Нельзя редактировать закрытый отчет');
        $.notify({'type':'error', 'message':'Отчет закрыт. Нельзя редактировать закрытый отчет'});
        return false;
    }
    ;

    if (checkArchive() == true) {
       // autoHideNotify('error', 'top right', 'Отчет находиться в архиве', 'Выбранный вами отчет находиться в архиве. Чтобы редактировать данный отчет, необходимо разархивировать его');
        $.notify({'type':'error', 'message':'Выбранный отчет находится в архиве. Необходимо его разархивировать'});
        return false;
    }
    ;

    var reportsId = $('#datatable tbody tr.selected').attr('data-id');
    var platform = $('#datatable tbody tr.selected').attr('platform-id');
    var reportType = $('#datatable tbody tr.selected').attr('report-type');
    var error = '';
    var oldPlatform = 0;
    var oldType = '';

    var reportsId = [];

    $('#datatable tbody tr.selected').each(function (index, item) {

        reportsId.push($(this).attr('data-id'));

        if (index == 0) {
            oldPlatform = $(this).attr('platform-id');
            oldType = $(this).attr('report-type');
        }
        ;

        if (oldPlatform != $(this).attr('platform-id') ||
            oldType != $(this).attr('report-type')) {
            error = 'Нельзя объединить данные отчеты';
        }

        oldPlatform = $(this).attr('platform-id');
        oldType = $(this).attr('report-type');

    })
    reportsId = reportsId.join(",")
    if (error != '') {

        $.notify({'type':'error', 'message':'Объединять можно только отчеты с одинаковым типом, созданные на одной площадке'});
        return false;
    } else {
        window.open(
            '/reports/matrix?type=' + reportType + '&mode=0' + '&id=' + reportsId + '&platform=' + platform,
            '_blank'
        );
    }
})

// Закрыть отчет
function closeReport(bunch) {

    $.ajax({
        type: 'POST',
        dataType: "json",
        url: "/assets/functions.php",
        cache: false,
        data: {
            "action": "closeEvaluation",
            "bunch": bunch,
        },
        success: function (msg) {
            // console.log(msg);
            location.reload();
        },
        error: function (data) {
            var error = data.responseJSON;
           // autoHideNotify('error', 'top right', error['message'], error['message2']);
            $.notify({'type':'error', 'message':error['message2']});
            return false;
        },
    });

}

// Отправить отчет в архив
$('*[data-action="archiveReport"]').click(function () {

    if (checkRowAmount_new() < 1) {
      //  autoHideNotify('error', 'top right', 'Не выбрано ни одной записи', 'Необходимо выбрать хотя бы одну запись');
        $.notify({'type':'error', 'message':'Необходимо выбрать хотя бы одну запись!'});
        return false;
    }
    ;

    confirm('info', 'top right', 'Данные отчета(ов) будут отправлены в архив.<br/> Через 30 дней они будут удалены безвозвратно');

    $(document).on('click', '.notifyjs-metro-base .no', function () {
        $(this).trigger('notify-hide');
    });

    $(document).on('click', '.notifyjs-metro-base .yes', function () {

        $(this).trigger('notify-hide');

        var removeId = [];

        $('#datatable tbody  tr.selected').each(function (index) {
            removeId.push($(this).attr('data-id'));
        })

        console.log(removeId);
        // return;

        $.ajax({
            type: 'POST',
            data: {
                "action": "archiveReport",
                "ids": removeId,
            },
            url: "/assets/functions.php",
            cache: false,
            success: function (data) {
                // console.log(data);
                location.reload();
            }
        });

    });

})

// Удалить отчет
$('*[data-action="removeReport"]').click(function () {

    if (checkRowAmount_new() < 1) {
        $.notify({'type':'error', 'message':'Необходимо выбрать хотя бы одну запись!'});
        return false;
    }
    ;


    confirm('info', 'top right', 'Данные отчета(ов) удаляться безвозвратно');

    $(document).on('click', '.notifyjs-metro-base .no', function () {
        $(this).trigger('notify-hide');
    });

    $(document).on('click', '.notifyjs-metro-base .yes', function () {

        $(this).trigger('notify-hide');

        var removeId = [];

        $('#datatable tbody  tr.selected').each(function (index) {
            removeId.push($(this).attr('data-id'));
        })

        console.log(removeId);
        // return;

        $.ajax({
            type: 'POST',
            data: {
                "action": "removeEvaluation",
                "ids": removeId,
            },
            url: "/assets/tasks.php",
            cache: false,
            success: function (data) {
                // console.log(data);
                location.reload();
            }
        });

    });

})



// Скачать отчет в формате excel
$('*[data-action="downloadReport"]').click(function () {


    if (checkRowAmount_new() < 1) {
        $.notify({'type':'error', 'message':'Необходимо выбрать запись!'});
        return false;
    }
    ;

    console.log(checkClosed());

    if (checkClosed() == 1) {
        $.notify({'type':'error', 'message':'Нельзя редактировать закрытый отчет'});
        return false;
    }
    ;

    if (checkArchive() == true) {
        //autoHideNotify('error', 'top right', 'Отчет находится в архиве', 'Выбранный вами отчет находится в архиве. Чтобы редактировать данный отчет, необходимо разархивировать его');
        $.notify({'type':'error', 'message':'Выбранный вами отчет находится в архиве. Для редактирования необходимо его разархивировать'});
        return false;
    }
    ;

    var reportsId = $('#datatable tbody  tr.selected').attr('data-id');
    var platform = $('#datatable tbody  tr.selected').attr('platform-id');
    var reportType = $('#datatable tbody  tr.selected').attr('report-type');
    var error = '';
    var oldPlatform = 0;
    var oldType = '';

    var reportsId = [];

    $('#datatable tbody  tr.selected').each(function (index, item) {

        reportsId.push($(this).attr('data-id'));

        if (index == 0) {
            oldPlatform = $(this).attr('platform-id');
            oldType = $(this).attr('report-type');
        }
        ;

        if (oldPlatform != $(this).attr('platform-id') ||
            oldType != $(this).attr('report-type')) {
            error = 'Нельзя объединить данные отчеты';
        }

        oldPlatform = $(this).attr('platform-id');
        oldType = $(this).attr('report-type');

    })

    reportsId = reportsId.join(",")

    if (error != '') {

        $.notify({'type':'error', 'message':'Объединять можно только отчеты с одинаковым типом, созданные на одной площадке'});
        return false;

    } else {

        $.notify({'type':'success', 'message':'Скачивание Excel файла началось. Это может занять некоторое время...'});
        window.open(
            '/app/views/reports/templates/all/d_evaluation.php?id=' + reportsId + '&platform=' + platform + '&type=' + reportType,
            '_self'
        );
    }
})


// Загрузить приложение к чек-листу
// $('*[data-action="loadAttachment"]').click(function (event) {
//     $('form[name="attachment"][data-id="' + $(event.target).attr('data-id') + '"]').find('input[name="attachment-file"]').trigger('click');
// })
// $('input[name="attachment-file"]').change(function () {
//     $(this).parents('form').submit();
// })
// $("form[name='attachment']").submit(function (e) {
//
//     e.preventDefault();
//
//     var formData = new FormData($(this)[0]);
//
//     $.ajax({
//         url: '/app/views/reports/load_attachment.php',
//         type: "POST",
//         data: formData,
//         async: false,
//         cache: false,
//         contentType: false,
//         processData: false,
//         success: function (data) {
//
//         },
//         error: function (data) {
//             console.log('Ошибка!');
//         },
//
//
//     }).done(function () {
//         location.reload();
//     });
//
//
// });

// Удаление приложение
// function removeAttachment(type, id) {
//
//     $.ajax({
//         type: 'POST',
//         dataType: "json",
        // data: {
        //     "action": "removeAttachment",
        //     "id": id,
        //     "type": type,
        // },
        // url: "/assets/functions.php",
        // cache: false,
        // async: false,
        // success: function (data) {
        //     console.log(data);
        // },
        // error: function () {
        //     console.log('Данные не получены');
        // }
    // }).done(function () {
    //     location.reload();
    // });
//
// }

// Загрузить отчет в формате excel
$('*[data-action="loadReport"]').click(function () {
    $('input[name="userfile"]').trigger('click');
});

$('input[name="userfile"]').change(function () {
    
    $("form[name='uploader']").submit();

});

$("form[name='uploader']").submit(function (e) {
    
    var formData = new FormData($(this)[0]);

    $.notify({'type':'info', 'message':'Пожалуйста, подождите. Идет отправка файла на сервер...'});

    $.ajax({
        url: '/app/views/reports/templates/all/l_evaluation.php',
        type: "POST",
        data: formData,
        async: true,
        cache: false,
        success: function (data) {
            var message = JSON.parse(data);
            
               // autoHideNotify(message['type'], 'top right', message['header'], message['text']);
                $.notify({'type':message['type'], 'message':message['text']});
                console.log(message['type']);

        },
        error: function (data) {
            console.log('Ошибка!');
        },

        contentType: false,
        processData: false
    });

    e.preventDefault();

});

function editEvaluation() {

    $.ajax({
        type: 'POST',
        url: "/assets/functions.php",
        cache: false,
        data: {
            "action": "editEvaluation",
            "id": $('input[name="report-id"]').val(),
            "comment": $('textarea[name="report-comment"]').val()
        },
        success: function (msg) {
            location.reload();
        }
    });

}

function fillForm(id) {

    $('input[name="report-id"]').val(id);
    // $('#report-job').html( $('[data-id="' + id + '"] .job').val() );

    var ctx = document.getElementById("report-chart").getContext('2d');

    // Массив значений статусов
    var statusArr = $('[data-id="' + id + '"] .status').val().split(",");

    statusArr.shift();

    var maxValue = 0;

    for (var i = 0; i < statusArr.length; i++) {
        maxValue += parseInt(statusArr[i]);
    }

    var myChart = new Chart(ctx, {
        type: 'horizontalBar',
        data: {
            labels: ["Не заполнено", "Более 90%", "От 71% до 90%", "От 41% до 70%", "Менее 40%"],
            datasets: [{
                /*label: '# of Votes',*/
                data: statusArr,
                backgroundColor: [
                    'rgba(20, 8, 45, 1)', // черный
                    'rgba(28, 168, 221, 1)', // синий
                    'rgba(51, 184, 108, 1)', // зеленый
                    'rgba(235, 193, 66, 1)', // желтый
                    'rgba(203, 42, 42, 1)' // красный
                ]
            }]
        },
        options: {
            legend: {display: false},
            responsive: false,
            scales: {
                xAxes: [{
                    ticks: {
                        beginAtZero: true,
                        max: maxValue,
                        stepValue: 1
                    }
                }]
            }
        }
    });

}

function setSingleStatus(event) {

    confirm('info', 'top right', 'Все статусы отчета будут изменены безвозвратно');

    $(document).on('click', '.notifyjs-metro-base .no', function () {
        $(this).trigger('notify-hide');
    });

    $(document).on('click', '.notifyjs-metro-base .yes', function () {

        $(this).trigger('notify-hide');

        var evaluation = $('#datatable tbody  tr.selected').attr('data-id');

        $.ajax({
            type: 'POST',
            data: {
                "action": "setSingleStatus",
                "evaluation": evaluation,
                "status": event.currentTarget.value
            },
            url: "/assets/functions.php",
            cache: false,
            success: function (data) {
                console.log(data);
            }
        }).done(function () {
            location.reload();
        });

    });

}
$('.collapse').each(function (element, value) {
    //  var
    $('#' + value.id).on('hidden.bs.collapse', function (e, selector) {
        $('#' + this.id).find("#datatable tr.selected").each(function () {
            $(this).toggleClass('selected info');
        });
    });
});

function checkRowAmount_new() {
        var table = $("#datatable tr.selected")
        if (table.length > 0) {
            $('.panel-heading .fa-edit').parent().addClass('btn-info');
            $('.panel-heading .fa-bar-chart-o').parent().removeClass('btn-primary');
            $('.panel-heading .fa-file-excel-o').parent().addClass('btn-success');
        }
        if (table.length == 1) {
            $('.panel-heading .fa-edit').parent().addClass('btn-info');
            $('.panel-heading .fa-bar-chart-o').parent().addClass('btn-primary');
            
        }
        if (table.length <= 0) {
            $('.panel-heading .fa-bar-chart-o').parent().removeClass('btn-primary');
            $('.panel-heading .fa-edit').parent().removeClass('btn-info');
            $('.panel-heading .fa-file-excel-o').parent().removeClass('btn-success');
        }
        ;
        return table.length;
    };

$('#datatable tbody').on('click', 'tr', function () {
        // var inputcheck = $(this).children().first().find('input[type=checkbox]');
        // if (inputcheck.prop("checked") == false) {
        //     //inputcheck.trigger("click");
        //     inputcheck.prop("checked", true);
        // } else {
        //     //inputcheck.trigger("click");
        //     inputcheck.prop("checked", false);
        // }
        $(this).toggleClass('selected info');
        checkRowAmount_new();
    });

// $('#datatable :checkbox').on('click', function (event) {
//         event.preventDefault();
//         event.stopPropagation();
//     });

// if ($('#selectAll').hasClass('allChecked')) {
//     var inputcheck = $('#datatable tbody').children().first().find('input[type=checkbox]');
//      $('input[type="checkbox"]').prop('checked', 'checked')
//    } else {
//      $('input[type="checkbox"]').prop('checked', 'false')
//    }


/*var el1 = $('#allfiles');
$(".file").each(function (element, value) {
    filearray($('#' + value.id), $(this).attr("data-bunch"));
});*/
