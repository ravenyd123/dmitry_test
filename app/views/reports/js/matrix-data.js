$(document).ready(function () {
    report = $('.report').attr('id');

    var action = $('textarea[name="actions"]');
    var recommendations = $('textarea[name="recommendations"]');
    var failure = $('textarea[name="reason-failure"]');
    var fio = $('select[name="fio"]');
    var dataplan = $('input[name="date-plan"]');

    // Отображение панели редактирования
    $('.report tbody tr').hover(
        function () {
            $(this).find('.matrix-edit').show();
        }, function () {
            $(this).find('.matrix-edit').hide();
        }
    );


    // Окраска статусов при загрузке страницы
    $('[data-status]').each(function () {
        switch (parseInt($(this).attr("data-status"))) {
            case 1:
                $(this).css("background", "#1ca8dd");
                break;
            case 2:
                $(this).css("background", "#00FF33");
                break;
            case 3:
                $(this).css("background", "#FFFF33");
                break;
            case 4:
                $(this).css("background", "#FF3300");
                break;
        }
    });


    $('select[name=status]').on('change', function (e) {

        if (report != 'calendar') {
            if (e.val != 0 && e.val != 5 && e.val != 1) {
                action.closest('div').removeClass('hidden');
                fio.closest('div.col-md-6').removeClass('hidden');
                dataplan.closest('div.col-md-6').removeClass('hidden');
            } else {
                action.closest('div').addClass('hidden');
            }

        } else {

            if (e.val != 0 && e.val != 5 && e.val != 1) {
                action.closest('div').removeClass('hidden');
                fio.closest('div.col-md-6').removeClass('hidden');
                dataplan.closest('div.col-md-6').removeClass('hidden');
                failure.closest('div').removeClass('hidden');
            } else {
                action.closest('div').addClass('hidden');
                failure.closest('div').addClass('hidden');
            }
        }

    })

    // Кнопка редактирования записи
    $('*[data-action="edit-data"]').click(function (e) {
        var id = $(this).parents('tr').attr('id');
        var node = getNodeByField('collector', 'id', id);
        // Номер строки
        $('#row').text(id);

        // Текст требования
        $('#demand').text(node['data'][0]['demand'].replace(/<(?:.|\n)*?>/gm, ''));

        // Плановый срок
        if ((node['data'][0]['date-plan'] != null) && (node['data'][0]['date-plan'] != 0)) {
            var date = new Date(node['data'][0]['date-plan'] * 1000);
            $('input[name="date-plan"]').datepicker('setDate', date);
        } else {
            $('input[name="date-plan"]').datepicker('setDate', null);
        }

        // ФИО ответственного за выполнение
        $('select[name="fio"]').select2('val', node['data'][0]['fio']);

        // Согласованность
        if (node['data'][0]['agreed'] != '') {
            $('select[name="agreed"]').val(1);
        } else {
            $('select[name="agreed"]').val('');
        }

        if (report != 'calendar') {
            if (node['data'][0]['status-compliance'] == null) {
                var status = 0;
            } else {
                status = parseInt(node['data'][0]['status-compliance']);
            }
            $('select[name="status"]').select2('val', status);

            if (status == '0' || status == '5' || status == '1') {
                action.closest('div.col-md-6').addClass('hidden');
            } else {
                action.closest('div.col-md-6').removeClass('hidden');
            }
            // $('select[name="status"]').val(node['data'][0]['status-compliance']);
            action.val(node['data'][0]['actions']);

        }
        ;
        if (report == 'calendar') {

            if (node['data'][0]['current-status-compliance'] == null) {
                status = 0;
            } else {
                status = parseInt(node['data'][0]['current-status-compliance']);
            }
            $('select[name="status"]').select2('val', status);

            if (status == '0' || status == '5' || status == '1') {
                action.closest('div.col-md-6').addClass('hidden');
                failure.closest('div').addClass('hidden');
            } else {
                failure.closest('div').removeClass('hidden');
                action.closest('div.col-md-6').removeClass('hidden');
            }


            failure.val(node['data'][0]['reason-failure']);
            recommendations.val(node['data'][0]['recommendations']);

            // Фактический срок
            if ((node['data'][0]['fact-date'] != 0) && (node['data'][0]['fact-date'] != null))  {
                var date = new Date(node['data'][0]['fact-date'] * 1000);
                $('input[name="fact-date"]').datepicker();
                $('input[name="fact-date"]').datepicker('setDate', date);
            } else {
                $('input[name="fact-date"]').val('')
            }

        }
        ;

        // файлы
        var upload1 = JSON.stringify({
            "id": node.data[0].id,
            "self": node.data[0]['self-evaluation'],
            "platform": node.data[0].platform
        });
        $.post('/app/views/tasks/upload.php', {all_data: node.data[0].id}).done(function (data) {
            var all_data = JSON.parse(data)
            var el1 = $('input#input-ru').fileinput({
                language: "ru",
                theme: 'gly',
                // maxFilePreviewSize:10000,
                uploadUrl: "/app/views/tasks/upload.php",
                uploadAsync: true,
                showCaption: true,
                showUpload: false, // hide upload button
                showRemove: false, // hide remove button
                showBrowse: true,
                showClose: false,
                hideThumbnailContent: false,//предварительный вид контенкта
                initialPreviewAsData: false,
                initialPreview: all_data.initialPreview,// просмотр файла
                initialPreviewConfig: all_data.initialPreviewConfig,// первоначальная конфигурация предварительного просмотра, если вы хотите, чтобы начальный предварительный просмотр отображался с данными загрузки сервера
                uploadExtraData: {upload: upload1},
                //maxFileSize: 100,
                dropZoneEnabled: false,//показ drop zona
                browseOnZoneClick: true,
                initialPreviewShowDelete: false,
                initialPreviewDownloadUrl: 'upload?downloads={key}',
                overwriteInitial: false,// добавить файлов к предварительному просмотру
                layoutTemplates: {
                    main1: '{preview}\n' +
                    '<div class="input-group {class}">\n' +
                    '  {caption}\n' +
                    '  <div class="input-group-btn">\n' +
                    '    {browse}\n' +
                    '  </div>\n' +
                    '</div>',
                },
                previewSettings: {
                    image: {width: "50px", height: "50px", 'max-width': "100%", 'max-height': "100%"},
                    html: {width: "100%", height: "70px"},
                    text: {width: "100%", height: "70px"},
                    office: {width: "100%", height: "70px"},
                    gdocs: {width: "100%", height: "70px"},
                    video: {width: "100%", height: "auto"},
                    audio: {width: "100%", height: "30px"},
                    flash: {width: "100%", height: "auto"},
                    object: {width: "100%", height: "auto"},
                    pdf: {width: "100%", height: "70px"},
                    other: {width: "100%", height: "70px"}

                }
            }).on("filebatchselected", function (event, files) {
                el1.fileinput("upload");
            }).on("filebeforedelete", function (event, id, index) {

                return new Promise(function (resolve, reject) {
                    bootbox.confirm({
                        title: "Подтверждение",
                        message: "Вы уверены что хотите удалить",
                        buttons: {
                            cancel: {
                                label: 'Отмена'
                            },
                            confirm: {
                                label: 'Подтвердить'
                            }
                        },
                        callback: function (result) {
                            if (result) {
                                resolve();
                            }
                        }
                    })
                });
            });
        })

        $('#selfModal').modal('show');
    })

})
