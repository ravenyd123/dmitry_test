$(document).ready(function () {
    var $_GET = new URLSearchParams(window.location.search);
    // Скачать отчет в формате excel (календарь)
    $('*[data-action="downloadCalendar"]').click(function () {
        var str = '/app/views/reports/templates/all/d_callendar.php?';
        if ($_GET.has('type')) {
            str += '&type=' + $_GET.get('type');
        }
        if ($_GET.has('bunch')) {
            str += '&bunch=' + $_GET.get('bunch');
        }
        if ($_GET.has('user')) {
            str += '&user=' + $_GET.get('user');
        }
        if ($_GET.has('mode')) {
            str += '&mode=' + $_GET.get('mode');
        }
        
        $.notify({'type':'success', 'message':'Скачивание Excel файла началось. Это может занять некоторое время...'});
        //autoHideNotify('success', 'top right', 'Скачивание файла', 'Скачивание Excel файла началось. Это может занять некоторое время...');
        window.open(str, '_self');
    });

// Самооценка, аудит и гос (скачать xls-файл)
    $('*[data-action="downloadxlsx"]').click(function () {
        var str = '/app/views/reports/templates/all/d_evaluation.php?';
        if ($_GET.has('type')) {
            str += '&type=' + $_GET.get('type');
        }
        if ($_GET.has('task_one')) {
            str += '&task_one=' + $_GET.get('task_one');
        }
        if ($_GET.has('platform')) {
            str += '&platform=' + $_GET.get('platform');
        }
        if ($_GET.has('id')) {
            str += '&id=' + $_GET.get('id');
        }
        if ($_GET.has('task_all')) {
            str += '&task_all=' + $_GET.get('task_all');
        }
        window.open(str, '_self');
        $.notify({'type':'success', 'message':'Скачивание Excel файла началось. Это может занять некоторое время...'});
       // autoHideNotify('success', 'top right', 'Скачивание файла', 'Скачивание Excel файла началось. Это может занять некоторое время...');
        window.open(str, '_self');
    });

// Скачать анотацию
    $('*[data-action="devan"]').click(function () {
        var str = '/app/views/reports/templates/gazprom/d_akt_wrd_gazprom.php?';
        var msg = 'Скачивание аннотации началось. Это может занять некоторое время...';
        cheklist_document(str,msg);
    });

    
// Скачать сск
    $('*[data-action="downloadSskPlan"]').click(function () {
        var str = '/app/views/reports/templates/knpz/d_plan_wrd_knpz.php?bunch='+ $_GET.get('bunch');
        var msg = 'Скачивание акта началось. Это может занять некоторое время...';
        cheklist_document(str,msg);
    });


// Скачать акт
    $('*[data-action="dakt"]').click(function () {
        var str = '/app/views/reports/templates/knpz/d_akt_wrd_knpz.php?';
        var msg = 'Скачивание акта началось. Это может занять некоторое время...';
        cheklist_document(str,msg);
    });


// Скачать План КиПД
    $('*[data-action="dkipd"]').click(function () {
        var str = '/app/views/reports/templates/gazprom/d_plan_wrd_gazprom.php?';
        var msg = 'Скачивание КиПД началось. Это может занять некоторое время...';
        calendar_document(str,msg);
    });

// Скачать Предписание
    $('*[data-action="dpred"]').click(function () {
        var str = '/app/views/reports/templates/knpz/d_plan_wrd_knpz.php?';
        var msg = 'Скачивание предписания началось. Это может занять некоторое время...';
        calendar_document(str,msg);
    });

// Отчеты для календарей
    function calendar_document (str, msg) {
        if ($_GET.has('platform')) {
            str += '&platform=' + $_GET.get('platform');
        }
        if ($_GET.has('bunch')) {
            str += '&bunch=' + $_GET.get('bunch');
        }
        $.notify({'type':'success', 'message':msg});
       // autoHideNotify('success', 'top right', 'Скачивание файла', msg);
        window.open(str, '_self');
    };

// Отчеты для чек-листы (не календарь)
    function cheklist_document (str, msg) {
        if ($_GET.has('type')) {
            str += '&type=' + $_GET.get('type');
        }
        if ($_GET.has('platform')) {
            str += '&platform=' + $_GET.get('platform');
        }
        if ($_GET.has('id')) {
            str += '&id=' + $_GET.get('id');
        }
	    if ($_GET.has('task_one')) {
            str += '&task_one=' + $_GET.get('task_one');
        }
        if ($_GET.has('task_all')) {
            str += '&task_all=' + $_GET.get('task_all');
        }
        $.notify({'type':'success', 'message':msg});
      //  autoHideNotify('success', 'top right', 'Скачивание файла', msg);
        window.open(str, '_self');
    };
});
