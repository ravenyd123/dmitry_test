<? include_once $_SERVER['DOCUMENT_ROOT'] . '/assets/functions.php'; ?>

<?
$db = Db::Connection();
$source = Sourse_conn;
# Если идет обновление требования
if (isset($_GET['demand'])) {

    $demandId = $_GET['demand'];

    $query = $db->query("SELECT * FROM `collector` WHERE `id` ={$demandId}");

    $query = $db->query("SELECT  c.*,
                                  r.bunch, r.type, r.typen,reg.symbol, r.creator, r.responsible, r.status
                                  FROM `collector` c
                                  LEFT JOIN `reports` r ON c.`self-evaluation` = r.id
                                  LEFT JOIN register reg ON c.document=reg.nd 
                                  WHERE
                                  c.`id` ={$demandId}");

    $type = $_GET['type'];

}

$today = new DateTime();
while ($row = $query->fetch_assoc()) {
    $qNM = "SELECT `status` FROM `nearmiss` WHERE `id_self` ={$row['self-evaluation']} LIMIT 1";
    $result = $db->query($qNM)->fetch_assoc();


    $rowClass = '';

# Если в календаре плановый срок просрочен
    if (isset($row['date-plan'])) {
        if ($row['date-plan'] < $today->getTimestamp() and $row['date-plan'] != 0) {
            $rowClass = "overdue";
        }
    }

# Если в календаре текущий статус соответствия стал 90% (СКОРЕЕ ВСЕГО ВЫ ЭТО УБЕРЕТЕ ЧЕРЕЗ НЕДЕЛЮ)
    if (isset($row['current-status-compliance']) && $row['current-status-compliance'] == 1) {
        $rowClass .= " done";
    }

# Если в календаре текущий статус соответствия стал 90%
    /*if ( intval($row['current-status-compliance']) == 1 && $type == 'calendar' )
    {
        continue;
    }*/

# Если требование не удовлетворяет настройкам критичности не выводим его
    if (!isset($_GET['user']) && !isset($_GET['demand']) && $type != 'calendar') {

        if ($row['criticality'] != 4) {

            $active = 0;

            if ($row['criticality'] == 3) {
                if (intval($row['criticality1']) == 3 ||
                    intval($row['criticality2']) == 3 ||
                    intval($row['criticality2']) == 3
                ) {
                    $active = 1;
                }
            }

            if ($row['criticality'] == 2) {
                if (intval($row['criticality1']) == 3 ||
                    intval($row['criticality2']) == 3 ||
                    intval($row['criticality2']) == 3 ||
                    intval($row['criticality1']) == 2 ||
                    intval($row['criticality2']) == 2 ||
                    intval($row['criticality2']) == 2
                ) {
                    $active = 1;
                }
            }

            if ($row['criticality'] == 1) {
                if (intval($row['criticality1']) < 3 &&
                    intval($row['criticality2']) < 3 &&
                    intval($row['criticality2']) < 3
                ) {
                    $active = 1;
                }
            }

            if ($active == 0) {
                continue;
            }
        }

    }
    ?>

    <tr class="<?= $rowClass; ?>" id="<?= $row['id']; ?>" data-fio="<?= $row['fio']; ?>">
        <? if ($type == 'calendar') { ?>
            <td class='ehs text-center'>
                <?
                // if ($_GET['mode'] <> '0') {
                
                if (in_array("U", $access['matrix']) AND ($result['status']!="Выполнен")) { ?>
                    <div class="matrix-edit">
                        <i class="fa fa-pencil" data-toggle="tooltip" data-placement="right" data-action="edit-data"
                           title="Редактировать запись"></i>
                    </div>
                <? }
                // }
                ?>
                <? if ($row['typechek']==2) { 
                    ?> Near Miss <?
                } else { ?>
                    <?= type_tasks[$row['type']]; ?>
                №<?= getFieldByField('reports', 'id', $row['self-evaluation'], 'bunch'); }?>
                от <?= date("d.m.Y", getFieldByField('reports', 'id', $row['self-evaluation'], 'date')) ?>
                <?= platforms_user[$row['platform']] ?>

                <!--            --><? //= getFieldByField($link, 'ehs', 'id', $row['ehs'], 'ehs_numbers'); ?>
            </td> <? } ?>
        <!--        подвид-->
        <!--        <td class='job text-center'>-->
        <!--            --><? //
        //             if ($type != 'calendar') {
        //                if (in_array("U", $access['matrix'])) { ?>
        <!--                    <div class="matrix-edit">-->
        <!--                        <i class="fa fa-pencil" data-toggle="tooltip" data-placement="right" data-action="edit-data"-->
        <!--                           title="Редактировать запись"></i>-->
        <!--                    </div>-->
        <!--                --><? // }
        //            } ?>
        <!--            --><? //= GetAllvid[$row['job']]; ?>
        <!--        </td>-->
        <td class='job text-center'>
            <?
            if ($type != 'calendar') {
                if (in_array("U", $access['matrix'])) { ?>
                    <div class="matrix-edit">
                        <i class="fa fa-pencil" data-toggle="tooltip" data-placement="right" data-action="edit-data"
                           title="Редактировать запись"></i>
                    </div>
                <? }
            } ?>
            <?= GetAllvid[$row['job']]; ?>
        </td>
        <td class='job text-center'>
            <?
            if ($type != 'calendar') {
                if (in_array("U", $access['matrix'])) { ?>
                    <div class="matrix-edit">
                        <i class="fa fa-pencil" data-toggle="tooltip" data-placement="right" data-action="edit-data"
                           title="Редактировать запись"></i>
                    </div>
                <? }
            } ?>
            <?= GetAllvid[$row['job_child']]; ?>
        </td>
        <td class='w350 demand'><div style="height:80px; overflow:auto;"><?= $row['demand']; ?><p style="color: #0a6aa1"><?= $row['pl_name'] ?></p>
            <? if (isset($row['paragraph-link'])) { ?>
            <p>Пункт: <a href='<?= $source ?><?= $row['paragraph-link']; ?>'
                         target="_blank"><?= $row['paragraph-name']; ?></a></p>
            <? } ?>
                     </div>
        </td>
        <? if ($row['typen']!=7){ ?>
        <td class='event'><?= $row['event']; ?></td>
    <? } ?>
        <? if ($type != 'calendar') { ?>
            <td class='w160 status'>

                <div data-status="<?= $row['status-compliance']; ?>">
                    <?php
                    switch (intval($row['status-compliance'])) {
                        case 0:
                            ?><?php
                            break;
                        case 5:
                            ?>N/A<?php
                            break;
                        case 1:
                            ?>более 90%<?php
                            break;
                        case 2:
                            ?>от 71% до 90%<?php
                            break;
                        case 3:
                            ?>от 41% до 70%<?php
                            break;
                        case 4:
                            ?>менее 40%<?php
                            break;
                    }
                    ?>
                </div>

            </td>
        <? } ?>
        <? if ($type == 'calendar') { ?>
            <td class='text-center'>
                <div data-status="<?= $row['status-compliance']; ?>">
                    <?php
                    switch (intval($row['status-compliance'])) {
                        case 1:
                            ?>более 90%<?php
                            break;
                        case 2:
                            ?>от 71% до 90%<?php
                            break;
                        case 3:
                            ?>от 41% до 70%<?php
                            break;
                        case 4:
                            ?>менее 40%<?php
                            break;
                    }
                    ?>
                </div>
            </td> <!--/*Статус соответствия \ Status of compliance*/-->
        <? } ?>

        <? if ($type == 'calendar' || $type == 'self-evaluation') { ?>
            <td class='fio text-center'><?= getUserName($row['fio']); ?></td>
            <td class='date-plan text-center'><?= (empty($row['date-plan']) ? "" : date("d.m.Y", $row['date-plan'])); ?> </td>
        <? } ?>

        <? if ($type != 'calendar') { ?>
            <td class='actions'><?= $row['actions']; ?></td>
            <!--            <td class='paragraph-name text-center'>-->
            <!--                <a href='--><? //= $source ?><!----><? //= $row['paragraph-link']; ?><!--'-->
            <!--                   target="_blank">--><? //= $row['paragraph-name']; ?><!--</a>-->
            <!--            </td>-->
            <!--            <td class='source text-center'>--><? //= Sourse_app//$row['source']; ?><!--</td>-->
            <td class='document'><div style="height:80px; overflow:auto"><?= $row['symbol']; ?><!--</div>--></td>
            <td class='Number_and_date'></td>
            <td class='actual_matrix'></td>
        <? } ?>

        <? if ($type == 'calendar') { ?>
            <td class='actions calendar-devider'>
                <div><?= $row['actions']; ?></div>
            </td>
            <td><?= $row['recommendations']; ?></td>
            <!--            <td>--><? //= getReportType($row['type']); ?>
            <!--                №--><? //= getFieldByField($link, 'reports', 'id', $row['self-evaluation'], 'bunch'); ?>
            <!--                от --><? //= date("d.m.Y", getFieldByField($link, 'reports', 'id', $row['self-evaluation'], 'date')) ?>
            <!--                --><? //= getFieldByField($link, 'platform', 'id', $row['platform'], 'name') ?><!-- </td>-->
            <td class='fact-date text-center'>
                <?= (is_null($row['fact-date']) || $row['fact-date'] == 0 ? "" : date("d.m.Y", $row['fact-date'])); ?>
            </td>
            <td class='w160 status'>

                <div data-status="<?= $row['current-status-compliance']; ?>">
                    <?php
                    switch (intval($row['current-status-compliance'])) {
                        case 1:
                            ?>более 90%<?php
                            break;
                        case 2:
                            ?>от 71% до 90%<?php
                            break;
                        case 3:
                            ?>от 41% до 70%<?php
                            break;
                        case 4:
                            ?>менее 40%<?php
                            break;
                    }
                    ?>
                </div>

            </td>
            <? if ($row['typen']!=7){ ?>
            <td class='no_actions'><?= $row['reason-failure']; ?></td>
        <? } ?>
            <td class='agreed text-center'>


                <? if ($row['agreed'] != '') { ?>
                    <button type="button" class="btn btn-success m-b-5 w-100">Утверждено</button>
                    Утвердил: <br> <?= getUserName($row['agreed']); ?>
                <? } else { ?>
                    <button type="button" class="btn btn-default m-b-5 w-100">Не Утверждено</button>
                <? } ?>

            </td>
        <? } ?>

    </tr>
<? } ?>
<script src="/app/views/reports/js/matrix-data.js"></script>
