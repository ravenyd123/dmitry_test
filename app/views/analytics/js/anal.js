$('document').ready(function () {
    // Раскрашивание процентных полос в общей таблице
    function percent_span(percent) {
        var span_class = 'bad';
        if (percent > 40) {
            span_class = 'good';
        }
        if (percent > 70) {
            span_class = 'excellent';
        }
        if (percent > 90) {
            span_class = 'perfect';
        }
        return '<span style="width: ' + percent + '%" class="percent_class ' + span_class + '">' + percent + '%</span>';
    }
        ///var datareport = <?=$reports ?>;
    if (datareport.length > 0) {

        $.each(datareport, function (key, alldata) {
            $('#all_table tbody').append('' +
                '<tr class="header_tr" data-key="' + key + '" data-platform="' + alldata.id_platform + '" data-bunch="' + alldata.bunch + '">' +
                '<td>' + alldata.platform + '</td>' +
                '</tr>');
            $.each(alldata.id_years, function (year, value) {
                // console.log(value.percent_audit);
                $('#all_table tbody').append('' +
                    '<tr data-key="' + key + '" data-platform="' + alldata.id_platform + '">' +
                    '<td>' + year + '</td>' +
                    '<td class="audit_analit">' + percent_span(value.percentage_audit) + '</td>' +
                    '<td  class="self_analit">' + percent_span(value.percentage_self) + '</td>' +
                    /* '<td>' + percent_span(data[i].id_year[j].gosorg) + '</td>' +*/
                    '</tr>');
            });
        });

        $("#all_table").append('<thead><tr>' +
        '<th>Площадки</th>' +
        '<th class="audit_analit">Результаты аудит</th>' +
        '<th class="self_analit">Результаты самооценки</th>' +
        '</tr></thead>');

        $('#list').show();
        $('#all_table span').show(300, function () {
        $('#all_table span').css({display: 'block'});

        /* var td_width = $('#all_table > tbody > tr > td:eq(1)').width();
         $('#all_delimiter').css({ width: td_width + "px" });*/
    });


        $("#all_table tr").click(function () {
        var d = parseInt(($(this).attr("data-key")));
        $('#chartWorksModal').modal('show');
        $("#detail_title").empty().append(datareport[d].platform);
        $("#detail_table").empty().append('<tbody></tbody>');


        $.each(datareport[d]["id_years"], function (key, value) {
        $.each(value["job"], function (id, job) {
        $("#detail_table tbody").append('' +
        '<tr data-platform="'+datareport[d].id_platform+'" data-bunch="' + datareport[d].bunch + '" data-job="' + id + '">' +
        '<td >' + job["rus"] + '</td>' +
        '<td class="audit_analit">' + percent_span(job["auditproc"]) + '</td>' +
        '<td class="self_analit">' + percent_span(job["selfproc"]) + '</td>' +
        '</tr>');
    });
        $("#detail_table").append('<thead><tr><th>Вид работ</th>' +
        '<th class="audit_analit">Результаты аудита</th>' +
        '<th class="self_analit">Результаты самооценки</th></tr></thead>');
        $('#detail_table span').show(300, function () {
        $('#detail_table span').css({display: 'block'});
    });

    })
        $.each(datareport[d]["id_years"],function (year,val) {
        if ($("#years").html()==''){
        $("#years").append('<a id="all_report" href="#">' +
        '<span>Полный отчет за<i style="background: red"></i> ' + year + '</span></a>');
        $("#pdf").append('<a href=# class="right button">Сохранить в PDF</a>');
    }

    })


        if ($('#inp_audit').prop("checked")==false){
        $('.audit_analit').css('display','none');
    } else{
        $('.audit_analit').removeAttr('style');
    }
        if ($('#inp_self').prop("checked")==false){
        $('.self_analit').css('display','none');
    } else{
        $('.self_analit').removeAttr('style');
    }
    });
    }


    });
    $('#pdf').click(function () {
    domtoimage.toJpeg(document.querySelector("#chartWorksModal .modal-content"), { quality: 0.95 })
        .then(function (dataUrl) {
            var doc = jsPDF('p','mm',[600,1014]);
            doc.addImage(dataUrl,'jpeg',0,0,600,1014);
            doc.save('Аналитика.pdf');
        });
});



