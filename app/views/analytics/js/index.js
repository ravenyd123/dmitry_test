$(document).ready(function () {

     $("input[name='min_date']").datepicker({
         setStartDate:''
     }
         // 'setStartDate',''
     );

    $('#an_form').on('submit', function (e) {
        $('.graf_all').removeClass('hidden');
        var platform = $('select[name="an_platform"]')

        e.preventDefault();

        var data = getFormData($(this));

        data.an_platform = platform.select2('val').join(',');

        data.action = "form";
        data.min_date = data.min_date != '' ? revers_date(data.min_date.split('.')) : '';
        data.max_date = data.max_date != '' ? revers_date(data.max_date.split('.')) : '';

        $.ajax({
            type: 'POST',
            data: data,
            dataType: "json",
            url: "/app/views/analytics/ajax.php",
            cache: false,
            complete: function (data_all) {
                data_all = data_all.responseJSON
              create_table(data.an_platform,data_all.data_job.job);
                var ctx = create_Chart()
                var chartOptions = {
                    showTooltips: false,
                    responsive: true,
                    tooltips: {
                        callbacks: {
                            title: function (tooltipItem, data) {
                                title = data_all.data_job.job[tooltipItem[0].xLabel];

                                return title;
                            }
                        }
                    },
                    legend: {
                        display: true,
                        // position: "left",

                    },
                    onClick: function (e, legendIndex) {



                        // this.chart.ctx.fillText("percentagegrgdfgdfgdfgdfg" , 50, 100);
                        // ctx.fillText('text', 0, 0)
                        var activePoints = techart.getElementAtEvent(e);
                        if (typeof activePoints !== 'undefined' && activePoints.length > 0) {
                            console.log(activePoints[0]._datasetIndex);//номер линий
                            // console.log(activePoints[0]._index);
                            // console.log(activePoints[0]._chart.config.data.labels[activePoints[0]._index]);
                            var gragN = activePoints[0]._datasetIndex;
                            var job = activePoints[0]._chart.config.data.labels[activePoints[0]._index];//видработ
                            $('.select2').select2("val", job).trigger('change');
                            // console.log('/analytics/report?action=report&job=' + job + '&gragN=' + gragN);
                            // window.open('/analytics/report?action=report&job='+job+'&bunch='+data.an_bunch,'_blank');
                        }
                    },
                    scales: {
                        xAxes: [{
                            // stacked: true, сложение столбцов а один ряд.
                            barPercentage: 1,
                            categoryPercentage: 0.8,
                            ticks: {
                                beginAtZero: true
                            }
                        }],
                        yAxes: [
                            {
                                display: true,
                                id: "y-axis-gravity",
                                scaleLabel: {
                                    display: true,
                                    labelString: "Количество нарушение",
                                    fontColor: "green"
                                },
                                ticks: {
                                    beginAtZero: true
                                }
                            }
                        ]
                    }
                };
                chartOptions.title = data_all.options.title;

                var data_c = {
                    type: data.an_graf,
                    data: data_all.data,
                    options: chartOptions
                }

                if (typeof techart !== "undefined") {
                    ctx = create_Chart()
                }

                techart = new Chart(ctx, data_c);
                // console.log(techart);
                // techart.chart.ctx.fillText("percentagegrgdfgdfgdfgdfg", 50, 100);
                //  console.log(techart.generateLegend());
                // $('#chart-legends').append(techart.generateLegend());
                // document.getElementById('chart-legends').innerHTML = techart.generateLegend()
                $('.job').empty();
                var i = 0
                $.each(data_all.data_job.job, function (el, index, array) {
                    if (i % 4 == 0) {
                        $('.job').append('<br>')
                    }
                    $('.job').append(index + ', ');
                    i++
                })

            },
            fail: function (msg) {
                console.log("fail".msg);
            }
        })
    });
    $("#detail_table").on("click", "tr", function () {
        window.open(
            '/app/views/analytics/matrix_old.php?job=' + $(this).attr("data-job") + '&bunch=' + $(this).attr("data-bunch") + '&platform=' + $(this).attr("data-platform"),
            '_blank'
        );
    });
    $("#years").on("click", "a", function () {
        window.open(
            '/app/views/analytics/matrix_old.php?&platform=' + $('#detail_table tr').eq(0).attr("data-platform") + '&bunch=' + $('#detail_table tr').eq(0).attr("data-bunch"),
            '_blank'
        );
    });
})
;

function create_Chart() {
    $('.cht').empty().append(' <canvas id="myChart" style="position: relative; height:60vh; width:80vw"></canvas>')
    return document.getElementById("myChart").getContext('2d')

}
function downloadPDF2() {
    var newCanvas = document.querySelector('#myChart');

    var newCanvasImg = newCanvas.toDataURL("image/png", 1.0);

    var doc = new jsPDF('l', 'pt', [1800, 640]);

    doc.addImage(newCanvasImg, 'PNG', 10, 10, newCanvas.width, newCanvas.height, 'none', 'SLOW');
    doc.save('Отчет_по_работе_с_нарушениями.pdf');
}

function revers_date(data) {
    return data[2] + "-" + data[1] + "-" + data[0];
}

function create_table(platform='',job) {
    $('.select2').select2('destroy');
    $('#datatable-filter div.ot').empty();
    // $('.table_graf').removeClass('hidden');
    // $('.cht').empty().append('')
    if ($.fn.dataTable.isDataTable('#dataTableExample')) {
        $('#datatable_graf').DataTable().destroy();

    }

    var table = $('#datatable_graf').dataTable({

        lengthChange: true,
        serverSide: true,
        destroy:true,
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
        dom: '<"pull-right" B>lfrtip',
        //stateSave: true,
        // buttons: ['colvis'],
        // "aaSorting": [[1, 'desc']],
        // "sScrollX": "100%",
        "buttons": [{
            extend: 'excel',
            text: 'Export excel',
            className: 'exportExcel',
            filename: 'Выгрузка отчета',
            exportOptions: {
                modifier: {
                    order : 'index',
                    page : 'all',
                    search : 'none'
                }
            }
        }],
        "scrollCollapse": true,
        "paging": true,
        "processing": true,
        ajax: {
            type: 'POST',
            url: "/analytics/get",
            data: {"action": "report_all","platform": platform},
            dataType: "json",
            cache: false,
        },
        columnDefs: [
            {
                "title": "№",
                "targets": [0],
                "className": "text-center",
                "data": "id",
                "orderable": false, "defaultContent": '', "width": "1%"
            },
            {
                "title": "Виды работ",
                "targets": [1],
                "className": "text-center",
                "data": "job",
                "defaultContent": '', "width": "1%"
            },
            {"title": "Предприятия", "targets": [2], "className": "text-center", "data": "platform", "width": "1%"},
            {"title": "Нарушение", "targets": [3], "className": "text-justify", "data": "actions", "width": "5%"},
            {
                "title": "Мероприятия",
                "targets": [4],
                "className": "text-justify",
                "data": "recommendations",
                "width": "5%"
            },
            {"title": "Плановый срок", "targets": [5], "className": "text-center", "data": "date_plan", "width": "0.5%"},
            {"title": "Факт срок", "targets": [6], "className": "text-center", "data": "date_fact", "width": "0.5%"},
            {
                "title": "Количество дней до просрочки",
                "targets": [7],
                "className": "text-center",
                "defaultContent": '',
                "width": "0.5%"
            },
            {
                "title": "Ответственный",
                "targets": [8],
                "className": "text-center",
                "data": "fio",
                // "visible": false,
                "width": "1%"
            },
            {
                "title": "job_n",
                "targets": [9],
                "className": "text-center",
                "data": "job_n",
                "visible": false,
                "width": "1%"
            },{
                "title": "Статус",
                "targets": [10],
                "className": "text-center",
                "data": "current-status-compliance",
                "width": "1%"
            },

        ],
        "initComplete": function () {

            this.api().column(1).every(function () {
                var column = this;
                var select = $('<select class="select2"><option value="1">Все</option></select>')
                    .appendTo($('#datatable-filter div.ot'))
                    .on('change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );

                        column
                            .search(val , false, false)
                            .draw();
                    })

                $.each(job,function (d, j) {
                    select.append('<option value="' + d + '">' + j + '</option>')
                });
            });

             $('select.select2').select2();
        },
        "createdRow": function (row, data, dataIndex) {
            $(row).find('td').eq(7).append(calcDate(data['date_plan'], data['date_fact']));
            $(row).attr("data-id", data['id']);
        },
        "language": {
            "processing": "Производиться загрузка данных",
            "lengthMenu": "Выводить по _MENU_ записей",
            "zeroRecords": "Ни одной записи не найдено",
            "info": "Показано страниц _PAGE_ из _PAGES_",
            "infoEmpty": "No records available",
            "infoFiltered": "(отфильтровано из _MAX_ записей)",
            "search": "Поиск:",
            "paginate": {
                "first": "Первая",
                "last": "Последняя",
                "next": "Следующая",
                "previous": "Предыдущая"
            },
        }
    }).api()
return table;
}
function calcDate(date1, date2) {
    message = ''

    if (date1 != '' && date2 != '') {
        var date1 = new Date(date1);
        var date2 = new Date(date2)
        var diff = Math.floor(date1.getTime() - date2.getTime());
        var day = 1000 * 60 * 60 * 24;

        var days = Math.floor(diff / day);
        var months = Math.floor(days / 31);
        var years = Math.floor(months / 12);


        message += days != 0 ? days + " дней " : '';
        message += months != 0 && months != -1 ? months + " месяц " : '';
        message += years != 0 && years != -1 ? years + " лет прошло " : '';
    } else {
        message += date1
        message += date2
    }

    return message
}






