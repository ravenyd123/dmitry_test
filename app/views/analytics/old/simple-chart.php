<div class="all">

  <div id="list">
    <table id="all_table"><!-- <thead><tr><th colspan="2">Сравнение показателей по площадкам и видам работ</th></tr></thead> --><tbody></tbody></table>
    <div id="all_delimiter"></div>
  </div>

  <div id="error"><p>Ошибка<span></span></p></div>

</div>

<script>
$('document').ready(function(){

  // Года
  var years = {
    '2015': '#ff0000',
    '2016': '#1e90ff'
  };

  // Разделитель для общей таблицы
  for ( var i = 10; i <= 100; i += 10 ){
    $('#all_delimiter').append('<p><span>' + i + '%</span></p>');
  }

  // Разделитель для компании
  for ( var i = 20; i <= 100; i += 20 ){
    $('#detail_delimiter').append('<p><span>' + i + '%</span></p>');
  }

  // Показ ошибки
  function show_error(message){
    $('#error > p > span').empty();
    if ( message != "" ){
      $('#error > p > span').append(': ' + message);
    }
    $('#error').show(300).delay(3000).hide(300);
  };

  // Раскрашивание процентных полос в общей таблице
  function percent_span(percent){
    var span_class = 'bad';
    if ( percent > 40 ){
      span_class = 'good';
    }
    if ( percent > 70 ){
      span_class = 'excellent';
    }
  if ( percent > 90 ){
    span_class = 'perfect';
  }
    return '<span style="width: ' + percent + '%" class="percent_class ' + span_class + '">' + percent + '</span>';
  }

  // Раскрашивание процентных полос в таблице компании
  function years_percents(data){
    var out = '';

    Object.keys(data).forEach(function (k) {
      out += '<span class="percent_class" style="width: ' + data[k] + '%; background: ' + years[k] + '">' + data[k] + '</span>';
    });

    return out;
  }


  // Получение данных с сервера
  function get_data(query){
    return new Promise(function(resolve, reject){
        var xhr = new XMLHttpRequest();
        xhr.open('GET', '/assets/2.php?q=' + query, true);
        xhr.onload = function () {
              if (this.status >= 200 && this.status < 300) {
                try {
                  var data = JSON.parse(xhr.response);
                  resolve(data);
                }
                catch(err){
                  reject( show_error(err) );
                }
              }
              else {
                reject( show_error() );
              }
            };
            xhr.onerror = function(){
              reject( show_error() );
            };
            xhr.send();
          });
  };


  // Экран компании
  function get_company(id){
    get_data("company&id=" + id + "&years=" + Object.keys(years))
    .then(function(data){
      if ( data.data.length > 0 ){
        $('#chartWorksModal').modal('show');
        $("#detail_title").empty().append(data.company);

        $("#detail_table").empty().append('<tbody></tbody>');
        for ( var i = 0; i < data.data.length; i++ ){
          if ( data.data[i].years ){
            $("#detail_table tbody").append('<tr><td>' + data.data[i].work + '</td><td>' + years_percents(data.data[i].years) + '</td></tr>');
          }
        }
      }
    })
    .then(function(){
      $("#detail_table").append('<thead><tr><th>Вид работ</th><th>Результаты самооценки</th></tr></thead>');
    })
    .then(function(){
      $('#detail_table span').show(300, function(){
        $('#detail_table span').css({ display: 'block' });
        var td_width = $('#detail_table > tbody > tr > td:eq(1)').width();
        $('#detail_delimiter').css({ width: td_width + "px" });
      });
    })
    .then(function(){
      $("#years").empty();
      Object.keys(years).forEach(function(k){
        $("#years").append('<span><i style="background: ' + years[k] + '"></i>' + k + '</span>');
      });
      $("#detail_company, #years, #detail_title").show();
      

    });
  } // get_company



  // Начальная загрузка - общий список
  get_data("all")
  .then(function(data){
    if ( data.length > 0 ){
      for ( var i = 0; i < data.length; i++ ){
        $('#all_table tbody').append('<tr data-company="' + data[i].id + '" ><td>' + data[i].place + '</td><td>' + percent_span(data[i].percent) + '</td></tr>');
      }
    }
  })
  .then(function(){
    $('#list').show();
    $('#all_table span').show(300, function(){
      $('#all_table span').css({ display: 'block' });
      var td_width = $('#all_table > tbody > tr > td:eq(1)').width();
      $('#all_delimiter').css({ width: td_width + "px" });
    });

    $("#all_table tr").click(function(){
      get_company( $(this).attr('data-company') );
    });
  });




});
</script>