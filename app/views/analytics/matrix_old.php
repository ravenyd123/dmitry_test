<? include_once $_SERVER['DOCUMENT_ROOT'] . '/app/views/header.php';?>

<?
$all_data = report_all();
$job = 0;
$platform = 0;
If (isset($_REQUEST['type'])) {
    $type = intval($_REQUEST['type']);
}
If (isset($_REQUEST['platform'])) {
    $platform = intval($_REQUEST['platform']);
}
If (isset($_REQUEST['job'])) {
    $job = intval($_REQUEST['job']);
}
If (isset($_REQUEST['bunch'])) {
    $bunch = intval($_REQUEST['bunch']);
}
function colortd($value)
{$t=[];
    switch ($value) {
        case 0:
            return $t = ["color" => "fivet", "name" => ""];
        case 1:
            return $t = ["color" => "one", "name" => "более 90%"];
        case 2:
            return $t = ["color" => "two", "name" => "от 71% до 90%"];
        case 3:
            return $t = ["color" => "three", "name" => "от 41% до 70%"];
        case 4:
            return $t = ["color" => "four", "name" => "менее 40%"];
        case 5:
            return $t = ["color" => "fivet", "name" => ""];
    }
}

?>

<link rel="stylesheet" href="css/analitic.css">
<!--<script type="text/javascript" src="/assets/main.js"></script>-->


<? /* if ($type == 'calendar') { */ ?><!--
<a href="#" id="calendar-xls" data-action="downloadCalendar">
  Скачать xls
</a>
--><? /* } */
$sch = 0;
$sch1 = 0;
$sch2 = 0;
?>
<!--<button id="btnExport" onclick="printpdf();"> EXPORT -->
<!--        </button>-->


<div class="wrapper" id="otchet_report">
    <table id="otchet" class='m-t-0 table table-striped table-bordered header-static order_otchet'>
        <!--      <thead>
              <tr>
                  <th colspan="8" class="p0">
                      <p>PepsiCo myEHS Audit Module</p>
                      <p class="text-left">Entity Code:Entity Code</p>
                      <p class="text-left">Entity Name:Entity Name</p>
                      <p class="text-left">Assessment Name:Assessment Name</p>
                  </th>
              </tr>
              <tr>
                  <th>Order</th>
                  <th class="w400">The interpretation of the requirement</th>
                  <th></th>
                  <th></th>
                  <th class="w140">Identified inconsistencies</th>
                  <th  class="w400" ></th>
                  <th class="with100" >Link</th>
                  <th class="with100" ></th>

              </tr>
              </thead>-->
        <tbody>
        <!--<a href="javascript:demoFromHTML()" class="button">Run Code</a>-->

        <?php
        foreach (job_view_all($bunch, $platform, $job) as $value) {
            $summa = 0;
            $summa1 = 0;
            $summa2 = 0;
            ?>
            <tr class="hg70">
                <td>№</td>
                <td class="w400"><?= $value['type'] ?></td>
                <td class="p0">Relevance</td>
                <td class="p0">Related Documents (EHS)</td>
                <td class="w140" style="width: 100px">Answer</td>
                <td class="w400">Comments/Recommendations</td>
                <td class="with100">Title of the document</td>
                <td class="with100">Item of the document</td>
            </tr>
            <?php
            foreach (all_data($value['job'], $value['self-evaluation'], $platform) as $key1 => $value1) {
                $soo1 = 1;
                $soo = 0;
                switch ($value1['status-compliance']) {
                    case 5:
                        $soo = 0;
                        $soo1 = 0;
                        break;
                    case 0:
                        $soo = 0;
                        $soo1 = 0;
                        break;
                    case 1:
                        $soo = 1;
                        break;
                    case 2:
                        $soo = 0.75;
                        break;
                    case 3:
                        $soo = 0.5;
                        break;
                    case 4:
                        $soo = 0;
                        break;
                }
                $summa += ($value1['krit']);
                $summa1 += ($value1['krit'] * $soo1);
                $summa2 += ($value1['krit'] * $soo);


                ?>
                <tr class="table-condensed" id="<?= $value1['id']; ?>">
                    <td><?= $key1 + 1 ?></td>
                    <td><?= $value1['demand'] ?></td>
                    <td><?= $value1['krit'] ?></td>
                    <td><?= $value1['ehs_numbers'] ?></td>

                    <td class="Status_of_compliance w140 <?php echo colortd($value1['status-compliance'])["color"]; ?>">
                        <div class="<?php echo colortd($value1['status-compliance'])["color"]; ?>">
                            <?php echo colortd($value1['status-compliance'])["name"] ?>
                        </div>
                    </td>
                    <td><?= $value1['actions-audit'] ?></td>
                    <td><?= $value1['symbol'] ?></td>
                    <td><a href='<?=Sourse_conn?><?= $value1['paragraph-link']; ?>'
                           target="_blank"><?= $value1['paragraph-name']; ?></a></td>
                </tr>
            <? }
            $sch += $summa;
            $sch1 += $summa1;
            $sch2 += $summa2 ?>
            <tr style="background-color: #00b0a7">
                <td></td>
                <td>Итого:</td>
                <td><?= $summa ?></td>
                <td><?= $summa1 ?></td>
                <td><?= $summa2 ?></td>
                <td><?php if ($summa1 !== 0) {
                        echo round(($summa2 / $summa1) * 100, 1);
                    } ?>%
                </td>
                <td></td>
                <td></td>
            </tr>
        <? } ?>
        <tr style="background-color: #6CCCA1">
            <td></td>
            <td>Итого по всем:</td>
            <td><?= $sch ?></td>
            <td><?= $sch1 ?></td>
            <td><?= $sch2 ?></td>
            <td><?php if ($sch1 !== 0) {
                    echo round(($sch2 / $sch1) * 100, 1);
                } ?>%
            </td>
            <td></td>
            <td></td>
        </tr>
        </tbody>
    </table>
</div>

<!--<input type="hidden" value="<? /*=$repotStatus;*/ ?>" id="repotStatus" />-->

<script>
  // function printpdf() {
//var content = document.getElementById("otchet_report");
//a = document.createElement("a")
//
//a.setAttribute("href", "data:text/plain," + content.innerHTML)
//a.setAttribute("download", "filename.txt")
//console.log(a);
//btnExport.onclick = function(){ a.click() }





// var a = document.body.appendChild(
//         document.createElement("a")
//     );

// a.download = "export.html";
// a.href = "data:text/html," + document.getElementById("otchet_report").innerHTML;

// a.innerHTML = "[Export conent]";


  // var htmlContent = document.getElementById('ter');
  // console.log(htmlContent);
  // var bl = new Blob(htmlContent, {type: "text/html"});
  // var a = document.createElement("a");
  // a.href = URL.createObjectURL(bl);
  // a.download = "your-download-name-here.html";
  // a.hidden = true;
  // document.body.appendChild(a);
  // a.innerHTML = "something random - nobody will see this, it doesn't matter what you put here";
  // a.click();








  // var pdf = new jsPDF('landscape');
  //       // source can be HTML-formatted string, or a reference
  //       // to an actual DOM element from which the text will be scraped.
  //       source = $('#otchet_report')[0];

  //       // we support special element handlers. Register them with jQuery-style 
  //       // ID selector for either ID or node name. ("#iAmID", "div", "span" etc.)
  //       // There is no support for any other type of selectors 
  //       // (class, of compound) at this time.
  //       specialElementHandlers = {
  //           // element with id of "bypass" - jQuery style selector
  //           '#bypassme': function (element, renderer) {
  //               // true = "handled elsewhere, bypass text extraction"
  //               return true
  //           }
  //       };
  //       margins = {
  //           top: 80,
  //           bottom: 60,
  //           left: 40,
  //           width: 522
  //       };
  //       // all coords and widths are in jsPDF instance declared units
  //       // 'inches' in this case
  //       pdf.fromHTML(
  //           source, // HTML string or DOM elem ref.
  //           margins.left, // x coord
  //           margins.top, { // y coord
  //               'width': margins.width, // max width of content on PDF
  //               'elementHandlers': specialElementHandlers
  //           },

  //           function (dispose) {
  //               // dispose: object with X, Y of the last line add to the PDF 
  //               //          this allow the insertion of new lines after html
  //               pdf.save('Test.pdf');
  //           }, margins
  //       );
    
// }

    // Скачать отчет в формате excel
    // $('*[data-action="downloadCalendar"]').click(function () {

    //     var id = [];

    //     $('.report.header2-static tr:gt(0)').each(function (index) {
    //         id.push($(this).attr('id'));
    //     })

    //     console.log(id);

    //     window.open(
    //         '/app/views/reports/download_calendar_excel.php?id=' + id,
    //         '_blank'
    //     );


        // $.ajax({
        //   type: 'GET',
        //       data: {
        //         "id": id,
        //       },
        //   url: '/app/views/reports/download_calendar_excel.php',
        //   cache: false,
        //   success: function(data){
        //     console.log(data);
        //     // location.reload();
        //   }
        // });

    // });

    /*var table= $('#'+$("table").attr("id")).DataTable({

     "initComplete":function () {
     $('.filter th input').each(function () {
     $(this).on( 'keyup', function () {
     table
     .columns($(this).parent('th').index())
     .search( this.value )
     .draw();
     } );
     });

     this.api().column(4).every( function () {
     var column = this;
     var select = $('<select class="form-control input-sm "><option value="">Все требования</option></select>')
     .appendTo( $('#filter_select') )
     .on( 'change', function () {
     var val = $.fn.dataTable.util.escapeRegex(
     $(this).val()
     );
     console.log(val);
     column
     .search( val ? '^'+val+'$' : '', true, false )
     .draw();
     } );

     $('.select_color').eq(0).find("option").each(function (index,text) {
     select.append(text)
     });


     } );
     },
     "sDom": '<"top">rt<"bottom"><"clear">',
     "ordering": false,
     fixedHeader: true,
     "bAutoWidth": false,
     "bInfo": false,
     "bLengthChange": false,
     "bPaginate": false,
     "iDisplayLength": 1000000,
     "language": {
     "zeroRecords": "Нет записей удовлетворяющих условия",
     "infoEmpty": "Нет записей удовлетворяющих условия"
     }

     });*/
     </script>