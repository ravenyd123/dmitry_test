<? include_once $_SERVER['DOCUMENT_ROOT'] . '/assets/functions.php'; ?>
<? include_once $_SERVER['DOCUMENT_ROOT'] . '/app/views/header.php'; ?>
<? include_once $_SERVER['DOCUMENT_ROOT'] . '/app/views/template.php'; ?>


<section class="content">
    <div class="wraper container-fluid">
        <div class="page-title">
            <h3 class="title">Аналитика</h3>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Сравнение показателей по площадкам и видам работ</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-3">
                                <div style="padding-left: 20px">Тип</div>
                                <div class="checkbox">
                                    <label class="cr-styled">
                                        <input type="checkbox">
                                        <i class="fa"></i>Аудит</label>
                                </div>
                                <div class="checkbox">
                                    <label class="cr-styled">
                                        <input type="checkbox">
                                        <i class="fa"></i>Чек лист</label>
                                </div>
                                <div class="checkbox">
                                    <label class="cr-styled">
                                        <input type="checkbox">
                                        <i class="fa"></i>Гос органы</label>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div>Дата</div>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="analit_datefrom" placeholder="Дата начала">
                                    <div class="help-block"></div>
                                </div>
                                <div class="form-group">
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="analit_dateto"  placeholder="Дата окончания">
                                        <div class="help-block"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div>Тип графиков</div>
                                <div class="radio">
                                    <label><input type="radio" name="diagstolb">Столбчатая диаграмма</label>
                                </div>
                                <div class="radio">
                                    <label><input type="radio" name="diagradar">Диаграмма "Радар</label>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div>Площадки</div>
                                <div class="form-group">
                                    <select class="form-control" name="analitic-platform">
                                        <option value="">Выберите площадку</option>
                                        <?
                                        foreach (GetPlatformUser() as $key => $row) { ?>
                                            <option value="<?= $key; ?>"><?= $row; ?></option>
                                        <? } ?>
                                    </select>
                                    <div class="help-block"></div>
                                </div>
                            </div>
                            <!--<div class="col-md-4">
								<ul class="header-filter text-right">
									<li class="m-0"><a href="#" target="_blank"><input type="button" class="btn btn-primary" id="save" value="Сохранить в PDF"></a></li>
								</ul>
							</div>	
						</div>

						<div class="row">
							<div class="col-md-12">
								<div id="pillar" class="chart active">
									<? /* include 'simple-chart_new.php'; */ ?>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div id="radar" class="chart inactive">
									<? /* include 'radar-chart.php'; */ ?>
								</div>
							</div>
						</div>-->
                        </div>
                        <div class="row">
                            <div class=" col-md-offset-4 col-md-3">
                                <div class="form-group">
                                    <!-- <a target="_blank" href="/reports/matrix?type=calendar"> -->
                                    <input type="button" data-action="analitics" class="btn btn-primary w-100" value="Сформировать отчет">
                                    <!-- </a> -->
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
            <!-- End Row -->

        </div>
</section>


<script>
    $(document).ready(function () {
        $('.header-filter [data-name]').click(function () {
            $('.chart').css('display', 'none');
            $('.chart').removeClass('active');
            $('#' + $(this).attr('data-name')).addClass('active');
        })
    });
    $("#detail_table").on("click", "tr", function () {
        window.open(
            '/app/views/analytics/matrix_old.php?job=' + $(this).attr("data-job") + '&platform=' + $(this).attr("data-platform") + '&bunch=' + $(this).attr("data-bunch"),
            '_blank'
        );
    });
    $("#years").on("click", "a", function () {
        window.open(
            '/app/views/analytics/matrix_old.php?&platform=' + $('#detail_table tr').eq(0).attr("data-platform") + '&bunch=' + $('#detail_table tr').eq(0).attr("data-bunch"),
            '_blank'
        );
    });
</script>