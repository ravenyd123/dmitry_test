      <canvas id="chart" class="pie" style="position: relative; z-index: 200; margin: 0 auto; width: 960px; "></canvas>
      <div id="chart-labels"></div>
      <div id="chartjs-tooltip"></div>

<script>

function check_label( tooltip, datasets ){
    try {
        var s = tooltip.body["0"].lines[0];
        datasets.some(function(el){
            if ( el.label == s.split(":")[0].trim() && el.hidden == false ){
                return true;
            }
        });
        return true;
    }
    catch(e){ 
        return false;
    }
}

Chart.defaults.global.tooltips.custom = function(tooltip) {

    // var tooltipEl = $('#chartjs-tooltip');
    // if (!tooltip.opacity) {
    //   tooltipEl.css({
    //     opacity: 0
    //   });
    //   $('.chartjs-wrap canvas').each(function(index, el) {
    //     $(el).css('cursor', 'default');
    //   });
    //   return;
    // }

    // tooltipEl.removeClass('above below no-transform');
    // if (tooltip.yAlign) {
    //   tooltipEl.addClass(tooltip.yAlign);
    // } else {
    //   tooltipEl.addClass('no-transform');
    // }

    // if (tooltip.body) {

    //     tooltipEl.html('<p>' + tooltip.title + '</p><p>' + tooltip.body["0"].lines["0"] + '%</p>');
    // }

    // var top = 0;
   
    // if (tooltip.yAlign) {
    //   var ch = 0;
    //   if (tooltip.caretHeight) {
    //     ch = tooltip.caretHeight;
    //   }
    //   if (tooltip.yAlign == 'above') {
    //     top = tooltip.y - ch - tooltip.caretPadding;
    //   } else {
    //     top = tooltip.y + ch + tooltip.caretPadding;
    //   }
    // }

    // var opacity = 1;

    // if ( tooltip.title != "" && check_label( tooltip, _chart.config.data.datasets ) && tooltip.body["0"].lines["0"].split(":").length > 1 ){
    //     //top = -5000;
    // }
    // else {
    //     top = -9000;
    //     left = -9000;
    //     opacity = 0;
    //     zindex = -1;
    // }

    // var position = $(this._chart.canvas)[0].getBoundingClientRect();
    // tooltipEl.css({
    //   opacity: opacity,
    //   width: tooltip.width ? (tooltip.width + 'px') : 'auto',
    //   left: position.left + tooltip.x + 'px',
    //   top: top + 'px',
    //   fontFamily: tooltip._fontFamily,
    //   fontSize: tooltip.fontSize,
    //   fontStyle: tooltip._fontStyle,
    //   padding: tooltip.yPadding + 'px ' + tooltip.xPadding + 'px',
    // });

};

var years = {
    '2015': '#ff0000',
    '2016': '#1e90ff'
  };

var data = {
        labels: [],
        datasets: []
    };

// Перенос строки у наименований предприятий на графике
function splitter(str, l){
    var strs = [];
    while(str.length > l){
        var pos = str.substring(0, l).lastIndexOf(' ');
        pos = pos <= 0 ? l : pos;
        strs.push(str.substring(0, pos));
        var i = str.indexOf(' ', pos)+1;
        if(i < pos || i > pos+l)
            i = pos;
        str = str.substring(i);
    }
    strs.push(str);
    return strs;
}


var options = {

    animation : false,

    title: {
        display: true
    },        
    responsive: true,
    layout: {
        padding: { top: 0, bottom: 0, left: 40, right: 40 },
    },
    
    tooltips: {
        enabled: true,

    },
    legend: {
        display: false
    },

    datasets: {
        display: false
    },

    scale: {
        display: true,
        gridLines: { color: "transparent" },
        ticks: {
            display: true,
            userCallback: function(label, index, labels) {
                if ( [0, 40, 70, 90].indexOf(label) > 0 || label === 10) {
                    if ( label === 10 ){ label = 0; }
                    return label + "%";
                }
                return "";
            },
        },

        pointLabels: {
            callback: function(pointLabel, index, labels) { 
                if ( pointLabel.length > 15 ){
                    //return pointLabel.split(' ', 2);
                }
                return splitter(pointLabel, 25);
            },
        },

    },
        
    };   

var ctx = $("#chart");

var _chart = new Chart(ctx, {
    type: 'radar',
    data: data,
    options: options
});



var shadow_array = [];

function hideAll(item){
    // console.log(item.closest('.modal').id);
    if (item.closest('.modal').id == 'worksModal') {
        _chart.config.data.datasets.forEach(function(el, i){
            if ( _chart.config.data.datasets[i].hidden == false && _chart.config.data.datasets[i].label.length > 0 ){
                _chart.config.data.datasets[i].hidden = true;
            }
        });
        $("#chart-labels input[type=checkbox]").prop('checked', false);        
    };
    if (item.closest('.modal').id == 'factoryModal') {
        $('#chart-factory input[type="checkbox"]').map(function(item, i, arr){
            _chart.config.data.labels.splice(0,_chart.config.data.labels.length);  
        })
        $("#chart-factory input[type=checkbox]").prop('checked', false);    
    }

    _chart.update();
}

function showAll(item){
    if (item.closest('.modal').id == 'worksModal') {
        _chart.config.data.datasets.forEach(function(el, i){
            if ( _chart.config.data.datasets[i].label.length > 0 ){  
                _chart.config.data.datasets[i].hidden = false;
            }
        });
        $("#chart-labels input[type=checkbox]").prop('checked', true);
    };
    if (item.closest('.modal').id == 'factoryModal') {
        $("#chart-factory .checkbox").map(function(item, i, arr){
            _chart.config.data.labels[item] = $("label", this)[0].innerText;
        })
        $("#chart-factory input[type=checkbox]").prop('checked', true);    
    }

    _chart.update();
}

    window.onload = function(){

        for ( var i = 20; i <= 100; i += 20 ){
            $('#delimiter').append('<p><span>' + i + '%</span></p>');
        }

        function show_error(error){
            console.log("error: " + error);
        }


        function get_data(query){
            return new Promise(function(resolve, reject){
                var xhr = new XMLHttpRequest();
                xhr.open('GET', '/app/views/analytics/data_new.php?' + query, true);
                xhr.onload = function () {
                      if (this.status >= 200 && this.status < 300) {
                        try {
                          var data = JSON.parse(xhr.response);
                          resolve(data);
                        }
                        catch(err){
                          reject( show_error(err) );
                        }
                      }
                      else {
                        reject( show_error() );
                      }
                    };
                    xhr.onerror = function(){
                      reject( show_error() );
                    };
                    xhr.send();
                  });
          };


        get_data("q=places")
        .then(function(data){
//            console.log(Array);
            if ( Array.isArray(data) ){
                _chart.config.data.labels = data;
                _chart.update();

            }
            console.log(_chart);
            return data;
            
        }).then(function(data){

            var datasets_data = [0, 100];
            for ( var i = 0; i < ( data.length - 2 ); i++ ){
                datasets_data.push(i);
            }

            _chart.config.data.datasets.push(
                {
                    label: "",
                    data: datasets_data,
                    backgroundColor: 'transparent',
                    borderColor: 'transparent',
                    borderWidth: 0,
                    pointBorderColor: 'transparent',
                    pointBackgroundColor: 'transparent',
                    hidden: false
                }
            );

            _chart.update();

            return get_data("q=works");

        }).then(function(data){

            if ( data.length > 0 ){
                for ( var i = 0; i < data.length; i++ ){
                    _chart.config.data.datasets.push(
                        {
                            label: data[i].work.name,
                            data: data[i].percents,
                            backgroundColor: 'transparent',
                            borderColor: data[i].work.color,
                            borderWidth: 1,
                            pointBorderColor: data[i].work.color,
                            pointBackgroundColor: data[i].work.color,
                            pointHoverBackgroundColor: data[i].work.color,
                            pointHoverBorderColor: data[i].work.color,
                            pointHoverBackgroundColor: data[i].work.color,
                            hidden: false
                        }
                    );
                }
                _chart.update();
            }
            return data;

        }).then(function(data){

            var circles = [
                { percent: 40, color: 'rgba(244,187,185,0.1)' },
                { percent: 70, color: 'rgba(255,246,189,0.2)' },
                { percent: 90, color: 'rgba(198,222,198,0.3)' },
                { percent: 100, color: 'rgba(219,232,241,0.5)' }
            ];

            circles.forEach(function(el){
                
                var datasets_data = [];

                for ( var i = 0; i < ( data.length ); i++ ){
                    datasets_data.push(el.percent);
                }

                _chart.config.data.datasets.push(
                {
                    label: "",
                    data: datasets_data,
                    backgroundColor: el.color,
                    borderColor: 'transparent',
                    borderWidth: 0,
                    color: '#ff0000',
                    pointBorderColor: 'transparent',
                    pointBackgroundColor: 'transparent',
                    hidden: false
                }
            );

            

            });

            _chart.update();

        }).then(function(){

            _chart.options.onClick = function(evt, elements) {

                $("#detail_screen > .title, #detail_screen > .years, #detail_screen > #table > table > tbody").empty();
                
                if (  elements[0] && elements[0]._chart.config.data.labels[ elements[0]._index ] ){

                    get_data("name=" + elements[0]._chart.config.data.labels[ elements[0]._index ] + "&years=" + Object.keys(years) )
                    .then(function(data){
                        
                        if ( data.company ){

                            $("#chartRadarModal .modal-header h4").text(data.company);
                            Object.keys(years).forEach(function(year){
                                $("#detail_screen > .years").append("<span><i style='background: " + years[year] + "'></i>" + year + "</span>");
                            });

                            for ( var i = 0; i < data.data.length; i++ ){

                                var tr = "<tr><td>" + data.data[i].work + "</td><td>";
                                Object.keys(data.data[i].years).forEach(function(k){
                                    tr += "<p style='background:" + years[k] + "; width: " + data.data[i].years[k] + "%;'>" + data.data[i].years[k] + "</p>";
                                });
                                tr += "</td></tr>";

                                $("#detail_screen > #table > table > tbody").append(tr);

                            }
                            $('#chartRadarModal').modal('show');
                        }

                    }).then(function(){
                        setTimeout(function() { 
                            var td_width = $('#detail_screen > #table > table > tbody > tr > td:eq(1)').width();
                            $('#delimiter').css({ width: td_width + "px" });
                            $("html, body").animate({ scrollTop: 0 }, 0);
                            $("#detail_screen > #table > table > tbody > tr > td p").show(0);
                        }, 600);
                    });
                }
            };    
            
            $("#show_hide").show();   
            
            $("#show_hide a").click(function(){
                // window[ $(this).attr('id') ].call();
                countWorkAmount();
                countFactoryAmount();
                return false;
            });

            $("#detail_screen > p.exit").click(function(){
                $("#detail_screen").hide();
            });

            // ВЫВОД ЧЕКБОКСОВ ПО ВИДАМ РАБОТ
            // Добавление чекбоксов
            _chart.config.data.datasets.forEach(function(e,index){
                if ( e.label.length > 0 ){
                    var ch = e._meta["0"].dataset.hidden == false ? " checked " : "";
                     $("#chart-labels").append('<div class="checkbox" data-label="' + e._meta["0"].dataset._datasetIndex + '"><label><input type="checkbox" ' + ch + ' name="label_id_' + e._meta["0"].dataset._datasetIndex + '">' + e.label + '</label></div>');
                }
            });

            // Вывод в 3 колонки
            var workAmount = (Math.floor(($('#chart-labels input[type="checkbox"]:checked').length + 3)/3));          
            for (var i = 0; i < _chart.config.data.datasets.length; i+=workAmount) {
               $('#chart-labels .checkbox').slice(i, i+workAmount).wrapAll("<div class='col-md-4'></div>");
            };

            // Пересчет кол-ва отмеченных видов работ
            countWorkAmount();
            $('#chart-labels input[type="checkbox"]').change(function(){
                countWorkAmount();
            })

            $("#chart-labels .checkbox").click(function(){
                _chart.config.data.datasets[ $(this).attr('data-label') ].hidden = !$("input[type=checkbox]", this).is(':checked');
                _chart.update();

            });

            // ВЫВОД ЧЕКБОКСОВ ПО ПРЕДПРИЯТИЯМ 
            // Добавление чекбоксов
            _chart.config.data.labels.forEach(function(e,index){
                if ( e.length > 0 ){
                     $("#chart-factory").append('<div class="checkbox" data-label="checked"><label><input type="checkbox" data-factory=' + index + ' name="factory_id_" checked="checked">' + e + '</label></div>');
                }
            });

            // Вывод в 3 колонки
            var factoryAmount = Math.floor(($('#chart-factory input[type="checkbox"]').length + 3)/3);          
            for (var i = 0; i < _chart.config.data.labels.length; i+=factoryAmount) {
               $('#chart-factory .checkbox').slice(i, i+factoryAmount).wrapAll("<div class='col-md-4'></div>");
            };

            // Пересчет кол-ва отмеченных предприятий
            countFactoryAmount();

            $('#chart-factory input[type="checkbox"]').change(function(e,index){
                _chart.config.data.labels.splice(0,_chart.config.data.labels.length);
                var iterator = 0;
                $("#chart-factory .checkbox").map(function(item, i, arr){
                    if ($("input[type=checkbox]", this).is(':checked')) {
                        _chart.config.data.labels[iterator] = $("label", this)[0].innerText;
                        iterator++;
                    };
                })

                // console.log(_chart.config.data);
                _chart.update();
                countFactoryAmount();
            })



            shadow_array = _chart.config.data.datasets.slice();

        });

    }

function countWorkAmount(quantity){
    if (quantity){
        workAmount = quantity;
    }else{
        workAmount = $('#chart-labels input[type="checkbox"]:checked').length
    }
    $('*[data-target="#worksModal"] span').text('(' + workAmount + ')');  
}

function countFactoryAmount(quantity){
    if (quantity){
        factoryAmount = quantity;
    }else{
        factoryAmount = $('#chart-factory input[type="checkbox"]:checked').length
    }

    if (factoryAmount < 3) {
        $('#factoryModal .modal-footer input[type="button"]').removeClass('btn-primary');
        $('#factoryModal .modal-footer input[type="button"]').addClass('btn-default');
        $('#factoryModal .modal-footer input[type="button"] , .modal-header .close').attr('data-dismiss','');
        $('#factoryModal .modal-footer p').removeClass('hide');
        $('#factoryModal').modal({backdrop: 'static', keyboard: false })
    };
    if (factoryAmount >= 3) {
        $('#factoryModal .modal-footer input[type="button"]').removeClass('btn-default');
        $('#factoryModal .modal-footer input[type="button"]').addClass('btn-primary');
        $('#factoryModal .modal-footer input[type="button"] , .modal-header .close').attr('data-dismiss','modal');
        $('#factoryModal .modal-footer p').addClass('hide');
    };

    $('#chart-factory input[type="checkbox"]').each(function(index, value){
        if ( $(this).is(':checked') ) {
            $('tr[data-company="' + $(this).attr('data-factory') +'"]').show();
        }else{
            $('tr[data-company="' + $(this).attr('data-factory') +'"]').hide();
        };
        
    })

    _chart.config.data.datasets.forEach(function(el, item){
        if (item == 0 || 
            item == _chart.config.data.datasets.length-1 ||
            item == _chart.config.data.datasets.length-2 ||
            item == _chart.config.data.datasets.length-3 ||
            item == _chart.config.data.datasets.length-4 )
        {return;}
        else
             { 
            _chart.config.data.datasets[item].data.splice(0,_chart.config.data.datasets[item].data.length);
            for (var i = 0; i < factoryAmount; i++) {
                _chart.config.data.datasets[item].data[i] = Math.floor(Math.random() * (100 - 0)) + 0;  
            };
        }

    });
    _chart.update();
    $('*[data-target="#factoryModal"] span').text('(' + factoryAmount + ')');  
}

</script>