<? include_once $_SERVER['DOCUMENT_ROOT'].'/assets/functions.php'; ?>
<? include_once $_SERVER['DOCUMENT_ROOT'].'/app/views/header.php'; ?>
<? include_once $_SERVER['DOCUMENT_ROOT'].'/app/views/template.php'; ?>

<section class="content">

<script src="/assets/old/chart.min.js"></script>
<link rel="stylesheet" type="text/css" href="/app/views/analytics/css/chart.css" />

	<div class="wraper container-fluid">
        <div class="page-title"> 
            <h3 class="title">Аналитика</h3> 
        </div>

		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">Сравнение показателей по площадкам и видам работ</h3>
					</div>
					<div class="panel-body">
						<div class="row">
							<div class="col-md-8">
								<ul class="header-filter">
									<li>
										<a href="#" data-target="#worksModal" data-toggle="modal" class="arrow-down">Виды работы<span></span></a>
									</li>
									<li>
										<a href="#" data-target="#factoryModal" data-toggle="modal" class="arrow-down">Предприятия<span></span></a>
									</li>
									<li>
										<div class="dropdown">
											<a href="#" class="arrow-down dropdown-toggle" data-toggle="dropdown">Тип графика</a>
										  <ul class="dropdown-menu">
										    <li><a href="#" data-name="radar">Диаграмма "Радар"</a></li>
										    <li><a href="#" data-name="pillar">Столбчатая диаграмма</a></li>
										  </ul>
										</div>
									</li>
								</ul>
							</div>
							<div class="col-md-4">
								<ul class="header-filter text-right">
									<li class="m-0"><a href="/analytics/report.pdf" target="_blank"><input type="button" class="btn btn-primary" id="save" value="Сохранить в PDF"></a></li>
								</ul>
							</div>	
						</div>

						<div class="row">
							<div class="col-md-12">
								<div id="pillar" class="chart active">
									<? include 'simple-chart.php'; ?>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div id="radar" class="chart inactive">
									<? include 'radar-chart.php'; ?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>
		<!-- End Row -->

    </div>
</section>



<script>
$(document).ready(function(){
	$('.header-filter [data-name]').click(function(){
		$('.chart').css('display','none');
		$('.chart').removeClass('active');
		$('#' + $(this).attr('data-name')).addClass('active');
	})
})
</script>