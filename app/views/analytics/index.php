<? include_once $_SERVER['DOCUMENT_ROOT'] . '/app/views/header.php'; ?>
<? include_once $_SERVER['DOCUMENT_ROOT'] . '/app/views/template.php'; ?>

<section class="content">
    <div class="wraper container-fluid">
        <div class="col-md-12">
            <form id="an_form" novalidate method="POST">
                <div class="portlet">
                    <div class="portlet-heading ">
                        <h4 class="portlet-title text-dark">Шаблон создания графиков</h4>
                        <div class="portlet-widgets">
                            <a data-toggle="collapse" data-parent="#accordion1" href="#menu1"><i
                                        class="ion-minus-round"></i></a>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div id="menu1" class="panel-collapse collapse in">
                        <div class="portlet-body">
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="form-group">

                                        <label for="an_graf">График:</label>
                                        <select id="an_graf" class="select2" name="an_graf">
                                            <option value="line">Линейный</option>
                                            <option value="bar">Гистограмма</option>
                                            <option value="horizontalBar">Гистограмма гориз</option>
                                        </select>

                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="an_report">Отчет:</label>
                                        <select id="an_report" class="select2" name="an_report">
                                            <option value="one">Первый</option>
                                            <option value="two">Второй</option>
                                        </select>

                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <label for="datepicker">Дата:</label>
                                    <div class="input-daterange input-group" id="datepicker">
                                        <input type="text" class="input-sm form-control" id="an_date_min"
                                               name="min_date" autocomplete="off"/>
                                        <span class="input-group-addon">До: </span>
                                        <input type="text" class="input-sm form-control" id="an_date_max"
                                               name="max_date" autocomplete="off"/>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="an_platform" class="control-label">Площадка:</label>

                                        <select id="an_platform" name="an_platform" required multiple class="select2"
                                                data-placeholder="Выберите площадку...">
                                            <? foreach (platforms_user as $key => $value) { ?>
                                                <option value="<?= $key ?>"><?= $value ?></option>
                                            <? } ?>
                                        </select>
                                    </div>
                                    <!--                                    <div class="form-group">-->
                                    <!--                                        <label for="an_bunch" class="control-label">Календарь мероприятий:</label>-->
                                    <!--                                        --><?php //$reports = getReportsByPlatform($link); ?>
                                    <!--                                        <select name="an_bunch" id="an_bunch" required multiple class="select2"-->
                                    <!--                                                data-placeholder="Выберите чек-листы по которым строить отчет...">-->
                                    <!--                                            --><? // foreach ($reports as $key => $value) { ?>
                                    <!--                                                <option value="-->
                                    <? //= $reports[$key]['bunch'] ?><!--">-->
                                    <!--                                                    -->
                                    <? //= type_tasks_new[$reports[$key]['typen']] ?><!--:-->
                                    <!--                                                    Чек лист №--><? //= $reports[$key]['bunch'] ?>
                                    <!--                                                    от --><? //= date('d.m.Y', $reports[$key]['date']); ?>
                                    <!--                                                    по-->
                                    <!--                                                    площадке --><? //= getFieldByField($link, 'platform', 'id', $reports[$key]['platform'], 'name'); ?>
                                    <!--                                                </option>-->
                                    <!--                                            --><? // } ?>
                                    <!--                                        </select>-->
                                    <!--                                    </div>-->
                                    <div class="form-group">
                                        <button class="create btn btn-success pull-right">Создать
                                            график
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div class="col-md-12">
            <div class="portlet">
                <div class="portlet-heading">
                    <h4 class="portlet-title text-dark">Результат работы по площадкам</h4>
                    <div class="portlet-widgets">
                        <a href="javascript:;" data-toggle="expand"><i class="ion-lightbulb"></i></a>
                        <span class="divider"></span>
                        <a href="javascript:;" data-toggle="reload"><i class="ion-refresh"></i></a>
                        <span class="divider"></span>
                        <a data-toggle="collapse" data-parent="#accordion1" href="#portlet2"><i
                                    class="ion-minus-round"></i></a>
                        <span class="divider"></span>
                        <a href="#" data-toggle="remove"><i class="ion-close-round"></i></a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div id="portlet2" class="panel-collapse collapse in">
                    <div class="portlet-body">
                        <div class="row">
                            <div class="col-md-12 form-inline m-b-10 ">
                                <div class="form-group">Тип</div>
                                <div class="checkbox form-group m-l-10">
                                    <label class="cr-styled">
                                        <input id="inp_audit" type="checkbox" checked>
                                        <i class="fa"></i>Аудит</label>
                                </div>
                                <div class="checkbox form-group m-l-10">
                                    <label class="cr-styled">
                                        <input id="inp_self" type="checkbox" checked>
                                        <i class="fa"></i>Чек лист</label>
                                </div>
                            </div>
                        </div>

                        <div class="row">

                            <? include 'anal.php'; ?>

                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="graf_all hidden">
            <div class="col-md-12">
                <div class="portlet">
                    <div class="portlet-heading">
                        <div class="portlet-widgets">
                            <!--                            <a href="javascript:;" data-toggle="expand"><i class="ion-lightbulb"></i></a>-->
                            <!--                            <span class="divider"></span>-->
                            <!---->
                            <!--                            <a href="javascript:;" data-toggle="reload"><i class="ion-refresh"></i></a>-->
                            <!--                            <span class="divider"></span>-->
                            <!---->
                            <!--                            <a data-toggle="collapse" data-parent="#accordion1" href="#portlet3"><i-->
                            <!--                                        class="ion-minus-round"></i></a>-->
                            <span class="divider"></span>
                            <a href="#" data-toggle="remove"><i class="ion-close-round"></i></a>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div id="portlet3" class="panel-collapse collapse in">
                        <div class="portlet-body">

                            <div class="row">
                                <div class="col-md-12 ">
                                    <button type="button" class="btn btn-success pull-right small"
                                            onclick="downloadPDF2()">Сохранить в PDF
                                    </button>
                                </div>
                                <div class="col-md-12 cht">

                                </div>
                                <div class="col-md-12 job"></div>
                            </div>
                          <div class="row">
                            <div class="m-b-15">
                                <div class="" id="datatable-filter">
                                    <div class="col-md-3 ot">
                                        <label>Виды работ</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <table id="datatable_graf" class="table table-hover table-striped">
                                </table>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


</section>
<script src="/node_modules/chart.js/dist/Chart.min.js"></script>
<!--<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>-->
<!--<script src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>-->
<!--<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"></script>-->
<script src="/node_modules/jszip/dist/jszip.min.js"></script>
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>-->
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>-->
<script src="/vendor/drmonty/datatables-buttons/js/buttons.html5.min.js"></script>
<!--<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>-->



<!--<script src="/assets/chart.min.js"></script>-->
<link rel="stylesheet" type="text/css" href="/node_modules/chart.js/dist/Chart.css"/>
<link rel="stylesheet" type="text/css" href="/node_modules/chart.js/dist/Chart.min.css"/>

<script type="text/javascript" src="/app/views/analytics/js/index.js"></script>

