<?php
include_once $_SERVER['DOCUMENT_ROOT'] . '/assets/functions.php';
include_once $_SERVER['DOCUMENT_ROOT'] . '/assets/all_const.php';
include_once('SSP.php');
$db = DB::Connection();

if (isset($_POST['action']) and $_POST['action'] == "form") {
    $mas = [];
    $label = []; //список$
    $not_performed = [];
    $inwork = [];
    $done = [];
    $blue = [];

    $job_n = [];

    $graf = $_POST['an_graf'];
    $report = $_POST['an_report'];
    $min_date = htmlspecialchars($_POST['min_date']);
    $max_date = htmlspecialchars($_POST['max_date']);
    $platform = htmlspecialchars($_POST['an_platform']);

    $kol = "SELECT c.job , COUNT( c.id) AS sm FROM collector c 
  LEFT JOIN  reports r ON r.id=c.`self-evaluation` ";


    $kol .= " WHERE c.`status-compliance`<>5 
  AND c.`status-compliance`<>0 
  AND c.`status-compliance`<>1";//`current-status-compliance`


    if ($min_date != '' and $max_date != '') {
        $kol .= " AND r.date BETWEEN UNIX_TIMESTAMP('{$min_date}') AND UNIX_TIMESTAMP('$max_date')";
    }

    if ($platform != '') {
        $kol .= " AND r.platform IN ({$platform})";
    } else {
        $kol .= " and r.platform in ({$_COOKIE['platform']})";
    }
//    $kol1 = $kol . " and r.platform in ({$_COOKIE['platform']}) GROUP BY c.job ";

// var_dump($kol);

    $res = $db->query($kol . " GROUP BY c.job ");

    while ($row = $res->fetch_assoc()) {
        $mas[$row['job']] = ["sum" => intval($row['sm']), "blue" => 0, "not_performed" => 0, "inwork" => 0, "done" => 0];
        $label[] = $row['job'];
    }

    //синий всего*
    //AND c.`date-plan`>0 запланированный зеленый*
    //AND c.`date-plan`>c.`fact-date` это просроченные красный
    //AND c.`date-plan`>c.`fact-date` это в работе желтый

    if ($report == "two") {//выполнено в срок
        $plan = $kol . " AND c.`current-status-compliance`= 1 AND c.`date-plan`>=c.`fact-date` GROUP BY c.job";
    } else {
        $plan = $kol . " AND LENGTH (c.fio)<>0 GROUP BY c.job";
    }

    $res = $db->query($plan);
    while ($row = $res->fetch_assoc()) {
        $mas[$row['job']]['done'] = intval($row['sm']);
    }

    if ($report == "two") {//Невыпонено с нарушением срока
        $red = $kol . " AND c.`current-status-compliance` <> 1 AND c.`date-plan`<IF(c.`fact-date`= '0' , UNIX_TIMESTAMP(NOW()), c.`fact-date`) GROUP BY c.job";
    } else {
        $red = $kol . " AND LENGTH (c.fio)=0 GROUP BY c.job";
    }

    $res = $db->query($red);
    while ($row = $res->fetch_assoc()) {
        $mas[$row['job']]['not_performed'] = intval($row['sm']);
    }

    if ($report == "two") {//Выпонено в не срока
        $qblue = $kol . " AND c.`current-status-compliance` = 1 AND c.`date-plan`<IF(c.`fact-date`= '0' , UNIX_TIMESTAMP(NOW()), c.`fact-date`) GROUP BY c.job";

        $res = $db->query($qblue);
        while ($row = $res->fetch_assoc()) {
            $mas[$row['job']]['blue'] = intval($row['sm']);
        }
    }


    if ($report == "two") {//В работе
        $work = $kol . " AND c.`current-status-compliance` <> 1 AND c.`date-plan`>c.`fact-date` GROUP BY c.job";
        $res = $db->query($work);
        while ($row = $res->fetch_assoc()) {
            $mas[$row['job']]['inwork'] = intval($row['sm']);
        }

    }

    foreach ($label as $key) {
        $job_n[$key] = $key . " " . GetAllvid[$key];
    };

    foreach ($mas as $nvalue) {
        $sum[] = $nvalue['sum'];
        $not_performed[] = $nvalue['not_performed'];
        $inwork[] = $report == "one" ? $nvalue['sum'] - $nvalue['not_performed'] : $nvalue['inwork'];
        $done[] = $nvalue['done'];
        $blue[] = $nvalue['blue'];
    };

    $data_all = ["data" => [
        "labels" => $label,
        "datasets" => [
            [
                "label" => "Количество нарушений",
                "yAxisID" => "y-axis-gravity",
                "data" => $sum,
                "borderColor" => 'rgba(0,94,175,0.45)',
                "backgroundColor" => 'rgba(0,94,175,0.45)',
                "fill" => false
            ],
            [
                "label" => $report == "two" ? "Выполнено в срок" : "Назначен исполнитель",
                "data" => $done,
                "yAxisID" => "y-axis-gravity",
                "borderColor" => 'rgba(4,175,0,0.45)',
                "backgroundColor" => 'rgba(4,175,0,0.45)',
                "fill" => false
            ],
            [
                "label" => "В работе",
                "data" => $inwork,
                "yAxisID" => "y-axis-gravity",
                "borderColor" => 'rgba(175,175,0,0.45)',
                "backgroundColor" => 'rgba(175,175,0,0.45)',
                "fill" => false
            ],
            [
                "label" => $report == "two" ? "Не выполнено " : "Не назначен исполнитель",
                "data" => $not_performed,
                "yAxisID" => "y-axis-gravity",
                "borderColor" => 'rgba(175,0,0,0.45)',
                "backgroundColor" => 'rgba(175,0,0,0.45)',
                "fill" => false
            ], [
                "label" => "Выполнено не в срок",
                "data" => $blue,
                "yAxisID" => "y-axis-gravity",
                "borderColor" => 'rgba(0,30,200,0.45)',
                "backgroundColor" => 'rgba(0,30,200,0.45)',
                "fill" => false
            ]
        ]],
        "options" => ["title" => [
            "display" => true,
            "text" => $graf == 'line' ? "Линейный график" : "Гистограмма график"
        ]
        ],
        "data_job" => ["job" => $job_n, "length" => count($job_n)]
    ];
    if ($report == "one") {
        array_splice($data_all['data']['datasets'], 4, 1);
        array_splice($data_all['data']['datasets'], 2, 1);
    }
    echo json_encode($data_all, JSON_UNESCAPED_UNICODE);
};

if (isset($_POST['action']) and $_POST['action'] == "report_all") {
    // DB table to use
    $platform_ext = $_POST['platform'];

    $table = 'collector';

// Table's primary key
    $primaryKey = 'id';


    $columns = array(
        array('db' => 'id', 'dt' => 'id','field'=>'id'),
        array('db' => 'job', 'dt' => 'job' ,'field'=>'job', 'formatter' => function ($d, $row) {
            return GetAllvid[$d];
        }),
        array('db' => 'job', 'dt' => 'job_n','field'=>'job'),
        array('db' => 'platform',
            'dt' => 'platform','field'=>'platform',
            'formatter' => function ($d, $row) {
                return platforms_user[$d];
            }),
        array('db' => 'actions', 'dt' => 'actions','field'=>'actions'),
        array('db' => 'recommendations', 'dt' => 'recommendations','field'=>'recommendations'),
        array('db' => 'date-plan',
            'dt' => 'date_plan','field'=>'date-plan',
            'formatter' => function ($d, $row) {

                return $d != 0 ? date("Y-m-d", $d) : "";
            }),
        array('db' => 'fact-date','field'=>'fact-date',
            'dt' => 'date_fact',
            'formatter' => function ($d, $row) {
                return $d != 0 ? date("Y-m-d", $d) : "";
            }),
        array('db' => 'fio','field'=>'fio',
            'dt' => 'fio',
            'formatter' => function ($d, $row) {
                return array_key_exists($d, getUserNameInPlatform(false, true)) ? getUserNameInPlatform(false, true)[$d] : '';//испрвить
            }),
        array('db' => 'current-status-compliance','field'=>'current-status-compliance',
            'dt' => 'current-status-compliance',
            'formatter' => function ($d, $row) {
//                $stats_match =["","Более 90%","От 71% до 90%","От 40% до 70%","Менее 40%","N/A"] ;
                return status_completed[$d];//испрвить
            }),
    );
// SQL server connection information
//    $sql_details = array(
//        'user' => 'root',
//        'pass' => 'sis1801emp',
//        'db' => 'Matrix_test',
//        'host' => '127.0.0.1',
//        'port' => '3307'
//    );


    $whereAll = "`status-compliance`<>5 
  AND `status-compliance`<>0 
  AND `status-compliance`<>1 ";

    if ($platform_ext != '') {
        $whereAll .= " AND platform in($platform_ext) ";
    }

    echo json_encode(SSP::simple($_POST, $table, $primaryKey, $columns, null, $whereAll));
}


function random_html_color()
{
    return sprintf('#%02X%02X%02X', rand(0, 255), rand(0, 255), rand(0, 255));
}