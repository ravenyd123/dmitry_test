<?php
/**
 * Created by PhpStorm.
 * User: ilya
 * Date: 03.07.2019
 * Time: 15:31
 */

include_once $_SERVER['DOCUMENT_ROOT'] . '/assets/DB.php';


$db = DB::Connection();

/* Авторизация */
if ((isset($_POST['login'])) AND (isset($_POST['password']))) {
  $login = $_POST['login'];
  $pass = $_POST['password'];
  $userdata = $db->query("SELECT `id`, `login`, `password`, `name`, `surname`, `father_name`, `email`, `platform`, `rols`  FROM users WHERE `login` = '".$login."' AND `password` = '".$pass."' LIMIT 1")->fetch_assoc();
  if ($userdata) {
    echo json_encode($userdata,JSON_FORCE_OBJECT);
  } else {
    echo "0";
  }
};


/* Синхронизация. Получение данных от сервера */
if ((isset($_POST['action'])) AND (isset($_POST['authdata']))) {
//$_POST['authdata'] = "d7696e1ddfcd2fa4bf4c5309f1152e1f.18baccd906d74a861e0a42f86eaf8bb7";
$authdata = explode(".", $_POST['authdata']);
$veryfync = $db->query("SELECT `id` FROM users WHERE MD5(MD5(`login`)) = '".$authdata[0]."' AND `password` = '".$authdata[1]."' LIMIT 1")->fetch_assoc();
if ($veryfync<>NULL){
    if ($_POST['action'] == "dloadNearMissFull"){
        $datanearmiss = $db->query("SELECT `id_tel`, `industry`, `actionIn`, `actionDo`, `status`, `typeNear`, `createDate`, `risk`, `dateValidation`, `platform`, `pl_comment` FROM nearmiss WHERE `id_`='".$veryfync['id']."'");
        $datasend = array();
        $K=0;
        foreach ($datanearmiss as $kay => $value){
            $datasend[$K]['id_tel'] = $value['id_tel'];
            if($value['industry']==NULL) {
                $datasend[$K]['industry']=0;
            } else {
                $datasend[$K]['industry'] = $value['industry'];
            }
            
            $datasend[$K]['actionIn'] = $value['actionIn'];
            $datasend[$K]['actionDo'] = $value['actionDo'];
            $datasend[$K]['status'] = $value['status'];
            $datasend[$K]['typeNear'] = $value['typeNear'];
            $datasend[$K]['createDate'] = $value['createDate'];
            $datasend[$K]['risk'] = $value['risk'];
            $datasend[$K]['dateValidation'] = $value['dateValidation'];
            $datasend[$K]['platform'] = $value['platform'];
            if ($value['pl_comment']==NULL) {
                $datasend[$K]['pl_comment']=' ';
            } else {
                $datasend[$K]['pl_comment'] = $value['pl_comment'];
            }
            $K++;
        }
       
        echo  json_encode($datasend, JSON_UNESCAPED_UNICODE | JSON_HEX_TAG);
    }
    if ($_POST['action'] == "dloadNearMissSync"){
        // $datanearmiss = $db->query("SELECT `id`, `id_tel`, `status`, `risk`, `dateValidation`, `platform`, `pl_comment` FROM nearmiss WHERE `id_iniciator`='".$veryfync['id']."' AND `flag`=1");
        $query="SELECT `id`, `id_tel`, `status`, `risk`, `dateValidation`, `platform`, `pl_comment` FROM nearmiss WHERE `flag`=1";
        $datanearmiss = $db->query($query);
        $datasend = array();
        $K=0;
        foreach ($datanearmiss as $kay => $value){
            $updateflag = $db->query("UPDATE `nearmiss` SET `flag`=0 WHERE `id`='".$value['id']."' LIMIT 1");
            $datasend[$K]['id_tel'] = $value['id_tel'];
            $datasend[$K]['status'] = $value['status'];
            $datasend[$K]['risk'] = $value['risk'];
            $datasend[$K]['dateValidation'] = $value['dateValidation'];
            $datasend[$K]['platform'] = $value['platform'];
            $datasend[$K]['pl_comment'] = $value['pl_comment'];
            $K++;
        }
        echo  json_encode($datasend, JSON_UNESCAPED_UNICODE | JSON_HEX_TAG);
     //   echo $datasend;
     //   echo $query;
       // echo "rtrrttrrtrtrtrtrtrtrtrtrtr";

    }
} else {
   // echo "trrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr";
    return 0; // Нет такого пользователя в базе
}

}

/* Синхронизация NearMiss (Mobile=>Server) */
if (isset($_POST['data'])) {
    //$data= json_decode(file_get_contents('test.json'));
    $data = json_decode($_POST['data']);

    $query = "INSERT HIGH_PRIORITY INTO nearmiss(id_tel, id_iniciator, industry, actionIn, actionDo, typeNear, risk, photo, createDate,platform,pl_comment,typeinput,adjustment)
  VALUES (?,?,?,?,?,?,?,?,?,?,?,1,1)";
    $stmt = $db->prepare($query);
    $query1 = "Select id from nearmiss";
    $prov = $db->prepare($query1);

    if (!($stmt = $db->prepare($query))) {
        echo "Не удалось подготовить запрос: (" . $db->errno . ") " . $db->error;
    };
    $data_return = [];
    foreach ($data as $nearmiss) {
//    var_dump(strtotime($nearmiss->Datavalidation));
//var_dump(date("Y-m-d H:i:s",strtotime($nearmiss->Datavalidation)));
        if (prov($nearmiss->Id, $nearmiss->Userid)) {
            $stmt->bind_param('iisssiissis',
                $nearmiss->Id,
                $nearmiss->Userid,
                $nearmiss->Industry,
                $nearmiss->ActionIn,
                $nearmiss->ActionDo,
                $nearmiss->TypeNear,
                $nearmiss->Risk,
//           $nearmiss->Geolocation,
                $nearmiss->Img,
                $nearmiss->createDate,
                $nearmiss->Platform,
                $nearmiss->pl_comment
            );

            if (!$stmt->execute()) {
                $status['type'] = 'error';
                $status['message'] = 'Произошла ошибка';
                $status['error'] = $stmt->error;
                print_r(json_encode($status, JSON_UNESCAPED_UNICODE));
                return;
            } else {
                $data_return[] = ["id" => $nearmiss->Id, "status" => 2];
            }
        } else {
            $data_return[] = ["id" => $nearmiss->Id, "status" => 1];
        }
    }
    echo json_encode($data_return);
};

function prov($id, $id_iniciator)
{
    $id = intval($id);
    $id_iniciator = intval($id_iniciator);
    $db = Db::Connection();
    $res = $db->query("Select id from nearmiss WHERE id_tel={$id} and id_iniciator={$id_iniciator}");
    return $res->num_rows == 0 ? true : false;
};

