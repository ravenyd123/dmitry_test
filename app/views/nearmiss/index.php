
<? include_once $_SERVER['DOCUMENT_ROOT'] . '/app/views/header.php'; ?>
<? include_once 'modal.php'; ?>
<? include_once $_SERVER['DOCUMENT_ROOT'] . '/app/views/template.php';
?>
 
   

    <!--Main Content -->
    <section class="content" id="nearmiss">

        <!-- Page Content -->

        <div class="wraper container-fluid">
            <div class="row">
                <div class="col-md-12 col-sx-12">
                    <div class="panel panel-default w-100">
                        <div class="panel-heading">
                            <h3 class="panel-title pull-left m-t-10">Near Miss</h3>
                            <div class="top_nav">
                                <? if (in_array("C", $access['tasks'])){ ?>
                                        <button data-toggle="tooltip" title="Добавить нарушение" class="btn btn-success" data-action="add-data"><i class="ion-plus"></i>
                                        </button>
                                    <br>
                                <? } ?>
                                <? if (in_array("U", $access['tasks'])) { ?>
                                    <button data-toggle="tooltip" title="Редактировать нарушение" class="btn btn-default" data-action="edit-data"><i class="ion-edit"></i>
                                    </button><br>
                                <? } ?>

                                <? if (in_array("R", $access['tasks'])) { ?>
                                    <button data-toggle="tooltip" title="Загрузить реестр нарушений" class="btn btn-warning btn-sm" data-action="loadcsv"><i class="ion-archive "></i>
                                    </button><br>
                                <? } ?>

                                 <?  if (in_array("R", $access['tasks'])) { ?>
                                    <button data-toggle="tooltip" title="Открыть календарь по Near Miss" class="btn btn-inverse btn-sm" data-action="calendar-nearmiss"><i class="ion-calendar"></i>
                                    </button><br>
                                <? } ?> 

                                <? if (in_array("R", $access['tasks'])) { ?>
                                    <button data-toggle="tooltip" title="Получить QR-код для работы в мобильном приложении" class="btn btn-pink" data-action="qr_code"><i
                                                class="ion-qr-scanner"></i>
                                    </button><br>
                                <? } ?>

                                <? if (in_array("D", $access['tasks'])) { ?>
                                    <button data-toggle="tooltip" title="Удалить нарушение" class="btn btn-default" data-action="remove-data"><i
                                                class="ion-trash-a"></i>
                                    </button>
                                <? } ?>

                                

                                <form class="hidden" name="uploader" enctype="multipart/form-data" method="POST">
                                    <input name="userfile" type="file" accept=".csv"/>
                                    <button type="submit" name="submit">Загрузить</button>
                                </form>
                                
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-body">
                            <div class="row m-b-15">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Отчеты <span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu" role="menu">
                                            <li>
                                                <a href="./nearmiss/prolog1" data-action="prilog1" target="_self">Отчет выполнения рейтингов ЦМК</a>
                                            </li>
                                            <li>
                                                <a href="./nearmiss/prolog2"  data-action="prilog2" target="_self">Статистика выполнения</a>
                                            </li>
                                            <li>
                                                <a href="./nearmiss/prolog3" data-action="prilog4"  target="_self">Статистика регистрации</a>
                                            </li>
                                            <li>
                                                <a href="./nearmiss/prolog4" data-action="prilog3"  target="_self">Реестр нарушений</a>
                                            </li>
                                        </ul>
                                    </div>
                                    <button type="button" class="btn btn-success" data-action="onlinestat">Онлайн статистика</button>
                                </div>
                            </div>
                        <div class="panel-body">
                            <div class="row m-b-15"> 
                                <div class="" id="datatable-filter">
                                    <div class="col-md-2 dev">
                                        <label>Источник</label>
                                    </div>

                                    <div class="col-md-2 pl">
                                        <label>Место нарушения</label>
                                    </div>

                                    <div class="col-md-2 inic">
                                        <label>Инициатор</label>
                                    </div>
                                    
                                    <div class="col-md-3 ot">
                                        <label>Опасн. условия/действия</label>
                                    </div>

                                    <div class="col-md-2 harnv">
                                        <label>Хар-ки NM</label>
                                    </div>

                                    <div class="col-md-2 otetstev">
                                        <label>Ответственный</label>
                                    </div>

                                    <div class="col-md-2 st">
                                        <label>Статус</label>
                                    </div>
                                   <!-- <div class="col-md-2 kor">
                                        <label>Проверка?</label>
                                    </div> -->
                                </div>

                            </div>
                        
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <table id="datatable" class="table table-hover table-striped">
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <!-- End Row -->

        </div>

    </section>
    <style>
        .selected {
            background-color: #d9edf7 !important;
        }
    </style>
    <script>

//        var data_recurs =<?php //echo json_encode(recursion(true))?>//;

    </script>
    <script type="text/javascript" src="/assets/admina/js/de_datetime.js"></script>
    <script type="text/javascript" src="/app/views/nearmiss/js/nearmiss.js"></script>
    <script type="text/javascript" src="/app/views/nearmiss/js/upload.js"></script>
    <script src="/node_modules/chart.js/dist/Chart.min.js"></script>

<? include 'footer.php'; ?>
