<?php
include_once $_SERVER['DOCUMENT_ROOT'] . '/assets/DB.php';
include_once $_SERVER['DOCUMENT_ROOT'] . '/assets/mail.php';
$db = DB::Connection();

/* Авторизация */
if ((isset($_POST['action'])) AND ($_POST['action']=="authorization")){
	if ((isset($_POST['login'])) AND (isset($_POST['password']))) {
		$login = $_POST['login'];
  		$pass = $_POST['password'];
  		$ip = $_POST['ip'];
  		$query = "SELECT `id`, `login`, `password`, `name`,
  						 `surname`, `father_name`, `email`  
  				  FROM users 
  				  WHERE `login` = '".$login."' AND
  				        `password` = '".$pass."' LIMIT 1";
  		$userdata = $db->query($query)->fetch_assoc();
  		if ($userdata) {
      		$hash = md5(generateCode(10)); // генерируем сессию
      		$now = date("Y-m-d H:i:s"); // Дата сейчас
      		$end = date("Y-m-d H:i:s",strtotime("+1 days")); // дата через сутки, дата окончания сессиии
  			$queryinsert = "INSERT INTO `sessions` SET
  					  `session`= '".$hash."',
  					  `login` = '".$login."', 
  					  `date_begin` = '".$now."',
  					  `date_end` = '".$end."',
  					  `ip` = '".$ip."',
  					  `user_agent` = 'Mobile'";
  			$insertdata = $db->query($queryinsert);
  			echo json_encode($userdata,JSON_FORCE_OBJECT);

  		} else {
  			echo "0";
  		}	
	}
}


/* Первичная загрузка Nearmiss, если новая установка или мобильник сменили */
if ((isset($_POST['action'])) AND ($_POST['action']=="GetPrimaryLoadNearMissMy")){
	$userid = $_POST['userid'];
	$query = "SELECT `id_tel`,
					 `job`,
					 `actionIn`,
					 `actionDo`,
					 `status`+0 AS `status`,
					 `createDate`,
					 `dateValidation`,
					 `platform`,
					 `pl_comment`
  			  FROM `nearmiss` 
  			  WHERE `id_iniciator` = '".$userid."' AND
  				    `adjustment` = 0";
  	$userdata = $db->query($query);
  	if ($userdata) {
  		$dataOutNearmiss = array();
  		  foreach ($userdata as $key => $value) {
  		  	if (!isset($value['id_tel'])) {
  		  		$value['id_tel']=-1; // костыль. Если нет id телефона, то мобилка крашится
  		  	}
			$dataOutNearmiss[$key]=$value;
  		  }
  		  echo json_encode($dataOutNearmiss);
  		// echo $query;
	}
}

/* Первичная загрузка Nearmiss, которая адресованна мне, если новая установка или мобильник сменили */
if ((isset($_POST['action'])) AND ($_POST['action']=="GetPrimaryLoadNearMissToMe")){
	
	$dataOutNearmiss = "";
  	
  		  echo json_encode($dataOutNearmiss);
	
}


/* Отправка новых NearMiss на мобилке при синхронизации */
if ((isset($_POST['action'])) AND ($_POST['action']=="setNearMisstoServer")){
	$data = json_decode($_POST['data'], true);
	foreach ($data as $nearmissinsert){
		$query = "SELECT `id` FROM `nearmiss` 
				  WHERE `id_tel`= ".$nearmissinsert['Id']." 
				  AND `id_iniciator` = ".$nearmissinsert['Userid']."
				  AND `typeinput`= 1";
		$datanearmiss = $db->query($query)->fetch_assoc();
	    $data_return = [];
		if ($datanearmiss) {
			 $data_return[] = ["id" => $nearmissinsert['Id'], "status" => 1];
		} else {
			$charNM = intval($nearmissinsert['CharacteristicsNM']) + 1;
			$queryinsert = "INSERT INTO `nearmiss` SET
	  					  `id_tel`= '".$nearmissinsert['Id']."',
	  					  `id_iniciator` = '".$nearmissinsert['Userid']."', 
	  					  `job` = '".$nearmissinsert['Industry']."',
	  					  `actionIn` = '".$nearmissinsert['ActionIn']."',
	  					  `actionDo` = '".$nearmissinsert['ActionDo']."',
	  					  `status`= '".$nearmissinsert['Status']."',
	  					  `characteristicsNM` = '".$charNM."', 
	  					  `createDate` = '".$nearmissinsert['createDate']."',
	  					  `geolocation` = '0',
	  					  `platform` = '".$nearmissinsert["Platform"]."',
	  					  `flag` = 0,
	  					  `pl_comment` = '".$nearmissinsert['pl_comment']."',
	  					  `typeinput` = 1,
	  					  `photo` = '".$nearmissinsert['Img']."',
	  					  `adjustment` = 0";

	  		$userdata = $db->query($queryinsert);
	  		$data_return[] = ["id" => $nearmissinsert['Id'], "status" => 2];
	  		//sendMailForeman("burge004@mail.ru");
            //sendMailForeman("viktor.korovkin@mail.ru");
            sendMailForeman("ravenyd123@mail.ru", $nearmissinsert["Platform"], $nearmissinsert['Userid']);
		}
	}

	//echo $queryinsert;
	echo json_encode($data_return);
}

/* Отправка новых и измененных NearMiss на мобилку */
if ((isset($_POST['action'])) AND ($_POST['action']=="getNearMisstoMobile")){
	$id_iniciator = $_POST['id_iniciator'];
	$query = "SELECT `id`,
					 `id_tel`,
					 `industry`,
					 `actionIn`,
					 `actionDo`,
					 `status`,
					 characteristicsNM+0 AS charMN,
					 `createDate`,
					 `dateValidation`,
					 `platform`,
					 `pl_comment`
  			    FROM `nearmiss` 
  			   WHERE `id_iniciator` = '".$id_iniciator."' AND
  		 		    `adjustment` = 0 AND
  		  		    `flag` = 1";
  	$userdata = $db->query($query);
  	if ($userdata) {
  		$dataOutNearmiss = array();
  		  foreach ($userdata as $key => $value) {
  		  	$value['charMN'] = $value['charMN']-1;
  		  	if (!isset($value['id_tel'])) {
  		  		$value['id_tel']=-1; // костыль. Если нет id телефона, то мобилка крашится
  		  	}
			$dataOutNearmiss[$key]=$value;
			$queryUpdate = "UPDATE `nearmiss` SET `flag`=0 WHERE `id`=".$value['id'];
			$db->query($queryUpdate);
  		  }
  		  echo json_encode($dataOutNearmiss);
	}
}


/* Получаем ответ с присвоенными id_tel на мобилке по новым NearMiss */
if ((isset($_POST['action'])) AND ($_POST['action']=="SetIdTelToServer")){
	$id = $_POST['id'];
	$id_tel = $_POST['id_tel'];
	$queryUpdate = "UPDATE `nearmiss` SET `id_tel`= ".$id_tel." WHERE `id`=".$id." LIMIT 1";
	$UpdateIdTel = $db->query($queryUpdate);
	if ($UpdateIdTel) {
		echo $queryUpdate;
	} else {
		echo $queryUpdate;
	}
}

/* Ответ при проверке сервера */
if ((isset($_POST['action'])) AND ($_POST['action']=="chekServer")){
	$_SERVER['PROTOCOL'] = isset($_SERVER['HTTPS']) && !empty($_SERVER['HTTPS']) ? 'https://' : 'http://';
	$kod = md5(md5($_SERVER['PROTOCOL'].$_SERVER['SERVER_NAME']));
	echo $kod;
}
function generateCode($length = 6)
{
    $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPRQSTUVWXYZ0123456789";
    $code = "";
    $clen = strlen($chars);
    while (strlen($code) <= $length) {
        $code .= $chars[mt_rand(0, $clen)];
    }
    return $code;
}









/* Получить задачи*/
if ($_POST['action'] == "GetMyTasks"){
	$id = $_POST['userid'];
    $Array = array();
    $actions = $db->query("SELECT `id`, `date`, `name`, `description`, `status`, `platform` FROM `tasks` WHERE `director`={$id} OR `responsible`={$id}");
    foreach ($actions as $value){
        $Array[] = $value;
    }
    echo json_encode($Array, JSON_UNESCAPED_UNICODE | JSON_HEX_TAG);
}


/* Получение NM при его создании на мобилке */
if ((isset($_POST['action'])) AND ($_POST['action']=="SaveNMAndSendToServer")){
	$data = json_decode($_POST['data'], true);
	foreach ($data as $nearmissinsert){
		$queryinsert = "INSERT INTO `nearmiss` SET
	  				  `id_tel`= '".$nearmissinsert['Id']."',
	  				  `id_iniciator` = '".$nearmissinsert['Userid']."', 
	  				  `job` = '".$nearmissinsert['Job']."',
	  				  `actionIn` = '".$nearmissinsert['ActionIn']."',
	  				  `actionDo` = '".$nearmissinsert['ActionDo']."',
	  				  `createDate` = '".$nearmissinsert['CreateDate']."',
	  				  `geolocation` = '".$nearmissinsert['Geolocal']."',
	  				  `platform` = '".$nearmissinsert["Platform"]."',
	  				  `flag` = 0,
	  				  `typeinput` = 1,
	  				  `photo` = '".$nearmissinsert['Img']."',
	  				  `adjustment` = 0";

	  		$userdata = $db->query($queryinsert);
	  		if ($userdata){
	  			$data_return[] = ["id" => $nearmissinsert['Id'], "status" => 2];
	  			//sendMailForeman("burge004@mail.ru");
            	//sendMailForeman("viktor.korovkin@mail.ru");
            	sendMailForeman("ravenyd123@mail.ru", $nearmissinsert["Platform"], $nearmissinsert['Userid']);
       		}
		
	}
	//echo $queryinsert;
	echo json_encode($data_return);
}

/* Получение NM при его создании на мобилке */
if ((isset($_POST['action'])) AND ($_POST['action']=="SaveNMAndSendToServer2")){
	$data = json_decode($_POST['data'], true);
	foreach ($data as $nearmissinsert){
		$queryinsert = "INSERT INTO `nearmiss` SET
	  				  `id_tel`= '".$nearmissinsert['Id']."',
	  				  `id_iniciator` = '".$nearmissinsert['Userid']."', 
	  				  `platform` = '".$nearmissinsert["Platform"]."',
	  				  `actionIn` = '".$nearmissinsert['ActionIn']."',
	  				  `actionDo` = '".$nearmissinsert['ActionDo']."',
	  				  `flag` = 3,
	  				  `typeinput` = 1,
	  				  `photo` = '".$nearmissinsert['Img']."',
	  				  `adjustment` = 0";

	  				  	  		$userdata = $db->query($queryinsert);
	  		if ($userdata){
	  			$data_return[] = ["id" => $nearmissinsert['Id'], "status" => 2];
	  			//sendMailForeman("burge004@mail.ru");
            	//sendMailForeman("viktor.korovkin@mail.ru");
            	//sendMailForeman("ravenyd123@mail.ru", $nearmissinsert["Platform"], $nearmissinsert['Userid']);
       		}
		
	}
	//echo $queryinsert;
	echo json_encode($data_return);
}

/* Проверка новых данных*/
if (isset($_POST['action']) AND ($_POST['action'] == "GetTasks")){
	$id = $_POST['userid'];
    $Array = array();
    $verificationtasks = $db->query("SELECT `id`, `date`, `name`, `description`, `deadline`, `finished`, `director`, `responsible`, `status`, `platform` FROM `tasks` WHERE (`director`={$id} OR `responsible`={$id}) AND `mobil_flag`=1");
    if ($verificationtasks){
    	foreach ($verificationtasks as $key => $tasksValue){
    		$Array[] = $tasksValue;
    	}
    }
    echo json_encode($Array, JSON_UNESCAPED_UNICODE | JSON_HEX_TAG);
}

/* Проверка новых NM*/
if (isset($_POST['action']) AND ($_POST['action'] == "NewNM")){
	$id = $_POST['userid'];

	$query = "SELECT `id`,
					 `actionIn`,
					 `actionDo`
					 
  			    FROM `nearmiss` 
  			   WHERE 
  		 		    `adjustment` = 0 AND
  		  		    `flag` = 3";
//echo $query;

  	$userdata = $db->query($query);
  	if ($userdata) {
  		$dataOutNearmiss = array();
  		  foreach ($userdata as $key => $value) {
  		  	
  		  	if (!isset($value['id_tel'])) {
  		  		$value['id_tel']=-1; // костыль. Если нет id телефона, то мобилка крашится
  		  	}
			$dataOutNearmiss[$key]=$value;
			$queryUpdate = "UPDATE `nearmiss` SET `flag`=0 WHERE `id`=".$value['id'];
			$db->query($queryUpdate);
  		  }
  		  echo json_encode($dataOutNearmiss);
	}
}

?>