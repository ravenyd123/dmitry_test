$(document).ready(function() {

	$('#nearset').select2({
	});

	/* УБираем выбранные элементы у других select */
	 $(document).on('change','select',function() {
          var s = $('select'),
          val = +this.value, data = $(this).data('val');
          s.not(this).each(function(indx, el){
          data &&  $('option[value="'+data+'"]', el).prop('disabled', false)
          val && $('option[value="'+val+'"]', el).prop('disabled', true)
                  });
          $(this).data({val:val});
          $(this).next().val(val);
      });

	/* загрузка отчета Nearmiss */
	$('#loadFile').click(function(){
		$('input[name="userfile"]').trigger('click');
	});

	$('input[name="userfile"]').change(function () {
	    $("form[name='uploader']").submit();
	});

	$("form[name='uploader']").submit(function (e) {
 	    var formData = new FormData($(this)[0]);
 	    $.notify({'type':'info', 'message':'Пожалуйста, подождите. Идет отправка файла на сервер...'});
	    $.ajax({
	        url: '/app/views/nearmiss/upload_file.php',
	        type: "POST",
	        data: formData,
	        async: true,
	        cache: false,
	        success: function (data) {
	            var action = "loadfile";
	            window.open(
	            '/app/views/nearmiss/upload_register.php?file=' + data + '&action=' + action, 
	            '_self'
	            );
	        },
	        error: function (data) {
	            console.log('Ошибка!');
	        },

	        contentType: false,
	        processData: false
	    });
	    e.preventDefault();
	});



	$('#saveSettings').click(function(){

	});

	function $_GET(key) {
    var s = window.location.search;
    s = s.match(new RegExp(key + '=([^&=]+)'));
    return s ? s[1] : false;
	};


	$('#nextImport').click(function(){
		var numberSelect =  $('.selectAttrNearmiss[data-id]').map(function() {
  					return this.dataset.id;
					}).get();
			var dataSelected = [];
			numberSelect.forEach(function(element) {
  				var selectedOption = $("[data-id=" + element + "] option:selected").val();
  				dataSelected[element] = selectedOption;
			});

			var chekedRadio = $('input[name=radiosNearmiss]:checked').attr('data-id')
			if (chekedRadio== null){
				$.notify({'type':'warning', 'message':'Не указана первая строка нарушений'});
			}
			var strGET = $_GET('file') 
			
			$.ajax({
	        url: '/nearmiss/ajax',
	        type: "POST",
	        dataType: "json",
	        data: {
            "action": "fileToNearmiss",
            "radio": chekedRadio,
            "filename": strGET,
            "selected": JSON.stringify(dataSelected)
        	},
	        async: false,
	        cache: false,
	     //   success: function (data) {
	            // console.log(data);
	            //$.notify({'type':'info', 'message':'Отчет реестр NearMiss удачно загружен'});
	          //  window.open('/app/views/nearmiss/index.php','_self');
	           
	      //  },
	      //  error: function (data) {
	      //      console.log('Ошибка!');
	     //       $.notify({'type':'error', 'message':'Ошибка в экспорте реестра'});
	      //  },
	      complete: function (data) {
	      		$.notify({'type':'info', 'message':'Отчет реестр NearMiss удачно загружен'});
	            window.open('/nearmiss','_self');
	        }
	    });

	});

});