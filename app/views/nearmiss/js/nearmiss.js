/**
 * Created by ilya on 02.07.2019.
 */
$(document).ready(function () {
    var imgSrc;

    $("form[name='nearmiss']").on("submit", function (event) {

        event.preventDefault();

        var dataform = getFormData($(this));
        var action = 'nearmissSave';

        remove = ['task_create_description',
            'job',
            'fio',
            'demand',
            'recommendations',
            'date_plan',
            'pl_comment',
            'id_responsible',
            'actionDo',
            'gehs'];//Исключения
        check_form(dataform, remove);

        $.ajax({
            type: 'POST',
            dataType: "text",
            contentType: "application/x-www-form-urlencoded;charset=utf-8",
            data: {[action]: dataform},
            url: "nearmiss/ajax",
            cache: false,
            beforeSend: function () {
                $('button[type=submit]').val('Ожидайте...').attr("disabled", true);
            },
            complete: function (dataout) {
                $('input[name="id_n"]').val(dataout.responseText);
                var fileinp= $('input#input-ru');
                fileinp.fileinput('getFilesCount')>0 ?  fileinp.fileinput("upload") : ''

                $('#nearmiss').modal('hide');
                var task_table = $('#datatable').dataTable().api();
                task_table.ajax.reload();
                $.notify({'type': 'info', 'message': 'Нарушение удачно сохранено'})
                $('button[type=submit]').val('Далее').attr("disabled", false);
                $('#nearmiss_modal').modal('hide');
            },
            error: function () {
                $.notify({'type': 'error', 'message': 'Произошла ошибка!'});
            }
        })
        //  }
    });


// Datatables
    var table = $('#datatable').dataTable({
        lengthChange: false,
        //stateSave: true,
        buttons: ['colvis'],
        "aaSorting": [[1, 'desc']],
        // "sScrollX": "100%",
        "scrollCollapse": true,
        "paging": true,
        "processing": true,
        ajax: {
            type: 'POST',
            dataType: "json",
            url: "nearmiss/ajax",
            data: {"action": "all"},
            cache: false,
            dataSrc: function (json) {
                return json.data
            },
        },
        columnDefs: [
            {
                "title": "",
                "targets": [0],
                "className": "text-center",
                "data": "id",
                "orderable": false,
                "defaultContent": '',
                "width": "1%",
                "visible": false
            },

            {
                "title": "№",
                "targets": [1],
                "className": "text-center",
                "data": "number",
                "width": "1%",
                "visible": false
            },

            {
                "title": "",
                "targets": [2],
                "className": "text-center",
                "orderable": false,
                "defaultContent": '', "width": "10%",
                "visible": false
            },
            {
                "title": " Дата рег-ии",
                "targets": [3],
                "className": "text-center",
                "data": "createDate",
                "type": "de_datetime",
                "width": "1%"
            },
            {
                "title": "Срок исполнения",
                "targets": [4],
                "className": "text-center",
                "data": "deadlineDate",
                "type": "de_datetime",
                "width": "1%",
            },
            {
                "title": "Выполн.",
                "targets": [5],
                "className": "text-center",
                "data": "dateValidation",
                "type": "de_datetime",
                "width": "1%",
                "visible": false
            },
            {
                "title": "Место обнаружения",
                "targets": [6],
                "className": "text-center",
                "data": "platform",
                "width": "5%"
            },
            {
                "title": "Инициатор",
                "targets": [7],
                "className": "text-center",
                "data": "name_iniciator",
                "width": "8%"
            },
            {
                "title": "Нарушение",
                "targets": [8],
                "className": "text-center",
                "data": "actionIn",
                "width": "20%"
            },
            {
                "title": "Опасн. условия / действия",
                "targets": [9],
                "className": "text-center",
                "data": "job_name",
                "width": "5%",
                "visible": true
            },
            {
                "title": "Хар-ки NM",
                "targets": [10],
                "className": "text-center",
                "data": "characteristicsNM",
                "width": "5%",
                "visible": true
            },
            {
                "title": "Ответственный",
                "targets": [11],
                "className": "text-center",
                "data": "name_responsible",
                "width": "5%",
                "visible": true
            },
            {
                "title": "Статус",
                "targets": [12],
                "className": "text-center",
                "data": "status",
                // "visible": false,
                "width": "5%"
            },
            {
                "title": "",
                "targets": [13],
                "className": "text-center",
                "data": "typeinput",
                "visible": false
            },
            {
                "title": "",
                "targets": [14],
                "className": "text-center",
                "data": "adjustment",
                "visible": false
            }

        ],
        "initComplete": function () {
            // $('[data-toggle="tooltip"]').tooltip();
            // table.buttons().container().appendTo('#datatable_wrapper .col-sm-6:eq(0)');
            // this.api().column(3).every(function () {
            //     var column = this;
            //
            //     var select = $('<select class="select3"><option value="">Все</option></select>')
            //         .appendTo($('#datatable-filter div.eh'))
            //         .on('change', function () {
            //             var val = $.fn.dataTable.util.escapeRegex(
            //                 $(this).val()
            //             );
            //             column
            //                 .search(val ? '^' + val + '$' : '', true, false)
            //                 .draw();
            //         })
            //
            //     column.data().unique().sort().each(function (d, j) {
            //         select.append('<option value="' + d + '">' + d + '</option>')
            //     });
            // });
            // this.api().column(4).every(function () {
            //     var column = this;
            //     var select = $('<select class="select3"><option value="">Все</option></select>')
            //         .appendTo($('#datatable-filter div.pl'))
            //         .on('change', function () {
            //             var val = $.fn.dataTable.util.escapeRegex(
            //                 $(this).val()
            //             );
            //
            //             column
            //                 .search(val ? '^' + val + '$' : '', true, false)
            //                 .draw();
            //         })
            //
            //     column.data().unique().sort().each(function (d, j) {
            //         select.append('<option value="' + d + '">' + d + '</option>')
            //     });
            // });
            // this.api().column(9).every(function () {
            //     var column = this;
            //     var select = $('<select class="select3"><option value="">Все</option></select>')
            //         .appendTo($('#datatable-filter div.ot'))
            //         .on('change', function () {
            //             var val = $.fn.dataTable.util.escapeRegex(
            //                 $(this).val()
            //             );
            //
            //             column
            //                 .search(val ? '^' + val + '$' : '', true, false)
            //                 .draw();
            //         })
            //
            //     column.data().unique().sort().each(function (d, j) {
            //         select.append('<option value="' + d + '">' + d + '</option>')
            //     });
            // });

            /* фильтр по требованию корректировки */
            this.api().column(11).every(function () {
                var column = this;
                var select = $('<select class="sel_filter"><option value="">Все</option></select>')
                    .appendTo($('#datatable-filter div.kor'))
                    .on('change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );
                        column
                            .search(val ? '^' + val + '$' : '', true, false)
                            .draw();
                    })

                column.each(function (d, j) {
                    select.append('<option value="1">Да</option>')
                    select.append('<option value="0">Нет</option>')
                });
            });

            /* фильтр по устройствам */
            this.api().column(10).every(function () {
                var column = this;
                var select = $('<select class="sel_filter"><option value="">Все</option></select>')
                    .appendTo($('#datatable-filter div.dev'))
                    .on('change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );
                        column
                            .search(val ? '^' + val + '$' : '', true, false)
                            .draw();
                    })

                column.each(function (d, j) {
                    select.append('<option value="1">Мобильные устройства</option>')
                    select.append('<option value="2">Из файла</option>')
                });
            });

            /* Фильтр по инициатору*/
            this.api().column(7).every(function () {
                var column = this;
                var select = $('<select class="sel_filter"><option value="">Все</option></select>')
                    .appendTo($('#datatable-filter div.inic'))
                    .on('change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );
                        column
                            .search(val ? '^' + val + '$' : '', true, false)
                            .draw();
                    })

                column.data().unique().sort().each(function (d, j) {
                    select.append('<option value="' + d + '">' + d + '</option>')
                });
            });

            /* Фильтр по по месту обнаружения */
            this.api().column(6).every(function () {
                var column = this;
                var select = $('<select class="sel_filter"><option value="">Все</option></select>')
                    .appendTo($('#datatable-filter div.pl'))
                    .on('change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );
                        column
                            .search(val ? '^' + val + '$' : '', true, false)
                            .draw();
                    })

                column.data().unique().sort().each(function (d, j) {
                    select.append('<option value="' + d + '">' + d + '</option>')
                });
            });

            /* Фильтр по статутсу */
            this.api().column(12).every(function () {
                var column = this;
                var select = $('<select class="sel_filter"><option value="">Все</option></select>')
                    .appendTo($('#datatable-filter div.st'))
                    .on('change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );
                        column
                            .search(val ? '^' + val + '$' : '', true, false)
                            .draw();
                    })

                column.data().unique().sort().each(function (d, j) {
                    select.append('<option value="' + d + '">' + d + '</option>')
                });
            });

            /* Фильтр по видам работ */
            this.api().column(9).every(function () {
                var column = this;
                var select = $('<select class="sel_filter"><option value="">Все</option></select>')
                    .appendTo($('#datatable-filter div.ot'))
                    .on('change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );
                        column
                            .search(val ? '^' + val + '$' : '', true, false)
                            .draw();
                    })

                column.data().unique().sort().each(function (d, j) {
                    select.append('<option value="' + d + '">' + d + '</option>')
                });
            });

            /* Фильтр по характеристикам N-nm */
            this.api().column(10).every(function () {
                var column = this;
                var select = $('<select class="sel_filter"><option value="">Все</option></select>')
                    .appendTo($('#datatable-filter div.harnv'))
                    .on('change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );
                        column
                            .search(val ? '^' + val + '$' : '', true, false)
                            .draw();
                    })

                column.data().unique().sort().each(function (d, j) {
                    if (d != null) {
                        select.append('<option value="' + d + '">' + d + '</option>')
                    }
                });
            });

            /* Фильтр по ответственным */
            this.api().column(11).every(function () {
                var column = this;
                var select = $('<select class="sel_filter"><option value="">Все</option></select>')
                    .appendTo($('#datatable-filter div.otetstev'))
                    .on('change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );
                        column
                            .search(val ? '^' + val + '$' : '', true, false)
                            .draw();
                    })

                column.data().unique().sort().each(function (d, j) {
                    select.append('<option value="' + d + '">' + d + '</option>')
                });
            });

            $('select.sel_filter').select2();
        },
        "createdRow": function (row, data, dataIndex) {
            // $(row).find("td").eq(0).append('<span>');
            // if (data['id_self'] > 0) {
            //     $(row).find("td").eq(0).append('<li title="По нарушению создан календарь" class="glyphicon glyphicon-calendar" aria-hidden="true"></li>');
            // }
            // if (data['photo'] > 0) {
            //     $(row).find("td").eq(0).append('<li title="В нарушении присутствует фотография" class="glyphicon glyphicon-picture" aria-hidden="true"></li>');
            // }
            // if (data['typeinput'] == 2) {
            //     $(row).find("td").eq(0).append('<li title="Нарушение загружено из файла" class="glyphicon glyphicon-open-file" aria-hidden="true"></li>');
            // }
            // if (data['typeinput'] == 1) {
            //     $(row).find("td").eq(0).append('<li title="Нарушение загружено из мобильного устройства" class="glyphicon glyphicon-phone" aria-hidden="true"></li>');
            // }
            //  if (data['adjustment'] == 1) {
            //     $(row).find("td").eq(0).append('<li title="Требуется корректировка!" class=" text-danger glyphicon glyphicon-pencil" aria-hidden="true"></li>');
            // }
            // $(row).find("td").eq(0).append('</span>');
            $(row).attr("data-id", data['id']);
            // $(row).attr("adjustment", data['adjustment']);

            if (data['id_status'] == 7) {
                $(row).addClass("text-danger");
            }
            if (data['id_status'] == 5) {
                $(row).addClass("text-success");
            }
        },
        "language": {
            "processing": "Производиться загрузка данных",
            "lengthMenu": "Выводить по _MENU_ записей",
            "zeroRecords": "Ни одной записи не найдено",
            "info": "Показано страниц _PAGE_ из _PAGES_",
            "infoEmpty": "No records available",
            "infoFiltered": "(отфильтровано из _MAX_ записей)",
            "search": "Поиск:",
            "paginate": {
                "first": "Первая",
                "last": "Последняя",
                "next": "Следующая",
                "previous": "Предыдущая"
            },
        }
    }).api()

    $.fn.dataTable.ext.search.push(
        function (settings, data, dataIndex) {
            //  return data['8'] != 'Архив';
            return data['8'];
            // console.log($(table.row(dataIndex)));
            // return $(table.row(dataIndex).node()).attr('data-user') == 5;
        }
    );
    table.draw();

    $('#datatable tbody').on('click', 'tr', function () {
        $(this).toggleClass('selected');
        checkRowAmount_new();
    });

// Кнопка добавления записи
    $('*[data-action="add-data"]').click(function () {
        clear();
        fileinput()
        // $('input#input-ru').fileinput('destroy').hide();

        var Data = new Date().toLocaleString();
        //  var Year = Data.getFullYear();
        //   var Month = Data.getMonth();
        //  var Day = Data.getDate();
        //  var Hour = Data.getHours();
        //  var Minutes = Data.getMinutes();
        //  var datastring = Day + "."+ Month + "." + Year + " " + Hour + ":" + Minutes;

        $('b#info').addClass('hidden');
        $('span#title').text('Создать Near Miss');
        $('select[name="status"]').prop("disabled", false);
        $('select[name="status"]').select2('val', '1').trigger('change');
        $('select[name="gehs"]').select2('val', '0').trigger('change');
        $('select[name="id_iniciator"]').select2('val', getCookie("id")).trigger('change');

        $('label#labelleft').text("Осталось до устранения:");
        $('select[name="id_iniciator"]').prop("disabled", false);
        $('textarea#pl_comment').prop("disabled", false);
        $('input#createDate').prop("disabled", false);

        $('input#createDate').val(Data);
        $('select[name="characteristicsNM"]').prop("disabled", false);
        $('select[name="platform"]').prop("disabled", false);
        $('textarea#actionDo').prop("disabled", false);
        $('textarea#actionIn').prop("disabled", false);
        $('input#date_plan').prop("disabled", false);
        $('select[name="job"]').prop("disabled", false);
        $('select[name="id_responsible"]').prop("disabled", false);
        $('textarea#demand').prop("disabled", false);
        $('textarea#recommendations').prop("disabled", false);

        $('button[data-action="addToCallendar"]').addClass('hidden');
        $('#nearmiss_modal').modal();
    })

//  Кнопка Согласования 
    $('*[data-action="approve"]').click(function () {

        $('select[name="status"]').select2('val', '5').trigger('change');
        $('button[data-action="approve"]').addClass('hidden');
        $('button[data-action="notapprove"]').addClass('hidden');
        var id = $("#datatable tr.selected").attr('data-id');
        $.ajax({
            type: 'POST',
            dataType: "json",
            data: {
                "action": "approve",
                "id": id
            },
            url: "app/views/nearmiss/ajax",
            cache: false,
            complete: function (dataout) {
                var task_table = $('#datatable').dataTable().api();
                task_table.ajax.reload();
                $.notify({'type': 'info', 'message': 'Устранение нарушения согласованно'});
                $('#nearmiss_modal').modal('hide');
            },
            error: function (jqXHR) {
                console.log(jqXHR)
            }
        })
    });


//  Кнопка НеСогласования 
    $('*[data-action="notapprove"]').click(function () {
        $('button[data-action="notapprove"]').addClass('hidden');
        $('button[data-action="approve"]').addClass('hidden');
        $('#reason_approve').modal();
    })

//  Кнопка qr-code
    $('*[data-action="qr_code"]').click(function () {

        $('#qrcode_modal').modal();
    })

// Печать QR кода 
    $('*[data-action="qrcodeprint"]').click(function () {
        Popup($(qrcodeforpront).html());
    })


// несогласование //

    $('*[data-action="savereason"]').click(function () {
        var id = $("#datatable tr.selected").attr('data-id');
        var textreason = $('textarea#reasontext').val();
        $.ajax({
            type: 'POST',
            dataType: "json",
            data: {
                "action": "notapprove",
                "id": id,
                "text": textreason
            },
            url: "app/views/nearmiss/ajax",
            cache: false,
            complete: function (dataout) {
                var task_table = $('#datatable').dataTable().api();
                task_table.ajax.reload();

                $('select[name="status"]').select2('val', '3').trigger('change');
                $('#reason_approve').modal('hide');
                $('#nearmiss_modal').modal('hide');
                $.notify({'type': 'warning', 'message': 'Не согласовано выполнение'});
            },
            error: function (jqXHR) {
                console.log(jqXHR)
            }


        });
    });

// Кнопка добавления нарушения в календарь
    $('*[data-action="addToCallendar"]').click(function () {
        // var id_iniciator = $('select[name="id_iniciator"]').select2('val');
        // var platform =  $('select[name="platform"]').select2('val');
        // var actionIn = $('textarea#actionIn').text();
        // var actionDo = $('textarea#actionDo').text();
        // var plandate = $('input#date_plan').val();
        // var job = $('select[name="job"]').select2('val');
        // var id_responsible = $('select[name="id_responsible"]').select2('val');
        // var demand = $('textarea#demand').val();
        // var recommendations = $('textarea#recommendations').val();

        event.preventDefault();
        var dataformCrt = getFormData($('#nearmiss'));
        removeCrtCallendar = ["actionDo", "recommendations", "pl_comment", "demand", "gehs"];
        //   if (($('select[name="platform"]').select2('val') == '')
        //     || ($('select[name="platform"]').select2('val')== '')
        //    || ($('textarea#actionIn').text()== '') 
        // //    || ($('textarea#actionDo').text()== '')
        //     || ($('input#date_plan').val()== '')
        //      || ($('select[name="job"]').select2('val')== '')
        //      || ($('select[name="id_responsible"]').select2('val')== '')
        //      || ($('textarea#demand').val()== '')) {
        //      $.notify({'type':'error', 'message':'Не все обязательные поля заполнены'});
        //    } else {
        check_form(dataformCrt, removeCrtCallendar);

        var a = 0;
        if ($('input#date_plan').val() == '') {
            $('input#date_plan').addClass('has-error');
            a++;
        } else {
            $('input#date_plan').removeClass('has-error');
        }

        if (($('select[name="job"]').select2('val') == '0') || ($('select[name="job"]').select2('val') == null)) {
            $('#jobinput').addClass('has-error');
            console.log("Нет видов работ");
            a++;
        } else {
            $('#jobinput').removeClass('has-error');
        }

        if (($('select[name="characteristicsNM"]').select2('val') == '0') || ($('select[name="characteristicsNM"]').select2('val') == null)) {
            $('#charHM').addClass('has-error');
            console.log("Нет видов работ");
            a++;
        } else {
            $('#charHM').removeClass('has-error');
        }

        if ($('select[name="id_responsible"]').select2('val') == null) {
            $('#responsibleinput').addClass('has-error');
            console.log("Нет Ответственного");
            a++;
        } else {
            $('#responsibleinput').removeClass('has-error');
        }
        if (a == 0) {

            var action = 'nearmissSave';

            check_form(dataformCrt, removeCrtCallendar);

            //if (check_form(dataform, remove)) {

            $.ajax({
                type: 'POST',
                dataType: "text",
                contentType: "application/x-www-form-urlencoded;charset=utf-8",
                data: {[action]: dataformCrt},
                url: "nearmiss/ajax",
                cache: false,
                beforeSend: function () {
                    $('button[type=submit]').val('Ожидайте...').attr("disabled", true);
                },
                complete: function (dataout) {

                    var action = 'addToCallendar';
                    var id = $('input[name="id_n"]').val();
                    $.ajax({
                        type: 'POST',
                        dataType: "text",
                        contentType: "application/x-www-form-urlencoded;charset=utf-8",
                        data: {
                            'action': action,
                            'id': id
                        },
                        url: "nearmiss/ajax",
                        cache: false,
                        beforeSend: function () {
                            $('button[type=submit]').val('Ожидайте...').attr("disabled", true);
                            $('button[data-action="addToCallendar"]').prop("disabled", true);
                        },
                        complete: function (dataout) {
                            //$('#nearmiss').modal('hide');
                            var task_table = $('#datatable').dataTable().api();
                            task_table.ajax.reload();
                            $.notify({'type': 'info', 'message': 'Календарь создан'});
                            $('button[type=submit]').val('Далее').attr("disabled", false);
                            // $('#nearmiss_modal').modal('hide');
                            $('button[data-action="addToCallendar"]').prop("disabled", false);
                        },
                        error: function () {
                            $.notify({'type': 'error', 'message': 'Произошла ошибка!'});
                            $('button[data-action="addToCallendar"]').prop("disabled", false);
                        }
                    })


                    $.notify({'type': 'info', 'message': 'Нарушение удачно сохранено'})
                    $('button[type=submit]').val('Далее').attr("disabled", false);
                    $('#nearmiss_modal').modal('hide');
                },
                error: function () {
                    $.notify({'type': 'error', 'message': 'Произошла ошибка!'});
                }
            })


//}
        }
    });

// Разворот картинки
    $('*[data-action="FullImages"]').click(function () {
        $("img#fullphoto").attr("src", imgSrc);
        $('#nmPhoto').modal();

// Закртытие второго модального окна с полной картинкой
        $('*[data-action="closefullimg"]').click(function () {
            $('#nmPhoto').modal('hide');
            $('#nearmiss_modal').css({'overflow-y': 'auto'}); // костыль чтобы скролл в первом модальном остался
        });

    });
// Кнопка редактирования записи
    $('*[data-action="edit-data"]').on('click', function () {

        var id = $("#datatable tr.selected").attr('data-id');

        clear();
        if (checkRowAmount_new() != 1) {
            $.notify({'type': 'error', 'message': 'Можно выбрать только одну запись!'});
            return false;
        }
        ;
        $.ajax({
            type: 'POST',
            dataType: "json",
            data: {
                "action": "one",
                "id": id,
            },
            url: "/nearmiss/ajax",
            cache: false,
            success: function (jqXHR, textStatus, errorThrown) {
                var nearmiss = jqXHR.data;

                var options = {
                    year: 'numeric',
                    month: 'numeric',
                    day: 'numeric',
                    hour: 'numeric',
                    minute: 'numeric',
                    second: 'numeric'
                };
                // console.log(nearmiss);

                dat = new Date(nearmiss.createDate);

                var adjustment = $("#datatable tr.selected").attr('adjustment');
                $('span#title').text('Редактирование NearMiss');
                if (nearmiss.reason != null) {
                    $('#infoapproveblock').removeClass('hidden');
                    $('#infoapprove').text(nearmiss.reason);
                } else {
                    $('#infoapproveblock').addClass('hidden');
                }
                // $('p#subdivision').text(nearmiss.subdivision);
                $('p#subdivision').text(nearmiss.name_division);

                //  $('p#affiliation').text(nearmiss.affiliation);

                $('p#affiliation').text(nearmiss.type_division);
                $('p#behavior').text(nearmiss.behavior);
                // if(adjustment==1) {
                $('#rowN').text(nearmiss.id);
                $('input[name="id_n"]').val(nearmiss.id);
                //  $('span#id_iniciator').text(nearmiss.id_iniciator);
                $('b#info').removeClass('hidden');
                $('select[name="id_iniciator"]').select2('val', nearmiss.id_iniciator).trigger('change');
                $('textarea#actionDo').text(nearmiss.actionDo);
                $('textarea#actionIn').text(nearmiss.actionIn);
                $('textarea#pl_comment').val(nearmiss.pl_comment);
                $('select[name="status"]').select2('val', nearmiss.id_status).trigger('change');
                $('select[name="characteristicsNM"]').select2('val', nearmiss.id_charHM).trigger('change');
                $('input#createDate').val(nearmiss.createDate);
                $('select[name="platform"]').select2('val', nearmiss.id_pl).trigger('change');

//
                $('select[name="gehs"]').select2('val', nearmiss.GEHSMS).trigger('change');

                $('select#iniciator_action').select2('val', nearmiss.initiator_action_id).trigger('change');
                if (nearmiss.initiator_action_id == 10) {
                    $("div#iniciator_action_text").removeClass('hidden');
                    $('textarea#iniciator_acttext').text(nearmiss.initiator_action_text);
                } else {
                    $("div#iniciator_action_text").addClass('hidden');
                    $('textarea#iniciator_acttext').text('');
                }


                $("#Img").attr("src", nearmiss.photo);
                imgSrc = nearmiss.photo;

                $('select[name="status"]').prop("disabled", true);
                $('input#date_plan').val(nearmiss.deadlineDate);

                if (nearmiss.deadlineDate != null) {
                    $('p#timeleft').text(nearmiss.timeleft);
                }
                $('select[name="job"]').select2('val', nearmiss.job).trigger('change');
                $('select[name="id_responsible"]').select2('val', nearmiss.id_responsible).trigger('change');
                $('textarea#demand').val(nearmiss.demand);
                $('textarea#recommendations').val(nearmiss.recommendations);
                if (nearmiss.id_status == 5) {
                    $('label#labelleft').text("Выполнен:");
                    $('p#timeleft').text(nearmiss.timeleft);
                } else if (nearmiss.timeleft < 0) {
                    $('label#labelleft').text("Просрочен:");
                    var month = Math.abs(Math.trunc(nearmiss.timeleft / 30));
                    var days = Math.abs(nearmiss.timeleft) - month * 30;
                    if (month > 0) {
                        text1 = month + " мес. и ";
                    }
                    $('p#timeleft').text(text1 + days + " дн.");
                } else {

                    $('label#labelleft').text("Осталось до устранения:");
                    var month = Math.trunc(nearmiss.timeleft / 30);
                    var days = nearmiss.timeleft - month * 30;
                    var text1 = "";
                    if (month > 0) {
                        text1 = month + " мес. и ";
                    }
                    $('p#timeleft').text(text1 + days + " дн.");
                }

                if (nearmiss.id_self != 0) {
                    $('select[name="id_iniciator"]').prop("disabled", true);
                    $('textarea#pl_comment').prop("disabled", true);
                    $('input#createDate').prop("disabled", true);
                    $('select[name="characteristicsNM"]').prop("disabled", true);
                    $('select[name="platform"]').prop("disabled", true);
                    $('textarea#actionDo').prop("disabled", true);
                    $('textarea#actionIn').prop("disabled", true);
                    $('input#date_plan').prop("disabled", true);
                    $('select[name="job"]').prop("disabled", true);
                    $('select[name="id_responsible"]').prop("disabled", true);
                    $('textarea#demand').prop("disabled", true);
                    $('textarea#recommendations').prop("disabled", true);


                    $('button[data-action="addToCallendar"]').addClass('hidden');
                    $('a.report_link').removeClass('hidden').attr('href', '/reports/matrix?type=calendar&self=' + nearmiss.id_self);
                    //$('.add_calendar').addClass('hidden');
                    $('#breach').collapse('show');
                    $('#vid_jobs').addClass('hidden');
                } else {
                    $('select[name="id_iniciator"]').prop("disabled", false);
                    $('textarea#pl_comment').prop("disabled", false);
                    $('input#createDate').prop("disabled", false);
                    $('select[name="characteristicsNM"]').prop("disabled", false);
                    $('select[name="platform"]').prop("disabled", false);
                    $('textarea#actionDo').prop("disabled", false);
                    $('textarea#actionIn').prop("disabled", false);
                    $('input#date_plan').prop("disabled", false);
                    $('select[name="job"]').prop("disabled", false);
                    $('select[name="id_responsible"]').prop("disabled", false);
                    $('textarea#demand').prop("disabled", false);
                    $('textarea#recommendations').prop("disabled", false);
                    $('button[data-action="addToCallendar"]').removeClass('hidden');
                    $('button[type="submit"]').removeClass('hidden');
                    $('a.report_link').addClass('hidden');
                    $('.add_calendar').removeClass('hidden');
                }
                /* Согласование */
                if (nearmiss.id_status == 4) {
                    $('button[data-action="approve"]').removeClass('hidden');
                    $('button[data-action="notapprove"]').removeClass('hidden');
                } else {
                    $('button[data-action="approve"]').addClass('hidden');
                    $('button[data-action="notapprove"]').addClass('hidden');
                }


                //upfile(upload1);
                //   $('#nearmiss_modal').modal();
                // $('#nearmiss_edit_from_file').modal();
                // } else {
                //     if (nearmiss.typeNear == '1') {
                //         $('select[name="status"]').prop("disabled", false);
                //     } else {
                //         $('select[name="status"]').prop("disabled", true);
                //     }
                //     $('#rowN').text(' ' + nearmiss.id);
                //     $('input[name="id_n"]').val(nearmiss.id);
                //     $('textarea#actionDo').text(nearmiss.actionDo);
                //     $('textarea#actionIn').text(nearmiss.actionIn);
                //     $('div#pl_comment').html(nearmiss.pl_comment);
                //     $('span#platform').text(nearmiss.platform);
                //     $('span#job').text(nearmiss.job_name);
                //     $('span#create_data').text(nearmiss.createDate);
                //     $('span#id_login').text(nearmiss.id_iniciator);
                //     $('select[name="status"]').select2('val', nearmiss.id_status).trigger('change');
                //     $('select[name="typeNear"]').select2('val', nearmiss.typeNear).trigger('change');
                //     $('select[name="id_pl"]').select2('val', nearmiss.id_pl).trigger('change');
                //     // $(' div#nearmiss span#demand').text(nearmiss.actionIn);
                //     $('textarea[name="demand"]').val('');
                //     if (nearmiss.photo != "") {
                //         $('.imgnull').addClass('hidden')
                //         $("#imgdata").attr("src", nearmiss.photo);
                //         $("div.fullimg a").attr("href", nearmiss.photo);
                //     }
                //     
                $.post('/app/views/tasks/upload.php', {all_data: id}).done(function (data) {
                    fileinput(JSON.parse(data));
                })

                $('#nearmiss_modal').modal();
                //     $('#nearmiss_edit').modal();
                // }
            },
            error: function (jqXHR) {
                console.log(jqXHR)
            }
        })

    })

    //Скачивание приложения №1 Отчет выполнения рейтингов ЦМК
    // $('*[data-action="prilog1"]').on('click',function () {
    // $.notify({'type':'success', 'message':'Скачивание отчета началось. Это может занять некоторое время...'});
    // window.open('/app/views/nearmiss/dload_progressReport.php', '_self');
    // });
    //Скачивание статистики по выполнению неармиссов
    // $('*[data-action="prilog2"]').on('click',function () {
    // $.notify({'type':'success', 'message':'Скачивание статистики началось. Это может занять некоторое время...'});
    // window.open('/app/views/nearmiss/dload_stats.php', '_self');
    // });
    //Скачивание реестра нарушений неармиссов
    // $('*[data-action="prilog3"]').on('click',function () {
    // $.notify({'type':'success', 'message':'Скачивание реестра началось. Это может занять некоторое время...'});
    // window.open('/app/views/nearmiss/dload_register.php', '_self');
    // });

    //Скачивание статистики по инициаторам
    $('*[data-action^="prilog"]').on('click', function () {

        $.notify({'type': 'success', 'message': 'Скачивание  началось. Это может занять некоторое время...'});
        // window.open('/app/views/nearmiss/dload_stats_iniciator.php', '_self');

    });

    //Скачивание статистики по инициаторам
    $('*[data-action="onlinestat"]').click(function () {
        $('#online_statistics').modal();
    });
    //Кнопка Календарь по Near Miss
    $('*[data-action="calendar-nearmiss"]').click(function () {
        window.open('/reports/matrix?type=calendar&mode=2', '_blank');
    });


// Кнопка удаления записи
    $('*[data-action="remove-data"]').click(function () {
        if (checkRowAmount_new() < 1) {
            $.notify({'type': 'error', 'message': 'Необходимо выбрать хотя бы одну запись!'});
            return false;
        }
        ;
        bootbox.confirm({
            title: "Подтверждение",
            backdrop: false,
            message: "Выбранные нарушения будут удалены. <br>Вы уверены что хотите выполнить действие? ",
            buttons: {
                cancel: {
                    label: '<i class="fa fa-times"></i>Нет'
                },
                confirm: {
                    label: '<i class="fa fa-check"></i>Да'
                }
            },
            callback: function (result) {
                if (result) {

                    $(this).trigger('notify-hide');

                    var removeId = [];

                    $('#datatable tr.selected').each(function (index) {
                        removeId.push($(this).attr('data-id'));
                    })

                    var attr = 'id=' + removeId + '&action=removeNearmiss';
                    $.ajax({
                        type: 'POST',
                        data: attr,
                        url: "nearmiss/ajax",
                        cache: false,
                        success: function (data) {
                            $.notify({'type': 'success', 'message': 'Все выделенные нарушения NearMiss были удалены'});
                            table.ajax.reload();
                        },
                        error: function () {
                            $.notify({'type': 'error', 'message': 'Ошибка удаления!'});
                        }
                    });
                }
            }
        });
    });

    function checkRowAmount_new() {
        var table = $("#datatable tr.selected")
        if (table.length > 0) {

            $('.panel-heading .ion-trash-a').parent().addClass('btn-danger');
            $('.panel-heading .ion-close').parent().addClass('btn-danger');
            $('.panel-heading .ion-unlocked').parent().addClass('btn-danger');
            $('.panel-heading .ion-edit').parent().removeClass('btn-info');
            $('.panel-heading .ion-eye').parent().removeClass('btn-primary');
            $('.panel-heading .ion-clipboard').parent().addClass('btn-purple');
            $('.panel-heading .ion-document').parent().removeClass('btn-warning');
            $('.panel-heading .ion-alert-circled').parent().removeClass('btn-danger');
            $('.panel-heading .ion-checkmark').parent().addClass('btn-success');
        }
        if (table.length == 1) {

            $('.panel-heading .ion-close').parent().addClass('btn-danger');
            $('.panel-heading .ion-unlocked').parent().addClass('btn-danger');
            $('.panel-heading .ion-edit').parent().addClass('btn-info');
            $('.panel-heading .ion-eye').parent().addClass('btn-primary');
            $('.panel-heading .ion-document').parent().addClass('btn-warning');
            $('.panel-heading .ion-alert-circled').parent().addClass('btn-danger');
        }
        if (table.length <= 0) {

            $('.panel-heading .ion-edit').parent().removeClass('btn-info');
            $('.panel-heading .ion-eye').parent().removeClass('btn-primary');
            $('.panel-heading .ion-trash-a').parent().removeClass('btn-danger');
            $('.panel-heading .ion-close').parent().removeClass('btn-danger');
            $('.panel-heading .ion-unlocked').parent().removeClass('btn-danger');
            $('.panel-heading .ion-clipboard').parent().removeClass('btn-purple');
            $('.panel-heading .ion-document').parent().removeClass('btn-warning');
            $('.panel-heading .ion-checkmark').parent().removeClass('btn-success');
            $('.panel-heading .ion-alert-circled').parent().removeClass('btn-danger');
        }
        ;
        return table.length;
    }


    /* загрузка отчета Nearmiss */
    $('*[data-action="loadcsv"]').click(function () {

        $('input[name="userfile"]').trigger('click');
    });

    $('input[name="userfile"]').change(function () {
        $("form[name='uploader']").submit();
    });

    $("form[name='uploader']").submit(function (e) {

        var formData = new FormData($(this)[0]);

        $.notify({'type': 'info', 'message': 'Пожалуйста, подождите. Идет отправка файла на сервер...'});

        $.ajax({
            url: '/app/views/nearmiss/models/upload_file.php',
            type: "POST",
            data: formData,
            async: true,
            cache: false,
            success: function (data) {
                var action = "loadfile";

                window.open(
                    '/app/views/nearmiss/models/upload_register.php?file=' + data + '&action=' + action,
                    '_self'
                );
            },
            error: function (data) {
                console.log('Ошибка!');
            },

            contentType: false,
            processData: false
        });

        e.preventDefault();

    });

    function fileinput(all_data=null) {
        $('input#input-ru').fileinput('destroy').off('filebeforedelete').fileinput({
            language: "ru",
            theme: 'gly',
            bsVersion: 3,
            // showPreview: false,
            // maxFilePreviewSize:10000,
            uploadUrl: "/app/views/tasks/upload.php",
            uploadAsync: true,
            showCaption: true,
            showUpload: false, // hide upload button
            showRemove: false, // hide remove button
            showBrowse: true,
            showClose: false,
            minFileCount: 0,
            maxFileCount: 10,
            allowedFileExtensions: ['png', 'jpg', 'jpeg', 'pdf'],
            hideThumbnailContent: false,//предварительный вид контенкта
            initialPreviewAsData: false,
            initialPreview: all_data ? all_data.initialPreview : false,// просмотр файла
            initialPreviewConfig: all_data ? all_data.initialPreviewConfig : false,// первоначальная конфигурация предварительного просмотра, если вы хотите, чтобы начальный предварительный просмотр отображался с данными загрузки сервера
            uploadExtraData: function () {
                var upload1 = JSON.stringify({
                    "id": $('input[name="id_n"]').val(),
                    "self": 100,
                    "platform": $('select[name="platform"]').select2('val')
                });
                return {upload: upload1}
            },
            //maxFileSize: 100,
            dropZoneEnabled: false,//показ drop zona
            browseOnZoneClick: true,
            initialPreviewShowDelete: false,
            initialPreviewDownloadUrl: '/nearmiss/upload?downloads={key}',
            overwriteInitial: false,// добавить файлов к предварительному просмотру
            // layoutTemplates: {
            //     main1: '{preview}\n' +
            //     '<div class="input-group {class}">\n' +
            //     '  {caption}\n' +
            //     '  <div class="input-group-btn">\n' +
            //     '    {browse}\n' +
            //     '  </div>\n' +
            //     '</div>',
            // },
            previewSettings: {
                image: {width: "100px", height: "100px", 'max-width': "100%", 'max-height': "100%"},
                html: {width: "50px", height: "70px"},
                text: {width: "50px", height: "70px"},
                office: {width: "50px", height: "70px"},
                gdocs: {width: "50px", height: "70px"},
                video: {width: "50px", height: "50px"},
                audio: {width: "50px", height: "30px"},
                flash: {width: "50px", height: "50px"},
                object: {width: "50px", height: "50px"},
                pdf: {width: "50px", height: "70px"},
                other: {width: "100px", height: "100px"},
            }
        })
            .on("filebatchselected", function (event, files) {
                // el1.fileinput("upload");
            }).on("filebeforedelete", function (event, id, index) {

            return new Promise(function (resolve, reject) {
                bootbox.confirm({
                    title: "Подтверждение",
                    message: "Вы уверены что хотите удалить",
                    buttons: {
                        cancel: {
                            label: 'Отмена'
                        },
                        confirm: {
                            label: 'Подтвердить'
                        }
                    },
                    callback: function (result) {
                        if (result) {
                            resolve();
                        }
                    }
                })
            });
        });
    }

    /* Загрузка фотографий */

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#Img').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);

        }
        var a = input.files[0];
        nameSrc = a['name'];
        //var formData = new FormData($(this)[0]);

    }

    $("#imgInp").change(function () {
        readURL(this);
    });
});
function Popup(data) {
    var mywindow = window.open('', 'my div', 'height=600,width=500');
    mywindow.document.write('<html><head><title>QR код</title>');
    mywindow.document.write('</head><body >');
    mywindow.document.write(data);
    mywindow.document.write('</body></html>');
    mywindow.document.close(); // necessary for IE >= 10
    mywindow.focus(); // necessary for IE >= 10
    mywindow.print();
    mywindow.close();
    return true;
}

$('*[data-action="uploadPhoto"]').click(function () {
    var formData = new FormData($(this)[0]);

});

//$('input[name="addPhoto"]').change(function () {
//   $("form[name='uploadPhoto']").submit();
//});
//$("form[name='uploadPhoto']").submit(function (e) {
//    var formData = new FormData($(this)[0]);
//   readURL(this);
//   console.log("Пошла загрузка");
//  console.log(this);
//$.notify({'type':'info', 'message':'Пожалуйста, подождите. Идет отправка файла на сервер...'});
//  $.ajax({
//      url: '/app/views/nearmiss/upload_photo.php',
//      type: "POST",
//     data: formData,
//     async: true,
//      cache: false,
//      success: function (data) {
//      },
//      error: function (data) {
//          console.log('Ошибка!');
//      },
//      contentType: false,
//      processData: false
//  });
//   e.preventDefault();
//});
//$("#imgInp").change(function() {
// readURL(this);
//});


function clear() {

    $('#rowN').text('');
    $('input[name="id_n"]').val('');
    $('textarea#actionDo').text('');
    $('textarea#actionIn').text('');

    $('p#behavior').text('');
    $('p#affiliation').text('');
    $('p#subdivision').text('');
    $('span#job').text('');
    $('span#platform').text('');
    $('span#id_iniciator').text('');
    $('p#timeleft').text('');

    $('input[name="date_plan"]').val('');
    $('input[name="createDate"]').val('');

    $('select[name="gehs"]').select2('val', '').trigger('change');
    $('select[name="status"]').select2('val', '').trigger('change');
    $('select[name="id_iniciator"]').select2('val', '').trigger('change');
    $('select[name="job"]').select2('val', '').trigger('change');
    $('select[name="platform"]').select2('val', '').trigger('change');
    $('select[name="characteristicsNM"]').select2('val', '').trigger('change');
    $('div#nearmiss span#demand').text('');
    $("div.fullimg a").attr("href", '');
    $("#imgdata").attr("src", '');
    $("#Img").attr("src", '');

    $('textarea#pl_comment').text();
    $('textarea[name="demand"]').val('');

    $('textarea[name="recommendations"]').val('');
    $('textarea[name="pl_comment"]').val('');
    $('select[name="id_responsible"]').select2('val', "").trigger('change');
    $('button[type="submit"]').removeClass('hidden');
    $('.imgnull').removeClass('hidden')
    $('a.report_link').addClass('hidden');
}

function add_job_children(job_chidren) {
    job_chidren.empty();
    //job_chidren.append('<option value="">Не выбранно</option>')

    $.map(getJOB(), function (item, i, arr) {
        job_chidren.append('<option value="' + i + '">' + item + '</option>')
    });
}
function f(d) {
    // console.log(d=="<p><br></p>");
    return d != "<p><br></p>" ? d : ''
}
function text_editor(div_indef) {
    var stext = $('.' + div_indef);
    stext.summernote('destroy');
    stext.summernote({
        //  airMode: true,
        lang: 'ru-RU',
        height: 100,
        minHeight: 50,
        maxHeight: 300,
        codemirror: { // codemirror options
            theme: 'monokai'
        },
        fontNames: ['Arial', 'Times New Roman', 'Helvetica'],
        disableDragAndDrop: true,
        toolbar: [
            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['font', ['strikethrough', 'superscript', 'subscript']],
            ['fontsize', ['fontsize']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']]
        ],
    })

    ;
}

/* дата в в нужный формат */
function givStringDate(myDate) {
    myDate = myDate.split(".");
    return myDate[1] + "/" + myDate[0] + "/" + myDate[2];
}

/* Выводим сколько осталось при смене даты планового срока */
$("input#date_plan").datepicker({
    dateFormat: 'dd.mm.yy',
    language: "ru",
    autoclose: true,
}).on("input change", function (e) {
    $(this).parents('.form-group').removeClass('has-error');
    $(this).siblings('.help-block').text('');
    var date = givStringDate($("input#date_plan").val());
    var now = givStringDate(new Date().toLocaleDateString());
    var timeleft = new Date(date).getTime() / 1000 - new Date(now).getTime() / 1000;
    var month = Math.trunc(timeleft / (60 * 60 * 24 * 30));
    var days = timeleft / (60 * 60 * 24) - month * 30;
    var textinput = "";
    if (month != 0) {
        textinput = month + " мес. и ";
    }
    $('label#labelleft').text("Осталось до устранения:");
    $('p#timeleft').text(textinput + days + " дн. ");


    // alert(textinput);
})

/* Действие при изменении инициатора */
$("select#id_iniciator").on("change", function (e) {
    var idselect = $("select#id_iniciator").val();
    if (idselect) {
        $.ajax({
            async: false,
            type: 'POST',
            data: {
                'action': 'getDivisionUser',
                'id': idselect
            },
            url: "nearmiss/ajax",
            cache: false,
            success: function (json) {
                var division = JSON.parse(json);
                $('p#subdivision').text(division.name_division);
                $('p#affiliation').text(division.name_type);
            },
            error: function (data) {
                alert("Ошибка! Такой пользователь не найден");
            }
        });
    }
});

/* Действие при изменении опасных условий труда или действий */
$("select#job").on("change", function (e) {
    var idselect = $("select#job").val();
    if (idselect) {
        $.ajax({
            async: false,
            type: 'POST',
            data: {
                'action': 'getCharastecNM',
                'id': idselect
            },
            url: "nearmiss/ajax",
            cache: false,
            success: function (json) {
                var charnm = JSON.parse(json);
                $('p#behavior').text(charnm.name);

            },
            error: function (data) {
                alert("Ошибка! Такой вид работ не найден");
            }
        });
    }
});

/* Онлайн статистика */
//var table = $('#onlineStats').dataTable({
//   lengthChange: false,
//stateSave: true,
//  buttons: ['colvis'],
// "aaSorting": [[1, 'desc']],
// "sScrollX": "100%",
//   "scrollCollapse": true,
//   "paging": true,
//   "processing": true,
//     ajax: {
//        type: 'POST',
//          dataType: "json",
//          url: "nearmiss/ajax",
//          data: {"action": "onlinestat"},
//          cache: false,
//         dataSrc: function (json) {
//              return json.data
//          },
//       }
//    });


/* Отоброжаем форму ввода, если выбрано "другое" в "какие действия были предприяны" */
$("select#iniciator_action").on("change", function (e) {
    var idselect = $("select#iniciator_action").val();
    console.log(idselect);
    if (idselect == 10) {
        $("div#iniciator_action_text").removeClass('hidden');
    } else {
        if (!$("div#iniciator_action_text").hasClass('hidden')) {
            $("div#iniciator_action_text").addClass('hidden');
        }
    }
});

function getCookie(name) {
  let matches = document.cookie.match(new RegExp(
    "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
  ));
  return matches ? decodeURIComponent(matches[1]) : undefined;
}