<?php
include_once $_SERVER['DOCUMENT_ROOT'] . '/assets/mail.php';

function getLoginById ($id)
{
    $db = Db::Connection();
    $result = $db->query("SELECT `login` FROM `users` WHERE `id`={$id}");
    $row = $result->fetch_assoc();
    return $row['login'];
}

class modelNearmiss
{
    private $db;
    private $data_array;
    public $id_report;
    public $id_bunch;
    public $id_new_report;

    public function __construct($data)
    {
        $this->db = Db::Connection();
        $this->getMaxBunch();
        if (!empty($data)) {
            $this->data_array = (object)$data;
        };
    }

    function getMaxBunch()
    {
        $result = $this->db->query("SELECT MAX(bunch) AS 'maxValue' FROM reports");
        $row = $result->fetch_assoc();
        return intval($row['maxValue']) + 1;

    }

    public function set($data)
    {
        $this->data_array = (object)$data;
    }

    public function getdata()
    {
        return $this->data_array;
    }

    public function add_collector()
    {
        $query = "INSERT HIGH_PRIORITY INTO collector(job,demand, platform,user,`self-evaluation`, `status-compliance`,fio,`date-plan`,actions,recommendations,date,typechek)
                  VALUES (?,?,?,?,?,4,?,?,?,?,UNIX_TIMESTAMP(NOW()),2);
        ";

        if (!$stmt = $this->db->prepare($query)) {
            echo "Не удалось подготовить запрос: (" . $this->db->errno . ") " . $this->db->error;
        }
        $stmt->bind_param('isiiisiss',
            $this->data_array->job,
            $this->data_array->demand,
            $this->data_array->id_pl,
            $_COOKIE["id"],
            $this->id_report,
            $this->data_array->fio,
            $this->data_array->date_plan,
            $this->data_array->action,
            $this->data_array->recommendations
        );
        
        if ($stmt->execute()) {
            /*записать id в nearmis*/
            $this->id_new_report = $this->db->insert_id;

         if($this->add_id_to_neamiss($this->db->insert_id, $this->data_array->fio)) 
            {
                return http_response_code(200);

            }

        } else {
         return http_response_code(404);
//            return $stmt->error_list;
        }
    }

    public function insert()/*добавить репорт*/
    {
        $query = "INSERT HIGH_PRIORITY INTO reports( bunch, status, date, creator, platform, job, typen)
                  VALUES (?,0,UNIX_TIMESTAMP(NOW()),?,?,?,6)";

        if (!$stmt = $this->db->prepare($query)) {
            echo "Не удалось подготовить запрос: (" . $this->db->errno . ") " . $this->db->error;
        }
        $stmt->bind_param('iiii', $this->id_bunch, $_COOKIE["id"], $this->data_array->id_pl, $this->data_array->job);

        if ($stmt->execute()) {
            $this->id_report = $this->db->insert_id;
            return true;
        } else {
            return false;
//            return $stmt->error_list;
        };

    }

    public function delete()
    {
    }

    public function find_platform()
    {
        $query = "SELECT r . bunch FROM reports r WHERE r . platform =
    {$this->data_array->id_pl} AND r . typen = 6 limit 1";
        $result = $this->db->query($query);
        if ($result->num_rows > 0) {
            $row = $result->fetch_assoc();
            $this->id_bunch = intval($row['bunch']);
            return true;
        } else {
            $this->id_bunch = $this->getMaxBunch();
            $this->id_report = 0;
            return false;
        }
    }

    public function find_job()
    {
        $query = "SELECT r . id FROM reports r WHERE r . bunch =
        {$this->id_bunch} AND r . job =
        {$this->data_array->job} AND r . typen = 6";
        $result = $this->db->query($query);
        if ($result->num_rows > 0) {
            $row = $result->fetch_assoc();
            $this->id_report = intval($row['id']);
            return true;
        } else {
            $this->id_report = 0;
            return false;
        }
    }

    public function add_id_to_neamiss($id_self,$login)
    {


        $query = "SELECT `id` FROM `users` WHERE `login` = '{$login}'";
        //var_dump($query);
        $result = $this->db->query($query)->fetch_assoc();
        $iduser = $result['id'];

        $query = "UPDATE nearmiss nm SET nm.id_self ={$id_self}, nm.id_responsible = {$iduser},   nm.status=3, nm.flag=1  WHERE nm.id={$this->data_array->id_n}";
        $this->db->query($query);
        if ($this->db->affected_rows > 0) {
            sendmailAddCollectorFromNearMiss($login);
            return true;
        } else {
            return false;
        };
    }
}
?>