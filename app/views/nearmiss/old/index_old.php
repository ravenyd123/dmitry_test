<?php
//include_once $_SERVER['DOCUMENT_ROOT'] . '/assets/all_const.php';
include_once $_SERVER['DOCUMENT_ROOT'].'/assets/functions.php';
include_once $_SERVER['DOCUMENT_ROOT'] .'/app/views/header.php';
include_once $_SERVER['DOCUMENT_ROOT'] .'/app/views/template.php';
$nearmissdata = mysqli_query($link,"SELECT * FROM `NearMiss`");

?>
 <section class="content">
<div class="wraper container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title pull-left m-t-10">Near miss</h3>
                            <div class="top_nav">
<!--                                    <a  data-target="#task_create" data-toggle="modal">-->
<!--                                        <button class="btn btn-success" data-action="add-data"><i class="ion-plus"></i>-->
<!--                                        </button>-->
<!--                                    </a>-->
<!--                                    <br>-->
                                    <button class="btn btn-default" data-action="edit-data"><i class="ion-edit"></i>
                                    </button><br>
<!--                                    <a data-target="#task_create_pk" data-toggle="modal">-->
<!--                                        <button class="btn btn-default" data-action="pk-data" id="create_pk">-->
<!--                                            <i class="glyphicon glyphicon-link"></i>-->
<!--                                        </button>-->
<!--                                        <br>-->
<!--                                    </a>-->
                                
                                <? if (in_array("D", $access['tasks'])) { ?>
                                    <button class="btn btn-default" data-action="remove-data"><i
                                                class="ion-trash-a"></i>
                                    </button>
                                <? } ?>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-body">
                            <div class="row m-b-15">
                                <div class="" id="datatable-filter">
                                    <div class="col-md-2 eh">
                                    </div>
                                    <div class="col-md-3 pl">
                                    </div>
                                    <div class="col-md-3 ot">
                                    </div>
                                    <div class="col-md-2 st">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <table id="datatable" class="table table-hover table-striped">
                                    	<thead>
            								<tr>
                								<th class="text-center"></th>
								                <th>Тема</th>
								                <th>Описание нарушения</th>
								                <th>Тип риска</th>
								                <th>Дата создания</th>
								                <th>Фото</th>
                
           									</tr>
           								</thead>
           							 <tbody>
<?php


 foreach($nearmissdata as $value){ ?>
										<tr>
											<td><?=$value['id']?></td>
											<td><?=$value['theme']?></td>
											<td><?=$value['actionIn']?></td>
											<td class="text-center"><?=$value['typeNear']?></td>
											<td class="text-center"><?=$value['createDate']?></td>
											<td class="text-center">
<?php
if ($value['photo'] != null) { 
	$imageData = base64_encode(file_get_contents($value['photo']));
	$src = 'data: '.mime_content_type($value['photo']).';base64,'.$imageData; ?>

	<a href=# >  <img src="<?=$src?>" alt="" height="50"/> </a>
<? } ?>

											</td>
										</tr>
<?}?>				

           							 </tbody>


                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <!-- End Row -->

        </div>

    </section>

?>