<!-- Основное окно при добавлении и редактировании NM -->
<div class="modal fade" id="nearmiss_modal" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg">
        <form class="nearmiss" id="nearmiss" name="nearmiss" method="POST">
            <input class="hidden" name="id_n" value="">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="row">
                        <div class="col-md-6 col-sm-3">
                            <h4 class="modal-title"><span id="title" >Редактирование NearMiss</span></h4>
                            <b id="info">Нарушение № <span id="rowN"></span></b>
                        </div>
                        <div class="col-md-5 col-sm-4">
                                <label for="status" class="col-md-3">Статус:<sup>*</sup></label>
                                <div class="col-md-9">
                                    <select id="status" class="select2" name='status' autocomplete="off" disabled required>
                                        <option value="0"></option>
                                        <option value="1">Новый</option>
                                        <option value="2">Просмотрен</option>
                                        <option value="3">В работе</option>
                                        <option value="4">На согласовании</option>
                                        <option value="5">Выполнен</option>
                                        <option value="6">Архив</option>
                                        <option value="7">Просрочен</option>
                                    </select>
                                </div>
                        </div>
                        <div class="col-md-1 col-sm-1">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-7 col-sm-3">
                            <div id="infoapproveblock" class="hidden">
                                <p><strong>Ранее не согласовано. Причина:</strong></p>
                                <p id="infoapprove"></p>
                            </div>
                        </div>
                        <div class="col-md-5 col-sm-3">
                            <button data-action="notapprove" type="button" class="btn btn-warning m-b-5 hidden">
                                Не согласовать
                            </button>
                            <button data-action="approve" type="button" class="btn btn-warning m-b-5 hidden">
                                Согласовать выполнение
                            </button>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-md-3 col-sm-3">
                            <div class="form-group">
                                <label for="createDate">Дата рег. нарушения:<sup>*</sup></label>
                                <input type="text" class="form-control datepicker" name="createDate" id="createDate"
                                       autocomplete="off" required>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-3">
                            <div class="form-group">
                                <label for="id_login">Инициатор:<sup>*</sup></label>
                                <select class="select2 form-control" id="id_iniciator" name="id_iniciator"
                                        autocomplete="off" required>
                                    <option value="">Не выбранно</option>

                                    <? foreach (getUserNameInPlatform(true, false) as $key => $value) { ?>
                                        <option value="<?= $key ?>"><?= $value ?></option>
                                    <? } ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-3">
                            <div class="form-group">
                                <label for="subdivision">Подразделение инициатора:</label>
                                <p id="subdivision"></p>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-3">
                            <div class="form-group">
                                <label for="affiliation">Принадлежность подразделения:</label>
                                <p id="affiliation" for="affiliation"></p>
                                </select>
                            </div>
                        </div>
                        <!--  <div class="col-md-4 col-sm-3">
                               <div class="form-group" id="charHM">
                                   <label for="characteristicsNM">Характеристика нарушения<sup>*</sup></label>
                                   <select id="characteristicsNM" class="select2" name='characteristicsNM' autocomplete="off" required>
                                       <option value="1">Поведенческий</option>
                                       <option value="2">Технический</option>
                                   </select>
                               </div>
                           </div> -->
                    </div>
                    <div class="row">
                        <div class="col-md-4 col-sm-5">
                            <div class="form-group">
                                <label for="platform_select">Опасные условия или действия:<sup>*</sup></label>
                                <select class="job select2 form-control" id="job" autocomplete="off" name='job'>
                                    <? $jobs = getJobForNM();
                                    foreach ($jobs as $key => $value) { ?>
                                        <option value="<?= $value['id'] ?>"><?= $value['type'] ?></option>
                                    <? } ?>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-3 col-sm-3">
                            <div class="form-group">
                                <label for="behavior">Характеристика NearMiss:</label>
                                <p id="behavior" for="behavior"></p>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-5 col-sm-5">
                            <div class="form-group">
                                <label for="platform_select">Место обнаружения:<sup>*</sup></label>
                                <select name="platform" id="platform_select" required
                                        class="select2 form-control">
                                    <? $platformsuser = GetOnlyMyGroupPlatform($_COOKIE['id']);
                                    //foreach (platforms as $key => $value)
                                    foreach ($platformsuser as $key => $value) { ?>
                                        <option value="<?= $key ?>"><?= $value ?></option>
                                    <? } ?>
                                </select>
                            </div>
                        </div>
                    </div>




                    <!-- row ниже можно будет удалить после конца апреля 2020. Этот код временный для показа. -->
                    <div class="row">
                        <div class="col-md-4 col-sm-5">
                            <div class="form-group">
                                <label for="platform_select">Виды работ<sup>*</sup></label>
                                <select class="typejob select2 form-control" id="typejob" autocomplete="off" name='typejob'>
                                    <? $jobs2 = getTypeJobForNM();
                                    foreach ($jobs2 as $key => $value) { ?>
                                        <option value="<?= $value['id'] ?>"><?= $value['type'] ?></option>
                                    <? } ?>
                                </select>
                            </div>
                        </div>
                    </div>






                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="actionIn"><b>Комментарий к обнаруженной опасности или место обнаружения<sup>*</sup></b></label>
                                <textarea required name="actionIn" class="actionIn form-control"
                                          id="actionIn">&nbsp;</textarea>
                            </div>
                        </div>
                        <div class="col-md-6 hidden" id="iniciator_action_text">
                            <div class="form-group">
                                <label for="actionDo " class="m-b-20" >
                                    <b>Введите что было сделано:<sup>*</sup></b>
                                </label>
                                <textarea name="iniciator_acttext" id="iniciator_acttext"
                                          class="iniciator_acttext form-control">&nbsp;</textarea>
                            </div>
                        </div>
                        <!--                        <div class="col-md-6">-->
                        <!--                            <div class="form-group">-->
                        <!--                                <label><b>Фотография нарушения:</b></label>-->
                        <!--                            </div>-->
                        <!--                            <div>-->
                        <!--                              <!--  <button name="addPhoto" title="Добавить фотографию" data-action="uploadPhoto">-->
                        <!--                                    Загрузить фото -->
                        <!--                                </button> -->
                        <!---->
                        <!--                                <form runat="server">-->
                        <!--                                     <input type='file' data-action="uploadPhoto" title="Добавить фотографию" id="imgInp" accept=".jpg, .jpeg, .png"/>-->
                        <!--                                <!-- <img id="blah" src="#" alt="your image" /> -->
                        <!--                                </form>-->
                        <!---->
                        <!--                            </div>-->
                        <!--                        </div>-->
                        <!--                        <div class="col-md-6">-->
                        <!--                            <div class="form-group">-->
                        <!--                                <a id="FullImages" data-action="FullImages">-->
                        <!--                                <img id="Img" src="" width="100">-->
                        <!--                            </a>-->
                        <!--                            </div>-->
                        <!--                        </div>-->
                    </div>

                    <? $db = DB::Connection();
                    $ehs = $db->query("SELECT * FROM `ehs` GROUP BY `ehs_numbers` "); ?>
                    <div class="row">
                        <div class="col-md-6 col-sm-6">
                            <div class="form-group" id="responsibleinput">
                                <label for="gehs">EHS:</label>
                                <select class="gehs select2" id="gehs" name="gehs" autocomplete="off">
                                    <option value="0" selected>Не выбранно</option>
                                    <? foreach ($ehs as $key => $value) { ?>
                                        <option value="<?= $value['id'] ?>"><?= $value['ehs_numbers'] ?>
                                            - <?= $value['ehs_name'] ?></option>
                                    <? } ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="actionDo"><b>Какие действия приняты:</b></label>
                                <select class="select2 iniciator_action" id="iniciator_action" name="iniciator_action"
                                        autocomplete="off">
                                    <option value="0" selected>Не выбранно</option>
                                    <?
                                    $actionIniciator = getIniciatorAction();
                                    foreach ($actionIniciator as $key => $value) { ?>
                                        <option value="<?= $value['id'] ?>"><?= $value['action_name'] ?></option>
                                    <? } ?>
                                </select>


                                <!-- <textarea name="actionDo"  id="actionDo" class="actionDo form-control">&nbsp;</textarea> -->
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>
                                    Фотография нарушения:
                                </label>
                                <form enctype="multipart/form-data">
                                    <div class="task_file_loading">
                                        <input id="input-ru" class="" name="fileToUpload[]" type="file" multiple>
                                    </div>
                                </form>
                            </div>
<!--                            <div class="form-group">-->
<!--                                <label><b>Фотография нарушения:</b></label>-->
<!--                            </div>-->
<!--                            -->
<!--                            <div>-->
<!--                                <!--  <button name="addPhoto" title="Добавить фотографию" data-action="uploadPhoto">-->
<!--                                      Загрузить фото-->
<!--                                  </button> -->
<!---->
<!--                                <form runat="server">-->
<!--                                    <input type='file' data-action="uploadPhoto" title="Добавить фотографию" id="imgInp"-->
<!--                                           accept=".jpg, .jpeg, .png"/>-->
<!--                                    <!-- <img id="blah" src="#" alt="your image" /> -->
<!--                                </form>-->
<!---->
<!--                            </div>-->
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <a id="FullImages" data-action="FullImages">
                                    <img id="Img" src="" width="100">
                                </a>
                            </div>
                        </div>

                    </div>


                    <div class="add_calendar">
                        <hr>
                        <div class="row">
                            <div class="col-md-3 col-sm-3">
                                <div class="form-group">
                                    <label>Плановый срок:<sup>*</sup></label>
                                    <input type="text" class="form-control datepicker" id="date_plan" name="date_plan"
                                           autocomplete="off">
                                    <div class="help-block"></div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4">
                                <div class="form-group">
                                    <label id="labelleft"></label>
                                    <p id="timeleft"></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-sm-6">
                            <div class="form-group" id="responsibleinput">
                                <label for="fio">ФИО ответственного:<sup>*</sup></label>
                                <select class="select2 form-control id_responsible" id="id_responsible"
                                        name="id_responsible" autocomplete="off">
                                    <option value="" selected>Не выбранно</option>
                                    <? foreach (getUserNameInPlatform(true, false) as $key => $value) { ?>
                                        <option value="<?= $key ?>"><?= $value ?></option>
                                    <? } ?>
                                </select>
                                <div class="help-block"></div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="demand">
                                    Необходимые мероприятия(требование):
                                </label>
                                <textarea class="demand form-control" name="demand" id="demand">&nbsp;</textarea>
                                <div class="help-block"></div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="recommendations">
                                    Мероприятия по предупреждению:
                                </label>
                                <textarea class="form-control recommendations" name="recommendations"
                                          id="recommendations">&nbsp;</textarea>
                                <div class="help-block"></div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <a href="#" target="_blank" class="report_link hidden">Открыть отчет</a>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="text-right">
                        <button type="button" class="btn btn-warning" data-action="addToCallendar">Сохранить и создать
                            отчет
                        </button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Отменить</button>
                        <button type="submit" class="btn btn-primary">Сохранить</button>
                    </div>
                </div>

            </div>
        </form>

    </div>
</div>


<!-- Всплывающее окно со согласованием -->
<div class="modal fade" id="reason_approve" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog">
        <form class="nearmiss" id="nearmiss" method="POST">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="row">
                        <div class="col-md-5">
                            <h4 id="title" class="modal-title">Причина несогласования</h4>
                        </div>

                        <div class="col-md-7">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>

                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <textarea class="reasontext form-control" name="reasontext"
                                      id="reasontext">&nbsp;</textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class="modal-footer">
                            <div class="text-right">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Отменить</button>
                                <button type="button" data-action="savereason" class="btn btn-primary">Сохранить
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>


<!-- всплывающее окно с полной картинкой -->
<div class="modal fade" id="nmPhoto" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg">
        <form class="fullphoto" id="fullphoto" method="POST">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="row">
                        <div class="col-md-12">
                            <button type="button" class="close" data-action="closefullimg">&times;</button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <img id="fullphoto" alt="Полная картинка" width="100%">
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>


<!-- модальное окно с QR кодом -->
<div class="modal fade" id="qrcode_modal" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg">
        <form class="fullphoto" id="fullphoto" method="POST">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="row">
                        <div class="col-md-11 col-sm-3">
                        </div>
                        <div class="col-md-1 col-sm-1">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-9 col-sm-3">
                        </div>
                        <div class="col-md-3 col-sm-4">
                            <button type="button" class="btn btn-info" data-dismiss="modal" data-action="qrcodeprint"><i
                                        class="ion-printer"></i> Печать
                            </button>
                        </div>
                    </div>
                    <div class="row" id="qrcodeforpront">
                        <div class="col-md-7 col-sm-3">
                            <?
                            include("qrcode/qrcode.php");
                            ?>
                        </div>
                        <div class="col-md-5 col-sm-4">
                            <br><br>
                            <h4>
                                QR-код для регистрации Near Miss через мобильное приложение
                            </h4>
                            <p> 1) Наведите объектив камеры вашего мобильного устройства на картинку QR-кода.
                            </p>
                            <p>2) Выберите на открывшейся странице режим работы с Near Miss.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<!-- модальное окно с онлайн статистикой -->
<div class="modal fade" id="online_statistics" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <div class="row">
                    <div class="col-md-11 col-sm-3">
                        <h4 id="title" class="modal-title">Онлайн статистика по текущему году</h4>
                    </div>
                    <div class="col-md-1 col-sm-1">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 col-sm-3">
                        <!--     <table id="onlineStats" class="table table-hover table-striped"> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

