$(document).ready(function () {
    // $("input.search").autocomplete({
    //     delay: 1000,
    //     minLength: 2,
    //     source: function (request, response) {
    //
    //         $.ajax({
    //             url: "/app/views/search/search.php",
    //             type: "POST",
    //             dataType: "json",
    //             data: {
    //                 "action": "stext",
    //                 "text": request.term
    //             },
    //             success: function (data) {
    //                 response($.map(data, function (item) {
    //                     return {
    //                         label: item.suggest + " docs =" + item.docs + " distance =" + item.distance,
    //                         value: item.suggest
    //                     }
    //                 }));
    //             }
    //         });
    //     },
    //     open: function () {
    //         setTimeout(function () {
    //             $('.ui-autocomplete').css('z-index', 2000);
    //         }, 0);
    //     },
    // }).data("ui-autocomplete")._renderItem = function (ul, item) {
    //     return $("<li class='list-group-item'></li>")
    //         .data("item.autocomplete", item)
    //         .append(item.label)
    //         .appendTo(ul);
    // };

    $("input.search").on('keyup', function (event) {
        var event = event || window.event;
        var key = event.keyCode || event.charCode;
        if (key == 13) {
            //  $("input.search").autocomplete("close");
            fullsearch()
        }
    })

    $('button.btn_search').on('click', function () {
        fullsearch()
    })
    $('ul.btn_search_select_index').on('click', function (e) {

        var tt = $(e.target);
        $(this).find('.active').removeClass('active');
        tt.closest("li").addClass("active");
        if ($('input.search').val().length > 2) {
            fullsearch();
        }
    })

});
//переод раскладки клавиатуры
var switcher = function (text, arrow) {
    var str = [], newstr = [];
    str[0] = {
        'й': 'q',
        'ц': 'w',
        'у': 'e',
        'к': 'r',
        'е': 't',
        'н': 'y',
        'г': 'u',
        'ш': 'i',
        'щ': 'o',
        'з': 'p',
        'х': '[',
        'ъ': ']',
        'ф': 'a',
        'ы': 's',
        'в': 'd',
        'а': 'f',
        'п': 'g',
        'р': 'h',
        'о': 'j',
        'л': 'k',
        'д': 'l',
        'ж': ';',
        'э': '\'',
        'я': 'z',
        'ч': 'x',
        'с': 'c',
        'м': 'v',
        'и': 'b',
        'т': 'n',
        'ь': 'm',
        'б': ',',
        'ю': '.',
        'Й': 'Q',
        'Ц': 'W',
        'У': 'E',
        'К': 'R',
        'Е': 'T',
        'Н': 'Y',
        'Г': 'U',
        'Ш': 'I',
        'Щ': 'O',
        'З': 'P',
        'Х': '[',
        'Ъ': ']',
        'Ф': 'A',
        'Ы': 'S',
        'В': 'D',
        'А': 'F',
        'П': 'G',
        'Р': 'H',
        'О': 'J',
        'Л': 'K',
        'Д': 'L',
        'Ж': ';',
        'Э': '\'',
        '?': 'Z',
        'Ч': 'X',
        'С': 'C',
        'М': 'V',
        'И': 'B',
        'Т': 'N',
        'Ь': 'M',
        'Б': ',',
        'Ю': '.',
    };
    str[1] = {
        'q': 'й',
        'w': 'ц',
        'e': 'у',
        'r': 'к',
        't': 'е',
        'y': 'н',
        'u': 'г',
        'i': 'ш',
        'o': 'щ',
        'p': 'з',
        '[': 'х',
        ']': 'ъ',
        'a': 'ф',
        's': 'ы',
        'd': 'в',
        'f': 'а',
        'g': 'п',
        'h': 'р',
        'j': 'о',
        'k': 'л',
        'l': 'д',
        ';': 'ж',
        '\'': 'э',
        'z': 'я',
        'x': 'ч',
        'c': 'с',
        'v': 'м',
        'b': 'и',
        'n': 'т',
        'm': 'ь',
        ',': 'б',
        '.': 'ю',
        'Q': 'Й',
        'W': 'Ц',
        'E': 'У',
        'R': 'К',
        'T': 'Е',
        'Y': 'Н',
        'U': 'Г',
        'I': 'Ш',
        'O': 'Щ',
        'P': 'З',
        '[': 'Х',
        ']': 'Ъ',
        'A': 'Ф',
        'S': 'Ы',
        'D': 'В',
        'F': 'А',
        'G': 'П',
        'H': 'Р',
        'J': 'О',
        'K': 'Л',
        'L': 'Д',
        ';': 'Ж',
        '\'': 'Э',
        'Z': '?',
        'X': 'ч',
        'C': 'С',
        'V': 'М',
        'B': 'И',
        'N': 'Т',
        'M': 'Ь',
        ',': 'Б',
        '.': 'Ю',
    };
    for (var j = 0; j <= 1; j++)
        if (arrow == undefined || arrow == j)
            for (var i = 0; i < text.length; i++)
                if (str[j][text[i]])
                    newstr[i] = str[j][text[i]];
    for (var i = 0; i < text.length; i++)
        if (!newstr[i])
            newstr[i] = text[i];
    return newstr.join('');
};

function fullsearch() {


    var word = $('input.search').val();
    // var word_e =switcher(word);
    var type = $('.btn_search_select_index li.active a').attr('name');
    var len_word = word.length;

    //  word='("'+word+'"|"'+word_e+'")'


    h = '<div class="wraper container-fluid">' +
        '<div class="row">' +
        '<div class="col-md-12">' +
        '<div class="panel panel-default">' +
        '<div class="panel-heading">' +
        '<h4>Поисковый запрос по слову: <b class="sText"> ' + word + ' </b></h4></div>' +
        '<div class="panel-body"><div class="fulltext"></div>' +
        '<div class="col-md-12">' +
        '<ul class="pager">' +
        '<li class="previous disabled"><a href="#"><i class="fa fa-long-arrow-left"></i>Назад</a></li>' +
        '<li class="next"> <a href="#">Далее<i class="fa fa-long-arrow-right"></i></a> </li>' +
        '</ul>' +
        '</div></div></div></div></div></div></div>';

    ndh = 0;
    textd = "";
    if (len_word >= '2') {
        var dialog = bootbox.dialog({
            message: '<p class="text-center mb-0"><i class="fa fa-spin fa-cog"></i>Пожалуйста подождите. Производится поиск ...</p>',
            closeButton: false
        });
        $.ajax({
            url: '/app/views/search/search.php',
            data: {
                "action": 'fulltext',
                "text": word,
                "type": type
            },
            type: "POST",
            dataType: "json",
            cache: false,
            // beforeSend: function(){
            //     dialog
            // },
            success: function () {
                dialog
            },
            complete: function (data) {
                // $('.select_list li').remove();
                if ($("section.content div").is('.fulltext')) {
                    $('.fulltext div').remove();
                    $('.sText').text(word);
                } else {
                    $('.wraper').remove();
                    $('.content').append(h);
                }


                $.map(data.responseJSON, function (st) {
                    if (ndh != st.nd) {
                        ndh = st.nd;
                        if (ndh != 0) {
                            textd += '</div>';
                            $('.fulltext').append(textd);
                        }
                        textd = "";
                        textd = '<div class="col-md-12"><div class="panel panel-color panel-info">'
                        if(type=='lnd'){
                           st.nd+='&ext=1'
                        };
                        textd+= '<a style="color:black" target="_blank" href="/nd?nd='+ st.nd + '">'

                        textd+= '<div class="panel-heading" style="font-size: small" >' +
                            '' + st.title + '</div></a>' +
                            '<div class="panel-body">'
                        if (st.job !== null) {
                            textd += '<p>Группа:<u>' + st.job + '</u></em></p>'
                        }

                        textd+='<p style="font-size: small">' + st.documsn + '</p></div>' +
                        // '<div class="panel-footer"><p style="color: #086b02"><em>' + st.status + ' с ' + st.date_added + '</em></p></div>' +
                        '</div>';
                    } else {
                        textd += '<hr><div class="panel-body">' +
                            '<p>Группа:<u>' + st.job + '</u></em></p>' +
                            '<p>' + st.documsn + '</p>' +
                            // '<p style="color: #086b02"><em>' + st.status + ' с ' + st.date_added + '</em></p>' +
                            '</div>'
                    }
                })
                dialog.modal('hide');
            },
            error: function (data) {
                dialog.modal('hide');
                autoHideNotify('error', 'top right', 'Уважаемый пользователь', 'При поиске произошла ошибка');
            }
        });
    }
}
