<? include_once $_SERVER['DOCUMENT_ROOT'].'/app/views/header.php'; ?>
<? include_once $_SERVER['DOCUMENT_ROOT'].'/app/views/template.php'; ?>

<?

// Выбираем ВСЕ ehs и виды работ
$ej = $db->query("SELECT ehs.id, ehs.ehs_numbers, ehs.ehs_name, ehs.Parent_id_job, types_jobs.type FROM `ehs`
						   LEFT JOIN `types_jobs`
						   ON ehs.parent_id_job = types_jobs.id
						   ORDER BY ehs.ehs_numbers ASC
 						   ");

// Собираем общий массив ehs и видов работ
while ($row = $ej->fetch_assoc()) {

	$columns[$row['id']]['ehs-id'] = $row['id'];
	$columns[$row['id']]['ehs-name'] = $row['ehs_name'];
	$columns[$row['id']]['ehs-number'] = $row['ehs_numbers'];
	$columns[$row['id']]['job-id'] = $row['Parent_id_job'];
	$columns[$row['id']]['job-name'] = $row['type'];

}

// Массив EHS
$ehs = $columns;

?>

<!--Main Content -->
<section id="editChunk" class="content">

	<!-- Page Content -->

	<div class="wraper container-fluid">
		<div class="page-title">
			<h3 class="title">Редактор требований</h3>
		</div>

		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">
					
						<h3 class="panel-title pull-left m-t-10">Установка связей между стандартами(EHS) и видами работ</h3>
						<div class="pull-right">
							<button class="btn btn-inverse" data-toggle="tooltip" data-placement="left" title="В таблице ниже вы можете добавить новые стандарты(EHS) и сопоставить с ними определенные виды работ. Чтобы добавить или отредактировать список возможных видов работ кликните по ссылке 'Редактировать виды работ' и отредактируйте список"><i class="ion-information"></i></button>
						</div>
						<div class="clearfix"></div>
					
					</div>
					<div class="panel-body">

						<div class="row">
							<div class="col-md-12">
								<div class="m-b-20" id="datatable-filter">
									<ul class="header-filter">
										<li>
											<a href="#" data-target="#editEHS" data-toggle="modal">Редактировать EHS</a>
										</li>
										<li>
											<a href="#" data-target="#editJOB" data-toggle="modal">Редактировать виды работ</a>
										</li>
									</ul>
			                    </div>
							</div>	
						</div>

	                    <table id="datatable" class='table table-striped table-bordered table-hover pad-5 pointer' id="ehs">
	                    	<thead>
	                    		<tr>
									<th>Номер EHS</th>
									<th>Наименование EHS</th>
									<th>Вид работы</th>
									<th>Сохранить</th>
									<th class="hidden"></th>
								</tr>
							</thead>
	                  	    
	                  	    <tbody>
		                       	<tr>
									<td>
										<input type="text" class="form-control w-100 ehs-number" 
										value=""
										onchange="buttonActivity(); getEhsName();" >
									</td>
									<td>
										<input type="text" class="form-control w-100 ehs-name" 
										value=""
										onchange="buttonActivity();" >
									</td>
									<td>
										<input type="text" class="form-control w-100 job-name" 
										value=""
										onchange="buttonActivity();" >
									</td>
									<td class="text-center">
										<input type="button" value="Установить связь" class="btn btn-default">
									</td>
									<td class="hidden">
										<input type="hidden" data-ehs="" data-job="">
									</td>
		                       	</tr>
	                  	    <? foreach($ehs as $key=>$value){?>
		                       	<tr>
									<td>
										<input type="text" class="form-control w-100 ehs-number"
										value="<?=$ehs[$key]['ehs-number'];?>"
										<? //if ( $access['standart-editor']['update'] == 0 ) { ?>
											disabled="disabled"
										<? //} ?>										
										onchange="buttonActivity(); getEhsName();">
										<p class="hidden"><?=$ehs[$key]['ehs-number'];?></p>
									</td>
									<td>
										<input type="text" class="form-control w-100 ehs-name" 
										value="<?=$ehs[$key]['ehs-name'];?>"
										disabled="disabled"
										onchange="buttonActivity();">
										<p class="hidden"><?=$ehs[$key]['ehs-name'];?></p>
									</td>
									<td>
										<input type="text" class="form-control w-100 job-name" 
										data-ehs="<?=$ehs[$key]['ehs-number'];?>"
										value="<?=$ehs[$key]['job-name'];?>"
										disabled="disabled"
										onchange="buttonActivity();">
										<p class="hidden"><?=$ehs[$key]['job-name'];?></p>
									</td>
									<td class="text-center">
										<!--<input type="button" value="Сохранить" class="btn btn-default">-->
										<? if ( $access['standart-editor']['delete'] == 1 ) { ?>
											<input type="button" value="Удалить" onclick="removeData();" class="btn btn-danger">
										<? } ?>
									</td>
		                       		<td class="hidden">
		                       			<input type="hidden" 
		                       			 data-ehs="<?=$ehs[$key]['ehs-id'];?>" 
		                       			 data-job="<?=$ehs[$key]['job-id'];?>">
		                       		</td>
		                       	</tr>
		                    <? } ?>   	
		                    </tbody>
	                        
	                    </table>
					</div>
				</div>
			</div><!-- .col-md-6 -->

		</div>
		<!-- End Row -->

	</div>
<input type="hidden" value="" id="jobs-array">
</section>

<? include 'footer.php'; ?>
<script>
$(document).ready(function () {

	/*var table = $('#datatable').dataTable({
		"pageLength": 500,
	       "columnDefs": [
			{ "width": "10%", "targets": 0 },
			{ "width": "35%", "targets": 1 },
			{ "width": "35%", "targets": 1 },
			{ "width": "20%", "targets": 3 },
        ],
        "order": [[ 0, 'asc' ]],
        "language": {
            "lengthMenu": "Выводить по _MENU_ записей",
            "zeroRecords": "Ни одной записи не найдено",
            "info": "Показано страниц _PAGE_ из _PAGES_",
            "infoEmpty": "Ни одной записи не найдено",
            "infoFiltered": "(отфильтровано из _MAX_ записей)",
            "search": "Поиск:",
		    "paginate": {
		        "first":      "Первая",
		        "last":       "Последняя",
		        "next":       "Следующая",
		        "previous":   "Предыдущая"
		    },
        }
	});*/

	$('#datatable').on( 'page.dt', function () {
		// autoCompleteInit( getEHS(),getJOB() );
	    // var info = table.page.info();
	    // $('#pageInfo').html( 'Showing page: '+info.page+' of '+info.pages );
	} );

// console.log( getEHS() );
// console.log( getJOB() );

autoComplete( getEHS(),'ehs-number' );
autoComplete( getJOB(),'job-name' );

});

function getEhsName(){
	$(event.target).parents('tr').find('.ehs-name').val( getFieldByField('ehs','ehs_numbers',event.target.value,'ehs_name') )
}

// Добавить или отредактировать запись
function editData(){
	$('#datatable .btn-primary').click(function( event ){



	    $.ajax({
	        type: 'POST',
	        url: "/assets/functions.php",
	        cache: false,
	        data: {
	    		"action": "setRelations",
	        	"ehs_numbers": $(this).parents('tr').find('td').eq(0).find('input[type="text"]').val(),
	        	"ehs_name": $(this).parents('tr').find('td').eq(1).find('input[type="text"]').val(),
	        	"type": $(this).parents('tr').find('td').eq(2).find('input[type="text"]').val(),
	        	"ehs_id": $(this).parents('tr').find('input[type="hidden"]').attr('data-ehs'),
	        	"job_id": $(this).parents('tr').find('input[type="hidden"]').attr('data-job'),
	        },
	        success: function (msg) {
	        	console.log(msg);
	 			location.reload();
	        }
	    });
	})
}

// Удалить запись
function removeData(){

    $.ajax({
        type: 'POST',
        url: "/assets/functions.php",
        cache: false,
        data: {
    		"action": "removeRelations",
        	"ehs_numbers": $(event.target).parents('tr').find('td').eq(0).find('input[type="text"]').val(),
        	"ehs_name": $(event.target).parents('tr').find('td').eq(1).find('input[type="text"]').val(),
        	"type": $(event.target).parents('tr').find('td').eq(2).find('input[type="text"]').val(),
        	"ehs_id": $(event.target).parents('tr').find('input[type="hidden"]').attr('data-ehs'),
        	"job_id": $(event.target).parents('tr').find('input[type="hidden"]').attr('data-job'),
        },
        success: function (msg) {
        	// console.log(msg);
 			location.reload();
        }
    });

}

function buttonActivity(){
	var error = false;
	$(event.target).parents('tr').find('input[type="text"]').each(function( index,item ){
		if ( item.value == '') {
			error = true;
			$(event.target).parents('tr').find('input[type="button"]').removeClass('btn-primary');
			return false;
		};
	})
	if (!error) {
		$(event.target).parents('tr').find('input[type="button"]').addClass('btn-primary');
	};
	editData();
}

// Автокомплит инициализация
// function autoCompleteInit(ehs,jobs){
// 	$('.ehs-number').each(function( index, value ){
// 		autoComplete(ehs,'ehs-number');
// 	})
// 	$('.job-name').each(function( index, value ){
// 		autoComplete(jobs,'job-name');
// 	})
// }

// Автокомплит
function autoComplete(arr,className){
    var itemArr = arr;

    // console.log(itemArr);

    function split( val ) {
      return val.split( /,\s*/ );
    }
    function extractLast( term ) {
      return split( term ).pop();
    }
 
    $( 'input.' + className )
      // don't navigate away from the field on tab when selecting an item
      .on( "keydown", function( event ) {
        if ( event.keyCode === $.ui.keyCode.TAB &&
            $( this ).autocomplete( "instance" ).menu.active ) {
          event.preventDefault();
        }
      })
      .autocomplete({
        minLength: 0,
	    open: function(){
	        setTimeout(function () {
	            $('.ui-autocomplete').css('z-index', 99999999999999);
	        }, 0);
	    },
        source: function( request, response ) {
          // delegate back to autocomplete, but extract the last term
          response( $.ui.autocomplete.filter(
            itemArr, extractLast( request.term ) ) );
        },
        focus: function() {
          // prevent value inserted on focus
          return false;
        },
        select: function( event, ui ) {
          var terms = split( this.value );
          // remove the current input
          terms.pop();
          // add the selected item
          terms.push( ui.item.value );
          // add placeholder to get the comma-and-space at the end
          terms.push( "" );
          this.value = terms.join( "" );

          return false;
        }
      });
}	

</script>