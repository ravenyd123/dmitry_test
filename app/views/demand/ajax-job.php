<div class="modal-body">
  <div class="row">
    <div class="job">
      <div class="col-md-10">
        <? if (in_array("C", $access['editor'])) { ?>
        <div class="form-group">
          <input class="form-control" type="text" name="job-new" value="" >
          <div class="help-block"></div>
        </div>
        <? } ?>
      </div> 
      <? if (in_array("C", $access['editor'])) { ?> 
      <div class="col-md-2 form-inline">
        <div class="form-group m-5">
          <a href="javascript:void(0);" onclick="addJob()">
            <i class="fa ion-plus text-success"></i>
          </a>
        </div>
      </div> 
      <? } ?>
    </div>
      <?php
      $db=Db::Connection();
      $result=$db->query("SELECT * FROM `types_jobs` ");
      while ($row = $result->fetch_assoc()) {
      ?>
    <div class="job">
      <div class="col-md-10">
        <div class="form-group">
          <input class="form-control" type="text" name="job-<?=$row['id'];?>" value="<?=$row['type'];?>" >
        </div>
      </div>  
      <div class="col-md-2 form-inline">
        <div class="form-group m-5">
          <? if (in_array("U", $access['editor'])) { ?>
          <a href="javascript:void(0);" onclick="saveJob(<?=$row['id'];?>)">
            <i class="fa ion-checkmark text-success"></i>
          </a>
          <? } ?>
          <div class="help-block"></div>
        </div>
        <div class="form-group m-5">
          <? if (in_array("D", $access['editor'])) { ?>
          <a href="javascript:void(0);" onclick="removeJob(<?=$row['id'];?>);">
            <i class="fa ion-close text-danger"></i>
          </a>
          <? } ?>
          <div class="help-block"></div>
        </div>
      </div>  
    </div>            
    <? } ?>
  </div>
</div>