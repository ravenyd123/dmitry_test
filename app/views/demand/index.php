<? include_once $_SERVER['DOCUMENT_ROOT'] . '/app/views/header.php'; ?>
<? include_once $_SERVER['DOCUMENT_ROOT'] . '/app/views/template.php'; ?>

<!--Main Content -->
<section class="content">

    <!-- Page Content -->
    <div class="wraper container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title pull-left m-t-10">Редактор требований</h3>
                        <div class="col-md-3" <?= $_COOKIE['company'] != 'all' ? "hidden" : "" ?>>
                            <select name="company" class="select2">
                                <? foreach (company as $key => $value) { ?>
                                    <option value="<?= $key; ?>" <?= $_COOKIE['company'] == $key ? "selected" : "" ?>><?= $value; ?></option>
                                <? } ?>
                            </select>
                        </div>
                        <div class="top_nav">

                            <? if (in_array("C", $access['tasks'])) { ?>
                                <a href="#" data-target="#demandModal" data-toggle="modal">
                                    <button class="btn btn-success" data-action="add-data" data-toggle="tooltip"
                                            data-placement="top" title="Создать требование"><i class="ion-plus"></i>
                                    </button>
                                </a><br>
                            <? } ?>
                            <? if (in_array("U", $access['tasks'])) { ?>
                                <button class="btn btn-default" data-toggle="tooltip" data-placement="top"
                                        title="Утвердить требования" data-action="approve-data"><i
                                            class="ion-checkmark"></i></button><br>
                                <button class="btn btn-default" data-action="edit-data" data-toggle="tooltip"
                                        data-placement="top" title="Редактировать требование"><i class="ion-edit"></i>
                                </button><br>
                            <? } ?>
                            <? if (in_array("D", $access['tasks'])) { ?>
                                <button class="btn btn-default" data-action="remove-data" data-toggle="tooltip"
                                        data-placement="top" title="Удалить требования"><i class="ion-trash-a"></i>
                                </button><br>
                            <? } ?>
                            <? if (in_array("U", $access['tasks'])) { ?>
                                <!--                            <a href="#" data-target="#add-listdata" data-toggle="modal">-->
                                <button class="btn btn-default" data-action="add-listdata" data-toggle="tooltip"
                                        data-placement="top" title="Создать список"><i class="ion-nuclear"></i>
                                </button>
                                <!--                            </a>-->
                            <? } ?>

                        </div>
                        <div class="clearfix"></div>
                    </div>

                    <div class="panel-body">

                        <!--                     <p id="load-text">Идет загрузка данных...</p>-->
                        <div id="ajax-data">

                            <div class="row m-b-15">

                                <div class="" id="datatable-filter">

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="doc">Название документа:</label>
                                            <div class="doc"></div>

                                        </div>
                                    </div>
                                                                            <div class="col-md-3 vid">
                                                                            </div>

                                                                            <div class="col-md-3 pvid eh">
                                                                            </div>

                                                                            <div class="col-md-3 st">

                                                                            </div>
                                                                            <div class="col-md-3 ot">

                                                                            </div>
                                                                            <div class="col-md-3 pl">

                                                                            </div>
                                                                            <div class="col-md-3 tp">

                                                                            </div>

<!--                                        <div class="col-md-3 ">-->
<!--                                            <div class="form-group">-->
<!--                                                <label for="column_1">Вид работ:</label>-->
<!--                                                <span id="column_1"></span>-->
<!--                                            </div>-->
<!--                                        </div>-->
<!---->
<!--                                        <div class="col-md-3">-->
<!--                                            <div class="form-group">-->
<!--                                                <label for="column_2">Подвид работ:</label>-->
<!--                                                <span id="column_2"></span>-->
<!--                                            </div>-->
<!--                                        </div>-->
<!--                                        <div class="col-md-3 ">-->
<!--                                            <div class="form-group">-->
<!--                                                <label for="column_3">Статус:</label>-->
<!--                                                <span id="column_3"></span>-->
<!--                                            </div>-->
<!--                                        </div>-->

                                    </div>

                                </div>

                                <div class="row">
                                    <div class="col-md-12 col-sm-12 col-xs-12">

                                        <table id="datatable"
                                               class="table table-hover table-striped table-bordered">
                                            <!--                                        <thead>-->
                                            <!--                                        <tr>-->
                                            <!--                                            <th class="text-center"><input type="checkbox" name="select_all" value="1"-->
                                            <!--                                                                           id="select-all"></th>-->
                                            <!--                                            <th>Вид работы</th>-->
                                            <!--                                            <th>Под вид работы</th>-->
                                            <!--                                            <th>Требование</th>-->
                                            <!--                                            <th>Документ</th>-->
                                            <!--                                            <th>Пункт</th>-->
                                            <!--                                            <th>Отрасль</th>-->
                                            <!--                                            <th>Статус</th>-->
                                            <!--                                            <th>Актуальность</th>-->
                                            <!---->
                                            <!--                                        </tr>-->
                                            <!--                                        </thead>-->

                                            <!--                                     <tbody>-->
                                            <? /*while ($row = mysqli_fetch_assoc($demands)) { ?>
										<tr class="text-center" data-id="<?=$row["id"];?>">
											<td><input type="checkbox" name="id[]"></td>
											<td class="text-center"><?=getEhsById($link,$row["ehs"]); ?></td>
											<td><?=getJobById($link,$row["job"]); ?></td>
											<td class="text-left"><?=html_entity_decode($row["demand"]);?></td>
											<td><?=getDocumentByNd($link,$row["document"]); ?></td>
											<td class='<?=demandStatusColor( $row["status"] )?>'>
												<?//=cropStatus( getDocumentActivityByNd($link,$row["document"]) ); ?>
												<?=$row["status"];?>
											</td>
											<td><?=date("d-m-Y",$row["date"]);?></td>
										</tr>
									<? }*/ ?>
                                            <!--                                        </tbody>-->
                                        </table>

                                    </div>
                                </div>

                            </div><!-- #load-data -->

                        </div>
                    </div>
                </div>

            </div>
            <!-- End Row -->
        </div>

</section>
<script>

    var jsonData = JSON.parse('<?php echo json_encode(recursion(true, true)); ?>');
    var type_list = JSON.parse('<?php echo json_encode(type_list) ?>');
    var platform = JSON.parse('<?php echo json_encode(platforms_user) ?>');

</script>

<script src="/node_modules/jstree/dist/jstree.min.js"></script>
<link rel="stylesheet" href="/node_modules/jstree/dist/themes/default/style.min.css"/>
<link rel="stylesheet" href="/app/views/demand/css/table.css">

<!--<link rel="stylesheet" href="/assets/admina/plugins/yadcf/jquery.dataTables.yadcf.css">-->
<script type="text/javascript" src="/node_modules/yadcf/jquery.dataTables.yadcf.js"></script>

<script type="text/javascript" src="/node_modules/bootstrap3-wysihtml5-bower/dist/bootstrap3-wysihtml5.min.js"></script>
<!--<script type="text/javascript" src="/assets/admina/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>-->
<script type="text/javascript" src="/app/views/demand/js/index.js">

</script>
<script type="text/javascript" src="/app/views/demand/js/listd.js"></script>

