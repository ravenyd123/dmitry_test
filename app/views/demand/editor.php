<? include_once $_SERVER['DOCUMENT_ROOT'].'/app/views/header.php'; ?>
<? include_once $_SERVER['DOCUMENT_ROOT'].'/app/views/template.php'; ?>

<?
$db=Db::Connection();
// Выбираем ВСЕ ehs и виды работ
$ej=$db->query("SELECT ehs.id, ehs.ehs_numbers, ehs.ehs_name, ehs.Parent_id_job, types_jobs.type FROM `ehs`
						   LEFT JOIN `types_jobs`
						   ON ehs.parent_id_job = types_jobs.id
						   ORDER BY ehs.ehs_numbers ASC
 						   ");

while ($row = $ej->fetch_assoc()) {

	$columns[$row['id']]['ehs-id'] = $row['id'];
	$columns[$row['id']]['ehs-name'] = $row['ehs_name'];
	$columns[$row['id']]['ehs-number'] = $row['ehs_numbers'];
	$columns[$row['id']]['job-id'] = $row['Parent_id_job'];
	$columns[$row['id']]['job-name'] = $row['type'];

}

// Массив EHS
$ehs = $columns;

?>

<!--Main Content -->
<section id="editChunk" class="content">

	<!-- Page Content -->

	<div class="wraper container-fluid">
		<div class="page-title">
			<h3 class="title">Редактор стандартов</h3>
		</div>

		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">
					
						<h3 class="panel-title pull-left m-t-10">Установка связей между стандартами(EHS) и видами работ</h3>
						<div class="pull-right">
							<button class="btn btn-inverse" data-toggle="tooltip" data-placement="left" title="В таблице ниже вы можете добавить новые стандарты(EHS) и сопоставить с ними определенные виды работ. Чтобы добавить или отредактировать список возможных видов работ кликните по ссылке 'Редактировать виды работ' и отредактируйте список"><i class="ion-information"></i></button>
						</div>
						<div class="clearfix"></div>
					
					</div>
					<div class="panel-body">

						<div class="row">
							<div class="col-md-12">
								<div class="m-b-20" id="datatable-filter">
									<ul class="header-filter">
										<li>
											<a href="#" data-target="#editJOB" data-toggle="modal">Редактировать виды работ</a>
										</li>
									</ul>
			                    </div>
							</div>	
						</div>

	                    <table id="datatable" class='table table-hover table-striped table-bordered table-hover pad-5 pointer' id="ehs">
	                    	<thead>
	                    		<tr>
									<th>Номер EHS</th>
									<th>Наименование EHS</th>
									<th>Вид работы</th>
									<th>Сохранить</th>
									<th class="hidden"></th>
								</tr>
							</thead>
	                  	    
	                  	    <tbody>
		                       	<tr id="new-relation">
									<td>
										<input type="text" class="form-control w-100 ehs-number" 
										value=""
										onchange="buttonActivity(); getEhsName();" >
									</td>
									<td>
										<input type="text" class="form-control w-100 ehs-name" 
										value=""
										onchange="buttonActivity();" >
									</td>
									<td>
										<input type="text" class="form-control w-100 job-name" 
										value=""
										onchange="buttonActivity();" >
									</td>
									<td class="text-center">
										<input type="button" value="Установить связь" class="btn btn-default">
									</td>
									<td class="hidden">
										<input type="hidden" data-ehs="" data-job="">
									</td>
		                       	</tr>
	                  	    <? foreach($ehs as $key=>$value){?>
		                       	<tr>
									<td>
										<input type="text" class="form-control w-100 ehs-number"
										value="<?=$ehs[$key]['ehs-number'];?>"
										<? //if ( $access['standart-editor']['update'] == 0 ) { ?>
											disabled="disabled"
										<? //} ?>										
										onchange="buttonActivity(); getEhsName();">
										<p class="hidden"><?=$ehs[$key]['ehs-number'];?></p>
									</td>
									<td>
										<input type="text" class="form-control w-100 ehs-name" 
										value="<?=$ehs[$key]['ehs-name'];?>"
										disabled="disabled"
										onchange="buttonActivity();">
										<p class="hidden"><?=$ehs[$key]['ehs-name'];?></p>
									</td>
									<td>
										<input type="text" class="form-control w-100 job-name" 
										data-ehs="<?=$ehs[$key]['ehs-number'];?>"
										value="<?=$ehs[$key]['job-name'];?>"
										disabled="disabled"
										onchange="buttonActivity();">
										<p class="hidden"><?=$ehs[$key]['job-name'];?></p>
									</td>
									<td class="text-center">
										<!--<input type="button" value="Сохранить" class="btn btn-default">-->
										<? if ( $access['standart-editor']['delete'] == 1 ) { ?>
											<input type="button" value="Удалить" onclick="removeData();" class="btn btn-danger">
										<? } ?>
									</td>
		                       		<td class="hidden">
		                       			<input type="hidden" 
		                       			 data-ehs="<?=$ehs[$key]['ehs-id'];?>" 
		                       			 data-job="<?=$ehs[$key]['job-id'];?>">
		                       		</td>
		                       	</tr>
		                    <? } ?>   	
		                    </tbody>
	                        
	                    </table>
					</div>
				</div>
			</div><!-- .col-md-6 -->

		</div>
		<!-- End Row -->

	</div>
<input type="hidden" value="" id="jobs-array">
</section>

<? include 'footer.php'; ?>
<script type="text/javascript" src="/app/views/demand/js/editor.js"></script>
