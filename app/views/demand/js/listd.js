$(document).ready(function () {
    var Ntr = [];//массив id требовании

    var platform_tree = $("#platform_tree_list").jstree({
        "checkbox": {
            "three_state": true,
            "cascade": "none"
        },
        "plugins": ["checkbox", "wholerow", "types", "conditionalselect"],
        "core": {
            "check_callback": false,
            "multiple": true,
            "themes": {
                "dots": false,
                "icons": true,
                "variant": false
            },
            data: jsonData
        },
        "types": {
            "#": {
                "max_depth": 3
            },
            "level0": {
                "icon": "fa fa-bank"
            },
            "level1": {
                "icon": "fa fa-briefcase"
            },
            "level2": {
                "icon": "fa fa-cube"
            }
        }
    }).bind("load_node.jstree", function (e, data) {
        // $(this).find('li[rel!=file]').find('.jstree-checkbox:first').hide();
    });
// получить данные
    $('*[data-action="add-listdata"]').click(function (e) {

        var trlen = 0;

        $('#add-listdata').data('action', ' ');

        Ntr = []
        // если равно 0
        trlen = checkRowAmount_new();

        if (trlen <= 0) {
            $.notify({'type':'error', 'message':'Необходимо выбрать хотя бы одну запись!'});
            return false;
        } else {
            $('#datatable tr.selected').each(function (index) {
                Ntr.push($(this).attr('id'));
            })
        }

        if (trlen > 1) {
            // добавить action
            $('#add-listdata').data('action', 'add');
            $.notify({'type':'error', 'message':'Выбранно несколько записей. Работает только добавление ,но не редактирование'});
        }

        var tree = $("#platform_tree_list");

        if (trlen == 1) {
            data = $('#datatable').dataTable().api().row('#' + Ntr[0]).data();

            console.log(data.sett_pl_int);

            if (data.sett_pl_int !== null) {

                $('#add-listdata').data('action', 'edit');

                data_fix = {platform: data.sett_pl_int, type: data.sett_type};
console.log(data_fix);
                //var tree = $("#platform_tree_list");

                tree.jstree("deselect_all");

                tree.jstree().select_node(data_fix.platform, true);

                $('div#list_type_demand input').each(function () {
                    if (data_fix.type.includes($(this).attr('value'))) {
                        $(this).attr('checked', "true");
                    } else {
                        $(this).removeAttr('checked');
                    }
                });
//            data = {sett_pl: [], sett_type: []};
            } else {
                console.log('1');
                tree.jstree("deselect_all");
                $('#add-listdata').data('action', 'add')
                $('div#list_type_demand input').each(function () {$(this).removeAttr('checked');})
            }


        }

        $('#add-listdata').modal()

    });

    function get_cookie(cookie_name) {
        var results = document.cookie.match('(^|;) ?' + cookie_name + '=([^;]*)(;|$)');
        if (results)
            return (unescape(results[2]));
        else
            return null;
    };

    $('#save_list').on('click', function (event) {
        //получить тип
        //получить статус редактор или add
        var type_l = [];

        $('div#list_type_demand input:checked').each(function () {
            type_l.push($(this).attr('value'));
        });
        //получиь площадки
        var pl = $("#platform_tree_list").jstree("get_selected");
        var alldata = {};
        $.each(Ntr, function (index, val) {
            var ss={};
            $.each(type_l, function (index, vall) {
                 ss[vall]=pl;
            });
            alldata[val] = ss
        });
        data = alldata;
        $.ajax({
            type: 'POST',
            data: {
                "action": $('#add-listdata').data('action'),
                "data": JSON.stringify(data)
            },
            dataType: "json",
            url: "/app/views/demand/list_data.php",
            cache: false,
            complete: function (data) {
                $('#datatable').DataTable().ajax.reload();
                $('#add-listdata').modal('hide');
            }
        });

    })

})
//сохранить данные

