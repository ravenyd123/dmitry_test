$(document).ready(function () {
    var company=$("select[name='company']");
    var text = "";
    //var register_table = $('#datatable').dataTable();
    // var executed = false;
    editor = $('.wysihtml5').wysihtml5({
        events: {
            "change": function () {
                if (editor.data("wysihtml5").editor.getValue() != '') {
                    $('.wysihtml5-sandbox').closest('.form-group').removeClass('has-error');
                    $('.wysihtml5-sandbox').closest('.form-group').find('.help-block').text('');
                }
                ;
            }
        }
    });
    //  checkRowAmount();
    var color = {
        "Недействующий": 'red',
        "Действующий": '#086b02',
        "На утверждении": '#a99e13b8',
        "Действующий (актуальный)": '#086b02'
    };

 var company_name=company.val();

    var table = $('#datatable').dataTable({
        // "deferRender": true,
        // "ajax": "demand/ajax?company="+company_name,
        "serverSide": true,
        "ordering": true,
        "iDisplayLength": 25,
        // "orderClasses": false,
        "processing": true,
        ajax: {
            type: 'POST',
            url: "/demand/ajax2",
            data: {"action": "demand_all","company": company_name},
            dataType: "json",
            cache: false,
        },
        //   "search": {"regex": true,"smart": false},
        columnDefs: [
            {
                "title": "№",
                "targets": [0],
                "className": "text-center",
                "data": "id",
                "orderable": false, "defaultContent": '', "width": "3%"
            },
            {
                "title": "Вид работы",
                "targets": [1],
                //"className": "text-center",
                "data": "job",
                "type": "de_datetime",
                "width": "10%"
            },
            {"title": "Под вид", "targets": [2], "className": "", "data": "job_child", "width": "3%"},
            {"title": "Требование", "targets": [3], "className": "", "data": "demand"},
            {"title": "Документ", "targets": [4], "className": "", "data": "document"},
            {
                "title": "Пункт",
                "targets": [5],
                "className": "text-center",
                "data": null,
                "width": "3%",
                "visible": false,
                "defaultContent": ''
            },
            {
                "title": "Отрасль",
                "targets": [6],
                "className": "text-center",
                "data": "industry",
                //"defaultContent": '',
                "visible": false,
                "width": "3%"
            },
            {
                "title": "Статус",
                "targets": [7],
                "className": "text-center",
                "data": "status",
                "visible": false,
                //    "width": "5%"
            },
            {
                "title": "Актуальность",
                "targets": [8],
                "className": "text-center",
                "data": "activity",
                "visible": false,
                //    "width": "10%"
            },
            {
                "title": "площадка",
                "targets": [9],
                "className": "text-center",
                "data": "sett_pl",
                "defaultContent": '',
                 "visible": true,
                //    "width": "10%"
            },
            {
                "title": "Тип",
                "targets": [10],
                "className": "text-center",
                "data": "sett_type",
                "visible": true,
                "defaultContent": '',
                //"visible": false,
                //    "width": "10%"
            }

        ],

        createdRow: function (row, data, dataIndex) {
           // $(row).find('td').eq(5).append('<a href="' + data.link + '" target=\'_blank\'>' + data.pname + '</a>');

            $(row).find('td').eq(3).append('<p><span style="color:' + color[data.status] + '"><em>"' + data.status + '</em>' +
                ' c ' + data.activity + '</span>" Отрасль:"' + data.industry + '"</p><span>Пункт: <a href="'
                + data.link + '" target=\'_blank\'>' + data.pname + '</a></span>');

            $(row).attr("id", data['id']);
        },
        initComplete: function () {
            // фильтрация по документам
            this.api().column(4).every(function () {
                var column = this;

                if (typeof $.cookie('document') !== 'undefined') {
                    column.search($.cookie('document')).draw();
                }

                var select = $('<select id="chooze-document" class="select2"><option value="">Все документы</option></select>')
                    .appendTo($('#datatable-filter .doc'))
                    .on('change', function () {

                        $.cookie('document', $(this).val(), {
                            expires: 5
                        });

                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );

                        column
                            .search(val ? val : '', true, false)
                            .draw();
                    });

                // console.log($.cookie("document"));
                column.data().unique().sort().each(function (d, j) {
                    if (d == $.cookie("document")) {
                        select.append('<option selected="selected" value="' + d + '">' + d + '</option>')
                    } else {
                        select.append('<option value="' + d + '">' + d + '</option>')
                    }

                });
            });
            // фильтрация по под вид
            // this.api().column(2).every(function () {
            //     var column = this;
            //     var select = $('<select class="select2 m-t-10"><option value="">Все под виды</option></select>')
            //         .appendTo($('#datatable-filter .pvid'))
            //         .on('change', function () {
            //             var val = $.fn.dataTable.util.escapeRegex(
            //                 $(this).val()
            //             );
            //
            //             column
            //                 .search(val ? '^' + val + '$' : '', true, false)
            //                 .draw();
            //         });
            //     column.data().unique().sort().each(function (d, j) {
            //         select.append('<option value="' + d + '">' + d + '</option>')
            //     });
            // });
            //фильт по отраслям
            // this.api().column(6).every(function () {
            //     var column = this;
            //     var select = $('<select class="select2 m-t-10"><option value="">Все отрасли</option></select>')
            //         .appendTo($('#datatable-filter .ot'))
            //         .on('change', function () {
            //             var val = $.fn.dataTable.util.escapeRegex(
            //                 $(this).val()
            //             );
            //
            //             column
            //                 .search(val ? '^' + val + '$' : '', true, false)
            //                 .draw();
            //         });
            //     let ar=column.data().unique().join(',').split(",");
            //
            //     $.each(my_unique(ar),function (index, value) {
            //         select.append('<option value="' + value + '">' + value + '</option>')
            //     });
            // });
            // фильтрация по видам работ
            // this.api().column(1).every(function () {
            //     var column = this;
            //     var select = $('<select class="select2 m-t-10"><option value="">Основной вид работ</option></select>')
            //         .appendTo($('#datatable-filter .vid'))
            //         .on('change', function () {
            //             var val = $.fn.dataTable.util.escapeRegex(
            //                 $(this).val()
            //             );
            //
            //             column
            //                 .search(val ? val : '', true, false)
            //                 .draw();
            //         });
            //
            //     column.data().unique().sort().each(function (d, j) {
            //         select.append('<option value="' + d + '">' + d + '</option>')
            //     });
            // });
            // фильтрация по статусу
            // this.api().column(7).every(function () {
            //     var column = this;
            //     var select = $('<select class="select2 m-t-10"><option value="">Любой статус</option></select>')
            //         .appendTo($('#datatable-filter .st'))
            //         .on('change', function () {
            //             var val = $.fn.dataTable.util.escapeRegex(
            //                 $(this).val()
            //             );
            //
            //             column
            //                 .search(val ? '^' + val + '$' : '', true, false)
            //                 .draw();
            //         });
            //     column.data().unique().sort().each(function (d, j) {
            //         select.append('<option value="' + d + '">' + d + '</option>')
            //     });
            // });
            //по площадке
            this.api().column(9).every(function () {
                var column = this;
                var select = $('<select class="select2"><option value="">Все</option></select>')
                    .appendTo($('#datatable-filter .pl'))
                    .on('change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );

                        column
                            .search(val ? val : '', true, false)
                            .draw();
                    });
                $.each(platform,function (j,d ) {
                    select.append('<option value="' + j + '">' + d + '</option>')
                });
                // column.data().unique().sort().each(function (d, j) {
                //
                //     select.append('<option value="' + d + '">' + d + '</option>')
                // });
            });
            // по типу
            this.api().column(10).every(function () {
                var column = this;
                var select = $('<select class="select2"><option value="">Все типы</option></select>')
                    .appendTo($('#datatable-filter .tp'))
                    .on('change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );
                  column
                      .search(val ? val  : '', true, false)
                      .draw();

                    });
                //  console.log(column.data().unique().sort().search('^' + 5 + '$'));//.split(',')
                // column.data().unique().sort().each(function (d, j) {
                //     select.append('<option value="' + d + '">' + d + '</option>')
                // });
               $.each(type_list,function (j,d ) {
                    select.append('<option value="' + j + '">' + d + '</option>')
                });
            });
            $('select.select2').select2();
        }

        ,
        //"bStateSave": true,
        "language": {
            "processing": "Производиться загрузка данных",
            "lengthMenu": "Выводить по _MENU_ записей",
            "zeroRecords": "Ни одной записи не найдено",
            "info": "Показано с _START_ по _END_ из _TOTAL_ записей",
            "infoEmpty": "Ни одной записи не найдено",
            "infoFiltered": "(отфильтровано из _MAX_ записей)",
            "search": "Поиск:",
            "paginate": {
                "first": "Первая",
                "last": "Последняя",
                "next": "Следующая",
                "previous": "Предыдущая"
            }
            ,
        }
    })

    table.yadcf([
        {
            column_number: 1,
            filter_container_id: 'column_1',
            filter_default_label: "Все",
            select_type: 'select2',
            filter_reset_button_text: false,
            select_type_options: {theme: 'bootstrap'},
            filter_match_mode: 'regex'
            // allowClear: false
        },{
            column_number: 2,
            filter_container_id: 'column_2',
            filter_default_label: "Все",
            select_type: 'select2',
            filter_reset_button_text: false,
            select_type_options: {theme: 'bootstrap'},
            filter_match_mode: 'regex'
            // allowClear: false
        },{
            column_number: 7,
            filter_container_id: 'column_3',
            filter_default_label: "Все",
            select_type: 'select2',
            filter_reset_button_text: false,
            select_type_options: {theme: 'bootstrap'},
            // allowClear: false
        },
    ]);

    company.on('change',function(e){
        company_name=e.target.value;
        table.api().ajax.url("demand/ajax?company=" + company_name);
        table.api().ajax.reload();
    });

    $('#datatable tbody').on('click', 'tr', function () {

        $(this).toggleClass('selected');
        checkRowAmount_new();
    });

// Добавление/редактирование записи
//'.edit-demand :input[name!="demand-industry[]"]'

    $('.edit-demand').submit(function (event) {
        event.preventDefault();

        var demandDocument = $('input[name="demand-document"]').val();
        var demandParagraphName = $('input[name="demand-paragraph-name"]').val();
        var demandParagraphLink = $('input[name="demand-paragraph-link"]').val();

        var demandDemand = editor.data("wysihtml5").editor.getValue();
        var demandindustry = $('select[name="demand-industry[]"]').select2().val();
        var demand_job_child = $('select[name="demand-job-child"]').select2().val();
        var demandCriticality1 = $('select[name="demand-criticality1"] option:selected').val();
        var demandCriticality2 = $('select[name="demand-criticality2"] option:selected').val();
        var demandCriticality3 = $('select[name="demand-criticality3"] option:selected').val();
        var demandPrivilege = $('input[name="demand-privilege"]').is(":checked");
        var chain = {};
        var emptyEHS = false;

        $('.chain').each(function (index, value) {
            if ($(this).find('input.demand-ehs').val() == '') {
                $(this).find('input.demand-ehs').closest('.form-group').addClass('has-error');
                $(this).find('input.demand-ehs').closest('.form-group').find('.help-block').text('Поле обязательно для заполнения');
                emptyEHS = true;
            }
            ;
            var chainEHS = $(this).find('input.demand-ehs').val();
            var chainJOB = $(this).find('select.demand-job option:selected').text();
            chain[chainEHS] = chainJOB;
        })

        var demandEvent = $('textarea[name="demand-event"]').val();
        var demandSource = $('input[name="demand-source"]').val();

        if (demandDocument == '' ||
            demandParagraphName == '' ||
            demandParagraphLink == '' ||
            demandDemand == '' ||
            demandCriticality1 == 0 ||
            demandCriticality2 == 0 ||
            demandCriticality3 == 0 ||
            demandEvent == '' ||
            demandSource == '' ||
            emptyEHS == true ||
            demandindustry == '' || demand_job_child == 0 || url_protect(demandParagraphLink) == false
        ) {
            if (demandDocument == '') {
                $('input[name="demand-document"]').closest('.form-group').addClass('has-error');
                $('input[name="demand-document"]').closest('.form-group').find('.help-block').text('Поле обязательно для заполнения');
            }
            ;
            if (demand_job_child == 0) {
                $('select[name="demand-job-child"]').closest('.form-group').addClass('has-error');
                $('select[name="demand-job-child"]').closest('.form-group').find('.help-block').text('Поле обязательно для заполнения');
            }
            ;
            if (demandParagraphName == '') {
                $('input[name="demand-paragraph-name"]').closest('.form-group').addClass('has-error');
                $('input[name="demand-paragraph-name"]').closest('.form-group').find('.help-block').text('Поле обязательно для заполнения');
            }
            ;
            if (demandParagraphLink == '' || url_protect(demandParagraphLink) == false) {
                $('input[name="demand-paragraph-link"]').closest('.form-group').addClass('has-error');
                $('input[name="demand-paragraph-link"]').closest('.form-group').find('.help-block').text('Поле обязательно для заполнения');
            }
            ;
            if (demandDemand == '') {
                $('textarea[name="demand-demand"]').closest('.form-group').addClass('has-error');
                $('textarea[name="demand-demand"]').closest('.form-group').find('.help-block').text('Поле обязательно для заполнения');
            }
            ;
            if (demandCriticality1 == 0) {
                $('select[name="demand-criticality1"]').closest('.form-group').addClass('has-error');
            }
            ;
            if (demandCriticality2 == 0) {
                $('select[name="demand-criticality2"]').closest('.form-group').addClass('has-error');
            }
            ;
            if (demandCriticality3 == 0) {
                $('select[name="demand-criticality3"]').closest('.form-group').addClass('has-error');
            }
            ;
            if (demandEvent == '') {
                $('textarea[name="demand-event"]').closest('.form-group').addClass('has-error');
                $('textarea[name="demand-event"]').closest('.form-group').find('.help-block').text('Поле обязательно для заполнения');
            }
            ;
            if (demandSource == '') {
                $('input[name="demand-source"]').closest('.form-group').addClass('has-error');
                $('input[name="demand-source"]').closest('.form-group').find('.help-block').text('Поле обязательно для заполнения');
            }
            ;
            if (demandSource == '') {
                $('select[name="demand-industry"]').closest('.form-group').addClass('has-error');
            }
            ;

            $(".modal").animate({scrollTop: 0}, 500);
            $.notify({'type':'error', 'message':'Форма заполнена не верно. Проверьте правильность заполнения полей формы'});
            return false;

        } else {
            $('input[name="demand-paragraph-link"]').val(url_protect(demandParagraphLink));

            $('input[type="submit"]').val('Идет выполнение...');

            if ($('input[name="demand-id"]').val() != '') {
                var action = 'editDemand';
            } else {
                var action = 'addDemand';
            }

            var attr = decodeURI($(this).serialize());
            attr = attr + '&action=' + action + '&demand-industry=' + demandindustry.join(', ');

            $.ajax({
                type: 'POST',
                data: attr,
                url: "/assets/functions.php",
                cache: false,
                success: function (msg) {
                    console.log(msg);
                },
                complete: function () {
                    $.notify({'type':'success', 'message':'Данные успешно сохранены'});
                    table.api().ajax.reload();
                    $('input[type="submit"]').val('Сохранить');
                    $('#demandModal').modal('hide');
                }
            })

            // setTimeout(function() { location.reload(); }, 1000);


        }
    })

// Добавление записи
    $('*[data-action="add-data"]').click(function () {

        $('#demand-status').hide();
        $('input[name="demand-document"]').val('');
        $('input[name="demand-paragraph-name"]').val('');
        $('input[name="demand-paragraph-link"]').val('');
        editor.data("wysihtml5").editor.setValue('');
        $('select[name="demand-industry"]').val('0');
        $('select[name="demand-criticality1"]').val('0');
        $('select[name="demand-criticality2"]').val('0');
        $('select[name="demand-criticality3"]').val('0');
        $('input[name="demand-privilege"]').prop("checked", false);
        $('input[name="demand-connected"]').val(parseInt(getMaxFieldValue('demands', 'connected')) + 1);
        $('textarea[name="demand-event"]').val('');
       // $('input[name="demand-source"]').val('ИСС "Техэксперт"');
        $('input[name="demand-id"]').val('');
        $('#demandModal .modal-header h4').text('Добавить требования');
        job_chidren = $('select[name="demand-job_child"]');
        add_job_children(job_chidren);

        $('.chain:not(:first)').remove();
        $('.demand-ehs').val('NO EHS');
        $('.demand-job').prop("disabled", true).find('option').remove();

    })


// Удаление записи
    $('*[data-action="remove-data"]').click(function () {
        if (checkRowAmount_new() < 1) {
            $.notify({'type':'error', 'message':'Необходимо выбрать хотя бы одну запись!'});
            return false;
        }
        ;

        var removeId = [];

        $('#datatable tr.selected').each(function (index) {
            removeId.push($(this).closest("tr").attr('id'));
        })

        var attr = $(this).parent().id;
        attr = 'ids=' + removeId + '&action=removeDemand';

        $.ajax({
            type: 'POST',
            data: attr,
            url: "/assets/functions.php",
            cache: false,
            success: function (msg) {
                table.api().ajax.reload();
            }
        });

    })

// Подтверждение записи
    $('*[data-action="approve-data"]').click(function () {

        if (checkRowAmount_new() < 1) {
            $.notify({'type':'error', 'message':'Необходимо выбрать хотя бы одну запись!'});
            return false;
        }
        ;

        var approveId = [];

        $('#datatable tbody tr.selected').each(function (index) {

            approveId.push($(this).closest("tr").attr('id'));
        })

        var attr = $(this).parent().id;
        attr = 'ids=' + approveId + '&action=approveDemand';

        $.ajax({
            type: 'POST',
            data: attr,
            url: "/assets/functions.php",
            cache: false,
            success: function (msg) {
                console.log(msg);
            }
        }).done(function () {
            table.api().ajax.reload();
        });

    })

// Редактирование записи
    $('*[data-action="edit-data"]').click(function () {

        if (checkRowAmount_new() != 1) {
            $.notify({'type':'error', 'message':'Можно выбрать только одну запись!'});
            return false;
        }
        ;

        $('#demand-status').show();

        fillForm($('#datatable tbody tr.selected').closest("tr").attr('id'));

    })

    autoComplete(getDocuments(), 'demand-document');
    autoCompleteInit();

})
;

function fillForm(id) {

    // Общие данные
    var jsonCommon = getNodeByField('demands', 'id', id);

    console.log(jsonCommon);
    // Данные по цепочкам EHS и видам работ
    var jsonChains = getNodeByField('demands', 'connected', jsonCommon['data'][0]['connected']);

    // Подгружаем цепочки из БД
    $('#chains').empty();
    for (var i = 0; i < jsonChains['data'].length; i++) {

        var ehs = getFieldByField('ehs', 'id', jsonChains['data'][i]['ehs'], 'ehs_numbers');

        var job = getFieldByField('types_jobs', 'id', jsonChains['data'][i]['job'], 'type');

        $("#chains").append('<div class="chain">' +
            '<div class="col-md-4">' +
            '<div class="form-group">' +
            '<label>EHS:</label>' +
            '<input type="text" class="form-control demand-ehs" value="' + ehs + '" name="demand-ehs-' + i + '" >' +
            '<div class="help-block"></div>' +
            '</div>' +
            '</div>' +

            ' <div class="col-md-7">' +
            '<div class="form-group">' +
            '<label>Вид работы:</label>' +
            '<select class="form-control demand-job" name="demand-job-' + i + '">' +
            '<option value="' + job + '" selected="selected">' + job + '</option>' +
            '</select>' +
            '<div class="help-block"></div>' +
            '</div>' +
            '</div>' +

            '<div class="col-md-1">' +
            '<div class="form-group m-t-30">' +
            '<a href="javascript:void(0);" class="remove-chunk" onclick="removeChain();"><i class="fa ion-close text-danger"></i></a>' +
            '<div class="help-block"></div>' +
            '</div>' +
            '</div>' +
            '<div class="clearfix"></div>' +
            '</div>'
        );
    }
    ;

    $('#demandModal .modal-header h4').text('Редактирование требования');
    $('input[name="demand-id"]').val(jsonCommon['data'][0]['id']);

    $('input[name="demand-document"]').val(getFieldByField('register', 'nd', jsonCommon['data'][0]['document'], 'symbol'));
    $('input[name="demand-paragraph-name"]').val(jsonCommon['data'][0]['paragraph-name']);
    $('input[name="demand-paragraph-link"]').val(jsonCommon['data'][0]['paragraph-link']);
    $('select[name="demand-status"]').val(jsonCommon['data'][0]['status']);

    editor.data("wysihtml5").editor.setValue(jsonCommon['data'][0]['demand']);

    $("select[name='demand-criticality1']").val(jsonCommon['data'][0]['criticality1']);
    $("select[name='demand-criticality2']").val(jsonCommon['data'][0]['criticality2']);
    $("select[name='demand-criticality3']").val(jsonCommon['data'][0]['criticality3']);
    $('select[name="demand-industry[]"]').select2().val(jsonCommon['data'][0]['industry'].split(',')).change();
    job_chidren = $('select[name="demand-job_child"]');
    add_job_children(job_chidren);
    job_chidren.select2().val(jsonCommon['data'][0]['job_child']).change();

    if (jsonCommon['data'][0]['privilege'] == '1') {
        $('input[name="demand-privilege"]').attr('checked', true);
    } else if (jsonCommon['data'][0]['privilege'] == '0') {
        $('input[name="demand-privilege"]').attr('checked', false);
    }

    $('textarea[name="demand-event"]').val(jsonCommon['data'][0]['event']);
    $('input[name="demand-source"]').val(jsonCommon['data'][0]['source']);
    $('input[name="demand-connected"]').val(jsonCommon['data'][0]['connected']);


    autoComplete(getDocuments(), 'demand-document');
    autoCompleteInit();

    $('#demandModal').modal('show');

}

function add_job_children(job_chidren) {
    job_chidren.empty();
    job_chidren.append('<option value="0">Не выбранно</option>')
    $.map(getJOB(), function (item, i, arr) {
        job_chidren.append('<option value="' + i + '">' + item + '</option>')
    });
}


// Добавление цепочки
function addChain() {
    $('.chain:last').clone().insertAfter($('.chain:last'));
    var chunkId = $('.chain:last').find('.demand-ehs').attr('name').split('-')
    $('.chain:last').find('.demand-ehs').attr('name', 'demand-ehs-' + (+chunkId[2] + 1)).val('');
    $('.chain:last').find('.demand-job').empty().prop("disabled", true).attr('name', 'demand-job-' + (+chunkId[2] + 1));

    // Удаление ошибочного класса при заполнении полей
    $('form input,form select,form textarea').change(function (event) {
        $(this).closest('.form-group').removeClass('has-error');
        $(this).closest('.form-group').find('.help-block').text('');
    });

    orderChains();
    autoCompleteInit();
}

// Удаление цепочки
function removeChain() {
    if ($('.chain').length == 1) {
        $.notify({'type':'error', 'message':'Необходимо заполнить хотя бы одну цепочку. Требование должно принадлежать хотя бы одной цепочке EHS + вид работы'});
        return false;
    }
    ;
    $(event.currentTarget).closest('.chain').remove();
    orderChains();
    autoCompleteInit();
}

// Порядок в цепочках
function orderChains() {
    $('.chain').each(function (index, value) {
        $(this).find('.demand-ehs').attr('name', 'demand-ehs-' + index);
        $(this).find('.demand-job').attr('name', 'demand-job-' + index);
    });
}

// Автокомплит инициализация
function autoCompleteInit() {
    $('.demand-ehs').each(function (index, value) {
        autoComplete(getEHS(), $(this).attr('name'), true);
    })
}

// Автокомплит
function autoComplete(arr, name, chunk = false) {
    var itemArr = arr;

    function split(val) {
        return val.split(/,\s*/);
    }

    function extractLast(term) {
        return split(term).pop();
    }

    $('input[name="' + name + '"]')
    // don't navigate away from the field on tab when selecting an item
        .on("keydown", function (event) {
            if (event.keyCode === $.ui.keyCode.TAB &&
                $(this).autocomplete("instance").menu.active) {
                event.preventDefault();
            }
        })
    .autocomplete({
            minLength: 0,
            open: function () {
                setTimeout(function () {
                    $('.ui-autocomplete').css('z-index', 99999999999999);
                }, 0);
            },
            source: function (request, response) {
                console.log(response);
                // delegate back to autocomplete, but extract the last term
                response($.ui.autocomplete.filter(
                    itemArr, extractLast(request.term)));
            },
            focus: function () {
                // prevent value inserted on focus
                return false;
            },
            select: function (event, ui) {
                var terms = split(this.value);
                // remove the current input
                terms.pop();
                // add the selected item
                terms.push(ui.item.value);
                // add placeholder to get the comma-and-space at the end
                terms.push("");
                this.value = terms.join("");

                if (chunk == true) {
                    var ehs = $('input[name="' + name + '"]').val();
                    var job = $('input[name="' + name + '"]').parents('.chain').find($('.demand-job'));
                    job.empty().prop("disabled", false);
                    $.map(getJOBbyEHS(ehs), function (item, i, arr) {

                        job.append('<option value="' + item + '">' + item + '</option>')
                    });
                }
                ;

                return false;
            }
        });
}
function url_protect(str) {
    var text = "";
    var re = [/^kodeks:\/\/[a-z]{2,9}\//i, /^https?:\/\/[a-z0-9`_\-\.]+\.[a-z]{2,9}\/[a-z]+\//i,/\w/i];
    re.forEach(function (item, i, arr) {
        if (item.test(str)) {
            text = str.replace(item, "");
        }
    });
    return text;
}
function checkRowAmount_new() {
    var table = $("#datatable tr.selected")
    if (table.length > 0) {
        $('.panel-heading .ion-nuclear').parent().addClass('btn-purple');
        $('.panel-heading .ion-trash-a').parent().addClass('btn-danger');
        $('.panel-heading .ion-close').parent().addClass('btn-danger');
        $('.panel-heading .ion-unlocked').parent().addClass('btn-danger');
        $('.panel-heading .ion-edit').parent().removeClass('btn-info');
        $('.panel-heading .ion-eye').parent().removeClass('btn-primary');
        $('.panel-heading .ion-clipboard').parent().addClass('btn-purple');
        $('.panel-heading .ion-document').parent().removeClass('btn-warning');
        $('.panel-heading .ion-alert-circled').parent().removeClass('btn-danger');
        $('.panel-heading .ion-checkmark').parent().addClass('btn-success');
    }
    if (table.length == 1) {
        $('.panel-heading .ion-nuclear').parent().addClass('btn-purple');
        $('.panel-heading .ion-close').parent().addClass('btn-danger');
        $('.panel-heading .ion-unlocked').parent().addClass('btn-danger');
        $('.panel-heading .ion-edit').parent().addClass('btn-info');
        $('.panel-heading .ion-eye').parent().addClass('btn-primary');
        $('.panel-heading .ion-document').parent().addClass('btn-warning');
        $('.panel-heading .ion-alert-circled').parent().addClass('btn-danger');
    }
    if (table.length <= 0) {

        $('.panel-heading .ion-edit').parent().removeClass('btn-info');
        $('.panel-heading .ion-nuclear').parent().removeClass('btn-purple');
        $('.panel-heading .ion-eye').parent().removeClass('btn-primary');
        $('.panel-heading .ion-trash-a').parent().removeClass('btn-danger');
        $('.panel-heading .ion-close').parent().removeClass('btn-danger');
        $('.panel-heading .ion-unlocked').parent().removeClass('btn-danger');
        $('.panel-heading .ion-clipboard').parent().removeClass('btn-purple');
        $('.panel-heading .ion-document').parent().removeClass('btn-warning');
        $('.panel-heading .ion-checkmark').parent().removeClass('btn-success');
        $('.panel-heading .ion-alert-circled').parent().removeClass('btn-danger');
    }
    ;
    return table.length;
}
function my_unique(arr) {
    var obj = {};

    for (var i = 0; i < arr.length; i++) {
        var str = arr[i].trim();
        obj[str] = true; // запомнить строку в виде свойства объекта
    }

    return Object.keys(obj); // или собрать ключи перебором для IE8-
}