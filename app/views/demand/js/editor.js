$(document).ready(function () {

    autoComplete(getEHS(), 'ehs-number');
    autoComplete(getJOB(), 'job-name');

});

function getEhsName() {
    $(event.target).parents('tr').find('.ehs-name').val(getFieldByField('ehs', 'ehs_numbers', event.target.value, 'ehs_name'))
}

// Добавить или отредактировать запись
$('#datatable input[type="button"]').click(function (event) {

    if (!$(this).hasClass("btn-primary")) {
        return false;
    }
    ;

    $.ajax({
        type: 'POST',
        url: "/assets/functions.php",
        cache: false,
        dataType: "json",
        data: {
            "action": "setRelations",
            "ehs_numbers": $(this).parents('tr').find('td').eq(0).find('input[type="text"]').val(),
            "ehs_name": $(this).parents('tr').find('td').eq(1).find('input[type="text"]').val(),
            "type": $(this).parents('tr').find('td').eq(2).find('input[type="text"]').val(),
            //"ehs_id": $(this).parents('tr').find('input[type="hidden"]').attr('data-ehs'),
            //"job_id": $(this).parents('tr').find('input[type="hidden"]').attr('data-job'),
        },
        success: function (data) {
            if (data['type'] == 'error') {
                $.notify({'type':'error', 'message': data['message']});
                return false;
            }
            ;
            if (data['type'] == 'success') {
                location.reload();
            }
            ;
            console.log(data);

        }
    });
})

// Удалить запись
function removeData() {

    $.ajax({
        type: 'POST',
        url: "/assets/functions.php",
        cache: false,
        data: {
            "action": "removeRelations",
            "ehs_numbers": $(event.target).parents('tr').find('td').eq(0).find('input[type="text"]').val(),
            "ehs_name": $(event.target).parents('tr').find('td').eq(1).find('input[type="text"]').val(),
            "type": $(event.target).parents('tr').find('td').eq(2).find('input[type="text"]').val(),
            "ehs_id": $(event.target).parents('tr').find('input[type="hidden"]').attr('data-ehs'),
            "job_id": $(event.target).parents('tr').find('input[type="hidden"]').attr('data-job'),
        },
        success: function (msg) {
            // console.log(msg);
            location.reload();
        }
    });

}

function buttonActivity() {
    var error = false;
    $(event.target).parents('tr').find('input[type="text"]').each(function (index, item) {
        if (item.value == '') {
            error = true;
            $(event.target).parents('tr').find('input[type="button"]').removeClass('btn-primary');
            return false;
        }
        ;
    })
    if (!error) {
        $(event.target).parents('tr').find('input[type="button"]').addClass('btn-primary');
    }
    ;
    // editData();
}

// Автокомплит
function autoComplete(arr, className) {
    var itemArr = arr;

    // console.log(itemArr);

    function split(val) {
        return val.split(/,\s*/);
    }

    function extractLast(term) {
        return split(term).pop();
    }

    $('#new-relation input.' + className)
    // don't navigate away from the field on tab when selecting an item
        .on("keydown", function (event) {
            if (event.keyCode === $.ui.keyCode.TAB &&
                $(this).autocomplete("instance").menu.active) {
                event.preventDefault();
            }
        })
        .autocomplete({
            minLength: 0,
            open: function () {
                setTimeout(function () {
                    $('.ui-autocomplete').css('z-index', 99999999999999);
                }, 0);
            },
            source: function (request, response) {
                // delegate back to autocomplete, but extract the last term
                response($.ui.autocomplete.filter(
                    itemArr, extractLast(request.term)));
            },
            focus: function () {
                // prevent value inserted on focus
                return false;
            },
            select: function (event, ui) {
                var terms = split(this.value);
                // remove the current input
                terms.pop();
                // add the selected item
                terms.push(ui.item.value);
                // add placeholder to get the comma-and-space at the end
                terms.push("");
                this.value = terms.join("");

                return false;
            }
        });
}

// Сохранение вида работы
function saveJob(id) {

    var type = $(event.target).parents('.job').find('input[type="text"]').val();

    $.ajax({
        type: 'POST',
        dataType: "json",
        data: {
            "action": "saveJob",
            "id": id,
            "type": type,
        },
        url: "/assets/functions.php",
        cache: false,
        async: false,
        success: function (data) {
            if (data['type'] == 'error') {
                $.notify({'type':'error', 'message':data['message']});
                return false;
            }
            ;
            if (data['type'] == 'success') {
               
                $.notify({'type':'success', 'message':data['message']});
                autoComplete(getJOB(), 'job-name');
                $("#job-list").empty();
                $("#job-list").load("/app/views/demand/ajax-job.php");
            }
            ;
            console.log(data);
        },
        error: function () {
            console.log('Данные не получены');
        }
    })
}

// Удаление вида работы
function removeJob(id) {

    $.ajax({
        type: 'POST',
        dataType: "json",
        data: {
            "action": "removeJob",
            "id": id,
        },
        url: "/assets/functions.php",
        cache: false,
        async: false,
        success: function (data) {
            if (data['type'] == 'success') {
                $.notify({'type':'success', 'message':data['message']});
                $("#job-list").empty();
                $("#job-list").load("/app/views/demand/ajax-job.php");
            }
            ;
        },
        error: function () {
            console.log('Данные не получены');
        }
    })
}

// Добавление вида работы
function addJob() {

    if (validateField($(event.target).parents('.job').find('input[type="text"]'))) {
        saveJob();
    }
    ;

}

