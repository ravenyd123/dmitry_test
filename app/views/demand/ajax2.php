<?php
include_once $_SERVER['DOCUMENT_ROOT'] . '/assets/functions.php';
include_once $_SERVER['DOCUMENT_ROOT'] . '/app/views/analytics/SSP.php';
/**
 * Created by PhpStorm.
 * User: ilya
 * Date: 08.08.2019
 * Time: 15:08
 */
if (isset($_POST['action']) and $_POST['action'] == "demand_all") {

    $company = $_POST['company'];
    $table2 = 'demands';
//    if (!empty($row['setting'])) {
//        $rr = json_decode($row['setting'], true);
//        if (!empty($rr['quick'])) {
//            $plat = $rr['quick'];
//            $type_l = 'quick';
//        } else {
//            $type_l = null;
//            $plat = null;
//        }
//    } else {
//        $type_l = null;
//        $plat = null;
//    }
// Table's primary key
    $primaryKey = 'id';
    $source = Sourse_conn;

    $columns = array(
        ['db' => 'p.id', 'dt' => 'id', 'field' => 'id'],
        ['db' => 'p.job', 'dt' => 'job', 'field' => 'job', 'formatter' => function ($d, $row) {
            return $d == '0' ? "нет" : GetAllvid[$d];
        }],
        ['db' => 'p.job_child', 'dt' => 'job_child', 'field' => 'job_child', 'formatter' => function ($d, $row) {
            return $d == '0' ? "нет" : GetAllvid[$d];
        }],
        ['db' => 'p.demand', 'dt' => 'demand', 'field' => 'demand'],
        ['db' => 'r.symbol', 'dt' => 'document', 'field' => 'symbol'],
        ['db' => 'p.status', 'dt' => 'status', 'field' => 'status'],
        ['db' => 'p.`paragraph-name`', 'dt' => 'pname', 'field' => 'paragraph-name'],
        ['db' => 'p.`paragraph-link`', 'dt' => 'link', 'field' => 'paragraph-link','formatter' => function ($d, $row) {
            return Sourse_conn.$d;
        }],
        ['db' => 'p.date', 'dt' => 'activity', 'field' => 'date', 'formatter' => function ($d, $row) {
            return date("d-m-Y", $d);
        }],
        ['db' => 'p.industry', 'dt' => 'industry', 'field' => 'industry'],

        ['db' => 'pl.settings', 'dt' => 'sett_pl','field' => 'settings','formatter' => function ($d, $row) {
            if(!empty($d)){
                $rr = json_decode($d, true);
//                $plat !== null ? implode(',</br>', $rr['quick']) : $plat,///переделать
//                var_dump(implode(',</br>', $rr['quick']));
               return !empty($rr['quick'])?implode(',</br>', $rr['quick']):'';
            }
        return '';
        }],
        ['db' => 'pl.settings', 'dt' => 'sett_type','field' => 'settings','formatter' => function ($d, $row) {
            if(!empty($d)){
                $rr = json_decode($d, true);

               return !empty($rr['quick'])?'quick':'';
            }
        return '';
        }],

    );
    $joinQuery = 'FROM demands p LEFT JOIN demand_pl pl ON ( p.id= pl.demand ) JOIN register r ON (r.nd= p.document)';
    $extraWhere = "MATCH(r.company) AGAINST('{$company}')";


    echo json_encode(SSP::simple($_POST, $table2, $primaryKey, $columns, $joinQuery, $extraWhere,null,null,true), JSON_UNESCAPED_UNICODE);
}

if (isset($_POST['action']) and $_POST['action'] == "GetListForSelect") {
    $namegroup = $_POST['NameGroup'];
    $query = "SELECT DISTINCT(`".$namegroup."`) AS `id`,
                     DISTINCT(tj.type) AS `name` 
                     FROM `demand_process` dp  
                     ORDER BY tj.type DESC
                     JOIN `types_jobs` tj ON tj.id=dp.`".$namegroup."`";
    
    if ($namegroup == "group_demand"){

    }
    
    $result = $db->query($query)->fetch_assoc();
    echo json_encode($result, JSON_UNESCAPED_UNICODE);

}