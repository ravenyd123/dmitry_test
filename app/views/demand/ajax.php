<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/assets/functions.php';
$db = Db::Connection();
//$demands = $db->query("SELECT p.id,industry,job ,job_child,p.demand,r.symbol AS document,p.date,p.status,p.industry,p.`paragraph-name` AS pname,p.`paragraph-link`AS plink
//                                   FROM demands p
//                                   JOIN register r ON r.nd= p.document
//                                   WHERE r.company LIKE '%" . $_COOKIE['company'] . "%'");
//print_r(json_encode($_GET['company'], JSON_UNESCAPED_UNICODE));

$demands = $db->query("SELECT p.id,
pl.settings as setting,
p.industry,
p.job,
p.job_child,
p.demand,
r.symbol AS document,
p.date,
p.status,
p.industry,
p.`paragraph-name` AS pname,
p.`paragraph-link`AS plink FROM demands p 
LEFT JOIN demand_pl pl ON p.id= pl.demand
                                   JOIN register r ON r.nd= p.document
                                   WHERE r.company LIKE '%" . $_REQUEST['company'] . "%' order by p.date asc");

$data["data"] = [];

$i = 1;
$source = Sourse_conn;
//$platform_name = GetAllPlatforms();
while ($row = $demands->fetch_assoc()) {
    if (!empty($row['setting'])) {
        $rr = json_decode($row['setting'], true);
        if (!empty($rr['quick'])) {
            $plat = $rr['quick'];
            $type_l = 'quick';
        } else {
            $type_l = null;
            $plat = null;
        }
    } else {
        $type_l = null;
        $plat = null;
    }


    $data['data'][] = [
        "id" => $row['id'],
        "job" => GetAllvid[$row['job']],
        "job_child" => $row['job_child'] == '0' ? "нет" : GetAllvid[$row['job_child']],
        "demand" => $row['demand'],
        "pname" => $row['pname'],
        "link" => $source . $row['plink'],
        "document" => $row['document'],
        "status" => $row['status'],
        "activity" => date("d-m-Y", $row["date"]),
        "industry" => $row['industry'],

        "sett_pl" => $plat !== null ? implode(',</br>', $plat) : $plat,///переделать
        "sett_pl_int" => $plat,///переделать
        "sett_type" => $type_l///переделать
    ];
}

//function ee($arr)
//{
//    if (gettype($arr) == 'array') {
//        $gg = [];
//        foreach ($arr as $t) {
//            $gg[] = type_list[$t];
//        }
//    } else {
//        $gg = null;
//    }
//    return $gg !== null ? implode(',</br>', $gg) : $gg;
//};

print_r(json_encode($data, JSON_UNESCAPED_UNICODE));

