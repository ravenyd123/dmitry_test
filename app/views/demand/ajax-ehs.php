<div class="modal-body">
  <div class="row">
    <div class="job">
      <div class="col-md-3">
        <div class="form-group">
          <input class="form-control" type="text" name="ehs-new-number" value="" >
          <div class="help-block"></div>
        </div>
      </div> 
      <div class="col-md-7">
        <div class="form-group">
          <input class="form-control" type="text" name="ehs-new-name" value="" >
          <div class="help-block"></div>
        </div>
      </div>  
      <div class="col-md-2 form-inline">
        <div class="form-group m-5">
          <a href="javascript:void(0);" onclick="addEhs()">
            <i class="fa ion-plus text-success"></i>
          </a>
        </div>
      </div> 
    </div>
      <?php
      $db=Db::Connection();
      $result=$db->query("SELECT * FROM `ehs` GROUP BY `ehs_numbers` ORDER BY `ehs_numbers` ");
      while ($row = $result->fetch_assoc()) {
      ?>
    <div class="job">
      <div class="col-md-3">
        <div class="form-group">
          <input class="form-control" disabled="disabled" type="text" name="ehs-number" value="<?=$row['ehs_numbers'];?>" >
        </div>
      </div>
      <div class="col-md-7">
        <div class="form-group">
          <input class="form-control" disabled="disabled" type="text" name="ehs-name" value="<?=$row['ehs_name'];?>" >
        </div>
      </div>  
      <div class="col-md-2 form-inline">
        <div class="form-group m-5">
          <? if ( $access['standart-editor']['delete'] == 1 ) { ?>
          <a href="javascript:void(0);" onclick="removeEhs(<?=$row['id'];?>);">
            <i class="fa ion-close text-danger"></i>
          </a>
          <? } ?>
          <div class="help-block"></div>
        </div>
      </div>  
    </div>            
    <? } ?>
  </div>
</div>