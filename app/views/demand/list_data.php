<? include_once $_SERVER['DOCUMENT_ROOT'] . '/assets/functions.php'; ?>
<?php


class list_self
{
    private $db;
    private $data_set = [];

    function __construct()
    {
        $this->db = DB::Connectionpdo();
    }

    public function set($data)
    {
        if (!empty($data)) {
            $data = json_decode($data);
        } else {
            $data = null;
        }
        return $this->data_set = $data;
    }

    public function keys()
    {
        return array_keys((array)$this->data_set);
    }

    public function get()
    {
        return $this->data_set;
    }

    public function update()
    {
        $insert_id = [];
        $pdo = $this->db;
        $query = "UPDATE demand_pl SET settings=:json WHERE demand=:demand";
        $stmt = $pdo->prepare($query);
        foreach ($this->data_set as $key => $data) {
            try {
                $stmt->execute(['demand' => $key, 'json' => json_encode($data)]);
                $insert_id[$key] = $stmt->rowCount();
            } catch (Exception $e) {
                echo 'Выброшено исключение: ', $e->getMessage(), "\n";
            }
        }
        return $insert_id;
    }

    public function insert()
    {
        $insert_id = [];
        $pdo = $this->db;
        $query = "INSERT INTO demand_pl(demand, settings) VALUES(:demand,:json)";
        $stmt = $pdo->prepare($query);
        foreach ($this->data_set as $key => $data) {
            try {
                $stmt->execute(['demand' => $key, 'json' => json_encode($data)]);
                $insert_id[$key] = $pdo->lastInsertId();
            } catch (Exception $e) {
                echo 'Выброшено исключение: ', $e->getMessage(), "\n";
            }
        }
        return $insert_id;
    }

    public function delete($keys)
    {
        $pdo = $this->db;
        $t=str_repeat('?,', count($keys) - 1) . '?';
        $q = "DELETE LOW_PRIORITY QUICK from demand_pl WHERE demand in({$t})";;
        $stmt = $pdo->prepare($q);//$this->keys()
        return $stmt->execute($keys);
    }


    public function pre_in_question()
    {
        if (!empty($this->keys())) {
//            $in  = str_repeat('?,', count($this->keys()) - 1) . '?';
//
//
//            $quest = "";
//            for ($i = 0; $i < count($this->keys()); $i++) {
//                $quest .= "?,";
//            }substr($quest, 0, -1)
            return str_repeat('?,', count($this->keys()) - 1) . '?';
        }
    }

    public function check()
    {
        if (!empty($this->data_set)) {
            $db = $this->db;
            $q = "select demand from demand_pl WHERE demand in({$this->pre_in_question()})";
            $result = $db->prepare($q);
            $result->execute($this->keys());
            return $result->fetchAll(PDO::FETCH_COLUMN);
        }
    }

//    public function diff_check()
//    {
//        return array_diff($this->keys(), $this->check());
//    }

}

if (isset($_POST['action'])) {
    $list = new list_self();
    $list->set($_POST['data']);


    if ($_POST['action'] == "add") {
        if (count($list->check()) > 0) {
            $list->delete($list->check());

        }
        echo json_encode($list->insert());
    }

    if ($_POST['action'] == "edit") {

        echo json_encode($list->update());

    }
    if ($_POST['action'] == "delete") {

        echo json_encode($list->delete($list->keys()));

    }

}


