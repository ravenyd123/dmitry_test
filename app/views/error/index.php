<? require_once $_SERVER['DOCUMENT_ROOT'].'/app/views/header.php'; ?>

	<body>

		<div class="wrapper-page fadeInDown">

			<div class="ex-page-content text-center">
				<h1>404</h1>
				<h2 class="font-light">Страница не найдена</h2>
				<br>
				<p>
					Страница, на которую Вы хотели перейти, не найдена. Возможно, введён некорректный адрес или страница была удалена
				</p>
				<br>
				<a class="btn btn-primary" href="/tasks"><i class="fa fa-angle-left"></i> Вернуться на главную</a>
			</div>

		</div>

	</body>

</html>