$(document).ready(function () {



// Datatables
    var table = $('#datatable').dataTable({
    	"pageLength": 15,
        //lengthChange: false,
       // stateSave: true,
       // buttons: ['colvis'],
        "aaSorting": [[1, 'desc']],
        "sScrollX": "100%",
        "scrollCollapse": true,
       // "paging": true,
        "processing": true,
        ajax: {
            type: 'POST',
            dataType: "json",
            url: "app/views/statistics/models/statistics.php",
            data: {"action": "getUsersLogs"},
            cache: false,
            dataSrc: function (data) {
               return data;
            },
        },

        columnDefs: [
            {
                "title": "",
                "targets": [0],
                "className": "text-center",
                "data": "iduser",
                "visible": false
            },
            {
                "title": "Дата",
                "targets": [1],
                "className": "text-center",
                "data": "date",
                "type": "de_datetime",
                "width": "1%"
            },
            {
                "title": "Пользователь",
                "targets": [2],
                "className": "text-center",
                "data": "fio",
               "width": "2%"
            },
            {
                "title": "IP",
                "targets": [3],
                "className": "text-center",
                "data": "ip",
                "width": "1%"
            },
            // {
            //     "title": "Хост",
            //     "targets": [4],
            //     "className": "text-center",
            //     "data": "d",
            //     "width": "5%",
            //     "visible": false

            // },
            {
                "title": "Место",
                "targets": [4],
                "className": "text-center",
                "data": "url",
                "width": "5%",
                "visible": false
            },
            {
                "title": "Страница",
                "targets": [5],
                "className": "text-center",
                "data": "doit",
                "width": "5%"
                //"visible": false
            },
            {
                "title": "Статус",
                "targets": [6],
                "className": "text-center",
                "data": "kod",
                "width": "5%"
                //"visible": false
            },
            //             {
            //     "title": "Тайминг",
            //     "targets": [7],
            //     "className": "text-center",
            //     "data": "ping",
            //     "width": "5%"
            //     //"visible": false
            // },
             {
                "title": "Браузер",
                "targets": [7],
                "className": "text-center",
                "data": "useragent",
                "width": "10%",
                //"visible": false
            },
            {
                "title": "",
                "targets": [8],
                "className": "text-center",
                "data": "browser",
                "width": "10%",
                "visible": false
            }
        ],
        "initComplete": function () {
        	/* Фильтр по браузерам*/
            this.api().column(8).every(function () {
                var column = this;
                var select = $('<select class="sel_filter"><option value="">Все</option></select>')
                    .appendTo($('#datatable-filter div.brows'))
                    .on('change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );
                        column
                            .search(val ? '^' + val + '$' : '', true, false)
                            .draw();
                    })

                column.data().unique().sort().each(function (d, j) {
                    select.append('<option value="' + d + '">' + d + '</option>')
                });
            });

            this.api().column(6).every(function () {
                var column = this;
                var select = $('<select class="sel_filter"><option value="">Все</option></select>')
                    .appendTo($('#datatable-filter div.st'))
                    .on('change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );
                        column
                            .search(val ? '^' + val + '$' : '', true, false)
                            .draw();
                    })

                column.data().unique().sort().each(function (d, j) {
                    select.append('<option value="' + d + '">' + d + '</option>')
                });
            });
           
            /* Фильтр по пользователю*/
            this.api().column(2).every(function () {
                var column = this;
                var select = $('<select class="sel_filter"><option value="">Все</option></select>')
                    .appendTo($('#datatable-filter div.user'))
                    .on('change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );
                        column
                            .search(val ? '^' + val + '$' : '', true, false)
                            .draw();
                    })

                column.data().unique().sort().each(function (d, j) {
                    select.append('<option value="' + d + '">' + d + '</option>')
                });
            });

            /* Фильтр по IP */
            this.api().column(3).every(function () {
                var column = this;
                var select = $('<select class="sel_filter"><option value="">Все</option></select>')
                    .appendTo($('#datatable-filter div.ip'))
                    .on('change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );
                        column
                            .search(val ? '^' + val + '$' : '', true, false)
                            .draw();
                    })

                column.data().unique().sort().each(function (d, j) {
                    select.append('<option value="' + d + '">' + d + '</option>')
                });
            });

        

            $('select.sel_filter').select2();
        },
                "language": {
            "processing": "Производиться загрузка данных",
            "lengthMenu": "Выводить по _MENU_ записей",
            "zeroRecords": "Ни одной записи не найдено",
            "info": "Показано страниц _PAGE_ из _PAGES_",
            "infoEmpty": "No records available",
            "infoFiltered": "(отфильтровано из _MAX_ записей)",
            "search": "Поиск:",
            "paginate": {
                "first": "Первая",
                "last": "Последняя",
                "next": "Следующая",
                "previous": "Предыдущая"
            },
        }
    })
	.api()

    //Кнопка скачивания лог файла
    $('*[data-action="dload-logfile"]').click(function () {

    	$.notify({'type':'success', 'message':'Скачивание лог-файла началось. Это может занять некоторое время...'});
    	window.open('/app/views/statistics/dload_log.php', '_self');

    });

   
	$('#datatable').on('mouseover', 'td', function() {
        // show popup for cell
        console.log($(this).text());
    }).on('mouseout', 'td', function() {
        // hide popup for cell
        console.log('2222222222');
    });

    
});