<? include_once $_SERVER['DOCUMENT_ROOT'] . '/app/views/header.php'; ?>
<? include_once $_SERVER['DOCUMENT_ROOT'] . '/app/views/template.php'; ?>
<? include_once  'models/statistics.php'; ?>
   
    <!--при добавление библиотек появляется баг-->
        <link rel="stylesheet" type="text/css" href="/node_modules/lightbox2/dist/css/lightbox.min.css"/>
    <!--    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/rowreorder/1.2.4/css/rowReorder.bootstrap.min.css"/>-->
        <script type="text/javascript" src="/node_modules/lightbox2/dist/js/lightbox.min.js"></script>
    <!--    <script type="text/javascript" src="https://cdn.datatables.net/responsive/2.2.2/js/responsive.bootstrap.min.js"></script>-->
    <!--    <script type="text/javascript" src="https://cdn.datatables.net/rowreorder/1.2.4/js/dataTables.rowReorder.min.js"></script>-->

    <!--Main Content -->
    <section class="content">

        <!-- Page Content -->

        <div class="wraper container-fluid">
            <div class="row">
                <div class="col-md-12 col-sx-12">
                    <div class="panel panel-default w-100">
                        <div class="panel-heading">
                            <h3 class="panel-title pull-left m-t-10">Статистика</h3>
                            	<div class="top_nav">
                                        <button data-toggle="tooltip" title="Скачать лог-файл" class="btn btn-success" data-action="dload-logfile"><i class="ion-clipboard"></i>
                                        </button>
                                 </div>   
                            <div class="clearfix"></div>
                        </div>

                        <div class="panel-body">
                            <div class="row m-b-15">
                                <div class="" id="datatable-filter">

                                    <div class="col-md-3 user">
                                        <label>Пользователь</label>
                                    </div>
                                    
                                    <div class="col-md-3 ip">
                                        <label>IP</label>
                                    </div>
                                   
                                    <div class="col-md-3 st">
                                        <label>Статус</label>
                                    </div>
                                    <div class="col-md-3 brows">
                                        <label>Браузер</label>
                                    </div>
                                    
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <table id="datatable" class="table table-hover table-striped">
								     </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <!-- End Row -->

        </div>

    </section>
    <script type="text/javascript" src="/assets/admina/js/de_datetime.js"></script>
<script type="text/javascript" src="/app/views/statistics/js/statistics.js"></script>
<? include 'footer.php'; ?>