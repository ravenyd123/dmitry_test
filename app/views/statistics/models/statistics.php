<?php
include_once $_SERVER['DOCUMENT_ROOT'] . '/assets/functions.php';
include_once $_SERVER['DOCUMENT_ROOT'] . '/assets/DB.php';
$db = DB::Connection();

function getNameErrorFromCode($str){
    if($str){
        if($str =="200"){
            $doitname= "Ok";
        }
        elseif($str =="204"){
            $doitname= "Нет контента";
        }
        elseif($str =="302"){
            $doitname= "Переадресация";
        }
        elseif($str =="400"){
            $doitname= "Некорректный запрос";
        }
        elseif($str =="401"){
            $doitname= "Нет авторизации";
        }
        elseif($str =="403"){
            $doitname= "Доступ запрещен";
        }
        elseif($str =="404"){
            $doitname= "Страница не найдена";
        }
        elseif($str =="406"){
            $doitname= "Неприемлемый запрос";
        }
        elseif($str =="500"){
            $doitname= "Внутрення ошибка сервера";
        }
        elseif($str =="503"){
            $doitname= "Сервер перегружен";
        }
    } else {
        $doitname= "Нет статуса";
    }
    return $doitname;

}

function getTypeChek($type){
    $db = DB::Connection();
    $query="SELECT `name` FROM `name_type_task` WHERE `type`='".$type."' LIMIT 1";
    $result = $db->query($query)->fetch_assoc();
    return $result;

}

function parseUrl($doitString) {
    $parts = parse_url($doitString);
    parse_str($parts['query'], $query);
    return $query;
}

function doitName($doitString){
    include_once $_SERVER['DOCUMENT_ROOT'] . '/assets/DB.php';
    $num=strpos($doitString, "?");
    if($num){
        $str=substr($doitString, 0, $num);
    } else {
        $str = $doitString;
    }

    if($str =="/logout"){
        $doitname= "Выход из учетной записи";
    }

    if($str =="/login"){
        $doitname= "Авторизация";
    }

    if($str =="/tasks"){
        $doitname= "Рабочий стол";
    }

    if($str =="/help"){
        $doitname= "Страница помощи";
    }

    if($str =="/settings"){
        $doitname= "Настройки";
    }

    if($str =="/statistics"){
        $doitname= "Статистика"; 
    }

    if($str =="/statistics"){
        $doitname= "Статистика";
    }

    if($str =="/demand"){
        $doitname= "Редактор требований";
    }

    if($str =="/demand/editor"){
        $doitname= "Редактор стандартов";
    }

    if($str =="/register"){
        $doitname= "Реестр документов";
    }
    
    if($str =="/app/views/register/dload_register.php"){
        $doitname= "Скачен xlsx реестр документов";
    }

    if($str =="/nd"){
        $query = parseUrl($doitString);
        $type = getDocumentByNd($query['nd']);
        $doitname= "Открыт документ:".$type;
    }

    if($str =="/analytics"){
        $doitname= "Аналитика";
    }

    if($str =="/reports/all"){
        $doitname= "Список чек-листов";
    }

    if($str =="/app/views/reports/templates/all/l_evaluation.php"){
        $doitname= "Загружен xlsx файл чек-листа на сервер";
    }

    if($str =="/app/views/reports/templates/all/l_calendar.php"){
        $doitname= "Загружен xlsx файл календаря на сервер";
    }

    if($str =="/reports/calendar"){
        $doitname= "Создание календаря";
    }

    if($str =="/app/views/reports/templates/all/d_evaluation.php"){
        $query = parseUrl($doitString);
        $name = getTypeChek($query['type']);
        $doitname = "Скачен xlsx файл чек-листа (".$name['name'].")";
    }

    if($str =="/app/views/reports/templates/all/d_callendar.php"){
        $query = parseUrl($doitString);
        $name = getTypeChek($query['type']);
        $doitname= "Скачен xlsx файл календаря (".$name['name'].")";
    }

    if($str =="/reports/matrix"){
        $query = parseUrl($doitString);
        $name = getTypeChek($query['type']);
        $doitname= "Редактирование чек-листа (".$name['name'].")";
    }

    if($str =="/tasks/task_new"){
        $query = parseUrl($doitString);
        $doitname = "Редактирование задачи №".$query['id'];
    }

    if($str =="/nearmiss"){
        $doitname= "NearMiss";
    }

    if($str =="/app/views/nearmiss/dload_register.php"){
        $doitname= "Скачен реестр NearMiss в xlsx";
    }

    if($str =="/app/views/nearmiss/upload_register.php"){
        $doitname= "Загружен файл csv с NearMiss на сервер";
    }

    if($str =="/api/LoadData.php"){
        $doitname= "Авторизация в мобильном приложении";
    }

    if($str =="/app/views/nearmiss/mobile.php"){
        $doitname= "Синхронизация NearMiss в мобильном приложении";
    }

    if($str =="/app/views/nearmiss/qrcode/menu.php"){
        $doitname= "qrcode NearMiss";
    }

return $doitname;
}

function getSmallFio($surname,$name,$fathername){
    if ($surname <> '' AND $name <> '' AND $fathername<> '') {
        $small_fio = $surname . ' ' . mb_substr($name, 0, 1, "utf-8") . '.' . mb_substr($fathername, 0, 1, "utf-8") . '.';
    }
    if ($surname <> '' AND $name <> '' AND $fathername == '') {
        $small_fio = $surname . ' ' . mb_substr($name, 0, 1, "utf-8") . '.';
    } else if 
       ($surname <> '' AND $name == '' AND $fathername == '') {
        $small_fio = $surname;    
        }
    return $small_fio;
}

function getBrowserName ($user_agent){
    if (is_int($pos = stripos($user_agent, 'Firefox'))) {
        $browser = "Firefox";
        $pos += 7;
        $version = getBrowserVersion($user_agent, $pos);
    }
    elseif (is_int($pos = stripos($user_agent, 'Android'))) {
        $browser = "Android";
        $pos += 7;
        $version = getBrowserVersion($user_agent, $pos);
    }    
    elseif (is_int($pos = stripos($user_agent, 'YaBrowser'))) { 
        $browser = "Яндекс";
        $pos += 9;
        $version = getBrowserVersion($user_agent, $pos);
    }
    elseif (is_int($pos = stripos($user_agent, 'Opera'))) {
        $browser = "Opera";
        $pos += 5;
        $version = getBrowserVersion($user_agent, $pos);
    }
    elseif (is_int($pos = stripos($user_agent, 'Chrome'))) {
        $browser = "Chrome";
        $pos += 6;
        $version = getBrowserVersion($user_agent, $pos);
    }
    elseif (is_int($pos = stripos($user_agent, 'MSIE'))) {
        $browser = "Internet Explorer";
        $pos += 4;
        $version = getBrowserVersion($user_agent, $pos);
    }
    elseif (is_int($pos = stripos($user_agent, 'Safari'))) {
        $browser = "iPhone/Mac Safari";
        $pos += 6;
        $version = getBrowserVersion($user_agent, $pos);
    }
    elseif (is_int($pos = stripos($user_agent, 'WellCompliance'))) {
        $browser = "Мобильное приложение WellCompliance";
        $pos += 14;
        $version = getBrowserVersion($user_agent, $pos);    
    }
    /* Боты и пауки */
    elseif (is_int($pos = stripos($user_agent, 'YandexMetrika'))) {
        $browser = "Яндекс.Метрика";
        $pos += 14;
        $version = getBrowserVersion($user_agent, $pos);    
    }

    else $browser = "Неизвестный";
    return $browser." ".$version;
}

function getBrowserVersion($agent = '', $start = 0) {
    $version = '';
    $length = strlen($agent);
    if($start >= $length) $start = $length - 1;
    for($i = $start; $i < $length; $i++) {
        if($agent{$i} === '(') continue;
        if(is_numeric($agent{$i}) or $agent{$i} === '.' or $agent{$i} === '/') {
            if($agent{$i} === '/' and empty($version)) continue;
            $version .= $agent{$i};
        } else
            if(!empty($version)) break;
    }
    return($version);
} 

if (isset($_POST['action']) and $_POST['action'] == "getUsersLogs") {
        $host = $_SERVER['HTTP_HOST'];
        $file = "/opt/lampp/logs/".$host."-access_log";
        $max=50;
        $allusers = getUsers();
        if(is_file($file)) // Проверяем есть ли файл
        {
            if($file=@file_get_contents($file)) // Пытаемся читать файл
            {
                $time=$time*3600;//разница часов во времени с сервером (UTC)
                $file=preg_replace( "#\r\n|\r|\n#",PHP_EOL,$file);//унификация делителя для разных ОС
                $file=explode(PHP_EOL,$file);
                $file=array_reverse($file);
                $send = array();
                foreach($file as $i=>$val) // Строки
                {
                    //if($i==$max)break;
                    if($val!=='')
                    {
                        $dataArray = explode('|',$val);
                        foreach ($dataArray as $key=>$data){
                            $data = str_replace('"','', $data);
                            switch ($key) {
                                case 0:
                                    $dataSend['ip'] = $data;
                                   // $dataSend['host'] = gethostbyaddr($data);
                                    break;
                                case 1:
                                    $dataSend['date'] = date('d.m.Y H:i:s',strtotime("+3 hours", strtotime(str_replace(['[',']'],'',$data))));
                                    $olddate = $dataSend['date'];
                                    break;
                                case 2:
                                   $iduser = intval($data);
                                    if(!in_array($iduser, array_column($allusers, 'id'))) {
                                        $dataSend['fio'] = "Неизвестный";
                                    } else {
                                        $dataSend['iduser'] = $iduser;
                                    $dataSend['fio'] = getSmallFio ($allusers[$iduser]['surname'], $allusers[$iduser]['first_name'], $allusers[$iduser]['father_name']);
                                    }
                                    break;
                                case 3:
                                    $doit = explode(' ', $data);
                                    if (!$doitString = doitName($doit[1])) {
                                        continue 3;
                                    } else {
                                        $dataSend['doit'] = "<a href='".Sourse_Нome.$doit[1]."' title='".Sourse_Нome.$doit[1]."' target='_blank'>".$doitString."</a>";
                                    }
                                    break;
                                case 4:
                                    $dataSend['kod'] = getNameErrorFromCode($data);

                                    break;
                                case 5:
                                    $dataSend['size'] = $data;
                                    break;
                                case 6:
                                    $dataSend['url'] = $data;
                                    break;
                                case 7:
                                    $dataSend['browser'] = getBrowserName($data);
                                    $dataSend['useragent'] = "<span title='".$data."'>".getBrowserName($data)."</span>";
                                    break;
                            }
                        }
                        array_push($send, $dataSend); 
                    }
                }
            }
            else $result='Файл не читается';
        }
        else $result='Не найден файл логов';
        echo json_encode($send,JSON_NUMERIC_CHECK);
    }
?>