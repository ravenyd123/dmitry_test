<? include_once $_SERVER['DOCUMENT_ROOT'] . '/app/views/header.php'; ?>
<? include_once $_SERVER['DOCUMENT_ROOT'] . '/app/views/template.php'; ?>


<?// $tasks = mysqli_query($link, "SELECT  t.*,p.name AS plname FROM `tasks` t JOIN platform p on p.id=t.platform WHERE
//                                      (t.`responsible` LIKE '%" . $_COOKIE['id'] . "%' OR
//                                      t.`executors` LIKE '%" . $_COOKIE['id'] . "%' OR
//                                      t.`director` LIKE '%" . $_COOKIE['id'] . "%')
//                                      and p.id IN (" . $_COOKIE['platform'] . ") and t.archive <> 1");
//var_dump($_COOKIE['platform']);
?>
    <!--Main Content -->

    <section class="content">

        <!-- Page Content -->

        <div class="wraper container-fluid test">
            <div class="row">
                <div class="col-md-12 col-sx-12">
                    <div class="panel panel-default w-100">
                        <div class="panel-heading">
                            <h3 class="panel-title pull-left m-t-10">Мои задачи</h3>
                            <div class="top_nav">
                                <? if (in_array("C", $access['tasks'])) { ?>
                                    <a data-target="#task_create" data-toggle="modal">
                                        <button class="btn btn-success" data-action="add-data"><i class="ion-plus"></i>
                                        </button>
                                    </a>
                                    <br>
                                <? } ?>
                                <? if (in_array("U", $access['tasks'])) { ?>
                                    <button class="btn btn-default" data-action="edit-data"><i class="ion-edit"></i>
                                    </button><br>
                                <? } ?>
                                <? if (in_array("U", $access['tasks'])) { ?>
                                    <a data-target="#task_create_pk" data-toggle="modal">
                                        <button class="btn btn-default" data-action="pk-data" id="create_pk">
                                            <i class="glyphicon glyphicon-link"></i>
                                        </button>
                                        <br>
                                    </a>
                                <? } ?>
                                <? if (in_array("D", $access['tasks'])) { ?>
                                    <button class="btn btn-default" data-action="remove-data"><i
                                                class="ion-trash-a"></i>
                                    </button>
                                <? } ?>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-body">
                            <div class="row m-b-15">
                                <div class="" id="datatable-filter">
                                    <div class="col-md-2 eh">
                                        <label>Тип задачи</label>
                                    </div>
                                    <div class="col-md-3 pl">
                                        <label>Предприятие</label>
                                    </div>
                                    <div class="col-md-3 ot">
                                        <label>Ответственный</label>
                                    </div>
                                    <div class="col-md-2 st">
                                        <label>Статус</label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <table id="datatable" class="table table-hover table-striped">
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <!-- End Row -->

        </div>

    </section>
    <style>
        .selected {
            background-color: #d9edf7 !important;
        }
    </style>
    <script>

        var data_recurs =<?php echo json_encode(recursion(true))?>;
    </script>
    <script type="text/javascript" src="/assets/admina/js/de_datetime.js"></script>
    <script type="text/javascript" src="/app/views/tasks/js/ind_task.js"></script>


<? include 'footer.php'; ?>