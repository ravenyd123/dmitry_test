// type задач текущая задача ,self ,audit,пкидв
var roles = {
    director: {
        disabled: {
            "task_id": true,
            "task_name": true,
            "task_type": false,//тип задач
            "task_status": false,//статус
            "task_priority": true,// приоритет
            "task_platform": false,//площадка
            "agree": true,// самооценка
            "task_text": true,//текст задачи
            "task_deadline": true,
            "task_startnoty": true,
            "task_repeattask": true,
            "task_director": true,
            "task_responsible": true,
            "task_executors": true,
            "task_subexecutors": true,
            "btn-file": true
        },
        hidden: {
            "btn-file": true,
            "comments-list": true,//комментария
            "edit_subtask": true,
            // "but_end": true,
            "delete_subtask": true,
            "add_subtask": true,
            "add_texttask": true,
            "task_subexecutors": true,
            "task_executors": false,
        }
    },
    responsible: {
        disabled: {
            //"btn-file":true,
            "task_name": false,
            "task_priority": true,
            "task_status": false,
            "task_subexecutors": false,
            "task_job": false,
            "task_startnoty": true,
            "task_repeattask": true
        },
        hidden: {
            //"btn-file":true,
            "add_texttask": true,
            "comments-list": true,
            "agree": true,
            "add_subtask": true,
            "task_job": true,
            "task_priority": true,
            "task_executors": false,
            "task_subexecutors": true,
            "add_agreement": false,
        }
    },
    executors: {
        disabled: {
            // "btn-file":true,
            "task_name": false,
            "task_status": false,
            "task_subexecutors": false,
            "task_executors": false,
        },
        hidden: {
            // "btn-file":true,
            "comments-list": true,
            "task_agree": false,
            "add_subtask": true,
        }
    },
    visitors: {
        disabled: {
            "task_name": false,
            "task_priority": true,
            "task_subexecutors": false,
            "task_job": false,
        },
        hidden: {
            "but_end": false,
            "agree": false,
            "comments-list": true,
            "task_job": true,
            "task_priority": true,
            "add_agreement": true,
            "task_executors": false,
            "add_subtask": false,

        }
    }
};
var name_status = ["", "Новая", "Выполнено", "Не выполнено", "В работе", "Не принято", "Требует согласования"];
$(document).ready(function () {
    // $('input#input-ru').fileinput('refresh', {showBrowse: false});
    // var  fastd=[];
    var quick = $("input[name='quick']").is(':checked');
    var type = {};
    var old_action = 0;
    //////////блок change////////////////////
    switch (status) {
        case '1'://Новая
            $('.but_start').removeClass('hidden');
            $('.btn-file').addClass('hidden');
            break;
        case '2'://Выполнено
            $('.agree').removeClass('hidden');
            $('#agree_act').addClass('hidden');
            $('#agree_diact').addClass('hidden');
            break;
        case '3'://не выполнено
            $('.agree').addClass('hidden');
            $('.but_end').addClass('hidden');
            break;
        case '4'://в работе
            $('.but_end').removeClass('hidden');
            $('.add_subtask').removeClass('hidden');
            break;
        case '5'://не принято
            $('.but_end').addClass('hidden');
            $('.add_subtask').addClass('hidden');
            $('.btn-file').addClass('hidden');
            break;
        case '6':// Требует согласования
            $('.add_subtask').addClass('hidden');
            $('.agree').removeClass('hidden');
            break;
    }//зависимост статуса от показа кнопок
    //при изменения даты
    $('div#date-task input').on('change', function (e) {
        var date = e.currentTarget.value;
        var name_input = e.target.id;
        var text = $('label[for=' + name_input + ']').text();
        var id = parseInt($('input[name="task_id"]').val());
        ajax_send(id, name_input, date, text)
    })

    //при изменения имя
    $('input#task_name').on('change', function (e) {
        var date = e.currentTarget.value;
        var name_input = e.target.id;
        var id = parseInt($('input[name="task_id"]').val());
        ajax_send(id, name_input, date, "Тема ")
    })

//при замене ответственного
    $('#task_responsible').on('change', function (e) {
        if (e.removed.id != 0) {
            bootbox.confirm({
                title: "Подтверждение",
                message: "Вы уверены что хотите поменять ответственного???",
                buttons: {
                    cancel: {
                        label: 'Отмена'
                    },
                    confirm: {
                        label: 'Подтвердить'
                    }
                },
                callback: function (result) {
                    if (result) {
                        var date = e.currentTarget.value;
                        var name_input = e.target.id;
                        var id = parseInt($('input[name="task_id"]').val());
                        ajax_send(id, name_input, date, "пункте ответственный на ")
                    }
                }
            });
        }
    });
    $('#task_subexecutors').on('change', function (e) {

        bootbox.confirm({
            title: "Подтверждение",
            message: "Вы уверены что хотите поменять ???",
            buttons: {
                cancel: {
                    label: 'Отмена'
                },
                confirm: {
                    label: 'Подтвердить'
                }
            },
            callback: function (result) {
                if (result) {

                    var date = e.val.join(',');
                    var name_input = "task_executors";
                    var id = parseInt($('input[name="task_id"]').val());

                    ajax_send(id, name_input, date, "пункте соисполнитель на ", e.hasOwnProperty('added') ? e.added.id : e.removed.id)
                }
            }
        });
    });
    ////

//изменения статуса
    $('select#task_status').on("change", function (e) {
        var b_action = e.currentTarget.value;
        var id = $('input[name="task_id"]').val();
        $.ajax({
            type: 'POST',
            //  async: false,
            dataType: "text",
            data: "action=accepted&id=" + id + "&b_action=" + b_action + "&old_action=" + old_action,
            url: "/app/views/tasks/ajax-component_new.php",
            cache: false,
            success: function (jqXHR, textStatus, errorThrown) {
                if (b_action != 3) {
                    app.addtask("events", "Изменился статус с <em>" + name_status[old_action] + "</em> на <em>" + name_status[b_action] + "</em>");

                }
                if (b_action == 3) {
                    $('.agree').addClass('hidden');
                    $('.but_end').addClass('hidden');
                }
                if (b_action == 4) {
                    attr_role(role);
                    hidden_role(role);
                }
                if (b_action == 4) {
                    $('.but_start').addClass('hidden');
                    $('.but_end').removeClass('hidden');
                }
                if (b_action == 5) {
                    $('.but_start').addClass('hidden');
                    $('.but_end').addClass('hidden');
                    $('.btn-file').addClass('hidden');
                }
                if (b_action == 2) {
                    $('.but_start').addClass('hidden');
                    $('.but_end').addClass('hidden');
                    $('#agree_act').addClass('hidden');
                    $('#agree_diact').addClass('hidden');
                }
                if (b_action == 6) {
                    $('.but_start').addClass('hidden');
                    $('.but_end').addClass('hidden');

                    $('#agree_act').removeClass('hidden');
                    $('#agree_diact').removeClass('hidden');
                    $('.agree').removeClass('hidden');
                }
            },
            error: function () {
                console.log("error");
            }
        });
    })


    /////////////////////////////------/////////////////
    if ((status == 1 || status == 2 || status == 6 || status == 5) && role != "director") {
        role_new = "visitors";
    } else {
        role_new = role
    }

//console.log(role_new);
// работа с ролью
    function attr_role(roler) {
        if (status != 0 || roler == 'director') {
            $.each(roles[roler].disabled, function (index, value) {

                if (value === true) {
                    $('#' + index).attr('disabled', false);
                } else {
                    $('#' + index).attr('disabled', true).removeAttr('name');
                }
            });
        }
    }

    attr_role(role_new);
///
    // /// согласование
    id = $('input[name="task_id"]').val();

    route = $('#route-task').dataTable({
        "paging": false,
        "searching": false,
        "sort": false,
        "scrollY": "100px",
        "info": false,
        "initComplete": function () {
            $('table#route-task tr.danger').on("click", function (e) {
                data = route.ajax.json().data;
                bootbox.alert(data[e.currentTarget.dataset.id].text_comment);
            })
        },
        "createdRow": function (row, data, dataIndex) {
            if (data['status'] === "Не согласованно") {
                $(row).addClass('danger');
            }
            if (data['status'] === "Согласованно") {
                $(row).addClass('success');
            }
            if (data['status'] === "Требует согласования") {
                $(row).addClass('active');
            }
            $(row).attr("data-id", dataIndex);
        },
        ajax: {
            url: "/app/views/tasks/ajax-component_new.php",
            data: {"routetask": "1", "id": id},
            dataType: "json",
            cache: false,
            contentType: "application/json; charset=utf-8",
            dataSrc: function (json) {
                var options = {
                    year: 'numeric',
                    month: 'numeric',
                    day: 'numeric',
                    hour: 'numeric',
                    minute: 'numeric',
                    second: 'numeric'
                };
                $.map(json.data, function (val, i) {
                    if (val.date != null) {
                        dat = new Date(val.date);
                        json.data[i].date = dat.toLocaleString("ru", options);
                    }
                })
                return json.data
            },
        },
        columnDefs: [
            {"title": "Дата", "targets": [0], "className": "text-center", "data": "date"},
            {"title": "ФИО", "targets": [1], "className": "text-center", "data": "name"},
            {"title": "Статус", "targets": [2], "className": "text-center", "data": "status"},
        ],
        language: {
            "lengthMenu": "Выводить по _MENU_ записей",
            "zeroRecords": "Ни одной записи не найдено",
            "info": "Показано страниц _PAGE_ из _PAGES_",
            "infoEmpty": "Ни одной записи не найдено",
            "infoFiltered": "(отфильтровано из _MAX_ записей)",
            "search": "Поиск:",
            "paginate": {
                "first": "Первая",
                "last": "Последняя",
                "next": "Следующая",
                "previous": "Предыдущая"
            }
        }
    }).api(); //вывод согласования


    $('#agree_act').on('click', function () {
        bootbox.confirm({
            title: "Подтверждение",
            message: "Подвердите согласования",
            backdrop: false,
            buttons: {
                cancel: {
                    label: 'Отмена'
                },
                confirm: {
                    label: 'Подтвердить'
                }
            },
            callback: function (result) {
                boxboot_ajax(result, 1)
            }
        });
    });


    $('#agree_diact').on('click', function () {
        bootbox.prompt({
            title: "Укажите причину несогласования",
            inputType: 'textarea',
            backdrop: false,
            buttons: {
                cancel: {
                    label: 'Отмена'
                },
                confirm: {
                    label: 'Подтвердить'
                }
            },
            callback: function (result) {
                if (result == "") {
                    bootbox.alert("Не указана причина несогласования")
                    return false;
                }
                boxboot_ajax(result, 0)
            }
        });
    });


    function boxboot_ajax(result, status) {
        if (result) {
            old_action = parseInt($('select#task_status').val());
            date = new Date;
            full_date = date.getFullYear() + "/" + date.getMonth() + "/" + date.getDate();
            id = $('input[name="task_id"]').val();
            data =
                {
                    id: id,
                    date: Date.now(),
                    idname: idcockie,
                    status: status,
                    text_comment: result === true ? "" : result
                }
            ;
            console.log(data);
            $.ajax({
                type: 'POST',
                dataType: "text",
                data: 'agree=' + id + '&data=' + JSON.stringify(data),
                url: "/app/views/tasks/ajax-component_new.php",
                cache: false,
                success: function (jqXHR, textStatus, errorThrown) {
console.log(textStatus);console.log(jqXHR);
                    if (status == 1) {
                        $('#task_status').select2('val', '2').trigger('change');
                        app.addtask("events", "Согласованно: " + idcockie_name);
                    } else {
                        $('#task_status').select2('val', '4').trigger('change');
                        app.addtask("events", "Не Согласованно: " + idcockie_name + " причина: " + data.text_comment);
                    }

                    route.ajax.reload();
                },
                error: function () {
                    console.log("error");
                }
            });
        }
    }

    // setInterval(function () {
    //     route.ajax.reload();
    // }, 5000);
//////////////////////////////////------/////////////////


    $('.button_action').on("click", function (e, i) {

        if (e.currentTarget.dataset.id != 3) {
            bootbox.confirm({
                title: "Подтверждение",
                backdrop: false,
                message: "Подвердите действие ",
                buttons: {
                    cancel: {
                        label: 'Отмена'
                    },
                    confirm: {
                        label: 'Подтвердить'
                    }
                },
                callback: function (result) {
                    if (result) {
                        var b_action = parseInt(e.currentTarget.dataset.id);
                        old_action = parseInt($('select#task_status').val());

                        let task_type = $('#task_type').val();

                        let agree_el = $('div#route_of_agreement_task').children('div.agree').length;
                        console.log(agree_el > 0 && $('div.agree').hasClass('hidden') && b_action == 2);


                        // if (agree_el > 0 && $('div.agree').hasClass('hidden') && b_action == 2) {
                        //     b_action = 6
                        // }

                        if (org_cockie == "pepsico" && b_action == 2 && task_type != 0) {
                            b_action = 6
                        }

                        $("select#task_status").select2('val', b_action).trigger('change');
                        
                        $('#agreement_task_report').removeClass('hidden');
                        $('#agreement_task_xls').removeClass('hidden');
                        $('#agreement_task_dl').removeClass('hidden');
                    }

                }
            });
        } else {
            bootbox.prompt({
                title: "Укажите причину невыполнения",
                inputType: 'textarea',
                backdrop: false,
                buttons: {
                    cancel: {
                        label: 'Отмена'
                    },
                    confirm: {
                        label: 'Подтвердить'
                    }
                },
                callback: function (result) {
                    if (result == "") {
                        bootbox.alert("Не указана причина невыполнения")
                        return false;
                    }
                    console.log(result);
                    if (result) {
                        var b_action = parseInt(e.currentTarget.dataset.id);
                        old_action = parseInt($('select#task_status').val());
                        app.addtask("events", "Статус : <em>" + name_status[b_action] + "</em> Причина: " + result);
                        $("select#task_status").select2('val', b_action).trigger('change');
                    }


                }
            });
        }
        ;

    })// Кнопки действия


    /// если тип 1 то
    var t_type = $('#task_type').val();
    if (t_type == '0') {
        var p = $('#subtask_job');
        p.closest(".form-group").addClass('hidden');
        p.removeAttr('name');
    }
/////////Таблица с Под Задачами //
    table = $('#datatable-task').dataTable({
        "paging": true,
        "searching": false,
        "dom": 'frt<"pull-left"p><"pull-right"B><"clearfix">',
        "scrollY": "400px",
        "scrollCollapse": true,
        "processing": true,
        buttons: [
            {text: 'Перейти к отчету', className: 'see_all', enabled: false},
            {text: 'Создать отчет', className: 'createButton', enabled: false},
            {text: 'Закрыть отчет', className: 'closeButton hidden', enabled: false}
        ],
        "initComplete": function () {
            var t_type = parseInt($('#task_type').val());

            if (t_type != 2 && t_type != 3 && t_type != 5 && t_type != 6) {
                table.buttons().destroy();
            }
            if (role_new == "visitors") {
                table.buttons().destroy();
            }
        },
        "createdRow": function (row, data, dataIndex) {
            if (data['status'] === "В работе") {
                $(row).addClass('active');
            }
            if (data['status'] === "Выполнено") {
                $(row).addClass('success');
            }
            if (data['status'] === "Не выполнено") {
                $(row).addClass('danger');
            }

            if (data['status'] === "Требует согласования") {
                $(row).addClass('danger');
            }

            $(row).find('td').eq(7).append('<ul class="list-inline"><li>' +
                '<a href="/tasks/task_new?action=edit&id=' + data['id'] + '" class="edit_subtask">' +
                '<span class="glyphicon glyphicon-pencil"></a>' +
                '</li><li>' +
                '<a class="delete_subtask hidden" onclick= subtask_del(' + data['id'] + ',this) >' +
                '<span class="glyphicon glyphicon-trash"></a>' +
                '</li></ul>');
            if ((t_type == 2 || t_type == 3 || t_type == 5 || t_type == 6)) {
                if (data['nreport'] == 0) {
                    $(row).find('td').eq(5).append(
                        '<span style="color: red" class="glyphicon glyphicon-exclamation-sign" title="Отчет по данной задаче не создан">');
                    if (role_new != "visitors") {
                        table.button(1).enable();
                        table.button(0).disable();
                    }

                    $('.createButton').addClass('btn-success');
                } else {
                    if (role_new != "visitors") {
                        $('.see_all').addClass('btn-success');
                        table.button(0).enable();
                        table.button(1).disable();
                    }
                }
                if (data['nreport'] != 0 && data['progress'] == "Заполнен") {
                    if (role_new != "visitors") {
                        table.button(2).enable();
                    }
                    $('.closeButton').addClass('btn-success');
                    $('.closeButton').removeClass('hidden');
                    $('.createButton').addClass('hidden');
                } else {
                    $('.createButton').removeClass('hidden');
                    $('.closeButton').addClass('hidden');
                }

            }
        },
        ajax: {
            url: "/app/views/tasks/ajax-component_new.php",
            data: {"datatable": "1", "id": id},
            dataType: "json",
            cache: false,
            contentType: "application/json; charset=utf-8",
            dataSrc: function (json) {
                var button = $('.createButton');

                json.data.length == 0 ? button.addClass('hidden') : button.removeClass('hidden');
                return json.data
            },
        },
        columnDefs: [
            {"title": "№", "targets": [0], "className": "text-center", "data": "id"},
            {"title": "Срок исполнения", "targets": [1], "className": "text-center", "data": "date"},
            {"title": "Тема", "targets": [2], "className": "text-center", "data": "name"},
            {"title": "Площадка / Вид работ", "targets": [3], "className": "text-center", "data": "platform"},
            {"title": "Статус", "targets": [4], "className": "text-center", "data": "status"},
            {"title": "%", "targets": [5], "className": "text-center", "data": "progress"},
            {"title": "Исполнители", "targets": [6], "className": "text-center", "data": "responsible"},
            {"title": "", "targets": [7], "orderable": false, "data": "position2", "className": "report"},
        ],
        language: {
            "processing": "Загрузка данных",
            "lengthMenu": "Выводить по _MENU_ записей",
            "zeroRecords": "Ни одной записи не найдено",
            "info": "Показано страниц _PAGE_ из _PAGES_",
            "infoEmpty": "Данные отсутствуют",
            "infoFiltered": "(отфильтровано из _MAX_ записей)",
            "search": "Поиск:",
            "paginate": {
                "first": "Первая",
                "last": "Последняя",
                "next": "Следующая",
                "previous": "Предыдущая"
            }
        }
    }).api();

     //кнопка закрыть
    table.button(0).action(function (e, dt, button, config) {

        // window.open('/reports/matrix?type=self-evaluation&id=' + nreport_all.slice(0, -1)
        //     + '&platform=' + platform.slice(0, -1)
        //     , '_blank');
        window.open('/reports/matrix?type=self-evaluation&task_all=' + id
            , '_blank');///type task=all и id or one +id
    });
    //кнопка закрыть
    table.button(2).action(function (e, dt, button, config) {
        bootbox.confirm({
            title: "Подтверждение",
            message: "Вы уверены, что хотите закрыть отчет?",
            backdrop: false,
            buttons: {
                cancel: {
                    label: 'Отмена'
                },
                confirm: {
                    label: 'Подтвердить'
                }
            },
            callback: function (result) {
                data_nreport = table.ajax.json();
                nreport_all = "";
                $.each(data_nreport.data, function (index, value) {
                    nreport_all += value.nreport + ","
                })
                console.log(nreport_all);
// ajax создание самооценки или аудит
                $.ajax({
                    type: 'POST',
                    dataType: "json",
                    data: {
                        "action": "status",
                        "nreport": nreport_all,
                    },
                    url: "/app/views/tasks/ajax-component_new.php",
                    cache: false,

                    success: function (data) {
                        var tema = "";
                        $.map(data, function (val, i) {
                            tema += '<p>' + data[i] + '</p>';
                        });
                        dt.ajax.reload();
                        app.addtask("events", "Отчет закрыт по " + tema);
                        bootbox.alert({
                            message: tema,
                        });
                    },
                    error: function () {
                        console.log('Данные не получены');
                    }
                });
            }
        });

    });
//кнопка создать отчет
    table.button(1).action(function (e, dt, node, config) {
        bootbox.confirm({
            title: "Подтверждение",
            message: "Вы уверены, что хотите создать отчет по выбранным задачам?",
            backdrop: false,
            buttons: {
                cancel: {
                    label: 'Отмена'
                },
                confirm: {
                    label: 'Подтвердить'
                }
            },
            callback: function (result) {
                if (result) {
                    // проверка создавалась ли самооценка если nreport pid !=0
                    // проверка создана ли новая подзадача и изменилась вид работ
                    var pid = parseInt($('input[name="task_id"]').val());

                    var type = "";
                    var t_type = parseInt($('#task_type').val());
                    var n_type = $('#task_type').select2("data").text;
                    switch (t_type) {
                        case 3:
                            type = "self-evaluation";
                            break;
                        case 4:
                            type = "audit";
                            break;
                        case 6:
                            type = "product_control";
                            break;
                        case 7:
                            type = "state_inspection";
                            break;

                            console.log(pid);
                    }
                    // ajax создание самооценки или аудит
                    $.ajax({
                        type: 'POST',
                        dataType: "json",
                        data: {
                            "action": "create_report",
                            "pid": pid,
                            "type": type,
                            "typen": t_type,
                            "quick": quick
                        },
                        url: "/app/views/tasks/ajax-component_new.php",
                        cache: false,

                        success: function (data) {
                            var tema = "";
                            $.map(data, function (val, i) {
                                tema += '<p>' + data[i] + '</p>';
                            });
                            dt.ajax.reload();
                            app.addtask("events", "Создан:" + n_type + " по " + tema);
                            bootbox.alert({
                                message: tema,
                            });
                        },
                        error: function () {
                            console.log('Данные не получены');
                        }
                    });
                }
                ;
            }
        });
    })

       /* Скрытие % при текущей задаче */
    if (t_type == 0 ) {
        table.column(5).visible(false);
    };

    // setInterval(function () {
    //     table.ajax.reload();
    // }, 15000);
/////

    ///что это
    if ($('table').is('#datatable-task') === false) {
        hidden_role(role_new);
    } else {
        table.on('draw.dt', function () {
            hidden_role(role_new);
        })
    }

    function hidden_role(roler) {
        $.each(roles[roler].hidden, function (index, value) {
            t = $("." + index);
            p = $("#" + index);
            if (value === true) {
                if (t.removeClass('hidden').length == 0) {
                    p.closest(".form-group").removeClass('hidden');
                }
            } else {
                if (t.addClass('hidden').length == 0) {
                    p.closest(".form-group").addClass('hidden');
                    p.removeAttr('name');
                }
            }
        });
    }

/// Добавить таск сплывающие окно
    var datajob;
    $(".add_subtask").on('click', function () {

        var count_rows = table.rows().eq(0).length;

        $('#subtask_executors').select2("val","");
        // var quick=$("input[name='quick']").is(':checked')

        $('form#edit_subtask').attr('id', 'subcreate_subtask');

        $('input[name="subtask_id"]').attr('name', 'subtask_pid');

        $('form#subcreate_subtask').append('<input type="hidden" value="' + $('input[name="task_director"]').val() + '" name="subtask_director"/>');

        var data = $('#task_deadline').val().split('.');
        var date = data[2] + "-" + data[1] + "-" + data[0];

        $("input[name='subtask_deadline']").datepicker('setEndDate', new Date(date))

        // $("input[name='subtask_startnoty']").datepicker({
        //     minDate: new Date(),
        //     maxDate: new Date(date)
        // })
        var pid = parseInt($('input[name="task_id"]').val());
        $('input[name="subtask_pid"]').val(pid);
        action = "subtask_vid";
        nreport = "";

        if (t_type == 4) {
            action = "subtask_vid_only"
            nreport = $('input#task_nreport').val()
        }

        $('#subtask_job').empty().select2('val', '');
        $.ajax({
            type: 'POST',
            dataType: "json",
            async: false,
            data: {
                "action": action,
                "pid": pid,
                "nreport": nreport
            },
            url: "/app/views/tasks/ajax-component_new.php",
            cache: false,
            success: function (data) {

                $('#subtask_job').empty().select2('val', '');

                platform1 = parseInt($('select.select2[name="task_create_platform"]').val());

                if (t_type != 4) {

                    $('div.level1').removeClass("hidden");

                    add_job(platform1, data, all_job, false, quick)

                } else {

                    if ($('div.all_platform div').hasClass("level1")) {
                        $('div.level1').addClass("hidden");
                    }

                    add_job(platform1, data, all_job, true)
                }

            },
            error: function () {
                console.log('Данные не получены');
            }
        }).done(function (json) {
            datajob = json;
        });
        count_rows = count_rows + 1;
        $('#subtask_name').val("Подзадача № " + count_rows);

        $('#subtasks').modal();
    });
    ////

    // работа с видом работ
    data_pl_hask = [];//нужно для левела
    var n_plat = $('#task_platform').val();
    // recursia_platform(data_recurs, n_plat);
    level = getlevel(data_recurs);
    data = Object.assign({}, find_id(data_recurs, n_plat));
    data_select(data, level);

    $('#level' + level).select2("val", n_plat).select2('readonly', true).trigger("change")

    $('#level' + (level + 1)).select2("val", '0').trigger("change")

    $('select.select2.pl').on('change', function (e) {

        $('#subtask_job').empty().select2('val', '');

        if (e.val == 0) {
            e.val = $('select.select2[name="task_create_platform"]').val()
        }
        add_job(e.val, datajob, all_job, false, quick)
    })


    function add_job(platform, datajob, all_job, pk = false, fast = false) {
        var fastd = [];
        if (fast) {
            $.ajax({
                type: 'POST',
                url: "/app/views/tasks/ajax-component_new.php",
                data: {action: "vid_fast_self", platform: platform},
                dataType: "json",
                async: false,
                complete: function (data) {
                    fastd = data.responseJSON
                }
            });
            all_job = fastd;
        }

        // console.log(res.responseText());
        var job = Object.assign({}, all_job);
        var pk_array = {};
        $.map(datajob, function (val, pl_dj) {
            if (pl_dj == platform) {
                if (pk == false) {
                    $.map(val.split(','), function (val, i) {
                        delete(job[val])
                    })
                } else {
                    $.map(val.split(','), function (val, i) {
                        pk_array[val] = job[val];
                    })
                    job = pk_array;
                }
            }

        });
        $.map(job, function (val, i) {
            if (val !== undefined) {
                $('#subtask_job').append('<option value=' + i + '>' + val + '</option>');
            }
        });
    }//вывод виды работ

    //

    $("form").on("submit", function (event) {
        $('#save_subtask').val('Ожидайте...').attr("disabled", true);
        form_action = [];
        event.preventDefault();
        form_action = event.target.id.split('_');
        var data1 = getFormData($(this));


        if (form_action[1] == "subtask") {

            var execut = $('select[name="subtask_subexecutors"]').val();
            data1["subtask_subexecutors"] = execut.join(',');
            if (t_type != 0) {
                var job = $('select[name="subtask_job"]').val();
                data1["subtask_job"] = job.join(',');
            }
        }

        //create subtask ,edit subtask, edit_task
        data1 = {[form_action[0]]: data1};

        ///что убрать с проверки формы
        remove = ["task_startnoty", "task_repeattask", "subtask_subexecutors", "subtask_startnoty", "subtask_description"];
        // console.log(check_form(data1[form_action[0]], remove));
        if (check_form(data1[form_action[0]], remove)) {
            //data1["task_description"] = $("#" + formName + " textarea[name$='zadacha']").summernote('code');
            $.ajax({
                type: 'post',
                dataType: "text",
                contentType: "application/x-www-form-urlencoded;charset=utf-8",
                data: data1,
                url: "/app/views/tasks/ajax-component_new.php",
                cache: false,
                success: function (dataout) {

                    app.addtask("events", "Создана задача");

                    var task_table = $('#datatable-task').dataTable().api();

                    task_table.ajax.reload();
                    //обнуление формы
                    if (form_action[1] = "subtasks") {
                        $('form#subcreate_subtask').attr('id', 'edit_subtask');
                        $('input[name="subtask_pid"]').attr('name', 'subtask_id');
                        $('input[name="subtask_deadline"]').val('');
                        $('input[name="subtask_pid"]').val('');
                        $('input[name="subtask_startnoty"]').val('');
                        $('input[name="subtask_name"]').val('');
                        $('textarea[name="subtask_description"]').val('');
                        $('select[name="subtask_subexecutors"]').select2().val('0').trigger('change');
                        $('select[name="subtask_executors"]').select2().val('0').trigger('change');
                        $('#save_subtask').val('Сохранить').attr("disabled", false);
                        $('#subtasks').modal('hide');
                        $.notify({'type':'success', 'message':'Задача успешно сохранена'});

                        //   window.location.href='/tasks/task_new?action=edit&id=28';
                    }
                },
                error: function () {
                    $.notify({'type':'error', 'message':'Произошла ошибка!'});
                }
            })
        } else {
            $.notify({'type':'warning', 'message':'Не все обязательные поля были заполнены'});

        }
    });

    $('.text_zadacha').on('click', function () {
        if (!$('.add_texttask').hasClass('hidden')) {
            text_editor('text_zadacha');
        }
    })


    // $('select#subtask_executors').on('change', function (e) {
    //     var sub = $('select#subtask_subexecutors');
    //     console.log(e);
    //     if (e.removed.id != "") {
    //         sub.find('option[value=' + (e.removed.id) + ']').prop('disabled', false).trigger('change');
    //     }
    //     sub.find('option[value=' + (e.added.id) + ']').prop('disabled', true).trigger('change');
    // });

});

function text_editor(div_indef) {
    var stext = $('.' + div_indef);
    stext.summernote({
        //  airMode: true,
        lang: 'ru-RU',
        height: 100,
        minHeight: 50,
        maxHeight: 300,
        focus: true,
        placeholder: 'Введите описание задание',
        fontNames: ['Arial', 'Times New Roman', 'Helvetica'],
        disableDragAndDrop: true,
        toolbar: [
            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['font', ['strikethrough', 'superscript', 'subscript']],
            ['fontsize', ['fontsize']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']]
        ],
        callbacks: {

            onInit: function () {
                var text_old = stext.summernote('code');
                $(".note-toolbar").append("<div class='note-btn-group btn-group' style='float: right'><button type='button' id='cancel_comment'>Отменить</button><button type='button' id='save_comments'>Сохранить</button><div>");
                $('#cancel_comment').on("click", function () {
                    bootbox.confirm({
                        title: "Подтверждение",
                        message: "Вы уверены,что хотите отменить изменения ?",
                        backdrop: false,
                        buttons: {
                            cancel: {
                                label: 'Отмена'
                            },
                            confirm: {
                                label: 'Подтвердить'
                            }
                        },
                        callback: function (result) {
                            if (result) {
                                stext.summernote('code', text_old);
                                stext.summernote('destroy')
                            }
                            ;
                        }
                    });
                });

                $('#save_comments').on("click", function () {
                    bootbox.confirm({
                        title: "Подтверждение",
                        message: "Вы уверены,что хотите измененить?",
                        backdrop: false,
                        buttons: {
                            cancel: {
                                label: 'Отмена'
                            },
                            confirm: {
                                label: 'Подтвердить'
                            }
                        },
                        callback: function (result) {
                            if (result) {

                                var id = $('input[name="task_id"]').val();
                                var text = stext.summernote('code').trim();
                                var name_input = 'task_description';
                                ajax_send(id, name_input, text, " Описание задачи")
                            }
                        }
                    });
                    stext.summernote('destroy');
                });
            }
            ,

            onBlur: function (e) {
                console.log(e.target.textContent, div_indef);
            }
            ,
            onFocus: function (e) {
            }
        }
    })
    ;
} ///надо избавиться

function ajax_send(id, name_input, date, text, sub_id = false) {

    $.ajax({
        type: 'POST',
        dataType: "text",
        data: {
            "action": "task_date",
            "id": id,
            "name_input": name_input,
            "date": date
        },
        url: "/app/views/tasks/ajax-component_new.php",
        cache: false,
        success: function (jqXHR, textStatus, errorThrown) {
            if (name_input == "task_responsible") {
                date = name_l[date]
            }
            ;
            if (name_input == "task_subexecutors") {
                date = name_l[sub_id]
            }
            ;
            app.addtask("events", "Изменения в " + text + " " + date);
            $.notify({'type':'success', 'message':'Дата успешно сохранена'});
        },
        error: function () {
            if (name_input == "task_responsible") {
                date = name_l[date]
            }
            ;
            app.addtask("events", "Изменения не сохранились по" + text + " " + date);
            $.notify({'type':'error', 'message':'Дата не сохранилась'});

        }
    })
}
// Установить дату меньше на период датапикер
function setFromDate(period, id_datapick, day, before = true, deadline) {

    period = parseInt(period);
    if ($("input[name=" + deadline + "]").val() == '') {
        $.notify({'type':'warning', 'message':'Сначала необходимо указать крайний срок'});
        return false;
    } else {
        // Получаем дату из поля ввода в формате dd.mm.yyyy
        var deadl = $("input[name=" + deadline + "]").val().split('.');

        var delay = new Date(deadl[2], (deadl[1] - 1), deadl[0]);

        switch (day) {
            case 'day': {
                if (before && day == "day") {
                    delay.setDate(delay.getDate() - period);
                } else {
                    delay.setDate(delay.getDate() + period);
                }
            }
                break;
            case 'mounth': {
                if (before && day == "mounth") {
                    delay.setMonth(delay.getMonth() - period);
                } else {
                    delay.setMonth(delay.getMonth() + period);
                    delay.setFullYear(delay.getFullYear());
                }
            }
                break;
            case 'year': {
                if (before && day == "year") {
                    delay.setFullYear(delay.getFullYear() - period);
                } else {
                    delay.setFullYear(delay.getFullYear() + period);
                }
            }
                break;
        }
        $('#' + id_datapick).datepicker('setDate', delay).trigger('change');
    }
    console.log()
}

function subtask_del(id, metod, pid = false) {
    metod = $(metod).attr('class');
    var data1 = [];
    pid = 5; //не забудь добавить забор pid
    $.ajax({
            type: 'POST',
            dataType: "text",
            data: {
                "action": metod,
                "id": parseInt(id),
                "pid": pid
            },

            url: "/app/views/tasks/ajax-component_new.php",
            cache: false,
            success: function (jqXHR, textStatus, errorThrown) {
                var task_table = $('#datatable-task').dataTable().api();
                var task = "subtask_";
                switch (metod) {
                    case 'delete_subtask': {
                        app.addtask("events", "Удалена задача №: " + parseInt(id));
                        task_table.ajax.reload();
                    }
                        break;
                    //          return data;
                }
            }
            ,
            error: function (error) {
                console.log("error");
            }
        }
    )
    ;
} //только для удаления  subtask

$("#checkbox_select_all").click(function () {
    if ($("#checkbox_select_all").is(':checked')) {
        $("#subtask_job").select2('destroy').find('option').prop('selected', 'selected').end().select2();
    } else {
        $("#subtask_job").select2('destroy').find('option').prop('selected', false).end().select2();
    }
});


