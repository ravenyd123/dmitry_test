$(document).ready(function () {

    $('button[data-action="add-data"]').on("click", function () {
        $('.all_platform').removeClass("hidden");
        $('#pkplatform').addClass("hidden");
        $('#pkplatform').closest('div').addClass("hidden");
        $("#task_create_type").val('').trigger('change').attr('disabled', false);
        $("#task_create_platform").val('').trigger('change').attr('disabled', false);
        $('#task_create_name').val('').attr('disabled', false);
        $("#task_create_description").summernote({airMode: true});
    });

   // $('div.s2id_task_create_responsible a').on('click',function (e) {
   //     console.log(e)
   // })



/// ПК и ПД
    $('button[data-action="pk-data"]').on('click', function () {

        var id = $('tr.selected').data('id'); //получить id

        var plid = $('tr.selected').data('data-plid'); //площадка id проверить сколько под задач присутвут

        var nreport = $('tr.selected').data('data-nreport');//получить nreport тоесть bunch 0

        all = table.ajax.json().data;


        var selectid = [];
        var plat = "";
        var error = "";
        var reportpkipd = "";
        var reportpkipd_name = "";
        flag = true;
        platf_id = 0;
        platf_name = '';

        $('tr.selected').each(function () {
            selectid.push($(this).data('id'))
        });

        $.each(all, function (i, val) {
            if (selectid.includes(parseInt(val.id))) {
                selectid_id_name = val.number_text;

                if (val.type_id == '1' || val.type_id == '0' || val.type_id == '4') {
                    flag = false;
                    error += "Тип задачи - Самооценка,Аудит; "
                }

                if (val.pkikd == 0) {
                    // flag = false;
                    error += "Задействован в других ПКиПД; "
                }

                if (val.status == "Новая") {
                    //   flag = false;
                    error += "Статус: Выполнено не обнаружен; "
                }

                if (plat == "") {
                    plat = val.platform_id
                }

                if (plat != val.platform_id) {
                    flag = false;
                    error += "Имеется разные площадки; "
                } else {
                    platf_id = val.platform_id;
                    platf_name = val.platform;
                }
                reportpkipd += val.id + ",";
                reportpkipd_name += selectid_id_name + ",";
            }

        })

        if (flag) {
            $('.all_platform ').addClass("hidden");
            $('#pkplatform').append(
                '<option value="' + platf_id + '" selected>' + platf_name + '</option>')
            $('#pkplatform').closest('div.form-group').removeClass('hidden')
            $('#pkplatform').attr('name', 'task_create_platform')
            $('#pkplatform').select2('val', platf_id).trigger('change')


            var text = 'ПКиПД создана по задачам №:' + reportpkipd_name;
            $("#task_create_description").summernote({airMode: true});
            $("#task_create_type").val(4).trigger('change').attr('disabled', true);
            $("#task_create_platform").val(plid).trigger('change').attr('disabled', true);
            if ($('form#task_create div.modal-header input').attr("name") !== "task_create_nreport") {
                $('form#task_create div.modal-header').append('<input type="hidden" name="task_create_nreport" value=' + reportpkipd + '>');
            }

            $('#task_create_name').val(text).attr('disabled', true);

            $('#task_create').modal();
        } else {
           // notify('warning', 'top right', 'Предупреждение', error);
            $.notify({'type':'error', 'message':'Ошибка!'});
        }
    });

    $("form").on("submit", function (event) {
        event.preventDefault();
        var dataform = getFormData($(this));
        remove = ['task_create_description'];//Исключения

        if (check_form(dataform, remove)) {
            $.ajax({
                type: 'post',
                dataType: "text",
                contentType: "application/x-www-form-urlencoded;charset=utf-8",
                data: {create: dataform},
                url: "/app/views/tasks/ajax-component_new.php",
                cache: false,
                beforeSend: function () {
                    $('input[type=submit]').val('Ожидайте...').attr("disabled", true);
                },
                complete: function (dataout) {
                    $('input[type=submit]').val('Далее').attr("disabled", false);
                    window.open(window.location.href + '/task_new?action=edit&id=' + dataout.responseText, '_self');
                },
                success: function (dataout) {
                    //    window.open(window.location.href + '/task_new?action=edit&id=' + dataout, '_self');
                },
                error: function () {
                    $.notify({'type':'error', 'message':'Произошла ошибка!'});

                }
            })
        }
    });


    // var login = "<?=$_COOKIE['login'];?>";

    //  $('.sortable').sortable();
// Datatables
    var table = $('#datatable').dataTable({
        lengthChange: false,
        //stateSave: true,
        buttons: ['colvis'],
        "aaSorting": [[1, 'desc']],
        // "sScrollX": "100%",
        "scrollCollapse": true,
        "paging": true,
        "processing": true,
        ajax: {
            url: "/app/views/tasks/ajax-component_new.php",
            data: {"action": "task_all"},
            dataType: "json",
            cache: false,
            contentType: "application/json; charset=utf-8",
            dataSrc: function (json) {
                return json.data
            },
        },
        columnDefs: [
            {
                "title": "№",
                "targets": [0],
                "className": "text-center",
                "data": null,
                "orderable": false, "defaultContent": '', "width": "8%"
            },
            {
                "title": "Дата",
                "targets": [1],
                "className": "text-center",
                "data": "date",
                "type": "de_datetime",
                "width": "10%"
            },
            {"title": "Тема", "targets": [2], "className": "text-center", "data": "name", "width": "20%"},
            {"title": "Тип Задачи", "targets": [3], "className": "text-center", "data": "type", "width": "10%"},
            {"title": "Предприятия", "targets": [4], "className": "text-center", "data": "platform", "width": "20%"},
            {"title": "Крайний срок", "targets": [5], "className": "text-center", "data": "deadline", "width": "5%"},
            {
                "title": "Приоритет",
                "targets": [6],
                "className": "text-center",
                "data": null,
                "defaultContent": '',
                "visible": false,
                "width": "5%"
            },
            {
                "title": "Надзорный орган",
                "targets": [7],
                "className": "text-center",
                "data": "authority",
                "visible": false,
                "width": "5%"
            },
            {"title": "Постановщик", "targets": [8], "className": "text-center", "data": "director", "width": "10%"},
            {
                "title": "Ответственный",
                "targets": [9],
                "className": "text-center",
                "data": "responsible",
                // "visible": false,
                "width": "5%"
            },
            {
                "title": "Статус",
                "targets": [10],
                "className": "text-center",
                "data": "status",
                "visible": false,
                "width": "5%"
            }

        ],
        "initComplete": function () {
            $('[data-toggle="tooltip"]').tooltip();
            table.buttons().container().appendTo('#datatable_wrapper .col-sm-6:eq(0)');
            this.api().column(3).every(function () {
                var column = this;

                var select = $('<select class="select3"><option value="">Все</option></select>')
                    .appendTo($('#datatable-filter div.eh'))
                    .on('change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );
                        column
                            .search(val ? '^' + val + '$' : '', true, false)
                            .draw();
                    })

                column.data().unique().sort().each(function (d, j) {
                    select.append('<option value="' + d + '">' + d + '</option>')
                });
            });
            this.api().column(4).every(function () {
                var column = this;
                var select = $('<select class="select3"><option value="">Все</option></select>')
                    .appendTo($('#datatable-filter div.pl'))
                    .on('change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );

                        column
                            .search(val ? '^' + val + '$' : '', true, false)
                            .draw();
                    })

                column.data().unique().sort().each(function (d, j) {
                    select.append('<option value="' + d + '">' + d + '</option>')
                });
            });
            this.api().column(9).every(function () {
                var column = this;
                var select = $('<select class="select3"><option value="">Все</option></select>')
                    .appendTo($('#datatable-filter div.ot'))
                    .on('change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );

                        column
                            .search(val ? '^' + val + '$' : '', true, false)
                            .draw();
                    })

                column.data().unique().sort().each(function (d, j) {
                    select.append('<option value="' + d + '">' + d + '</option>')
                });
            });
            this.api().column(10).every(function () {
                var column = this;
                var select = $('<select class="select3"><option value="">Все</option></select>')
                    .appendTo($('#datatable-filter div.st'))
                    .on('change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );

                        column
                            .search(val ? '^' + val + '$' : '', true, false)
                            .draw();
                    })

                column.data().unique().sort().each(function (d, j) {
                    select.append('<option value="' + d + '">' + d + '</option>')
                });
            });
            $('select.select3').select2();
        },
        "createdRow": function (row, data, dataIndex) {
            if (data['pkikd'] >0) {
                $(row).find("td").eq(0).append('<span class="glyphicon glyphicon-file" style="top: 3px;" aria-hidden="true" title="Открыть отчет № '+ data['pkikd'] +'"></span>');
            }
            if (data['file'] >0) {
                $(row).find("td").eq(0).append('<span class="glyphicon glyphicon-paperclip" style="top: 3px;" aria-hidden="true" title="Файлов: '+ data['file'] +'"></span>');
            }
            if (data['status'] === "В работе") {
                $(row).find("td").eq(0).prepend('<span class="badge bg-warning" title="В работе">' + data['number_text'] + '</span>');
            }
            if (data['status'] === "Новая") {
                $(row).find("td").eq(0).prepend('<span class="badge bg-primary" title="Новая">' + data['number_text'] + '</span>');
            }
            if (data['status'] === "Выполнено") {
                $(row).find("td").eq(0).prepend('<span class="badge bg-success" title="Выполнено">' + data['number_text'] + '</span>');
            }
            if (data['status'] === "Не выполнено" || data['status'] === "Не принято") {
                $(row).find("td").eq(0).prepend('<span class="badge bg-danger" title="'+data['status']+'">' + data['number_text'] + '</span>');
            }
            if (data['status'] === "Требует согласования") {
                $(row).find("td").eq(0).prepend('<span class="badge bg-info" title="Требует согласования">' + data['number_text'] + '</span>');
            }
            $(row).attr("data-id", data['id']);

            //data-plid="<?= $row["platform"] ?>"
            // data-nreport="<?= $row["nreport"] ?>
            // $(row).find("td").eq(6).addClass(data["priority"] == 1 ? 'task-red' : 'task-green').append('<div></div>');
        },
        "language": {
            "processing": "Производиться загрузка данных",
            "lengthMenu": "Выводить по _MENU_ записей",
            "zeroRecords": "Ни одной записи не найдено",
            "info": "Показано страниц _PAGE_ из _PAGES_",
            "infoEmpty": "No records available",
            "infoFiltered": "(отфильтровано из _MAX_ записей)",
            "search": "Поиск:",
            "paginate": {
                "first": "Первая",
                "last": "Последняя",
                "next": "Следующая",
                "previous": "Предыдущая"
            },
        }
    }).api()
    // table.column("Дата").visible(false);

    $('#datatable tbody').on('click', 'tr', function () {
        $(this).toggleClass('selected');
        checkRowAmount_new();
    });


// Кнопка добавления записи
    /*  $('*[data-action="add-data"]').click(function () {
     window.open(window.location.href + '/task_new?action=add', '_self');
     })*/
// Кнопка редактирования записи
    $('*[data-action="edit-data"]').click(function () {
        var id = $("#datatable tr.selected").attr('data-id');
        if (checkRowAmount_new() != 1) {
            $.notify({'type':'error', 'message':'Можно выбрать только одну запись!'});

            return false;
        }
        ;
        window.open(window.location.href = '/tasks/task_new?action=edit&id=' + id, '_self');
    })
// Кнопка удаления записи
    $('*[data-action="remove-data"]').click(function () {

        if (checkRowAmount_new() < 1) {
            $.notify({'type':'error', 'message':'Необходимо выбрать хотя бы одну запись!'});

            return false;
        }
        ;

        // confirm('info', 'top right', 'Данные будут удалены безвозвратно.<br/> Вы уверены что хотите выполнить действие?');

        // $(document).on('click', '.notifyjs-metro-base .no', function () {
        // $(this).trigger('notify-hide');
        // });

        // $(document).on('click', '.notifyjs-metro-base .yes', function () {
        bootbox.confirm({
            title: "Подтверждение",
            backdrop: false,
            message: "Выбранные задачи будут удалены. <br>Вы уверены что хотите выполнить действие? ",
            buttons: {
                cancel: {
                    label: '<i class="fa fa-times"></i>Нет'
                },
                confirm: {
                    label: '<i class="fa fa-check"></i>Да'
                }
            },
            callback: function (result) {
                if (result) {

                    $(this).trigger('notify-hide');

                    var removeId = [];

                    $('#datatable tr.selected').each(function (index) {
                        removeId.push($(this).attr('data-id'));
                    })
                    console.log(removeId);
                    var attr = 'ids=' + removeId + '&action=removeTask' + '&myid=' + get_cookie('id');
                    ;
                    $.ajax({
                        type: 'POST',
                        data: attr,
                        url: "/assets/functions.php",
                        cache: false,
                        success: function (data) {
                            console.log(data);
                            dataArr = JSON.parse(data)
                            if (dataArr['type'] == 'error') {
                                $.notify({'type':'error', 'message':'<br>Задачи могут быть удалены если: <br>* Вы являетесь постановщиком задачи <br>* Статус задач и всех подзадач "Новая" <br>* В задачах и подзадачах не создан отчет'});
                            }
                            if (dataArr['type'] == 'warning') {
                                $.notify({'type':'danger', 'message': dataArr['message'] + '<br>Задачи могут быть удалены если: <br>* Вы являетесь постановщиком задачи <br>* Статус задач и всех подзадач "Новая" <br>* В задачах и подзадачах не создан отчет'});
                                table.ajax.reload();
                            }
                            if (dataArr['type'] == 'success') {
                                $.notify({'type':'success', 'message':'Все выделенные задачи с подзадачими были успешно удалены'});

                                table.ajax.reload();
                            }

                        }
                    });
                }
            }
        });
    });

    function checkRowAmount_new() {
        var table = $("#datatable tr.selected")
        if (table.length > 0) {

            $('.panel-heading .ion-trash-a').parent().addClass('btn-danger');
            $('.panel-heading .ion-close').parent().addClass('btn-danger');
            $('.panel-heading .ion-unlocked').parent().addClass('btn-danger');
            $('.panel-heading .ion-edit').parent().removeClass('btn-info');
            $('.panel-heading .ion-eye').parent().removeClass('btn-primary');
            $('.panel-heading .ion-clipboard').parent().addClass('btn-purple');
            $('.panel-heading .ion-document').parent().removeClass('btn-warning');
            $('.panel-heading .ion-alert-circled').parent().removeClass('btn-danger');
            $('.panel-heading .ion-checkmark').parent().addClass('btn-success');
        }
        if (table.length == 1) {

            $('.panel-heading .ion-close').parent().addClass('btn-danger');
            $('.panel-heading .ion-unlocked').parent().addClass('btn-danger');
            $('.panel-heading .ion-edit').parent().addClass('btn-info');
            $('.panel-heading .ion-eye').parent().addClass('btn-primary');
            $('.panel-heading .ion-document').parent().addClass('btn-warning');
            $('.panel-heading .ion-alert-circled').parent().addClass('btn-danger');
        }
        if (table.length <= 0) {

            $('.panel-heading .ion-edit').parent().removeClass('btn-info');
            $('.panel-heading .ion-eye').parent().removeClass('btn-primary');
            $('.panel-heading .ion-trash-a').parent().removeClass('btn-danger');
            $('.panel-heading .ion-close').parent().removeClass('btn-danger');
            $('.panel-heading .ion-unlocked').parent().removeClass('btn-danger');
            $('.panel-heading .ion-clipboard').parent().removeClass('btn-purple');
            $('.panel-heading .ion-document').parent().removeClass('btn-warning');
            $('.panel-heading .ion-checkmark').parent().removeClass('btn-success');
            $('.panel-heading .ion-alert-circled').parent().removeClass('btn-danger');
        }
        ;
        return table.length;
    }

});
$(document).ready(function () {
    level = getlevel(data_recurs);
    //var n_plat = $('#task_platform').val();
    data_select(data_recurs, level);
})
