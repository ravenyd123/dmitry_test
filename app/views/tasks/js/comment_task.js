Vue.component('button-counter', {//чтоб от коммента2 избавить нужно добавить id в объект
    props: ['comm', 'connect'],
    computed: {
        normedate: function () {
            var dat = new Date();
            dat.setTime(this.comm.date * 1000);
            var options = {
                year: 'numeric',
                month: 'numeric',
                day: 'numeric',
                hour: 'numeric',
                minute: 'numeric',
                second: 'numeric'
            };
            return dat.toLocaleDateString('ru', options)
        },
    },
    template: `<li>
<hr v-if="connect!=0">
    <div class="media-left">
        <img src="/assets/admina/img/icon.png" width="30">
    </div>
    <div class="media-body">
           <a href="#">Задача № {{comm.id_task}} </a> <span>ФИО: {{comm.fio}}</span>
        <small>Дата: {{normedate}}</small>
        <div class="pull-right">
            <ul class="list-inline" v-if="comm.type=='comments'">
                <li class="pull-right p-0"><a href="#" v-on:click="$emit('deletetask',comm.id)"><span class="glyphicon glyphicon-trash"></span></a></li>
            </ul>
        </div>
    <div style="word-break: break-all"><p v-html="comm.text"></p></div>
    <slot></slot>
    </div>
</li>`
});

// var all_data = {
//     "comments": {
//         "1": {
//             "type": "comments",
//             "id": 1,
//             "id_task": "406",
//             "id_autor": "42",
//             "id_whom": null,
//             "fio": "Иванов И.П.",
//             "answer": {
//                 "1": {
//                     "type": "comments",
//                     "id": 2,
//                     "id_task": "406",
//                     "id_autor": "42",
//                     "id_whom": null,
//                     "fio": "Иванов И.П.",
//                     "date": 1545379138,
//                     "text": "kjghkjgk"
//                 },
//                 "2": {
//                     "type": "comments",
//                     "id": 2,
//                     "id_task": "406",
//                     "id_autor": "42",
//                     "id_whom": null,
//                     "fio": "Иванов И.П.",
//                     "date": 1545379138,
//                     "text": "kjghkjgk"
//                 }
//             },
//             "date": 1545377964,
//             "text": "dfgdfgdfg"
//         },
//         "2": {
//             "type": "comments",
//             "id": 2,
//             "id_task": "406",
//             "id_autor": "42",
//             "id_whom": null,
//             "fio": "Иванов И.П.",
//             "answer": {
//                 1: {
//                     "type": "comments",
//                     "id": 2,
//                     "id_task": "406",
//                     "id_autor": "42",
//                     "id_whom": null,
//                     "fio": "Иванов И.П.",
//                     "date": 1545379138,
//                     "text": "kjghkjgk"
//                 }
//             },
//             "date": 1545379138,
//             "text": "kjghkjgk"
//         }
//     },
//     "events": {
//         "1": {
//             "type": "events",
//             "id": 1,
//             "id_task": "406",
//             "id_autor": "42",
//             "fio": "Иванов И.П.",
//             "answer": 1,
//             "date": 1545377964,
//             "text": "dfgdfgdfg"
//         },
//         "2": {
//             "type": "events",
//             "id": 2,
//             "id_task": "406",
//             "id_autor": "42",
//             "fio": "Иванов И.П.",
//             "answer": 1,
//             "date": 1545379138,
//             "text": "kjghkjgk"
//         }
//     }
// };


//console.log(all_data);

var app = new Vue({
    el: ".comments-list",
    data: {
        textinp: '',
        comments: [],
        events: []
    },
    created() {
        axios.post('/app/views/tasks/ajax-component_new.php?sub_task_comment', {
            id_task: $('input[name="task_id"]').val()
        }).then(function (response) {
            if (response.data != '') {
                app.comments = response.data.comments;
                app.events = response.data.events;
            } else {
                app.comments = {};
                app.events = {};
            }
        }).catch(error => {
            console.log(error)
        })
    },
    // computed: {
    //     sortedTopics() {
    //         const getter = v => v[Object.keys(v)[0]]
    //         const sorter = this.sortByObjectProperty("date", getter)
    //         return this.events.sort(sorter)
    //     }
    // },
    methods: {
        // sortByObjectProperty(prop, getter) {
        //     return (a, b) => {
        //         a = getter(a), b = getter(b)
        //         if (a[prop] < b[prop])
        //             return -1;
        //         if (a[prop] > b[prop])
        //             return 1;
        //         return 0;
        //     }
        // },
        addtask(type_t = "comments", text = ""){
            axios.post('/app/views/tasks/ajax-component_new.php?sub_task_comment', {
                type: type_t,
                textinp: type_t == "events" ? text : this.textinp,
                id_task: $('input[name="task_id"]').val(),
                id_author: this.$cookies.get('id'),
                // id_whom: $('#user-list').select2().val()
            }).then(
                response => (this.$set(this[type_t], response.data.id, response.data))
            ).catch((error) => {
                console.log(error)
            });
            if (type_t == 'comments') {
                this.textinp = '';
            }
        },
        deletask: function (val) {
            console.log(this.comments[val].id);
            axios.post('/app/views/tasks/ajax-component_new.php?sub_task_comment_del', {
                id_task: $('input[name="task_id"]').val(),
                id_del: this.comments[val].id
            }).then(response => (this.$delete(this.comments, val)
            )).catch(error => {
                console.log(error)
            })
            ;
        },
    }


});


//console.log(app);
