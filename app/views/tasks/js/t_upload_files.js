var all_data;
$(document).ready(function () {
    task_id = $('input[name="task_id"]').val();
    $.ajax({
        type: 'POST',
        url: '/app/views/tasks/upload.php',
        data: {all_data: task_id},
        async:true,
        dataType: "json",
        complete: function (data) {
            all_data = data.responseJSON
            var upload1 = JSON.stringify({"id": task_id});
            var el1 = $('input#input-ru').fileinput({
                language: "ru",
                theme: 'gly',
                // maxFilePreviewSize:10000,
                uploadUrl: "/app/views/tasks/upload.php",
                uploadAsync: true,
                showUpload: false, // hide upload button
                showRemove: false, // hide remove button
                hideThumbnailContent: false,//предварительный вид контенкта
                initialPreviewAsData: false,
                initialPreview: all_data.initialPreview,// просмотр файла
                initialPreviewConfig: all_data.initialPreviewConfig,// первоначальная конфигурация предварительного просмотра, если вы хотите, чтобы начальный предварительный просмотр отображался с данными загрузки сервера
                uploadExtraData: {upload: upload1},
                //maxFileSize: 100,
                showCaption: true,
                showBrowse: true,
                dropZoneEnabled: false,//показ drop zona
                browseOnZoneClick: true,
                initialPreviewDownloadUrl: 'upload?downloads={key}',
                overwriteInitial: false,// добавить файлов к предварительному просмотру
                layoutTemplates: {
                    main1: '{preview}\n' +
                    '<div class="input-group {class}">\n' +
                    '  {caption}\n' +
                    '  <div class="input-group-btn">\n' +
                    '    {browse}\n' +
                    '  </div>\n' +
                    '</div>',
                },
                previewSettings: {
                    image: {width: "50px", height: "50px", 'max-width': "100%", 'max-height': "100%"},
                    html: {width: "100%", height: "70px"},
                    text: {width: "100%", height: "70px"},
                    office: {width: "100%", height: "70px"},
                    gdocs: {width: "100%", height: "70px"},
                    video: {width: "100%", height: "auto"},
                    audio: {width: "100%", height: "30px"},
                    flash: {width: "100%", height: "auto"},
                    object: {width: "100%", height: "auto"},
                    pdf: {width: "100%", height: "70px"},
                    other: {width: "100%", height: "70px"}
                }
            }).on("filebatchselected", function (event, files) {
                el1.fileinput("upload");
            }).on("filebeforedelete", function (event, id, index) {
                return new Promise(function (resolve, reject) {
                    bootbox.confirm({
                        title: "Подтверждение",
                        message: "Вы уверены что хотите удалить",
                        buttons: {
                            cancel: {
                                label: 'Отмена'
                            },
                            confirm: {
                                label: 'Подтвердить'
                            }
                        },
                        callback: function (result) {
                            if (result) {
                                resolve();
                            }
                        }
                    })
                });
            });
        }
})
    // $.post('/app/views/tasks/upload.php', {all_data: task_id}).done(function (data) {
    //     all_data = JSON.parse(data)
    //     var upload1 = JSON.stringify({"id": task_id});
    //     var el1 = $('input#input-ru').fileinput({
    //         language: "ru",
    //         theme: 'gly',
    //         // maxFilePreviewSize:10000,
    //         uploadUrl: "/app/views/tasks/upload.php",
    //         uploadAsync: true,
    //         showUpload: false, // hide upload button
    //         showRemove: false, // hide remove button
    //         hideThumbnailContent: false,//предварительный вид контенкта
    //         initialPreviewAsData: false,
    //         initialPreview: all_data.initialPreview,// просмотр файла
    //         initialPreviewConfig: all_data.initialPreviewConfig,// первоначальная конфигурация предварительного просмотра, если вы хотите, чтобы начальный предварительный просмотр отображался с данными загрузки сервера
    //         uploadExtraData: {upload: upload1},
    //         //maxFileSize: 100,
    //         showCaption: true,
    //         showBrowse: true,
    //         dropZoneEnabled: false,//показ drop zona
    //         browseOnZoneClick: true,
    //         initialPreviewDownloadUrl: 'upload?downloads={key}',
    //         overwriteInitial: false,// добавить файлов к предварительному просмотру
    //         layoutTemplates: {
    //             main1: '{preview}\n' +
    //             '<div class="input-group {class}">\n' +
    //             '  {caption}\n' +
    //             '  <div class="input-group-btn">\n' +
    //             '    {browse}\n' +
    //             '  </div>\n' +
    //             '</div>',
    //         },
    //         previewSettings: {
    //             image: {width: "50px", height: "50px", 'max-width': "100%", 'max-height': "100%"},
    //             html: {width: "100%", height: "70px"},
    //             text: {width: "100%", height: "70px"},
    //             office: {width: "100%", height: "70px"},
    //             gdocs: {width: "100%", height: "70px"},
    //             video: {width: "100%", height: "auto"},
    //             audio: {width: "100%", height: "30px"},
    //             flash: {width: "100%", height: "auto"},
    //             object: {width: "100%", height: "auto"},
    //             pdf: {width: "100%", height: "70px"},
    //             other: {width: "100%", height: "70px"}
    //         }
    //     }).on("filebatchselected", function (event, files) {
    //         el1.fileinput("upload");
    //     }).on("filebeforedelete", function (event, id, index) {
    //         return new Promise(function (resolve, reject) {
    //             bootbox.confirm({
    //                 title: "Подтверждение",
    //                 message: "Вы уверены что хотите удалить",
    //                 buttons: {
    //                     cancel: {
    //                         label: 'Отмена'
    //                     },
    //                     confirm: {
    //                         label: 'Подтвердить'
    //                     }
    //                 },
    //                 callback: function (result) {
    //                     if (result) {
    //                         resolve();
    //                     }
    //                 }
    //             })
    //         });
    //     });
    // })
});