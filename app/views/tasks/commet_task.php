<div class="row comments-list hidden" xmlns:v-on="http://www.w3.org/1999/xhtml">
    <div class="col-md-12">
        <ul class="nav nav-tabs">
            <li class="">
                <a href="#task-comments-all" data-toggle="tab"
                   aria-expanded="true">
                    <span class="visible-xs"><i class="fa fa-home"></i></span>
                    <span class="hidden-xs">Все</span>
                </a>
            </li>
            <li class="active">
                <a href="#task-comment" data-toggle="tab" aria-expanded="false">
                    <span class="visible-xs"><i class="fa fa-home"></i></span>
                    <span class="hidden-xs">Комментарии</span>
                </a>
            </li>
            <li class="">
                <a href="#task-events" data-toggle="tab" aria-expanded="false">
                    <span class="visible-xs"><i class="fa fa-user"></i></span>
                    <span class="hidden-xs">События</span>
                </a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active " id="task-comment">
                <ul class="media-list">
                    <button-counter v-on:deletetask="deletask"
                                    v-for="(comment,key) in comments"
                                    v-bind:key=key
                                    v-bind:connect=key
                                    v-bind:comm="comment">
                    </button-counter>
                </ul>
                <hr>
                <div class="row">
                    <div class="col-md-10">
                        <input id="btn-input" type="text" class="form-control" v-model="textinp" autocomplete="off"
                               placeholder="Введите текст..."/>
                    </div>
                    <div class="col-md-2">
                        <button type="button" v-on:click="addtask()" class="btn btn-primary" id="btn-chat">
                            Отправить
                        </button>
                    </div>
                </div>
            </div>
            <div class="tab-pane" id="task-events" style="overflow-y: auto;max-height: 200px;">
                <ul class="media-list">
                    <button-counter v-for="(comment,key) in events"
                                    v-bind:key=key
                                    v-bind:connect=key
                                    v-bind:comm="comment">
                    </button-counter>
                </ul>
            </div>
            <div class="tab-pane" id="task-comments-all">
                <p>Временно недоступно
                </p>
            </div>
        </div>
    </div>
</div>
<script src="/node_modules/vue/dist/vue.min.js"></script>
<script src="/node_modules/axios/dist/axios.min.js"></script>
<script src="/node_modules/vue-cookies/vue-cookies.js"></script>
<script src="/app/views/tasks/js/comment_task.js"></script>