<?php
include_once $_SERVER['DOCUMENT_ROOT'] . '/assets/functions.php';


$users = user_in_platforms;
$db = Db::Connection();

/// работа с index
if (isset($_REQUEST['action']) and $_REQUEST['action'] == "task_all") {
    echo json_encode(task_all());
}
function task_all()
{
    $username = getUserNameInPlatform();
    // $platform = GetAllPlatforms();
    $db = DB::Connection();
    $result = $db->query("
SELECT  t.*,p.name AS plname,t.typen as type_id,p.id as plat_id,t.deadline,t.date
,(SELECT t2.type FROM tasks t2 WHERE t2.id = t.pid LIMIT 1 )AS type_parent,(SELECT COUNT(a.id) FROM attachments a WHERE a.t_id = t.id LIMIT 1 )AS file
FROM `tasks` t JOIN platform p on p.id=t.platform WHERE
    (t.`responsible` ={$_COOKIE['id']} OR t.`executors`={$_COOKIE['id']} OR t.`director`={$_COOKIE['id']}) and t.archive IS NULL and p.id IN (" . $_COOKIE['platform'] . ")");
    $arr = ["data" => []];
    while ($row = $result->fetch_assoc()) {

        if ($row['pid'] != 0) {
            $number_text = mb_substr($row['type_parent'], 0, 1, "UTF-8") . numberFormat($row['pid'], 4) . "_" . numberFormat($row['id'], 4);
        } else {
            $number_text = mb_substr($row['type'], 0, 1, "UTF-8") . numberFormat($row['id'], 4);
        }

        $arr['data'][] = [
            "id" => $row['id'],
            "number_text" => $number_text,
            "name" => $row['name'],
            "type" => type_tasks_new[$row['type_id']],
            "type_id" => $row['type_id'],
            "status" => $row['status'],
            "date" => date("d.m.Y H:i:s", strtotime($row['date'])),//$row['date'],//DATE_FORMAT(t.date,'%d.%m.%Y %H:%I:%S')
            "responsible" => $username[$row['responsible']],
            "platform" => $row['plname'],
            "platform_id" => $row['plat_id'],
            "director" => $username[$row['director']],
            "pkikd" => $row['nreport'],// Создан отчет пкипд
            "deadline" => $row['deadline'] == 0 ? "" : date("d.m.Y", strtotime($row['deadline'])),
            "priority" => $row['priority'],
            "authority" => $row['authority'],
            "file" => $row['file']
        ];
    }
    return $arr;
}


/*Создание таска*/
if (isset($_REQUEST['create'])) {

    echo create_task($_REQUEST['create']);
}
/*Создать подзадачу*/
if (isset($_REQUEST['subcreate'])) {
    create_task(false, $_REQUEST['subcreate']);
}

function create_task($type = false, $subtask = false) //nreportt
{
    $db = DB::Connection();
    $query = '';

    if (gettype($subtask) == "array" and $subtask) {

        foreach ($subtask as $key => $value) {
            if ($value != "") {

                $key = preg_replace('/\w+_/', "", $key);
                if ($key == "deadline" or $key == "startnoty") {
                    $value = date("Y.m.d", strtotime($value));
                }

                if ($key == "executors") {
                    $key = 'responsible';
                }

                $query .= $key . "='" . htmlspecialchars(trim($value)) . "',";
            }
        }
        $query .= "typen='1',type='2'";

        /*$query=trim($query,',');*/
        $result = $db->query("INSERT INTO tasks set {$query}");

        // var_dump($query);
    } elseif (gettype($type) == "array" and $type) {
        foreach ($type as $key => $value) {
            $key = preg_replace('/\w+_/', "", $key);
            if ($key === "deadline" or $key === "startnoty") {
                $value = date("Y.m.d", strtotime($value));
            }
            if ($key === "nreport") {
                $value = trim($value, ',');
            }
            if ($key == "type") {
                $query .= "typen ='" . $value . "',";
            }
//            if ($key == "quick") {
//                $query .= "quick ='" . $value?'1':'0' . "',";
//            }

            $query .= $key . "='" . htmlspecialchars(trim($value)) . "',";
        }
        $query .= "director='{$_COOKIE['id']}'";
        $result = $db->query("INSERT INTO tasks set {$query}");
    } else {
        return false;
    };
    if ($result != false) {
        $data_id = $db->insert_id;
        sendmail_to_nTask($data_id);
        return ($data_id == 0) ? http_response_code(404) : $data_id;
    } else {
        return http_response_code(404);
    }
}


/* Изменить таск */
if (isset($_REQUEST['edit'])) {

    $query = "";
    $where = "";
    $data = [];
    foreach ($_REQUEST['edit'] as $key => $value) {
        if ($value == '')
            $key = preg_replace('/\w+_/', "", $key);
        if ($key == "name" or $key == "description" or $key == "authority") {
            $value = '"' . htmlspecialchars(trim($value)) . '"';
        }
        if ($key == "deadline" or $key == "startnoty" or $key == "repeat" or $key == "repeattask") {
            if ($value != '') {
                $value = '"' . date("Y.m.d", strtotime($value)) . '"';
            }
        }
        if ($key == "director" or $key == "executors" or $key == "subexecutors" or $key == "job") {
            $value = '"' . $value . '"';
        }
        if ($key != "id" and $key != "action") {
            $query .= $key . "=" . $value . ",";
        }
        if ($key == "id") {
            $where .= " where id=" . $value;
        }
    }
    $query = trim($query, ',') . $where;
    $result = $db->query("update tasks set {$query}");
    if ($db->affected_rows == 0) {
        return http_response_code(404);
    } else {
        return http_response_code(200);
    };
}


////////////////Работа Саб тасками
if (isset($_POST['action']) and $_POST['action'] == "edit_subtask") {
    $id = intval($_POST['id']);
    $pid = intval($_POST['pid']);

    $job = subtask_plans($pid);

    $jobvid = plans_text($job);
    $data = [
        "field" => task_fields($id),
        "vid" => vid_for_task($pid, $id)
    ];

    echo json_encode($data);
};
/*Принять задачу */
if (isset($_POST['action']) and $_POST['action'] == "accepted") {
    $id = intval($_POST['id']);
    $b_action = intval($_POST['b_action']);
    $status_old = intval($_POST['old_action']);
    $query = "update tasks set status={$b_action} where id={$id}";
    $result = $db->query($query);
    if ($db->affected_rows == 0) {
        //echo json_encode($query, JSON_UNESCAPED_UNICODE);
        return http_response_code(404);
    } else {
        sendmail_to_status($id, $status_old);
        return http_response_code(200);
    };

};

//Обновить статус во всех задачах статус закрыт nreportt
if (isset($_REQUEST['action']) and $_REQUEST['action'] == "status") {
    $nreport = trim($_POST['nreport'], ",");
    $result = $db->query("update reports set status='1' where id in ({$nreport})");
    if ($db->affected_rows == 0) {
        $status['message'] = "Произошла ошибка";
        echo json_encode($status, JSON_UNESCAPED_UNICODE);
        return http_response_code(404);
    } else {
        $status['message'] = "Отчет закрыт успешно";
        echo json_encode($status, JSON_UNESCAPED_UNICODE);
        return http_response_code(200);
    };
}

/* удалить таск или саб таск */
if (isset($_POST['action']) and $_POST['action'] == "delete_subtask") {
    delete_task($_POST['id']);
}
function delete_task($id, $type = false) //nreportt
{
    $db = DB::Connection();

    $sel = "SELECT id,nreport,job FROM tasks t WHERE t.id={$id} limit 1";

    $data = $db->query($sel)->fetch_object();

    if ($data->nreport != "0") {

        $query = "SELECT r.status FROM reports r WHERE r.bunch in({$data->nreport})"; //status
        $info_report = $db->query($query)->fetch_assoc();
//          нужно проверить статус открыт закрыт в работе

        if ($info_report['status'] == "0") {
            $query1 = "DELETE LOW_PRIORITY QUICK FROM collector WHERE `self-evaluation` IN(SELECT id FROM reports r WHERE r.job IN ({$data->job}) AND r.bunch ={$data->nreport});
                       DELETE LOW_PRIORITY QUICK FROM reports WHERE bunch IN({$data->nreport}) and job in ({$data->job});    
                       DELETE FROM tasks WHERE id={$data->id} limit 1";
            $res = $db->multi_query($query1);
        }
    } else {
        $query1 = "DELETE FROM tasks WHERE id={$data->id} limit 1";
        $res = $db->query($query1);
    }
    echo json_encode($info_report, JSON_UNESCAPED_UNICODE);
    if ($db->affected_rows == 0) {
        return http_response_code(404);
    } else {
        return http_response_code(200);
    };
}

//согласование boxboot_ajax
if (isset($_REQUEST['agree'])) {
    $id = intval($_REQUEST['agree']);
    $data = $db->query("select agreement from tasks where id={$id}")->fetch_assoc();
    if (!is_null($data["agreement"])) {
        $obj = json_decode($data["agreement"], true);
    } else {
        $obj = [];
    }
    $getdate = json_decode($_REQUEST['data'], true);
    if ($getdate['status'] == 1) {
        sendmail_not_agreement($id, $getdate['text_comment'], true);
    } else {
        sendmail_not_agreement($id, $getdate['text_comment'], false);
    }

    array_push($obj, json_decode($_REQUEST['data'], true));
    $data_end = json_encode($obj, JSON_UNESCAPED_UNICODE);

    $query = "update tasks set agreement='{$data_end}' where id={$id}";
    $result = $db->query($query);

    if ($result) {
        return http_response_code(200);
    } else {
        return http_response_code(404);
    };
}
//согласование вывод таблицы
if (isset($_REQUEST['routetask'])) {

    $result = $db->query("select agreement from tasks WHERE id={$_REQUEST['id']} limit 1");
    $data1 = $result->fetch_assoc();
    if ($data1['agreement'] == "null" or is_null($data1['agreement'])) {
        $t = ["data" => ["date" => '1']];
    } else {
        $all = json_decode($data1['agreement']);

        foreach ($all as $value) {
            $tt[] = [
                "date" => $value->date,
                "name" => user_in_platforms[$value->idname],
                "status" => $value->status == 1 ? "Согласованно" : "Не согласованно",
                "text_comment" => $value->text_comment
            ];

        }
        $t = ["data" => $tt];
    };


    echo json_encode($t);
}


////////////////////////*комментария*/

if (isset($_REQUEST['sub_task_comment'])) {

    $id_comment = 0;
    $flag = false;
    $params = json_decode(file_get_contents('php://input'));

    $data_comments = fill_task_comments($params->id_task);
    //var_dump($data_comments["comments"]=='');
    if (isset($params->textinp)) {
        $type = $params->type;
        $arr = ["id" => $id_comment,
            "id_task" => $params->id_task,
            "id_autor" => $_COOKIE['id'],
            "id_whom" => $params->id_whom,
            "fio" => user_in_platforms[$_COOKIE['id']],
            "answer" => "",
            "type" => $type,
            "date" => strtotime("now"),
            "text" => $params->textinp
        ];


        if (is_null($data_comments["comments"]) or $data_comments["comments"] == "") {
            $obj = ['comments' => [], 'events' => []];
            //array_push($obj->$type, $arr);
            $obj[$type] = [$arr];
        } else {
            $obj = json_decode($data_comments["comments"], true);
            if (count($obj[$type]) == 0) {
                $obj[$type] = [$arr];
            } else {
                $max_key = count($obj[$type]);
                $arr['id'] = $max_key + 1;
                $obj[$type][$max_key + 1] = $arr;
            }
        }
        $obj = json_encode($obj, JSON_UNESCAPED_UNICODE);
        $que = "UPDATE tasks SET comments ='{$obj}' WHERE id={$params->id_task}";
        $result = $db->query($que);
        if ($db->affected_rows > 0) {
            echo json_encode($arr, JSON_UNESCAPED_UNICODE);
        } else {
            http_response_code(404);
        }
    } else {
        echo $data_comments["comments"];
    };

}

if (isset($_REQUEST['sub_task_comment_del'])) {
    $del = 1;

    $params = json_decode(file_get_contents('php://input'));
    $data_comments = fill_task_comments($params->id_task);
    $obj = json_decode($data_comments['comments'], true);
    unset($obj['comments'][$params->id_del]);
    $obj = json_encode($obj, JSON_UNESCAPED_UNICODE);
    $que = "UPDATE tasks SET comments ='{$obj}' WHERE id={$params->id_task}";
    $result = $db->query($que);
    if ($db->affected_rows > 0) {
        http_response_code(200);
    } else {
        http_response_code(404);
    }

}

function fill_task_comments($id)
{
    $db = Db::Connection();
    return $db->query("SELECT `comments`
		        FROM `tasks`
		        WHERE id={$id} LIMIT 1")->fetch_assoc();
}

////////////////////////////////ajax_send ////////
if (isset($_REQUEST['action']) and $_REQUEST['action'] == "task_date") {

    $id = intval($_REQUEST['id']);
    $name_input = preg_replace('/\w+_/', "", htmlspecialchars($_REQUEST['name_input']));
    if ($name_input != "name" and $name_input != "responsible" and $name_input != "executors" and $name_input != "description") {
        $date = date("Y.m.d", strtotime($_REQUEST['date']));
    } else {
        $date = htmlspecialchars($_REQUEST['date']);
    }
    if ($date == "") {
        $date = 0;
    };
    $Q = "update tasks set {$name_input}='{$date}' where id ={$id}";

    $db->query($Q);
    if ($db->affected_rows == 0) {
        return http_response_code(404);
    } else {
        sendmail_to_respons($id);
        return http_response_code(200);
    };
}

/*заполнение таблицы саб таска*/
if (isset($_REQUEST['datatable'])) {
    echo json_encode(subtask_all($_GET['id']));
}
function subtask_all($pid)
{
    $username = getUserNameInPlatform();
    $platform = GetAllPlatforms();
    $db = DB::Connection();//здесь ошибка nreportt перечисление
    $result = $db->query("SELECT t.id,t.name,(t.status) as status,t.platform as platf,t.executors,DATE_FORMAT(t.deadline,'%d.%m.%Y') as deadline,nreport,job,
(SELECT COUNT(c.id) FROM collector c WHERE c.`self-evaluation` IN (SELECT id FROM reports rt WHERE rt.bunch=t.nreport AND rt.job REGEXP REPLACE(CONCAT ('^',t.job,'$'),',','|')) ) AS max_k,
(SELECT COUNT(c.id) FROM collector c WHERE c.`self-evaluation` IN (SELECT id FROM reports rt WHERE rt.bunch=t.nreport AND rt.job REGEXP REPLACE(CONCAT ('^',t.job,'$'),',','|')) AND c.`status-compliance`>0) AS min_k,responsible
 FROM tasks t WHERE t.pid={$pid}");
    $progress = "";
    $arr = ["data" => []];
    while ($row = $result->fetch_assoc()) {
        if ($row['nreport'] != "0") {
            $progress = $row['min_k'] != $row['max_k'] ? $row['min_k'] . "/" . $row['max_k'] : "Заполнен";
        } else {
            $progress = "";
        }
        $arr['data'][] = [
            "id" => $row['id'],
            "name" => $row['name'],
            "status" => $row['status'],
            "date" => $row['deadline'],
            "agreement" => "Согласовать",
            "progress" => $progress,
            "responsible" => $username[$row['responsible']],
            "platform" => $platform[$row['platf']].' / '.getJobById($row['job']),
            "platform_id" => $row['platf'],
            "nreport" => $row['nreport'] == "0" ? 0 : $row['nreport'],
            "position2" => "",
        ];
    }
    return $arr;
}


/*вид работ мину другие задач*/
if (isset($_REQUEST['action']) and $_REQUEST['action'] == "subtask_vid") {
    // echo json_encode(plans_text(subtask_plans($_REQUEST['pid'])));
    echo json_encode(subtask_plans($_REQUEST['pid']));
}

if (isset($_REQUEST['action']) and $_REQUEST['action'] == "subtask_vid_only") {
    $id_taks = explode(',', $_REQUEST['nreport']); //nreportt
    $pid = intval($_REQUEST['pid']);
    echo json_encode(plans_diff($id_taks, $pid));
}
if (isset($_REQUEST['action']) and $_REQUEST['action'] == "vid_fast_self") {
    $platform = intval($_REQUEST['platform']);
    echo json_encode(vid_fast_self($platform));
}

/*Создать самооценку или аудит*/ //здесь можно любой отчет  поправить тип
if (isset($_REQUEST['action']) and $_REQUEST['action'] == "create_report") {
    $job_vid = [];
    $platform_vid = 0;
    $platf = 0;
    $protbunch = 0;
    $flag = false;
    $job_old = [];
    $type = $_REQUEST['type'];
    $typen = $_REQUEST['typen'];
    $pid = intval($_REQUEST['pid']);
    $quick = $_REQUEST['quick'];
    $query = "SELECT  t.id,t.platform, t.responsible  as executors ,t.nreport, t.job FROM tasks t
 WHERE t.pid={$pid} and t.nreport='0' ORDER BY t.platform ASC";
    $result = $db->query($query);
    //var_dump $query;

    if ($result->num_rows > 0) {

        while ($uRow = $result->fetch_assoc()) {

            if ($platf != $uRow['platform']) {//проверка сушествует ли отчет на площадке
                $platf = intval($uRow['platform']);

//                $platform = $db->query("SELECT id,bunch FROM `reports`
//		                             WHERE `platform` = " . $platf . " AND
//		                             `type` = '" . $type . "' AND
//									 `status` = 0 LIMIT 1
//		                             ");// status 0 открытый отчет
                $platform = $db->query("SELECT id,bunch,GROUP_CONCAT(job) as job FROM `reports` WHERE `platform` = " . $platf . " AND `typen` = '" . $typen . "' AND `status` = 0 GROUP BY bunch");

                if ($platform->num_rows > 0) {
                    $pbunch = $platform->fetch_assoc();
                    $protbunch = $pbunch['bunch'];
                    $job_old = explode(',', $pbunch['job']);

                    $status[] = "На площадке № " . platforms_user[$platf] . " уже открыт тип " . type_tasks_new[$typen] . " № " . $protbunch;
//                    print_r(json_encode($status, JSON_UNESCAPED_UNICODE));
//                    return;
                    //$flag = true; ///выход
                } else {
                    $protbunch = 0;
                    // $flag = false;
                }
            }


            foreach (explode(",", $uRow['job']) as $value) {
                if (in_array($value, $job_old)) {
                    $status[] = "На площадке № " . platforms_user[$platf] . "уже присутствует вид работ = " . $value;

                } else {
                    $job_vid[] = [
                        "job" => $value,
                        "executors" => $uRow['executors'],
                        "id" => $uRow['id'],
                        "nreport" => $uRow['nreport'],
                        "platform" => $uRow['platform'],
                        "bunch" => $protbunch //если отчет открыт
                    ];
                }

            };


        };

    } else {
        $status['type'] = 'error';
        $status['message'] = 'Произошла ошибка сбор инфы';
        print_r(json_encode($status, JSON_UNESCAPED_UNICODE));
        return;
    }

    /*//   $platform_id = intval($_REQUEST['platform']);


//     Проверяем есть уже ли по данной платформе открытый отчет
        /*    $platform = $db->query("SELECT id FROM `reports`
                                             WHERE `platform` = " . $platform_id . " AND
                                             `type` = '" . $type . "' AND
                                             `status` = 0 LIMIT 1
                                             ");
            if ($platform->num_rows > 0) {

                $status['type'] = 'error';
                $status['message'] = 'По данной платформе есть открытый отчет';
                $status['message2'] = 'Нельзя создавать несколько групп отчетов в одном отчетном периоде';
                print_r(json_encode($status, JSON_UNESCAPED_UNICODE));
                return;
            }
    // Получаем максимальный идентификатор группы отчетов и увеличиваем его на 1
        $res = $db->query("SELECT MAX(`bunch`) AS 'maxValue' FROM reports ");
        $row = $res->fetch_assoc();

        $reportGroupId = intval($row['maxValue']) + 1;

     Время создания отчета*/
    $now = strtotime("now");

    $user = $_COOKIE['id'];
    $actual = 1;

    $query = "INSERT INTO collector(
  ehs, job,job_child, demand, `event`, `paragraph-name`, `paragraph-link`, source, document, connected, privilege, criticality1, criticality2, criticality3, `demand-id`, `self-evaluation`, `date`, platform, `user`, actual, calendar)
SELECT d.ehs,
       d.job,d.job_child,
       d.demand,
       d.`event`,
       d.`paragraph-name`,
       d.`paragraph-link`,
       d.source,
       d.document,
       d.connected,
       d.privilege,
       d.criticality1,
       d.criticality2,
       d.criticality3,
       d.id,?,d.date,?,?,?,NULL FROM demands d 
       LEFT JOIN register r ON r.nd=d.document 
       WHERE d.job =? and r.company LIKE '%{$_COOKIE['company']}%' and r.status='Действующий' ";
    if ($quick == "true") {
        $query = "INSERT INTO collector(
  ehs, job,job_child, demand, `event`, `paragraph-name`, `paragraph-link`, source, document, connected, privilege, criticality1, criticality2, criticality3, `demand-id`, `self-evaluation`, `date`, platform, `user`, actual, calendar)
SELECT d.ehs,
       d.job,d.job_child,
       d.demand,
       d.`event`,
       d.`paragraph-name`,
       d.`paragraph-link`,
       d.source,
       d.document,
       d.connected,
       d.privilege,
       d.criticality1,
       d.criticality2,
       d.criticality3,
       d.id,?,d.date,?,?,?,NULL FROM demand_pl dp 
       LEFT JOIN demands d ON dp.demand = d.id 
       LEFT JOIN register r ON r.nd=d.document
       WHERE (JSON_EXTRACT(dp.settings,'$.quick'),'one',?)IS NOT NULL AND d.job=? and r.company LIKE '%{$_COOKIE['company']}%' and r.status_n=1";
    }

    $stmt = $db->prepare($query);

    if (!($stmt = $db->prepare($query))) {
        echo "Не удалось подготовить запрос: (" . $db->errno . ") " . $db->error;
    }
    if ($quick == "true") {
        $stmt->bind_param('iisiii', $self, $platform_id, $user, $actual, $platform_id, $job);
    } else {
        $stmt->bind_param('iisii', $self, $platform_id, $user, $actual, $job);
    }


// $data_first=$job_vid[0]["id"];
    $data = [];

    if (count($job_vid) != 0) {
        foreach ($job_vid as $value) {
            if ($value['bunch'] > 0) {

                if ($value['nreport'] == "0") { //к старому банчу

                    $status[] = "На площадке № " . $value['platform'] . " успешно добавлены вид работ " . GetAllvid[$value['job']];
                    $reportGroupId = $value['bunch'];
                    $platform_vid = $value['platform'];
                }///  а если нет

            } else {

                if ($platform_vid != $value['platform']) {

                    $status[] = "На площадке № " . $value['platform'] . " успешно создана тип " . type_tasks_new[$typen];

                    $platform_vid = $value['platform'];

                    $res = $db->query("SELECT MAX(`bunch`) AS 'maxValue' FROM reports ")->fetch_assoc();
                    $reportGroupId = intval($res['maxValue']) + 1;
                }

            }

            $q_report = "INSERT INTO `reports` SET
					                `bunch`='" . $reportGroupId . "',
					                `status`='0',
					                `responsible`='" . $value['executors'] . "',
					                `creator`='" . $user . "',
					                `platform`='" . $platform_vid . "',
					                `type`='" . $type . "',
					                `typen`='" . $typen . "',
					                `date` = '" . $now . "',
					                `job` ='" . $value['job'] . "'";
            $db->query($q_report);


            if ($db->insert_id != 0) {

                $self = $db->insert_id;
                $platform_id = $platform_vid;
//                $data[$value["id"]] .= $self . ",";
                $data[$value["id"]] = $reportGroupId;

                // $job_vid[$key]['nreport']=$self;
                $job = $value["job"];

                if (!$stmt->execute()) {
                    $status['type'] = 'error';
                    $status['message'] = 'Произошла ошибка';
                    $status['error'] = $stmt->error;
                    print_r(json_encode($status, JSON_UNESCAPED_UNICODE));
                    return;
                }
            } else {
                $status['type'] = 'error insert report';
                $status['message'] = 'Произошла ошибка ' . $db->insert_id;
                $status['error'] = $db->insert_id;
                print_r(json_encode($status, JSON_UNESCAPED_UNICODE));
                return;
            }
        }


        foreach ($data as $id => $value) {
            $value = trim($value, ",");
            $query = "UPDATE tasks t set t.nreport='" . $value . "' WHERE t.id={$id}";
            $res = $db->query($query);
            sendmail_to_self($id);
        }
    }
    echo json_encode($status, JSON_UNESCAPED_UNICODE);
}

?>