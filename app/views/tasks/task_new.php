<?php
include_once $_SERVER['DOCUMENT_ROOT'] . '/app/views/header.php';
include_once $_SERVER['DOCUMENT_ROOT'] . '/app/views/template.php';
include_once 'model/task.php';

if (isset($_GET['action'])) {
    if ($_GET['action'] == "edit") {
        $row = task_data($_GET['id']);
        if ($row == false) {
            ?>
            <script>document.location.href="/tasks"</script>

             <?
        }

        // var_dump(task_data($_GET['id']));
    };
}


$role = "";
$name = $_COOKIE['id'];
$users = user_in_platforms;

switch ($name) {
    case $row['director']:
        $role = "director";
        break;
    case $row['responsible']:
        $role = "responsible";
        break;
    case $row['executors']:
        $role = "executors";
        break;
    default:
        $role = "visitors";
        break;
}

//данные для связанных задач переделать


$link_task = task_fields_all(explode(',', $row['nreport']));

?>
<section class="content" id="app">
    <div class="wraper container-fluid">

        <form id="edit_task" novalidate method="POST">
            <input type="hidden" value="<?= $row['id'] ?>" name="task_id"/>
            <div class="panel panel-default w-100">
                <div class="">
                    <div class="row">
                        <!--навигация-->
                        <nav>
                            <ol class="breadcrumb m-b-10">
                                <li><a href="/tasks">Рабочий стол</a></li>
                                <?php if ($row['pid'] != 0) { ?>
                                    <li><a href="/tasks/task_new?action=edit&id=<?= $row['pid'] ?>
                                            "><?= type_tasks_new[$row['type_parent']] ?>
                                            № <?= $row['pid'] ?></a></li>
                                <? } ?>
                                <li class="active"><?= type_tasks_new[$row['typen']] ?>
                                    № <?= $row['id'] ?></li>
                            </ol>
                        </nav>
                    </div>
                    <div class="row">
                        <div class="col-md-9">
                            <input type="text" id="task_name" name="task_name" class="form-control"
                                   value="<?= $row['name'] ?>" placeholder="Наименование задачи">
                        </div>
                        <!-- блок кнопок-->
                        <div class="col-md-6">
                            <div class="btn-group">
                                <?php if ($row['status']=="1" and $role!="visitors"){ ?>
                                    <div class="but_start hidden ">
                                        <button type="button" data-id="4"
                                                class="btn btn-primary btn-sm button_action btn-rounded btn-custom">
                                            Принять в
                                            работу
                                        </button>
                                        <button type="button" data-id="5"
                                                class="btn  btn-sm button_action btn-danger btn-custom">
                                            Не
                                            принять
                                        </button>
                                    </div>
                                <? } ?>
                                <?php if ($row['status'] != "2" and $row['status'] != "5"and $role!="visitors"  ) { ?>
                                    <div class="but_end hidden">
                                        <button type="button" data-id="2"
                                                class="btn btn-success btn-sm button_action btn-rounded btn-custom">
                                            Выполнить
                                        </button>
                                        <button type="button" data-id="3"
                                                class="btn  btn-sm button_action btn-danger btn-custom">
                                            Не 
											выполнять
                                        </button>
                                    </div>
                                <? } ?>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="panel-body">
                    <div class="col-md-9 col-sm-9">
                        <div class="row">
                            <!------Детали задачи----------->
                            <div class="col-md-6">
                                <div class="task_header">
                                    <h5><a data-target="#bg-default" data-toggle="collapse">
                                            <strong>Детали задачи</strong></a>
                                    </h5>
                                </div>
                                <div class="form-horizontal">
                                    <div id="bg-default" class="collapse in">
                                        <div class="form-group">
                                            <label for="task_nabmer" class="col-md-4 control-label">№ Задачи:</label>
                                            <div class="col-md-8">
                                                <p class="form-control-static"><?= $row['pid'] != 0 ? numberFormat($row['pid'],4) . "_" : "" ?><?php echo mb_substr($row['name_type'], 0, 1, "UTF-8") . numberFormat($row['id'], 4) ?></p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="task_type" class="col-md-4 control-label">Тип задачи:</label>
                                            <div class="col-md-8">
                                                <select class="select2" name="task_type" id="task_type" disabled>
                                                    <?
                                                    foreach (type_tasks_new as $key => $value) { ?>
                                                        <option value="<?= $key ?>" <?= $key == $row['typen'] ? "selected" : "" ?>><?= $value ?></option>
                                                    <? } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="task_status" class="col-md-4 control-label">Статус:</label>
                                            <div class="col-md-8">
                                                <select class="select2" name="task_status" id="task_status" disabled>
                                                    <?
                                                    foreach (type_status as $key => $value) { ?>
                                                        <option value="<?= $key ?>" <?= $key == $row['status'] ? "selected" : "" ?>><?= $value ?></option>
                                                    <? } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="task_platform" class="col-md-4 control-label">Площадка:</label>
                                            <div class="col-md-8">
                                                <select id="task_platform" name="task_platform" required class="select2"
                                                        disabled
                                                        data-placeholder="Выберите площадку...">
                                                    <?
                                                    foreach (platforms_user as $key => $value) { ?>
                                                        <option value="<?= $key ?>" <?= $key == $row['platform'] ? "selected" : "" ?>><?= $value ?></option>
                                                    <? } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <?php if ($role == "executors") { ?>
                                            <div class="form-group">
                                                <label for="task_department" class="col-md-4 control-label">Цех:</label>
                                                <div class="col-md-8">
                                                    <select name="task_department" id="task_department" required
                                                            class="select2"
                                                            disabled>
                                                        <?
                                                        foreach (platforms_user as $key => $value) { ?>
                                                            <option value="<?= $key ?>" <?= $key == $row['platform'] ? "selected" : "" ?>><?= $value ?></option>
                                                        <? } ?>
                                                    </select>
                                                </div>
                                            </div>
                                        <? } ?>


                                        <?php if (($role == "responsible" or $role == "director") and $row['job'] != "" and $row['job'] != null and ($row['typen'] != 0)) { ?>
                                            <div class="form-group">
                                                <label for="task_priority" class="col-md-4 control-label">Виды
                                                    работ:</label>
                                                <div class="col-md-8">
                                                    <?php $vid = GetAllvid; ?>
                                                    <select name="task_job" id="task_job" required class="select2"
                                                            multiple disabled>
                                                        <?php
                                                        foreach (explode(',', $row['job']) as $key => $value) { ?>
                                                            <option value="<?= $value ?>"
                                                                    selected><?= $vid[$value] ?></option>
                                                        <? } ?>
                                                    </select>
                                                </div>
                                            </div>
                                        <? } ?>

                                    <?php
                                    if ($row['typen'] == '1' and ($row['status'] == "4" or $role=="director")) {$classbutton='';} else {$classbutton='hidden';} 
                                    if ($row['nreport'] != 0) {?>
                                        
                                            <div id="agreement_task_report" class="btn-group <?= $classbutton ?>"><a class="pull-right"
                                                <?php if ($row['type_parent'] == '4'){  ?>
                                                    href="/reports/matrix?type=calendar&vid=<?= $row['nreport']?>&type_job=<?= $row['job'] ?>"
                                                <? }else{ ?>
                                                    href="/reports/matrix?type=self-evaluation&task_one=<?= $row['id']?>"
                                               <? } ?>
                                                target=" _blank">
                                            <button type="button" class="btn btn-success">Перейти к отчету</button>
                                            </a></div>
                                            <div id="agreement_task_xls" class="btn-group <?= $classbutton ?>">
                                                <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                 Скачать <span class="caret"></span>
                                                </button>
                                                <ul class="dropdown-menu" role="menu">
                                                    <li>
                                                        <a href="#">В формате Excel</a>
                                                    </li>
                                                    <li>
                                                        <a href="#">По форме заказчика</a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <a id="agreement_task_dl" href="#" class="btn btn-danger <?= $classbutton ?>" role="button">Загрузить отчет</a>
                                        <? } else if ( $row['name_type'] == 'Подзадача') { ?>
                                            <span id="agreement_task_report" class="<?= $classbutton ?>"><span class="label label-danger">
                                             Отчет не создан</span></span>
                                        <? } ?>   
                                    </div>
                                
                                </div>
                            </div>
                            <!------Согласование-------->
                            <div class="col-md-6">
                                <div class="task_header">
                                    <h5><a data-target="#route_of_agreement_task"
                                           data-toggle="collapse"><strong>Согласование</strong></a>
                                    </h5>
                                </div>
                                <div id="route_of_agreement_task" class="collapse in">
                                    <?php if ($row['typen'] != 0 ) { ?>
                                    <div class="agree hidden">
                                        <table id="route-task" class="table table-hover table-striped"></table>
                                        <div class="pull-right add_agreement m-t-5">
                                            <?php
                                            if (($row['typen'] != 1 and ($role == "responsible" or $role == "director")) or ($row['typen'] == 1 and $role == "director")){ ?>
                                                <button type="button"
                                                        class="btn btn-sm btn-success btn-rounded btn-custom"
                                                        id="agree_act">
                                                    Согласовать
                                                </button>
                                                <button type="button" class="btn btn-sm btn-danger btn-custom"
                                                        id="agree_diact">
                                                    Не согласовать
                                                </button>
                                            <? } ?>
                                        </div>
                                    </div>
                                    <?php }?>
                                </div>

                            </div>
                    </div>
                        <!------Описание задачи----------->

                        <?php if ($row['typen'] == 4) { ?>
                            <div class="task_header">
                                <h5><a data-target="#related_task" data-toggle="collapse"><strong>Связанные задачи:
                                        </strong></a>
                                </h5>
                            </div>
                            <div id="related_task" class=" collapse in">
                                <input id="task_nreport" type="hidden" value="<?= $row['nreport'] ?>"/>
                                <p>ПКиПД создана по задачам:</p>
                                <ul>
                                    <?php
                                    foreach ($link_task as $key => $value) {
                                        ?>
                                        <li>
                                            <a href="/tasks/task_new?action=edit&id=<?= $value['id'] ?>">Дата: <?= $value['date'] ?>
                                                № <?= $value['id'] ?> Тема: <?= $value['name'] ?>
                                                Тип: <?= $value['type'] ?> </a></li>
                                    <? } ?>
                                </ul>
                            </div>
                        <? } ?>
                        <div class="task_header">
                            <h5><a data-target="#bg-textarea" data-toggle="collapse"><strong>Описание
                                        задачи:</strong></a>
                                <ul class="list-inline" style="float:right">
                                    <li><a href="#" class="add_texttask hidden"
                                          <span class="glyphicon glyphicon-pencil"></span></a>
                                    </li>
                                </ul>
                            </h5>
                        </div>
                        <!--текст задачи-->
                        <div id="bg-textarea" class=" collapse in">
                            <div class="text_zadacha">
                                <?= htmlspecialchars_decode($row['description']) ?>
                            </div>
                        </div>
                        <!------ Вложение ----------->
                        <div class="task_header">
                            <h5><a data-target="#bg-file" data-toggle="collapse"><strong>Вложенные
                                        файлы:</strong></a>
                            </h5>
                        </div>
                        <div id="bg-file" class="collapse in">
                            <form enctype="multipart/form-data">
                                <div class="task_file_loading">
                                    <input id="input-ru" name="fileToUpload[]" type="file" multiple>
                                </div>
                            </form>
                        </div>
                        <!------Подзадачи----------->
                        <?php if ($role != 'executors' and $row['typen'] != '1') { ?>
                            <div class="task_header">
                                <h5><a data-target="#subtask"
                                       data-toggle="collapse"><strong>Подзадачи:</strong></a>
                                    <ul class="list-inline" style="float:right">
                                        <li><a href="#" class="add_subtask hidden">
                                                <span class="glyphicon glyphicon-plus"></span>
                                            </a></li>
                                    </ul>
                                </h5>
                            </div>
                            <div id="subtask" class="collapse in">
                                    <?
                                    foreach (type_list as $key => $value) { ?>
                                        <div class="checkbox-inline">
                                            <label class="cr-styled">
                                                <input type="checkbox" name=<?= $key ?> <?=$row['quick']?"checked":"" ?> disabled>
                                                <i class="fa"></i><?= $value ?></label>
                                        </div>
                                    <? } ?>
                                <table id="datatable-task" class="table table-hover table-striped">
                                </table>
                            </div>
                        <? } ?>
                        <!------Комментарии Актвность----------->
                        <div class="task_header">
                            <h5><a data-target="#Comment_task"
                                   data-toggle="collapse"><strong>Активность</strong></a>
                            </h5>
                        </div>

                        <div id="Comment_task" class="collapse in">
                            <?php include $_SERVER['DOCUMENT_ROOT'] . "/app/views/tasks/commet_task.php" ?>
                        </div>
                    </div>
                    <!------Дата----------->
                    <div class="col-md-3">
                        <div class="task_header">
                            <h5><a data-target="#date-task"
                                   data-toggle="collapse"><strong>Дата:</strong></a>
                            </h5>
                        </div>
                        <div id="date-task" class="collapse in">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label for="task_create_date" class="control-label col-md-3">Создано:</label>
                                    <div class="col-md-9">
                                        <input id="task_create_date" type="text" class="form-control" required
                                               autocomplete="off"
                                               value="<?= $row['date'] == 0 ? "" : $row['date'] ?>" disabled>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="task_deadline" class="control-label ">Срок исполнения:</label>

                                <input id="task_deadline" type="text" class="form-control datepicker" required
                                       autocomplete="off"
                                       name="task_deadline"
                                       value="<?= $row['deadline'] == 0 ? "" : $row['deadline'] ?>" disabled>

                            </div>

                            <div class="form-group">
                                <label for="task_startnoty" class="control-label">Напоминать начиная с:</label>

                                <input type="text" class="form-control datepicker" required autocomplete="off"
                                       name="task_startnoty" id="task_startnoty"
                                       value="<?= $row['startnoty'] == 0 ? "" : $row['startnoty'] ?>" disabled>
                                <div>
                                    <a class="m-r-10 " href="javascript:void(0);"
                                       onclick="setFromDate('7','task_startnoty','day',true,'task_deadline')">
                                        <small>за неделю</small>
                                    </a>
                                    <a class="m-r-10" href="javascript:void(0);"
                                       onclick="setFromDate('14','task_startnoty','day',true,'task_deadline')">
                                        <small>за 2 недели</small>
                                    </a>
                                    <a class="m-r-10" href="javascript:void(0);"
                                       onclick="setFromDate('1','task_startnoty','mounth',true,'task_deadline')">
                                        <small>за 1 месяц</small>
                                    </a>
                                    <a class="" href="javascript:void(0);"
                                       onclick="setFromDate('3','task_startnoty','mounth',true,'task_deadline')">
                                        <small>за 3 месяца</small>
                                    </a>
                                </div>

                            </div>
                            <?php if ($role != "executors" and $row['typen'] != '1' and $row['typen'] != '4') { ?>
                                <div class="form-group">
                                    <label for="task_repeattask" class="control-label">Повтор задачи:</label>
                                    <input type="text" class="form-control datepicker" required autocomplete="off"
                                           name="task_repeattask" id="task_repeattask"
                                           value="<?= $row['repeattask'] == 0 ? "" : $row['repeattask'] ?>"
                                           disabled>
                                    <div>
                                        <a class="m-r-10" href="javascript:void(0);"
                                           onclick="setFromDate('1','task_repeattask','mounth',false,'task_deadline')">
                                            <small>через 1 месяц</small>
                                        </a>
                                        <a class="m-r-10" href="javascript:void(0);"
                                           onclick="setFromDate('3','task_repeattask','mounth',false,'task_deadline')">
                                            <small>через 3 месяц</small>
                                        </a>
                                        <a class="m-r-10" href="javascript:void(0);"
                                           onclick="setFromDate('6','task_repeattask','mounth',false,'task_deadline')">
                                            <small>через 6 месяцев</small>
                                        </a>
                                        <a class="" href="javascript:void(0);"
                                           onclick="setFromDate('1','task_repeattask','year',false,'task_deadline')">
                                            <small>через 1 год</small>
                                        </a>
                                    </div>

                                </div>
                            <? } ?>
                        </div>
                        <!------Люди----------->
                        <div class="task_header">
                            <h5><a data-target="#people-task"
                                   data-toggle="collapse"><strong>Люди:</strong></a>
                            </h5>
                        </div>
                        <div id="people-task" class="collapse in">
                            <div class="form-group">
                                <label for="task_director">Автор: </label>
                                <input type="hidden" name="task_director" value="<?= $_COOKIE['id']; ?>"
                                       disabled/>
                                <p id="task_director"> <?= getUserid($row['director']); ?></p>
                            </div>
                            <div class="form-group">
                                <label for="task_responsible" class="control-label">Ответственный:</label>
                                <select id="task_responsible" name="task_responsible" required class="select2"
                                        disabled>
                                    <option value="">Выберите ответственного</option>
                                    <?
                                    foreach ($users as $key => $value) { ?>
                                        <option value="<?= $key ?>" <?= $key == $row['responsible'] ? "selected" : "" ?> ><?= $value ?></option>
                                    <? } ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="task_executors" class="control-label">Исполнитель:</label>

                                <select id="task_executors" name="task_executors" required class="select2" disabled
                                        data-placeholder="Выберите ответственного...">
                                    <option value="">Выберите ответственного</option>
                                    <?
                                    $executors = explode(",", $row['executors']);
                                    foreach ($users as $key => $value) { ?>
                                        <option value="<?= $key ?>" <?= in_array($key, $executors) ? "selected" : "" ?> ><?= $value ?></option>
                                    <? } ?>
                                </select>
                            </div>
                            <?php
                            if ($row['typen'] == '1') { ?>
                                <div class="form-group">
                                    <label for="task_subexecutors" class="control-label">Соисполнитель:</label>
                                    <select id="task_subexecutors" name="task_subexecutors" required class="select2"
                                            disabled multiple
                                            data-placeholder="Выберите ответственного...">
                                        <option value="">Выберите ответственного</option>
                                        <?
                                        $subexecutors = explode(",", $row['subexecutors']);
                                        foreach ($users as $key => $value) { ?>
                                            <option value="<?= $key ?>" <?= in_array($key, $subexecutors) ? "selected" : "" ?> ><?= $value ?></option>
                                        <? } ?>
                                    </select>
                                </div>
                            <? } ?>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="text-right">
                        <input type="button"
                            <?php if ($row['pid'] != 0) { ?>
                                onclick="window.location.href='/tasks/task_new?action=edit&id=<?= $row['pid'] ?>'"
                            <? } else { ?>
                                onclick="window.location.href='/tasks'"
                            <? } ?>
                               class="btn btn-primary"
                               value="Готово"/>
                    </div>
                </div>
        </form>
    </div>
</section>
<link href="/app/views/tasks/css/task.css" rel="stylesheet">
<!--<script src="../../../assets/admina/js/vue.js"></script>-->
<script src="/app/views/tasks/js/task.js"></script>
<script src="/app/views/tasks/js/t_upload_files.js"></script>

<script>
    var typen = "<?php echo $row['typen']?>";

    var type_parent = "<?php echo $row['type_parent']?>";

    var role = "<?php echo $role ?>";
 var data = "<?php echo $row['nreport']?>";
//console.log(data)
    var status = "<?php echo $row['status'] ?>";


    var idcockie = "<?php echo $_COOKIE['id'] ?>";
    var org_cockie = "<?php echo $_COOKIE['company'] ?>";

    var idcockie_name = "<?php echo user_in_platforms[$_COOKIE['id']]?>";
    var name_l =<?php echo json_encode(user_in_platforms) ?>;
    var data_recurs =<?php echo json_encode(recursion(true))?>;
    var all_job =<?php echo json_encode(GetAllvid)?>;

    console.log(role);
    console.log('typen ='+typen);

    console.log('status ='+status);
    console.log('type_parent ='+type_parent);

</script>