<?php

function task_data($id)
{
    $db = DB::Connection();
    $result = $db->query("select t.id,t.pid,t.name,platform,(t.status+0)as status,priority,responsible,director,responsible,executors,subexecutors,typen,
t.description,agreement,job,DATE_FORMAT(t.deadline,'%d.%m.%Y') as deadline,quick,
DATE_FORMAT(t.date,'%d.%m.%Y') as date,
DATE_FORMAT(t.startnoty,'%d.%m.%Y') as startnoty,
DATE_FORMAT(t.finished,'%d.%m.%Y') as finished,agreement,
DATE_FORMAT(t.repeattask,'%d.%m.%Y') as repeattask,type as name_type,(t.type+0)as type ,nreport,(SELECT t2.typen FROM tasks t2 WHERE t2.id = t.pid AND t2.archive is null LIMIT 1 ) AS type_parent FROM tasks t WHERE t.id={$id} AND t.archive is null limit 1");
    if ($result == null) {return false;}
    return $result->fetch_assoc();
}


function task_fields_all($ids)
{
    $db = DB::Connection();
    $ids = implode(",", $ids);
    $result = $db->query("select id,DATE_FORMAT(t.date,'%d.%m.%Y') as date,
       name,
       description,
       DATE_FORMAT(t.deadline,'%d.%m.%Y') as deadline,
DATE_FORMAT(t.finished,'%d.%m.%Y') as finished,
       closer,
       authority,
       priority,
       director,
       responsible,
       executors,
       STATUS+0 as status,
DATE_FORMAT(t.startnoty,'%d.%m.%Y') as startnoty,
       `whom-noty`,
       comments,
       subtask,
DATE_FORMAT(t.repeattask,'%d.%m.%Y') as repeattask ,type,agreement,subexecutors,job,type,
       platform from tasks t WHERE t.id in({$ids})");

    return $result->fetch_all(MYSQLI_ASSOC);
}







?>