<?php
include_once $_SERVER['DOCUMENT_ROOT'] . '/assets/functions.php';
$db = db::Connection();
$target_dir = $_SERVER['DOCUMENT_ROOT'] . "/uploads/";
//Архивацию,Разархивацию и блоб
if (isset($_GET["downloads"])) {

    $data = file_id($_GET['downloads']);
    $path_to_file = $target_dir . $data["t_id"] . "/" . $data["name"];
    if (file_exists($path_to_file)) {
        $size = filesize($path_to_file);

        Header("HTTP/1.1 200 OK");
        Header("Connection: close");
        Header("Content-Type: application/octet-stream");
        Header("Accept-Ranges: bytes");
        Header("Content-Disposition: Attachment; filename=" . $data["name"]);
        Header("Content-Length: " . $size);

// Открыть файл для чтения и отдавать его частями
        $file = fopen($path_to_file, 'r');
        while (!feof($file)) {
            // Если соединение оборвано, то остановить скрипт
            if (connection_aborted()) {
                fclose($file);
                break;
            }
            echo fread($file, 50000);
            // Пазуа в 1 секунду. Скорость отдачи 10000 байт/сек
            sleep(1);
        }
        fclose($file);
    } else {
        header("Location: " . $_SERVER['HTTP_REFERER']);
    }

}

if (isset($_POST["upload"])) {
    // $target_dir = "uploads/";
    $data=json_decode($_POST["upload"]);
    $task_id = $data->id;
    $b = [];
    if (!isset($_FILES["fileToUpload"])) {
        exit;
    }
    $file_data = $_FILES["fileToUpload"];
    foreach ($file_data["error"] as $key => $error) {


        $name = htmlspecialchars($file_data["name"][$key]);
        $size = intval($file_data["size"][$key]);
        //
        if (property_exists($data, 'platform') and property_exists($data, 'self')){
            $dir = $target_dir . $data->platform ."/". $data->self ."/". $task_id;
        }else{
            $dir = $target_dir . $task_id;
        }
         //
        $target_file = $dir . "/" . basename($name);

        if (file_exists($target_file)) {
            echo json_encode(["error" => "файл уже существует"]);
            exit;
        }

        if ($size > 20000000) {
            echo json_encode(["error" => "К сожалению, ваш файл слишком большой"]);
            exit;
        }
        ///не работает блоб с docx
        $blob = "ремонт";
        if ($error != 0) {
            $error = 'Error: ' . $error . '<br>';
            echo json_encode(["error" => $error]);
            exit;
        }

        if (!is_dir($dir)) {
            mkdir($dir, 0755,true);
        }

        if (!move_uploaded_file($file_data["tmp_name"][$key], $target_file)) {
            $error = "Фаил неудалось переместить";
            echo json_encode(["error" => $error]);
            exit;
        }
        $query = "INSERT HIGH_PRIORITY INTO attachments(t_id,name,path,data,size,type_m)
       VALUES('{$task_id}','{$name}','{$dir}','{$blob}','{$size}','{$file_data["type"][$key]}')";
        $result = $db->query($query);
        if ($result) {
            $b[] = ["error" => "",
                "errorkeys" => [],
                "initialPreview" => ["<img class='kv-preview-data file-preview-image' src='http://test.company-dis.ru/assets/admina/img/icon.png'>"],
                "initialPreviewConfig" => [
                    ["caption" => $name, "size" => $size,
                        "width" => '120px',
                        "url" => '/app/views/tasks/upload.php?delete',
                        "key" => $db->insert_id,
                    ]
                ],
                "initialPreviewThumbTags" => [],
                "append" => true];
        }else{
            $error="Ошибка фаил не записан";
            if (unlink($target_file)) {
                $error.=" и фаил не удален";
            }
            echo json_encode(["error" => $error]);
          //проверка на невыполнение нужно удалить фаил который недобавился в базу
        }
    }

    If (count($b) != 0) {
        echo json_encode($b);
    } else {
        $error = "Запрос на удаление ошибка";
        echo json_encode(["error" => $error]);
    }

}
if (isset($_GET["delete"])) {
    $file = file_id($_POST["key"]);
    $id = intval($_POST["key"]);
    $target_file = $file["path"] . "/" . $file["name"];

    if (!file_exists($target_file)) {
        echo json_encode(["error" => "Файла несущеcтвует"]);
        exit;
    }
    if (unlink($target_file)) {
        if (count(scandir($file["path"])) < 3) {

            rmdir($file["path"]);
        }
    } else {
        echo json_encode(["error" => "Удаление нет"]);
        exit;
    }

    $query = "DELETE LOW_PRIORITY QUICK FROM attachments WHERE id ={$id} LIMIT 1";
    $result = $db->query($query);
    if ($result) {
        echo json_encode([]);
    }else{
        echo json_encode(["error"=>"Ошибка удаление"]);
    }

}
function file_id($id)
{
    {
        $db = db::Connection();
        $result = $db->query("SELECT a.t_id,
       a.name,
       a.path FROM attachments a WHERE a.id ='{$id}' limit 1");
        $allfiles = $result->fetch_assoc();
        return $allfiles;
    }
}

if (isset($_POST["all_data"])) {
    $id = intval($_POST["all_data"]);
    echo json_encode(allfiles_on_bunch1($id));
}
function allfiles_on_bunch1($id_task)
{
    $initialPreviewConfig = [];
    $initialPreview = [];
    $db = db::Connection();
    $result = $db->query("SELECT id, t_id, name, path, size FROM attachments WHERE t_id IN ($id_task)");
    if ($result->num_rows>0) {
        while ($row = $result->fetch_assoc()) {
            $initialPreviewConfig[] = [
                "caption" => $row["name"],
                "size" => $row["size"],
                "width" => '100px',
                "url" => '/app/views/tasks/upload.php?delete',
                "key" => $row["id"]
            ];
            array_push($initialPreview, "<img class='kv-preview-data file-preview-image' src='http://test.company-dis.ru/assets/admina/img/icon.png'>");
        }
    }

    return ["error" => "",
        "errorkeys" => [],
        "initialPreview" => $initialPreview,
        "initialPreviewConfig" => $initialPreviewConfig,
        "initialPreviewThumbTags" => [],
        "append" => true];

}

