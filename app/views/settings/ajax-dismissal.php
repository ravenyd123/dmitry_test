<?php
include_once $_SERVER['DOCUMENT_ROOT'] . '/assets/functions.php';
//include_once $_SERVER['DOCUMENT_ROOT'] . '/assets/class.php';
if ($_POST['action'] == "load_info") {
    $id = $_POST['id'];
// $id = 56;
    $query = "SELECT login, name, surname, `father_name`, status, platform
        		  FROM `users` 
		          WHERE id='" . $id . "' LIMIT 1";
    $result = $db->query($query)->fetch_assoc();
    $query = "SELECT id, name, description, director, responsible, executors
		          FROM `tasks` 
		          WHERE director='" . $id . "' OR 
              responsible='" . $id . "' OR 
              executors='" . $id . "'";
    $result_tasks = $db->query($query);

    ob_start();
    $query_users = getUsersFromCompany();
    ?>
    <select id="successor_l" class="select2" data-placeholder="Выберите преемника" tabindex="-1">
        <option value="-1">&nbsp</option>
        <? foreach ($query_users as $key => $value) {
            $fio_full = $value["surname"] . " " . mb_substr($value["name"], 0, 1, "utf-8") . "." . mb_substr($value["father_name"], 0, 1, "utf-8"); ?>
            <option <? if ($value["id"] == $id) { ?> selected="selected" <?
            } ?> value="<?= $value["id"] ?>"><?= $fio_full ?>
            </option><?
        } ?></select>
    <?php
    $obj_list_users = ob_get_clean();
    foreach ($result_tasks as $key => $value) {
        $tasks[] = $value;
        $tasks[$key]['description'] = strip_tags(htmlspecialchars_decode($tasks[$key]['description']));
        if ((int)$tasks[$key]['director'] == $id) {
            $tasks[$key]['fio_director'] = $obj_list_users;
        } else {
            $tasks[$key]['fio_director'] = getSmallFioFromId($tasks[$key]['director']);
        }
        if ((int)$tasks[$key]['responsible'] == $id) {
            $tasks[$key]['fio_responsible'] = $obj_list_users;
        } else {
            $tasks[$key]['fio_responsible'] = getSmallFioFromId($tasks[$key]['responsible']);
        }
        if ((int)$tasks[$key]['executors'] == $id) {
            $tasks[$key]['fio_executors'] = $obj_list_users;
        } else {
            $tasks[$key]['fio_executors'] = getSmallFioFromId($tasks[$key]['executors']);
        }
    }
// echo"<pre>";
    $arr_platform = explode(',', $result['platform']);
    foreach ($arr_platform as $value) {
        $str_name = getPlatformsNames($value);
        $arr_platform_name[] = $str_name['name'];
    }
    $fio_full = $result['surname'] . " " . $result['name'] . " " . $result['father_name'];
    /* Упаковываем в json */
    $out = json_encode(array(
        my_name => $result['name'],
        my_surname => $result['surname'],
        my_father_name => $result['father_name'],
        my_status => $result['status'],
        my_login => $result['login'],
        my_fio_full => $fio_full,
        platform_name => $arr_platform_name,
        logins => $logins,
        tasks => $tasks
    ));
    echo $out;
}

if ($_POST['action'] == "update") {
    // var_dump ($_POST);
    $data = json_decode(stripslashes($_POST['data']));

    $status = $_POST['status'];
    $id_user = $_POST['id'];
    $data = array_slice($data, 1);
    $user_l = [];
    foreach ($data as $key => $value) {
        $query_tasks = $db->query("UPDATE `tasks` SET 
                    `director`= IF('" . $value[0] . "' != '','" . $value[0] . "',`director`),
                    `responsible`= IF('" . $value[1] . "' != '','" . $value[1] . "',`responsible`),
                    `executors`= IF('" . $value[2] . "' != '','" . $value[2] . "',`executors`)
                    WHERE `id`='" . $value[3] . "'");
        array_push($user_l, $value[0]);
        array_push($user_l, $value[1]);
        array_push($user_l, $value[2]);
    }
    $status_user = $db->query("UPDATE `users` SET 
                                `status` = '" . $status . "'
                                 WHERE `id`='" . $id_user . "'");
    if (($query_tasks == true) AND ($status_user == true)) {
        $message = 1;
        $unique_user = array_unique($user_l);
        $list = array_diff($unique_user, array('', $id_user)); // ID users кому переназначили
        $send_mail_trans_tasks = sendmail_trans_task_of_dismissal($id_user, $list);
        $send_mail_dismissal = sendmail_of_dismissal($id_user);

    }
    $out = json_encode(array(
        message => $message
    ));
    echo $out;
}
?>