<? include_once $_SERVER['DOCUMENT_ROOT'] . '/assets/functions.php'; ?>

<?
$users = getUsers();
$allPlatforms = getPlatforms();

$query_users = getUsersFromCompany();

foreach ($users as $key => $value) {
    $platforms = explode(',', $users[$key]['platform']);
    foreach ($platforms as $platform) {
        $users[$key]['nb_platform']++;
        $users[$key]['platform-name'] .= "- " . $allPlatforms[$platform]['name'] . "<br/>";
    }

}


?>
<div class="panel-heading">
    <div class="top_nav">
        <a href="#" data-target="#edit-user" data-toggle="modal">
            <button class="btn btn-success btn-sm" data-action="add-user" title="Добавить сотрудника"
                    data-placement="top" data-toggle="tooltip">
                <i class="fa fa-user-plus"></i>
            </button>
        </a><br>

        <a href="#" data-target="#edit-user">
            <button class="btn btn-default btn-sm" data-action="edit-user" data-placement="top" data-toggle="tooltip"
                    title="Редактировать профиль">
                <i class="ion-edit"></i>
            </button>
        </a><br>

        <a href="#" data-target="#edit-user">
            <button class="btn btn-default btn-sm" data-action="dismissal-user" data-placement="top"
                    data-toggle="tooltip" title="Увольнение">
                <i class="fa fa-user"></i>
            </button>
        </a><br>

        <button class="btn btn-default btn-sm" data-action="remove-user" title="Удалить" data-placement="top"
                data-toggle="tooltip">
            <i class="ion-trash-a"></i>
        </button><br>

        <a href="#" data-target="#password_users_reset">
            <button class="btn btn-default btn-sm" data-action="#password_users_reset" data-placement="top"
                    data-toggle="tooltip" title="Сброс пароля">
                <i class="fa fa-repeat"></i>
            </button>
        </a><br>



    </div>
    <div class="clearfix"></div>
</div>

<div class="panel-body">
    <div class="table-responsive">
        <table id="datatable" class="table table-hover table-bordered dataTable">
            <thead>
            <tr>
                <th class="text-center hidden"></th>
                <th>Логин</th>
                <th>ФИО</th>
                <th>Почта</th>
                <th>Телефон</th>
                <th>Подразделение</th>
                <th>Площадка</th>
            </tr>
            </thead>
            <tbody>
            <? foreach ($users as $key => $value) { ?>

                <?
                # Если пользователь не администратор он не должен видеть администраторов
                if ($role != 'admin' && $users[$key]["level"] == 'admin') {
                    continue;
                }
                ?>

                <tr class="text-left" data-id="<?= $key; ?>">
                    <td class="text-center hidden"><input type="checkbox" name="idu[]" onfocus="false"></td>
                    <?
                    $txt_title = "title='Активный'";

                    // var_dump($users[$key]);
                    $txt_style = "black";
                    if ($users[$key]["status"] == 2) {
                        $txt_title = "title='Уволен'";
                        $txt_style = "text-danger";
                    }
                    if ($users[$key]["status"] == 1) {
                        $txt_title = "title='В декрете'";
                        $txt_style = "text-info";
                    }
                    ?>
                    <td class='text-center <?= $txt_style ?>' <?= $txt_title ?>><i
                                class="ion-android-social-user"></i> <?= $users[$key]["login"]; ?></td>
                    <td class='<?= $txt_style ?>' <?= $txt_title ?>><?= $users[$key]["surname"] ?>
                        <?= $users[$key]["first_name"] ?> <?= $users[$key]["father_name"] ?>
                    <td class='<?= $txt_style ?>' <?= $txt_title ?>><i
                                class="ion-ios7-email"> <?= $users[$key]["email"]; ?></i></td>
                    <td class='<?= $txt_style ?>' <?= $txt_title ?>><i
                                class='ion-ios7-telephone'> <?= $users[$key]["phone"]; ?></i></td>

                    <td class='<?= $txt_style ?>' <?= $txt_title ?>><i> <?= $users[$key]["name_division"]; ?> (<?=$users[$key]["name_type"];?>) </i></td>

                    <td class='<?= $txt_style ?>'>


                        <div class="panel-group panel-group-joined" id="accordion-test">

                            <div class=" text-center">

                                <div data-toggle="collapse" data-parent="#accordion-test"
                                     href="#platforms<?= $users[$key]["id"] ?>" class="collapsed <?= $txt_style ?>"
                                     aria-expanded="false">
                                    Площадки (<?= $users[$key]['nb_platform'] ?>)
                                </div>

                            </div>
                            <div id="platforms<?= $users[$key]["id"] ?>" class="panel-collapse collapse"
                                 aria-expanded="false" style="">
                                <div class="panel-body <?= $txt_style ?>" <?= $txt_title ?>>
                                    <?= $users[$key]['platform-name'] ?>
                                </div>
                            </div>


                        </div>

                    </td>


                </tr>
            <? } ?>
            </tbody>
        </table>
    </div>
</div>

<div class="modal fade" id="dismissal-user" data-backdrop="static">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Уволнение пользователя: </h4>
                <p class="text-info" id="fio_full"></p>

            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Статус: </label>


                            <div class="col-sm-9">
                                <select id="dismissal" class="select2 select2-offscreen"
                                        data-placeholder="Выберите статус пользователя" tabindex="-1" name="status_user"
                                        title="Выберите статус пользователя">
                                    <option value="0">Активный</option>
                                    <option value="1">Декрет</option>
                                    <option value="2">Уволен</option>
                                </select>
                            </div>
                        </div>
                        <br><br>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Выберите преемника:</label>
                            <div class="col-sm-9">
                                <select id="successor" class="select2 " data-placeholder="Выберите преемника"
                                        tabindex="-1" title="Выберите преемника">
                                    <option value="-1">&nbsp</option>
                                    <? foreach ($query_users as $key => $value) {
                                        $fio_full = $value['surname'] . " " . $value['name'] . " " . $value['father_name']; ?>
                                        <option value="<?= $value['id'] ?>"><?= $fio_full ?></option>
                                    <? } ?>

                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5 platforms-list row text-right"><h4>Площадки пользователя:</h4>
                        <h6 id="platform_name" class="text-info row text-right"></h6>
                    </div>
                    <div class="">
                        <div class="col-md-12">
                            <h4 class="">Поставленные задачи</h4>
                            <div class="form-group">
                                <table id="dt-table-dismissal" class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th>ID задачи</th>
                                        <th>Задача</th>
                                        <th>Описание</th>
                                        <th>Кто назначил</th>
                                        <th>Ответственный</th>
                                        <th>Исполнитель</th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div id="chart"></div>


                    <!-- <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th>№</th>
                                            <th>Задача</th>
                                            <th>Кто назначил</th>
                                            <th>Преемник</th>
                                        </tr>
                                    </thead>
                                    <tbody id="tbody">


                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div> -->

                </div>
            </div>
            <div class="modal-footer">
                <div class="text-right">
                    <input type="button" onclick="window.location.href='/settings'" value="Отменить"
                           class="btn btn-default">
                    <!-- <input type="button" id="dismissal_button_false" value="Сохранить" class="btn btn-primary disabled" title="Вы пока не можете сохранить"> -->
                    <input type="button" id="dismissal_button" value="Сохранить" title="Сохранить"
                           class="btn btn-primary">
                </div>

            </div>
        </div>
    </div>
</div>
<script src="/app/views/settings/js/user.js"></script>

 








 