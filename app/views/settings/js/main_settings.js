$(document).ready(function () {

$.fn.editable.defaults.mode = 'inline';
$.fn.editable.defaults.mode = 'popup';     
var nameVerificationCounter = $("a.xetable-name").length;
var descriptionVerificationCounter = $("a.xetable-description").length;
var i=0;
while ( i <= nameVerificationCounter-1 ) {

// Меняем названия проверок
$('#nameVerification'+i).editable({
    type: 'text',
    url: '/app/views/settings/ajax-main-settings.php',    
    pk: i,    
   // title: 'Enter username',
    ajaxOptions: {
        dataType: 'json'
    },
    success: function(response, newValue) {
        if(!response) {
            return "Unknown error!";
        }          
        
        if(response.success === false) {
             return response.msg;
        }
    }        
});
i++;
}


// Меняем описание проверок
var k=0;
while ( k <= descriptionVerificationCounter-1 ) {

$('#descriptionVerification'+k).editable({
    emptytext: 'Нет',
    type: 'text',
    url: '/app/views/settings/ajax-main-settings.php',    
    pk: k,    
   // title: 'Enter username',
    ajaxOptions: {
        dataType: 'json'
    },
    success: function(response, newValue) {
        if(!response) {
            return "Unknown error!";
        }          
        
        if(response.success === false) {
             return response.msg;
        }
    }        
});
k++;
}




});