$(document).ready(function () {
    $('#datatable').dataTable({
        "search": true,
        fixedHeader: true,
        "bAutoWidth": false,
        stateSave: true,
        // "aoColumnDefs": [{ "sWidth": "20%", "aTargets": [ 3 ] },
        //                  { "sWidth": "15%", "aTargets": [ 4 ] },
        //                  { "sWidth": "7%", "aTargets": [ 5 ] }   ],
        "columnDefs": [
            {"width": "15%", "targets": [1]},
            {"width": "20%", "targets": [3]},
            {"width": "20%", "targets": [4]},
            {"width": "25%", "targets": [5]}

        ],
        "language": {
            "lengthMenu": "Выводить по _MENU_ записей",
            "zeroRecords": "Ни одной записи не найдено",
            "info": "Показано с _START_ по _END_ из _TOTAL_ записей",
            "infoEmpty": "Ни одной записи не найдено",
            "infoFiltered": "(отфильтровано из _MAX_ записей)",
            "search": "Поиск:",
            "paginate": {
                "first": "Первая",
                "last": "Последняя",
                "next": "Следующая",
                "previous": "Предыдущая"
            },
        }
    });

    $("#datatable").on("change", function () {
        var val = $(this).val();
        oTable.search(val).draw();
    });

    


    $('#datatable tbody').on('click', 'tr', function () {
        var inputcheck = $(this).children().first().find('input[type=checkbox]')
        if (inputcheck.prop("checked") == false) {
            //inputcheck.trigger("click");
            inputcheck.prop("checked", true);
        } else {
            //inputcheck.trigger("click");
            inputcheck.prop("checked", false);
        }
        $(this).toggleClass('selected info');
        checkRowAmount_new();

    })
    var platform_tree = $("#platform_tree").jstree({
        "checkbox": {
            "three_state": true,
            "cascade": "none"
        },
        "plugins": ["checkbox", "wholerow", "types", "conditionalselect"],
        "core": {
            "check_callback": false,
            "multiple": true,
            "themes": {
                "dots": false,
                "icons": true,
                "variant": false
            },
            data: jsonData
        },
        "types": {
            "#": {
                "max_depth": 3
            },
            "level0": {
                "icon": "fa fa-bank"
            },
            "level1": {
                "icon": "fa fa-briefcase"
            },
            "level2": {
                "icon": "fa fa-cube"
            }
        }
    }).bind("load_node.jstree", function (e, data) {
        // $(this).find('li[rel!=file]').find('.jstree-checkbox:first').hide();
    });

    // Сохранение формы
    $('.edit-user').submit(function (event) {
        event.preventDefault();

        var error = '';

        // Проверяем заполненность полей
        $('.edit-user input,.edit-user textarea,.edit-user select').each(function (index) {

            var attr = $(this).attr('required');

            if (typeof attr !== typeof undefined && attr !== false) {

                if ($(this).val() == '') {
                    $(this).closest('.form-group').addClass('has-error');
                    $(this).closest('.form-group').find('.help-block').text('Поле обязательно для заполнения');

                    error = 'Обязательные поля не заполнены';
                    $.notify({'type':'warning', 'message':'Не все обязательные поля заполнены!'});
                }
                ;
            }
        })

        if (error != '') {
            console.log(error);
            return false;
        }
        ;

        $('input[type="submit"]').val('Идет выполнение...');

        var access = {};

        $('#access select').each(function (index, item) {
            access[$(item).attr('name')] = $(item).val()
        });

        var platforms_sel = $("#platform_tree").jstree("get_selected");

        data_upload = decodeURI($(this).serialize());
        for (var i in platforms_sel) {
            // console.log(data_upload);
            data_upload = data_upload + '&platform=' + platforms_sel[i];
        }


        $.ajax({
            async: false,
            type: 'POST',
            dataType: "json",
            data: {
                'action': 'updateUsers',
                'data': data_upload,
                'access': JSON.stringify(access)
            },
            url: "/app/views/settings/model/user.php",
            cache: false,
            success: function (data) {
                 users_table.ajax.reload();
                $('#edit-user').modal('hide');
               // autoHideNotify('success', 'top right', data['message'], data['message2']);
                $.notify({'type':'success', 'message':data['message2']});
                location.reload()
               //  datatable.ajax.reload( null, false );
            },
            error: function (data) {
                var error = data.responseJSON;
                data = data.responseJSON;
                //autoHideNotify('error', 'top right', data['message'], data['message2']);
                $.notify({'type':'error', 'message':data['message2']});
                return false;
            }
        });

        $('input[type="submit"]').val('Сохранить');

    })

    // Кнопка удаления записи
    $('*[data-action="remove-user"]').click(function () {

        if (checkRowAmount() < 1) {
            $.notify({'type':'error', 'message':'Необходимо выбрать хотя бы одну запись!'});
            return false;
        }
        ;

        confirm('info', 'top right', 'Пользователь(и) будут удалены');

        $(document).on('click', '.notifyjs-metro-base .no', function () {
            $(this).trigger('notify-hide');
        });

        $(document).on('click', '.notifyjs-metro-base .yes', function () {

            $(this).trigger('notify-hide');

            var removeId = [];

            $('#datatable tbody input[type="checkbox"]:checked').each(function (index) {
                removeId.push($(this).closest("tr").attr('data-id'));
            })

            var attr = 'ids=' + removeId + '&action=removeUsers';

            // console.log(attr);

            $.ajax({
                type: 'POST',
                data: attr,
                url: "/app/views/settings/model/user.php",
                cache: false,
                success: function (msg) {
                    location.reload();
                }
            });

        });

    })

    // Кнопка редактирования пользователя
    $('*[data-action="edit-user"]').click(function () {

        var id = $('#datatable tbody input[type="checkbox"]:checked').closest("tr").attr('data-id');

        // console.log(id);

        if (checkRowAmount() != 1) {
            $.notify({'type':'error', 'message':'Необходимо выбрать только одну запись!'});
            return false;
        }
        ;

        fillTaskForm(id);

    })

    // Кнопка массового сброса пароля
    $('*[data-action="#password_users_reset"]').click(function () {

        if (checkRowAmount() < 1) {
            $.notify({'type':'error', 'message':'Необходимо выбрать хотя бы одну запись!'});
            return false;
        }
        ;
        confirm('info', 'top right', 'Будут созданы новые пароли пользователей и отправлены им на e-mail');

        $(document).on('click', '.notifyjs-metro-base .no', function () {
            $(this).trigger('notify-hide');
        });

        $(document).on('click', '.notifyjs-metro-base .yes', function () {

            $(this).trigger('notify-hide');

        var id = []
        $('#datatable tbody input[type="checkbox"]:checked').each(function (index) {
                id.push($(this).closest("tr").attr('data-id'));
            })
        var attr = 'ids=' + id + '&action=passUsersReset';

            // console.log(attr);

            $.ajax({
                type: 'POST',
                data: attr,
                url: "/assets/functions.php",
                cache: false,
                success: function (msg) {
                    var msg = JSON.parse(msg);
                  //  autoHideNotify(msg['type'], 'top right', msg['message'], msg['message2']);
                    $.notify({'type':msg['type'], 'message':msg['message2']});
                    //location.reload();

                }
            });
        });

    })
    // Кнопка увольнения пользователя
    $('*[data-action="dismissal-user"]').click(function () {
        var id = [];
        id = $('#datatable tbody input[type="checkbox"]:checked').closest("tr").attr('data-id');

        if (checkRowAmount() != 1) {
            $.notify({'type':'error', 'message':'Необходимо выбрать только одну запись'});
            return false;
        }
        ;

        dismissalForm(id);

    })

    // Кнопка добавления пользователя
    $('*[data-action="add-user"]').click(function () {

        // Меняем заголовок
        $('#edit-user .modal-header h4').text('Добавить пользователя');

        // Очищаем все поля
        $('.modal-body input,.modal-body textarea,.modal-body select').each(function (index) {
            $(this).val('');
        })

        $('input[name="login"]').attr('disabled', false);
        $('input[name="operation"]').val('add');

        $('*[data-action="reset-password"]').hide();

        // Чистим селекты
        $('.modal .select2').val(["C","R","U","D"]).trigger('change');
        $("select#division").select2("val","");

    })

    // Сброс пароля пользователя
    $('*[data-action="reset-password"]').click(function () {
        var login = $('input[name="login"]').val();
        var name = $('input[name="name"]').val();
        var email = $('input[name="email"]').val();
        resetPassword(login, name, email);
    })

    // Заполнение формы
    function fillTaskForm(id) {

        $('#edit-user .modal-header h4').text('Редактировать пользователя');

        var json = getNodeByField('users', 'id', id);
        // console.log(json);

        // Чистим селекты
        $('.modal .select2').val(null).trigger('change');

        // Тип операции
        $('input[name="operation"]').val('edit');

        // Подразделение пользователя
       // $('select#division').val(json['data'][0]['division']);
     //   $('select[name="level"]').trigger('change');
        $("select#division").select2("val", json['data'][0]['division']);

        // Платфоры
        var platforms = json['data'][0]['platform'].split(',');
        $("#platform_tree").jstree("deselect_all");
        $("#platform_tree").jstree().select_node(platforms, true);

        //     selected = platform_tree.jstree("get_selected");

        // Группа пользователей
        var level = json['data'][0]['level'];
        $('select[name="level"]').val([level]);
        $('select[name="level"]').trigger('change');

        $('input[name="login"]').val(json['data'][0]['login']);
        // $('input[name="login"]').attr('disabled',true);

        $('input[name="surname"]').val(json['data'][0]['surname']);
        $('.edit-user input[name="name"]').val(json['data'][0]['name']);
        $('input[name="father_name"]').val(json['data'][0]['father_name']);

        $('input[name="email"]').val(json['data'][0]['email']);
        $('input[name="phone"]').val(json['data'][0]['phone']);

        // Права доступа
        var access = JSON.parse(json['data'][0]['access']);

        if (typeof( json['data'][0]['access'] ) != 'undefined' && json['data'][0]['access'] != null) {
            for (var key in access) {
                $('select[name="' + key + '"]').val(access[key]);
                $('select[name="' + key + '"]').trigger('change');
            }
        }

        $('*[data-action="reset-password"]').show();

        $('#edit-user').modal('show');
    }
});
// Увольнение сотрудника
function dismissalForm(id) {

    $('#dismissal-user .modal-header h4').text('Увольнение сотрудника');
    var json = getNodeByField('users', 'id', id);
    var user_id = $("#datatable input[type='checkbox']:checked").closest("tr").attr("data-id");

// console.log(user_id);
    $.ajax({
        url: '/app/views/settings/ajax-dismissal.php',
        data: {
            'id': user_id,
            'action': 'load_info'
        },
        type: "POST",
        success: function (data) {

            data = JSON.parse(data);
            document.getElementById('fio_full').innerHTML = data['my_fio_full'];
            var platforms_name = data['platform_name'];
            for (var i in platforms_name) {
                $("#platform_name").clone().text(platforms_name[i]).appendTo(".platforms-list");
            }
            ;
// console.log (data['platform_name']);
// document.getElementById('dismissal').value = data['status'];
            const select = document.querySelector('#dismissal').getElementsByTagName('option');
            for (let i = 0; i < select.length; i++) {
                if (select[i].value === data['my_status']) {
                    select[i].selected = true;
                    $('#dismissal').select2().val(i);
                }
            }

// $('#dismissal_button_false').on('click', function () {
//     autoHideNotify('warning', 'top right', 'Увольнение', 'Вы не выбрали преемника для задач');
// });
// $("#successor").change(function(){

//     $("#dismissal_button_false").removeClass().addClass("hidden");
//     $("#dismissal_button").removeClass().addClass("btn btn-primary");
// });

            $(document).ready(function () {
                var table = $('#dt-table-dismissal').DataTable({
                    "order": [[0, "desc"]],
                    fixedHeader: true,
                    "bAutoWidth": false,
                    "columnDefs": [
                        {
                            className: "hidden", "targets": [0]
                        },
                        {
                            "defaultContent": "-",
                            "targets": "_all"
                        }
                    ],
                    data: data['tasks'],
                    "bPaginate": false,

                    columns: [
                        {data: 'id', class: 'idfor hidden'},
                        {data: 'name', class: 'url-text'},
                        {data: 'description', class: 'url-text'},
                        {data: 'fio_director', class: 'text-center'},
                        {data: 'fio_responsible', class: 'text-center'},
                        {data: 'fio_executors', class: 'text-center'}
                    ],
                    "language": {
                        "lengthMenu": "Выводить по _MENU_ записей",
                        "zeroRecords": "Ни одной записи не найдено",
                        "info": "Показано с _START_ по _END_ из _TOTAL_ записей",
                        "infoEmpty": "Ни одной записи не найдено",
                        "infoFiltered": "(отфильтровано из _MAX_ записей)",
                        "search": "Поиск:",
                        "paginate": {
                            "first": "Первая",
                            "last": "Последняя",
                            "next": "Следующая",
                            "previous": "Предыдущая"
                        },
                    }
                });

                $('.url-text').click(function () {

                    var idfor = $(this).closest("tr").find(".idfor").text();
                    window.open('tasks/task_new?action=edit&id=' + idfor);

                });


                $("#dt-table-dismissal").css("width", "100%");

                $("#successor").change(function () {
                    var sel = $(this).val();
                    $("#successor_l option[selected='selected']").removeAttr('selected');
                    $("#successor_l option[value='" + sel + "']").attr('selected', 'selected');
                });


                $('#dismissal_button').on('click', function () {
                    bootbox.confirm({
                        title: "Подтверждение",
                        backdrop: false,
                        message: "Вы уверены, что хотите передать данные задачи другому пользователю?",
                        buttons: {
                            cancel: {
                                label: '<i class="fa fa-times"></i>Отмена'
                            },
                            confirm: {
                                label: '<i class="fa fa-check"></i>Подтвердить'
                            }
                        },
                        callback: function (result) {
                            if (result) {
                                var table = $('#dt-table-dismissal');
                                var trs = $('tr', table);
                                var arr_tasks = [];
                                
                                if (trs.length != 2) { 
                                trs.each(function (index, value) {

                                    var e = document.getElementById("successor_l");
                                    strUser = e.options[e.selectedIndex].value;
                                    var numb_cells = [3, 4, 5];
                                    var dir = [];
                                    // console.log(numb_cells);
                                    numb_cells.forEach(function (entry, i) {
                                         
                                        var str = document.getElementById('dt-table-dismissal').rows[index].cells[entry];
                                        // var selid = str.options.selectedIndex;
                                        
                                        if (str.getElementsByTagName('select').length === 1) {
                                           
                                            dir[i] = str.firstElementChild.options[str.firstElementChild.selectedIndex].value;
                                           
                                        } else {
                                            
                                            dir[i] = "";
                                        }
                                    });
                                    dir[3] = document.getElementById('dt-table-dismissal').rows[index].cells[0].innerHTML;
                                    arr_tasks[index] = [dir[0], dir[1], dir[2], dir[3]];

                                });
                            } else {
                                var arr_tasks = '';
                            }

                               
                                var jsonString = JSON.stringify(arr_tasks);
// console.log(jsonString);
                                var status_user = $('#dismissal').select2().val();
                                $.ajax({
                                    url: '/app/views/settings/ajax-dismissal.php',
                                    data: {
                                        "data": jsonString,
                                        "status": status_user,
                                        "id": user_id,
                                        "action": "update"
                                    },
                                    type: "POST",
                                    cache: false,
                                    success: function (data) {

                                        data = JSON.parse(data);
                                        $.notify({'type':'success', 'message':'Задачи успешно переназначены'});

                                        $("#dismissal-user").modal('hide');
                                        // console.log(data['message']);
                                        // location.reload();
                                    },
                                    error: function (data) {
                                        $.notify({'type':'error', 'message':'Ошибка переназначения задач!'});

                                    }
                                });
                            }
                        }

                    });

                });

                $('#dismissal-user').modal('show');
                $("#dismissal-user").on('hidden.bs.modal', function () {
                   // table.ajax().reload();
                    location.reload();
                });
            });
        },
        error: function () {
            alert("No PHP script: ");
        }
    })
}

function checkRowAmount_new() {
    var table = $("#datatable tr.selected")
    if (table.length > 0) {
        $('.panel-heading .fa-user').parent().removeClass('btn-warning');
        $('.panel-heading .ion-trash-a').parent().addClass('btn-danger');
        $('.panel-heading .ion-edit').parent().removeClass('btn-info');

    }
    if (table.length == 1) {
        $('.panel-heading .fa-user').parent().addClass('btn-warning');
        $('.panel-heading .ion-edit').parent().addClass('btn-info');

    }
    if (table.length <= 0) {
        $('.panel-heading .ion-edit').parent().removeClass('btn-info');
        $('.panel-heading .fa-user').parent().removeClass('btn-warning');
        $('.panel-heading .ion-trash-a').parent().removeClass('btn-danger');
    }
    ;
    return table.length;
}
