$(function () {


        $("#jstree").jstree({
            "plugins": ["themes", "html_data", "dnd", "wholerow", "contextmenu", "types", "state"],
            "core": {
                "check_callback": true,
                "multiple": false,
                "themes": {
                    "dots": false,
                    "icons": true,
                    "variant": false
                },
                data: jsonData

            },
            "contextmenu": {
                "items": customMenu
            },

            "types": {
                "#": {
                    "max_depth": 3
                },
                "level0": {
                    "icon": "fa fa-bank"
                },
                "level1": {
                    "icon": "fa fa-briefcase"
                },
                "level2": {
                    "icon": "fa fa-cube"
                }
            }
        }).bind("rename_node.jstree", function (e, data) {
            if (data.text != data.old && data.text != " ") {
                if (ajax_tree("rename", data.node.id, false, data.text)) {
                 //   autoHideNotify('success', 'top right', false, 'Переименовано с ' + data.old + ' на ' + data.text);
                    $.notify({'type':'success', 'message':'Переименовано с ' + data.old + ' на ' + data.text});
                } else {
                   // autoHideNotify('error', 'top right', false, 'Ошибка')
                    $.notify({'type':'error', 'message':'Произошла ошибка'});

                }
            }
        }).bind("move_node.jstree", function (e, data) {
            //  console.log("Drop node " + data.node.id + " to " + data.parent);
            if (ajax_tree("move", data.node.id, data.parent)) {
                $.notify({'type':'success', 'message':'Удачно перемещено'});
            } else {
                $.notify({'type':'error', 'message':'Произошла ошибка!'});
            }
        });
    }
);
function customMenu(node) {

    var tree = $("#jstree").jstree(true);
    var items = {
        "Create": {
            "separator_before": false,
            "separator_after": false,
            "label": "Создать",
            "type": "create",
            "action": function (obj) {
                var level = ["level1", "level2"];
                var txt = ["ООО", "Цех"];
                var number = node.parents.length - 1;
                var create_n = tree.create_node(node, {text: txt[number], "type": level[number]});
                var tt = ajax_tree("create", false, obj.reference.prevObject[0].id);
                tree.set_id(create_n, tt);
                tree.edit(tt);
                //console.log($node.parents.length-1);
                if (tt) {
                   // autoHideNotify('success', 'top right', false, 'Создан новый ' + txt[number])
                    $.notify({'type':'success', 'message':'Создан новый ' + txt[number]});
                } else {
                    $.notify({'type':'error', 'message':'Произошла ошибка'});
                }
            }
        },
        "Rename": {
            "separator_before": false,
            "separator_after": false,
            "label": "Переименовать",
            "action": function (obj) {
                tree.edit(node);
            }
        },
        "Remove": {
            "separator_before": false,
            "separator_after": false,
            "label": "Удалить",
            "action": function (obj) {
                tree.delete_node(node);
                //     console.log($node);
                if (ajax_tree("delete", node.id, false, false)) {
                    $.notify({'type':'success', 'message':'Удачно удалено' + node.text });
                } else {
                    $.notify({'type':'error', 'message':'Произошла ошибка!'});
                }
                ;

            }
        }
    };
    if (node.type === "level2") {
        delete items.Create
    }
    return items
}
function ajax_tree(action, node = false, node_to = false, name = false) {
    var data1 = "";
    switch (action) {
        case "create":
            data1 = {
                "action_tree": action,
                "node": node_to,
                "name": name
            };
            break;
        case "rename":
            data1 = {
                "action_tree": action,
                "node": node,
                "name": name
            };
            break;
        case "delete":
            data1 = {
                "action_tree": action,
                "node": node
            };
            break;
        case "move":
            data1 = {
                "action_tree": action,
                "node": node,
                "node_to": node_to
            };
            break;
    }
    var tt = 0;
    $.ajax({
        type: 'POST',
        url: "/assets/jsTree/trees.php",
        cache: false,
        async: false,
        data: data1,
        success: function (msg) {
            if (action == "create") {
                tt = parseInt(msg);

                //  console.log("qqq " + tt);
            } else {
                tt = msg
            }
        }
    });
    return tt;
}

//    function root_dir() {
//        var jstree = $('#jstree').jstree(true);
//        var name = $("#root_dir")[0].value;
//        if (name != '') {
//            var value = ajax_tree("create", false, false, name);
//            if (value) {
//                var node = jstree.create_node('#', {"text": name, "type": "level0"}, "first", function () {
//                    autoHideNotify('success', 'top right', false, 'Добалена новая площадка ' + name);
//                }, true);
//                jstree.set_id(node, value);
//            } else {
//                autoHideNotify('error', 'top right', false, 'Ошибка')
//            }
//
//        } else {
//            // $(".help-block").addClass('has-error');
//            $(".help-block").text('Поле обязательно для заполнения');
//        }
//
//    }
$(document).ready(function () {
    $('.example-button').on('click', function () {
        var jstree = $('#jstree').jstree(true);
        bootbox.prompt({
            title: "Добавить площадку",
            inputType: 'text',
            callback: function (name) {
                if (name != '' && name != null) {
                    var value = ajax_tree("create", false, false, name);
                    if (value) {
                        var node = jstree.create_node('#', {
                            "text": name,
                            "type": "level0"
                        }, "first", function () {
                            $.notify({'type':'success', 'message':'Добалена новая площадка ' + name});
                        }, true);
                        jstree.set_id(node, value);
                    } else {
                        $.notify({'type':'error', 'message':'Произошла ошибка!'});
                    }

                }
            }
        });
    })
})
