 <?
include_once $_SERVER['DOCUMENT_ROOT'] . '/assets/functions.php';
$nearmissElemnt = array("Дата нарушения", "Инициатор", "Место нарушения", "Текст нарушения", "Комментарий к нарушению", "Ответственный за исправление", "Необходимые корректирующие действия", "Срок исполнения", "Статус");
 ?>
 <div class="row">
    <div class="col-md-12">
        <div class="col-md-12">
            <button type="button" class="btn btn-info m-b-5">
                                    Загрузить файл
            </button>
            <button type="button" class="btn btn-primary m-b-5">
                                    Сохранить настройки
            </button>
            <button type="button" class="btn btn-success m-b-5">
                                    Продолжить импорт
            </button>
    </div>   
    <div class="col-md-12 container">
      

       
<?

$row = 0;
if (($handle = fopen($_SERVER['DOCUMENT_ROOT'] ."/app/views/settings/234.csv", "r")) !== FALSE) {
    echo '<table width="100px" class="table" border="1">';
    while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
      //  $data = array_map("cp1251_encode", $data); //added
        $num = count($data);
        if ($row == 0) {
            echo '<thead><tr>';
        }else{
           
            echo '<tr>
                    <td>
                        <label class="cr-styled" for="radio_string_'.$row.'">
                            <input type="radio" id="radio_string_'.$row.'" name="example-radios1" value="option1">
                                <i class="fa">
                                </i>
                        </label>';
        }
       
        for ($c=0; $c < $num; $c++) {
            //echo $data[$c] . "<br />\n";
            if(empty($data[$c])) {
               $value = "&nbsp;";
            }else{
               $value = $data[$c];
            }
            if ($row == 0) {
                 ?>
            
                <td><select id="nearset" class="select2" data-placeholder="Номер столбца"
                                        tabindex="-1" title="Номер столбца">
                                    <option value="-1">Столбец не используется</option>
                                        <? foreach ($nearmissElemnt as $key=> $options) { ?>
                                    <option value="<?=$key?>"> <?=$options ?></option>
                                        <? } ?>
                </select></td>
            <? } else {
                if ($c==0){
                    echo '<h4><small><center>'.mb_strimwidth($value, 0, 200, "...").'</center></small></h4></td>';
                } else {
                   echo '<td><h4><small><center>'.mb_strimwidth($value, 0, 200, "...").'</center></small></h4></td>'; 
                }
                
            }
            
        }
       
        if ($row == 0) {
            echo '</tr></thead><tbody>';
        }else{
            echo '</tr>';
        }
        $row++;
    }
    echo '</tbody></table>';
    fclose($handle);
}

?>
          
    </div>
</div>
<script src="/app/views/settings/js/nearmiss_settings.js"></script>
<link href="/app/views/settings/css/nearmiss-style.css" rel="stylesheet">