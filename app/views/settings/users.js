
  function checkRowAmount_new() {
        var table = $("#datatable tr.selected")
        if (table.length > 0) {
            $('.panel-heading .ion-trash-a').parent().addClass('btn-danger');
            $('.panel-heading .ion-close').parent().addClass('btn-danger');
            $('.panel-heading .ion-unlocked').parent().addClass('btn-danger');
            $('.panel-heading .ion-edit').parent().removeClass('btn-info');
            $('.panel-heading .ion-eye').parent().removeClass('btn-primary');
            $('.panel-heading .ion-clipboard').parent().addClass('btn-purple');
            $('.panel-heading .ion-document').parent().removeClass('btn-warning');
            $('.panel-heading .ion-alert-circled').parent().removeClass('btn-danger');
            $('.panel-heading .ion-checkmark').parent().addClass('btn-success');
        }
        if (table.length == 1) {
            $('.panel-heading .ion-close').parent().addClass('btn-danger');
            $('.panel-heading .ion-unlocked').parent().addClass('btn-danger');
            $('.panel-heading .ion-edit').parent().addClass('btn-info');
            $('.panel-heading .ion-eye').parent().addClass('btn-primary');
            $('.panel-heading .ion-document').parent().addClass('btn-warning');
            $('.panel-heading .ion-alert-circled').parent().addClass('btn-danger');
        }
        if (table.length <= 0) {
            $('.panel-heading .ion-edit').parent().removeClass('btn-info');
            $('.panel-heading .ion-eye').parent().removeClass('btn-primary');
            $('.panel-heading .ion-trash-a').parent().removeClass('btn-danger');
            $('.panel-heading .ion-close').parent().removeClass('btn-danger');
            $('.panel-heading .ion-unlocked').parent().removeClass('btn-danger');
            $('.panel-heading .ion-clipboard').parent().removeClass('btn-purple');
            $('.panel-heading .ion-document').parent().removeClass('btn-warning');
            $('.panel-heading .ion-checkmark').parent().removeClass('btn-success');
            $('.panel-heading .ion-alert-circled').parent().removeClass('btn-danger');
        }
        ;
        return table.length;
    }
