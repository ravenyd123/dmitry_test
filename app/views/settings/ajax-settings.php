<? include_once $_SERVER['DOCUMENT_ROOT'].'/assets/functions.php';
   include_once 'model/settings.php';
// Выбираем платформу с которой будем работать
if (isset($_POST['platform']) ) {
	$platform = $_POST['platform'];
}else{
    if(isset($_COOKIE['platform'])) {
        $cookie_platform = $_COOKIE['platform'];
        $platform_arr = explode(',', $cookie_platform);
        $platform = reset($platform_arr);
    } else {
        echo "У вас не установлена ни одна площадка";
    }   
}

// Выбираем критичность текущей платформы
$settings = $db->query("SELECT platform.id, platform.name, platform.criticality 
                                                    FROM `platform` AS platform
                                                    WHERE platform.id = ".$platform)->fetch_assoc();

?>

<hr>

<form action="" id="settings" class="m-t-15" methos="POST">

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label for="color">Настройки площадки:</label>
                <select class="select2" name="select-platform" onchange="changePlatform(event);">
                    <? $platforms = getPlatformsFromUser($_COOKIE['id']); ?>
                    <? foreach ($platforms as $key => $value) { ?>
                        <option value="<?=$platforms[$key]["id"];?>" 
                        <?
                            if (isset($_POST['platform'])) {
                                echo ( $platforms[$key]["id"] == $_POST['platform'] ? 'selected=""' : '' );
                            }
                        ?>
                        ><?=$platforms[$key]["name"];?></option>
                    <? } ?>

                </select>
                <div class="help-block"></div>
            </div>
        </div> 
        <div class="col-md-offset-2 col-md-4">
            <div class="form-group m-t-25">
                <input type="button" data-action="saveSettings" class="btn btn-primary w-100" value="Сохранить настройки">
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label class="control-label">Выберите степень критичности оценки площадки:</label>
                <div class="">
                    <div class="radio">
                        <label class="cr-styled" for="criticality1">
                            <input type="radio" id="criticality1" name="criticality" value="3"
                            <?=( $settings['criticality'] == 3 ? 'checked="checked"' : '' );?>
                            >
                            <i class="fa"></i> В самооценке используются только сильно критичные требования </label>
                    </div>
                    <div class="radio">
                        <label class="cr-styled" for="criticality2">
                            <input type="radio" id="criticality2" name="criticality" value="2"
                            <?=( $settings['criticality'] == 2 ? 'checked="checked"' : '' );?>
                            >
                            <i class="fa"></i> В самооценке используются сильно критичные и средне критичные требования </label>
                    </div>
                    <div class="radio">
                        <label class="cr-styled" for="criticality3">
                            <input type="radio" id="criticality3" name="criticality" value="1"
                            <?=( $settings['criticality'] == 1 ? 'checked="checked"' : '' );?>
                            >
                            <i class="fa"></i> В самооценке используются только средне критичные требования </label>
                    </div>
                    <div class="radio">
                        <label class="cr-styled" for="criticality4">
                            <input type="radio" id="criticality4" name="criticality" value="4"
                            <?=( $settings['criticality'] == 4 || is_null($settings['criticality']) ? 'checked="checked"' : '' );?>
                            >
                            <i class="fa"></i> В самооценке используются все требования </label>
                    </div>
                </div>
            </div>
        </div> 
    </div>

</form>

<script>

// Сохранить настройки
$('*[data-action="saveSettings"]').click(function(){

    $.ajax({
        type: 'POST',
        url: "/assets/functions.php",
        cache: false,
        data: {
            "action": "saveSettings",
            "platform": $('select[name="select-platform"] option:selected').val(),
            "criticality": $("input[name='criticality']:checked").val()
        },
        success: function (msg) {
            console.log(msg);
            $.notify({'type':'success', 'message':'Настройки предприятия успешно сохранены'});
            // console.log(msg);
            location.reload();
        }
    });

})
</script>