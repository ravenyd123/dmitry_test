<? include_once $_SERVER['DOCUMENT_ROOT'].'/app/views/header.php'; ?>
<? include_once $_SERVER['DOCUMENT_ROOT'].'/app/views/template.php'; ?>

<!--Main Content -->
<section id="settings" class="content">
    <!-- Page Content -->
    <div class="wraper container-fluid">
	
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default p-0">

                    <ul class="nav nav-tabs">
                        <li class="">
                            <a href="#main_tab" data-toggle="tab" aria-expanded="false">
                                <span class="visible-xs"><i class="fa fa-home"></i></span>
                                <span class="hidden-xs">Основные</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="#setting_tab" data-toggle="tab" aria-expanded="false">
                                <span class="visible-xs"><i class="fa fa-home"></i></span>
                                <span class="hidden-xs">Настройки площадок</span>
                            </a>
                        </li>
                        <li class="active">
                            <a href="#profile_tab" data-toggle="tab" aria-expanded="true">
                                <span class="visible-xs"><i class="fa fa-user"></i></span>
                                <span class="hidden-xs">Пользователи</span>
                            </a>
                        </li>
                        <? if ($role == 'admin') { ?>
                            <li class="">
                                <a href="#planform_tab" data-toggle="tab" aria-expanded="false">
                                    <span class="visible-xs"><i class="fa fa-envelope-o"></i></span>
                                    <span class="hidden-xs">Площадки</span>
                                </a>
                            </li>                       
                        <? } ?>
                         
                    </ul>
                    <div class="tab-content">

                        <div class="tab-pane" id="setting_tab">
                            <div>
                                <? include_once $_SERVER['DOCUMENT_ROOT'].'/app/views/settings/ajax-settings.php'; ?>
                            </div>
                        </div>

                        <div class="tab-pane active" id="profile_tab">
                            <? include $_SERVER['DOCUMENT_ROOT']."/app/views/settings/ajax-user.php"; ?>
                        </div>

                        <div class="tab-pane" id="main_tab">
                            
                                <? include_once $_SERVER['DOCUMENT_ROOT']."/app/views/settings/main-settings.php"; ?>
                            
                        </div>

                        <? if ($role == 'admin') { ?>
                            <div class="tab-pane" id="planform_tab">
                                <form id="list" method="POST" action="">
                                    <? include $_SERVER['DOCUMENT_ROOT']."/app/views/settings/ajax-platform.php"; ?>
                                </form>
                            </div>
                        <? } ?>

                    </div>

                </div>
            </div><!-- .col-md-12 -->
        </div><!-- .row -->
    </div><!-- .wrapper -->
</section>

<? include 'footer.php'; ?>

<script src="/app/views/settings/js/profil_func.js"></script>
