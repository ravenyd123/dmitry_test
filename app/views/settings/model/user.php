<?php
include_once $_SERVER['DOCUMENT_ROOT'] . '/assets/functions.php';
$db = Db::Connection();
# Добавление/редактирование пользователя
if ($_POST['action'] == 'updateUsers') {

    $data = explode('&', $_POST['data']);

    // Преобразуем в массив для работы
    foreach ($data as $key => $value) {

        $parts = explode('=', $value);

        $conditions[$parts[0]] = urldecode($parts[1]);

        if ($parts[0] == 'platform') {
            $platform .= $parts[1] . ",";
        }

    }

    $conditions['access'] = $_POST['access'];

    $operation = $conditions['operation'];
    unset($conditions['operation']);

    // Проверяем есть ли такой Логин
    $user = $db->query("SELECT `id` FROM `users` WHERE `login` = '" . $conditions['login'] . "'");

    if ($operation == 'add') {

        $email = $db->query("SELECT `id` FROM `users` WHERE `email` = '" . $conditions['email'] . "'");

        // Проверяем есть ли уже такой логин
        if (mysqli_num_rows($user) > 0) {
            header('HTTP/1.1 403 Forbidden');
            $status['type'] = 'error';
            $status['message'] = 'Операция не выполнена';
            $status['message2'] = 'Данный логин уже используется в системе';
            print_r(json_encode($status, JSON_UNESCAPED_UNICODE));
            return;
        }

        // Проверяем есть ли уже такой email
        if (mysqli_num_rows($email) > 0) {
            header('HTTP/1.1 403 Forbidden');
            $status['type'] = 'error';
            $status['message'] = 'Операция не выполнена';
            $status['message2'] = 'Данный email уже используется в системе';
            print_r(json_encode($status, JSON_UNESCAPED_UNICODE));
            return;
        }
        // Проверяет существует ли домен у такого email
        $domen_of_mail = substr($conditions['email'], strrpos($conditions['email'], "@") + 1);
        if (checkdnsrr($domen_of_mail, "MX") == false) {
            header('HTTP/1.1 403 Forbidden');
            $status['type'] = 'error';
            $status['message'] = 'Операция не выполнена';
            $status['message2'] = 'Не правильный email. ' . $domen_of_mail . ' не существует';
            print_r(json_encode($status, JSON_UNESCAPED_UNICODE));
            return;
        }
    }

    $conditions['platform'] = trim($platform, ',');
    $conditions['company'] = $_COOKIE['company'];
    // $conditions['status'] = 3;
    // $conditions['level'] = "user";
    setcookie("platform", $conditions['platform'], time() + 3600 * 24, "/");

    foreach ($conditions as $key => $value) {
        $string .= " `" . $key . "`='" . $value . "',";
    }

    $string = trim($string, ',');

    if (mysqli_num_rows($user) == 0) {
        $password = substr(uniqid(), 0, 7);
        $hash = md5(md5($password));
        $string .= ", `password`='" . $hash . "'";
        $q = "INSERT INTO `users` SET `status`=3, `level`='auditor', " . $string;
        $query = $db->query($q);
        $id_insert = $db->insert_id;
        $namePlatform = getPlatformsNames(GetPlatformsByFirstLevel($id_insert));
        $replace = [$conditions['email'] => [
            "{username}" => $conditions['name']." ".$conditions['father_name'],
            "{firstplatform}" => $namePlatform['name'],
            "{login}" => $conditions['login'],
            "{password}" => $password
        ],"info@sunkt.ru"=>[
            "{username}" => $conditions['name'],
            "{login}" => $conditions['login'],
            "{password}" => $password
        ]
        ];
        send_pass($replace);
       // sendPassword($conditions['name'], $conditions['login'], $conditions['email'], $password, 'create');
       // sendPassword($conditions['name'], $conditions['login'], 'info@sunkt.ru', $password, 'create');

    } else {
        $qv = "UPDATE `users` SET " . $string . " WHERE `login`='" . $conditions['login'] . "'";
        
        $query = $db->query($qv);
        
    }

    //var_dump($query);
    if ($query == true) {
        // Успешное выполнение
        $status['type'] = 'success';
        $status['message'] = 'Операция выполнена';
        $status['message2'] = 'Информация пользователя изменена';
        print_r(json_encode($status, JSON_UNESCAPED_UNICODE));
        return;
    }
}

# Удаление пользователей
if ($_POST['action'] == 'removeUsers') {
    $idArr = $_POST['ids'];
    $db->query("DELETE FROM `users` WHERE `id` IN ($idArr)");
}

//# Получение пользователей
//if ($_POST['action'] == 'getUsers') {
//    $userInfo = $db->query( "SELECT * FROM `users` WHERE `company`= '" . $_COOKIE['company'] . "'");
//    while ($uRow = mysqli_fetch_array($userInfo)) {
//        $fio[] = $uRow['surname'] . ' ' . $uRow['name'] . ' ' . $uRow['father_name'];
//    }
//    echo json_encode($fio, JSON_UNESCAPED_UNICODE);
//}