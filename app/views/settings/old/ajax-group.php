<? include_once $_SERVER['DOCUMENT_ROOT'].'/assets/functions.php'; ?>
<div class="row">
  <div class="obj">
    <div class="col-md-10">
      <div class="form-group">
        <input class="form-control" type="text" name="name" value="" >
        <div class="help-block"></div>
      </div>
    </div>  
    <div class="col-md-2 form-inline">
      <div class="form-group m-5">
        <a href="javascript:void(0);" onclick="addObj()">
          <i class="fa ion-plus text-success"></i>
        </a>
      </div>
    </div>
    <input type="hidden" name="table" value="teams" >
  </div>
  <? $result = mysqli_query("SELECT * FROM `teams` ");
  while ($row = mysqli_fetch_assoc($result)) { ?>
  <div class="obj" data-id="<?=$row['id'];?>">
    <div class="col-md-10">
      <div class="form-group">
        <input class="form-control" type="text" name="name" value="<?=$row['name'];?>" >
      </div>
    </div>  
    <div class="col-md-2 form-inline">
      <div class="form-group m-5">
        <? if ( $access['standart-editor']['update'] == 1 ) { ?>
        <a href="javascript:void(0);" onclick="saveObj(<?=$row['id'];?>)">
          <i class="fa ion-checkmark text-success"></i>
        </a>
        <? } ?>
        <div class="help-block"></div>
      </div>
      <div class="form-group m-5">
        <? if ( $access['standart-editor']['delete'] == 1 ) { ?>
        <a href="javascript:void(0);" onclick="saveObj(<?=$row['id'];?>,'removeObj');">
          <i class="fa ion-close text-danger"></i>
        </a>
        <? } ?>
        <div class="help-block"></div>
      </div>
    </div>  
    <input type="hidden" name="table" value="teams" >
  </div>            
  <? } ?>
</div>