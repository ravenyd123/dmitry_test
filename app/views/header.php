<? include_once $_SERVER['DOCUMENT_ROOT'] . '/assets/DB.php'; ?>
<? include_once $_SERVER['DOCUMENT_ROOT'] . '/assets/functions.php'; ?>
<? include_once $_SERVER['DOCUMENT_ROOT'] . '/assets/all_const.php'; ?>
    <!DOCTYPE html>
    <html lang="ru">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Digital Compliance">
    <meta name="author" content="ДИС">

    <link rel="shortcut icon" href="/assets/admina/img/icon.png">

    <link rel="stylesheet" href="/assets/admina/css/my_menu.css">
    <!--Icon fonts css-->

    <link href="/vendor/driftyco/ionicons/css/ionicons.min.css" rel="stylesheet"/>
    <link href="/node_modules/font-awesome/css/font-awesome.min.css" rel="stylesheet" media="all"/>


    <!-- Bootstrap CSS -->
    <link href="/node_modules/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="/assets/admina/css/bootstrap-reset.css" rel="stylesheet">

    <!--bootstrap-wysihtml5 -->
    <link rel="stylesheet" href="/node_modules/bootstrap3-wysihtml5-bower/dist/bootstrap3-wysihtml5.css"/>
 

    <!-- Basic Plugins -->
    <script src="/node_modules/jquery/dist/jquery.min.js"></script>
    <script src="/node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
<!--    <script src="/node_modules/pace/pace.js"></script>-->

    <!-- jQuery UI -->
    <link rel="stylesheet" href="/node_modules/jquery-ui-dist/jquery-ui.min.css"/>
    <script type="text/javascript" src="/node_modules/jquery-ui-dist/jquery-ui.min.js"></script>
    <!--    Datepicker -->



    <link rel="stylesheet" type="text/html" href="/node_modules/bootstrap-datepicker/dist/css/bootstrap-datepicker3.standalone.min.css"/>
    <script type="text/javascript" src="/node_modules/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript" src="/node_modules/bootstrap-datepicker/dist/locales/bootstrap-datepicker.ru.min.js"></script>
    <!-- jQuery cookie -->
    <script type="text/javascript" src="/node_modules/jquery.cookie/jquery.cookie.js"></script>

<!--     Vue.js -->
<!--     <script src="/node_modules/vue/dist/vue.min.js"></script> -->

    <!-- Offline JS -->
    <!--    <script type="text/javascript" src="/assets/offline/offline.min.js"></script>-->
    <!--    <link rel="stylesheet" href="/assets/offline/offline-theme-chrome.css">-->
    <!--    <link rel="stylesheet" href="/assets/offline/offline-language-russian.css">-->

    <!-- Notification -->
<!--    <link href="/assets/admina/plugins/notifications/notification.css" rel="stylesheet"/>-->
<!--    <script src="/assets/admina/plugins/notifications/notify.min.js"></script>-->
<!--    <script src="/assets/admina/plugins/notifications/notify-metro.js"></script>-->
<!--    <script src="/assets/admina/plugins/notifications/notifications.js"></script>-->

    <!-- DataTables -->
    <script src="/vendor/datatables/datatables/media/js/jquery.dataTables.min.js"></script>


    <script src="/vendor/datatables/datatables/media/js/dataTables.bootstrap.min.js"></script>
    <link href="/vendor/datatables/datatables/media/css/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>

    <script type="text/javascript" src="/vendor/drmonty/datatables-buttons/js/dataTables.buttons.js"></script>
    <script type="text/javascript" src="/vendor/drmonty/datatables-buttons/js/buttons.bootstrap.min.js"></script>

    <link rel="stylesheet" type="text/css" href="/vendor/drmonty/datatables-buttons/css/buttons.bootstrap.min.css"/>
    <script type="text/javascript" src="/vendor/drmonty/datatables-buttons/js/buttons.colVis.min.js"></script>
    <!--
     <link href="/assets/admina/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
     <script src="/assets/admina/plugins/datatables/jquery.dataTables.min.js"></script>-->
    <!--    ROWGROUP TABLE-->
    <!--    <script src="/assets/admina/plugins/datatables/js/dataTables.rowGroup.min.js"></script>-->
    <!--    <script src="/assets/admina/plugins/datatables/js/rowGroup.bootstrap.min.js"></script>-->
    <!--    <link href="/assets/admina/plugins/datatables/css/rowGroup.bootstrap4.min.css" rel="stylesheet" type="text/css"/>-->

    <!--FIX HEADER TABLE -->
    <!--    <script src="/assets/admina/plugins/datatables/js/dataTables.fixedHeader.min.js"></script>-->
    <!--    <link href="/assets/admina/plugins/datatables/css/fixedHeader.bootstrap.min.css" rel="stylesheet"/>-->

    <!--    <script src="/assets/FixedHeader/js/dataTables.fixedHeader.min.js"></script>-->
    <!--    <link href="/assets/FixedHeader/css/da" rel="stylesheet"/>-->

    <!-- Select2 -->
    <script src="/node_modules/select2/select2.js"></script>
    <link href="/node_modules/select2/select2.css" rel="stylesheet"/>
    <link href="/node_modules/select2/select2-bootstrap.css" rel="stylesheet"/>


    <!-- <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script> -->

    <!--Morris Chart CSS -->
    <!--    <link rel="stylesheet" href="/assets/admina/plugins/morris/morris.css">-->

    <!-- Custom styles -->
    <link href="/assets/admina/css/style.css" rel="stylesheet"/>
    <link href="/assets/admina/css/helper.css" rel="stylesheet"/>
    <link href="/assets/admina/css/style-responsive.css" rel="stylesheet"/>
    <!-- <link rel="stylesheet" href="/assets/main.css">-->

    <!-- Main js -->
    <script src="/assets/admina/js/app.js"></script>

    <!--    <script src="/assets/admina/plugins/jquery-multi-select/jquery.multi-select.js"></script>-->
    <!--Drag and drop uploads files-->
    <!--    <link href="/assets/admina/plugins/dropzone/dropzone.css" rel="stylesheet" type="text/css"/>-->
    <!--    <script src="/assets/admina/plugins/dropzone/dropzone.js"></script>-->
    <? if (!isset($_GET['type'])) {
        echo "<title>Well Compliance</title>";
    } ?>

    <!-- Для работа с файлами-->
    <link href="/node_modules/bootstrap-fileinput/css/fileinput.min.css" media="all" rel="stylesheet"/>
    <script src="/node_modules/bootstrap-fileinput/js/fileinput.js" type="text/javascript"></script>
    <script src="/node_modules/bootstrap-fileinput/js/locales/ru.js" type="text/javascript"></script>

    <script src="/node_modules/bootstrap-fileinput/themes/gly/theme.js" type="text/javascript"></script>

    <!--bootstrap-summernote-->
    <script src="/node_modules/summernote/dist/summernote-bs4.min.js"></script>
    <link href="/node_modules/summernote/dist/summernote-bs4.css" rel="stylesheet"/>
    <!--Bootbox alert-->
    <script src="/node_modules/bootbox/bootbox.min.js"></script>


    <!--  Поиск-->
    <script src="/app/views/search/search.js"></script>

    <!-- Нотификация -->
    <script type="text/javascript" src="/node_modules/bootstrap-notify/bootstrap-notify.min.js"></script>
    
</head>
<? include $_SERVER['DOCUMENT_ROOT'] . '/assets/modals.php'; ?>