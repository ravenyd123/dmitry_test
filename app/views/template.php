<!-- Header -->
<header class="top-head container-fluid navbar-fixed-top">
    <!-- logo -->
    <div class="logo hidden-xs">
        <a href="/tasks" class="logo-expanded img-responsive"><img src="/img/well_white_logo.png" alt="logo"
                                                                   style="width: 40px; height: 40px">
            <span class="nav-text">Well Compliance</span>
        </a>
    </div>
    <!-- end logo -->
    <button type="button" class="navbar-toggle pull-left">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-toggle ion-navicon-round"></span>
    </button>

    <div role="search" class="navbar-left app-search pull-left hidden-xs w-30 ">
        <input type="text" class="form-control search" id="context" value="" maxlength="400" autocomplete="on"
               placeholder="введите фразу для поиска" style="padding-right: 80px;">

        <div class="btn-group grp_search ">
            <button type="button" class="btn btn-effect-ripple btn_search"><b>Поиск</b></button>
            <button type="button" class="btn dropdown-toggle dropdown-toggle-split btn_search_select"
                    data-toggle="dropdown" aria-expanded="false">
                <i class="caret"></i>
            </button>
            <ul class="dropdown-menu btn_search_select_index" role="menu">
                <li class="active"><a class="dropdown-item" name="rt_document">Документ</a></li>
                <li><a class="dropdown-item" name="demand">Требования</a></li>
                <li><a class="dropdown-item" name="lnd">ЛНД</a></li>
            </ul>
        </div>
    </div>

    <!--    <div class="loader"></div>-->
    <!-- Right navbar -->
    <ul class="list-inline navbar-right top-menu top-right-menu">
        <!-- Messages -->

        <!-- <li class="dropdown">
            <a data-toggle="dropdown" class="dropdown-toggle" href="#"> <i class="fa fa-envelope-o "></i> <span class="badge badge-sm up bg-purple count">4</span> </a>
            <ul class="dropdown-menu extended fadeInUp animated nicescroll" tabindex="5001">
                <li>
                    <p>
                        Сообщения
                    </p>
                </li>
                <li>
                    <a href="#"> <span class="pull-left"><img src="/assets/admina/img/avatar-2.jpg" class="img-circle thumb-sm m-r-15" alt="img"></span> <span class="block"><strong>Иванов Иван</strong></span> <span class="media-body block">Прислал вам сообщение
                        <br>
                        <small class="text-muted">3 минуты назад</small></span> </a>
                </li>
                <li>
                    <a href="#"> <span class="pull-left"><img src="/assets/admina/img/avatar-3.jpg" class="img-circle thumb-sm m-r-15" alt="img"></span> <span class="block"><strong>Иванов Иван</strong></span> <span class="media-body block">Прислал вам сообщение
                        <br>
                        <small class="text-muted">15 минуты назад</small></span> </a>
                </li>
                <li>
                    <a href="#"> <span class="pull-left"><img src="/assets/admina/img/avatar-4.jpg" class="img-circle thumb-sm m-r-15" alt="img"></span> <span class="block"><strong>Иванов Иван</strong></span> <span class="media-body block">Отправил вам новый файл
                        <br>
                        <small class="text-muted">20 минуты назад</small></span> </a>
                </li>
                <li>
                    <a href="#"> <span class="pull-left"><img src="/assets/admina/img/avatar-5.jpg" class="img-circle thumb-sm m-r-15" alt="img"></span> <span class="block"><strong>Ray Shannon</strong></span> <span class="media-body block">Отправил вам подарок
                        <br>
                        <small class="text-muted">20 минуты назад</small></span> </a>
                </li>
                <li>
                    <p class="text-center">
                        <a href="inbox.html">Все сообщения</a>
                    </p>
                </li>
            </ul>
        </li> -->

        <!-- End messages -->

        <!-- Notification(Near Miss) -->

        <li class="dropdown">
            <a target="_blank" href="/reports/matrix?type=calendar&mode=2&user=<?= $_COOKIE['login']; ?>"> <i
                        class="fa ion-calculator"></i> <span
                        class="badge badge-sm up bg-pink count"><?= $notification->getMyNearMissNotice($_COOKIE['login']); ?></span>
            </a>
        </li>

        <!-- Notification(calendar) -->
        <li class="dropdown">
            <a target="_blank" href="/reports/matrix?type=calendar&user=<?= $_COOKIE['login']; ?>"> <i
                        class="fa fa-calendar-o"></i> <span
                        class="badge badge-sm up bg-pink count"><?= $notification->getCalendarNotice($_COOKIE['login']); ?></span>
            </a>
        </li>
        <!-- End Notification -->

        <!-- Notification(tasks) -->
        <!--<li class="dropdown">
            <a href="/tasks"> <i class="fa fa-bell-o"></i> <span
                        class="badge badge-sm up bg-pink count"><?= $notification->getNotice($_COOKIE['login']); ?></span>
            </a>
        </li> -->
        <!-- End Notification -->

        <!-- User Menu Dropdown -->
        <li class="dropdown text-center">

            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                <span class="username"><? user_header(); ?> </span> <span class="caret"></span> </a>
            <ul class="dropdown-menu extended pro-menu fadeInUp animated" tabindex="5003"
                style="overflow: hidden; outline: none;">
                <!-- <li>
                    <a href="#" ><i class="fa fa-briefcase"></i>Профиль</a>
                </li>
                <li>
                    <a href="#"><i class="fa fa-cog"></i> Настройки</a>
                </li> -->
                <li>
                    <a href="/logout"><i class="fa fa-sign-out"></i> Выход</a>
                </li>
            </ul>
        </li>
        <!-- End User Menu Dropdown -->
    </ul>
    <!-- End Right Navbar -->

</header>
<!-- End Header -->

<!-- Aside Menu -->
<aside class="left-panel">

    <!-- Navbar -->
    <nav class="navigation">
        <ul class="list-unstyled">

            <? if (in_array("R", $access['tasks'])) { ?>
                <li id="t_tasks">
                    <a href="/tasks"><i class="ion-home"></i> <span class="nav-label">Рабочий стол</span></a>
                </li>
            <? } ?>

            <!--            --><? // if (in_array("R", $access['register'])) { ?>
            <!--                <li id="" class="has-submenu">-->
            <!--                    <a href="#">-->
            <!--                        <i class="fa ion-clipboard m-t-5"></i>-->
            <!--                        <span class="nav-label">Нормативно-техническая документация</span>-->
            <!--                        <span class="caret pull-right"></span>-->
            <!--                    </a>-->
            <!--                    <ul class="list-unstyled">-->
            <!--                        <li>-->
            <!--                            <a href="#">Локальная нормативная документация</a>-->
            <!--                        </li>-->
            <!--                        <li>-->
            <!--                            <a target="_blank" href=-->
            <? //= Sourse_conn ?><!--Нормативно-техническая документация</a>-->
            <!--                        </li>-->
            <!--                    </ul>-->
            <!--                </li>-->
            <!--            --><? // } ?>
            <? if (in_array("R", $access['reports'])) { ?>
                <li id="t_analytics">
                    <a href="/nearmiss"><i class="fa ion-flame"></i> <span class="nav-label">Near Miss
                            <?
                            $a = $notification->getNearMissNotice();
                            if ($a <> '') { ?>
                                <div
                                        class="badge badge-sm up-menu bg-pink text-right"> <?= $a; ?></div> <? } ?>
            </span></a>
                </li>
            <? } ?>
            <? if (in_array("R", $access['reports']) == 1 || in_array("R", $access['calendar'])) { ?>
                <li id="t_reports" class="has-submenu">
                    <a href="#">
                        <i class="ion-settings m-t-5"></i>
                        <span class="nav-label">Платформа<br>мониторинга</span>
                        <span class="caret pull-right"></span>
                    </a>
                    <ul class="list-unstyled">
                        <!--					--><? // if ( in_array("C", $access['reports']) ){ ?>
                        <!--					<li id="t_add">-->
                        <!--						<a href="/reports/add">Создать чек-лист</a>-->
                        <!--					</li>-->
                        <!--					--><? // } ?>
                        <?
                        if (in_array("R", $access['reports'])) { ?>
                            <li id="t_all">
                                <a href="/reports/all">Список чек-листов</a>
                            </li>
                        <? } ?>
                        <? if (in_array("R", $access['calendar'])) { ?>
                            <li id="t_calendar">
                                <a href="/reports/calendar">Календарь мероприятий</a>
                            </li>
                        <? } ?>
                    </ul>
                </li>
            <? } ?>

            <? if (in_array("R", $access['analytics'])) { ?>
                <li id="t_analytics">
                    <a href="/analytics"><i class="fa ion-stats-bars"></i> <span class="nav-label">Аналитика</span></a>
                </li>
            <? } ?>

            <? if (in_array("R", $access['register'])) { ?>
                <li id="t_register" class="has-submenu">
                    <a href="#">
                        <i class="ion-funnel m-t-5"></i>
                        <span class="nav-label">Реестры</span>
                        <span class="caret pull-right"></span>
                    </a>
                    <ul class="list-unstyled">
                        <li>
                            <a href="/register">Реестр документов</a>
                        </li>
                        <li>
                            <a href="/register/opo">ОПО</a>
                        </li>
                        <li>
                            <a href="/register">ТУ</a>
                        </li>
                    </ul>

                </li>
            <? } ?>

            <? if (in_array("R", $access['editor'])) { ?>
                <li id="t_editor">
                    <a href="/demand/editor"><i class="ion-shuffle"></i> <span
                                class="nav-label">Редактор стандартов</span></a>
                </li>
            <? } ?>

            <? if (in_array("R", $access['demands'])) { ?>
                <li id="demand">
                    <a href="/demand"><i class="ion-network"></i> <span class="nav-label">Редактор требований</span></a>
                </li>
            <? } ?>

            <? if (in_array("R", $access['settings'])) { ?>
                <li id="settings">
                    <a href="/settings"><i class="ion-gear-b "></i> <span class="nav-label">Настройки</span></a>
                </li>
            <? } ?>

            <? if (in_array("R", $access['settings'])) { ?>
                <li id="statistics">
                    <a href="/statistics"><i class="ion-arrow-graph-down-right"></i> <span
                                class="nav-label">Статистика</span></a>
                </li>
            <? } ?>


            <? if (in_array("R", $access['settings'])) { ?>
                <li id="t_analytics">
                    <a href="/feedback"><i class="fa  fa-comments"></i> <span class="nav-label">Обратная связь
                            <?
                            $a = $notification->feedBack();
                            if ($a <> '') { ?>
                                <div
                                        class="badge badge-sm up-menu bg-pink text-right"> <?= $a; ?></div> <? } ?>
            </span></a>
                </li>
            <? } ?>




            <? if (in_array("R", $access['help'])) { ?>
                <li id="help">
                    <a href="/help"><i class="ion-help-circled"></i> <span class="nav-label">Помощь</span></a>
                </li>
            <? } ?>

        </ul>
    </nav>
    <!-- End Navbar -->
    <div class="logo hidden-xs" style="position: fixed; text-align: center; bottom: 20px; width: inherit;">

        <a href="" class="img-responsive"><img src="/img/isupb_logo.png" style="width: 170px" alt="logo">
            <span class="nav-text"></span> </a>
    </div>

</aside>


<!-- End Aside -->
