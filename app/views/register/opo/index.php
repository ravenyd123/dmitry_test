<? include_once $_SERVER['DOCUMENT_ROOT'] . '/assets/functions.php';
 include_once $_SERVER['DOCUMENT_ROOT'] . '/app/views/header.php';
 include_once $_SERVER['DOCUMENT_ROOT'] . '/app/views/template.php';
// include_once $_SERVER['DOCUMENT_ROOT'] . '/assets/class.php';?>
<link href="opo.css" rel="stylesheet">
<section class="content">

    <!-- Page Content -->

    <div class="wraper container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title pull-left m-t-10">ОПО</h3>
                        <div class="top_nav">
                            <? if (in_array("C", $access['tasks'])) { ?>
                                <a href="#" data-target="#reg_opo" data-toggle="modal">
                                    <button class="btn btn-success" data-action="add_data_opo"><i class="ion-plus"></i>
                                    </button>
                                </a><br>
                            <? } ?>
                            <? if (in_array("U", $access['tasks'])) { ?>
                                <button class="btn btn-default" data-action="edit_data_opo"><i class="ion-edit"></i>
                                </button><br>
                            <? } ?>
                            <? if (in_array("D", $access['tasks'])) { ?>
                                <button class="btn btn-default" data-action="remove_data_opo"><i
                                            class="ion-trash-a"></i></button>
                            <? } ?>

                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <table id="table_task_opo" class="table table-hover table-striped table-bordered">
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <!-- End Row -->

    </div>

</section>
<script src="/app/views/register/opo/opo.js"></script>
