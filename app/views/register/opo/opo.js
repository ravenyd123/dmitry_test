$(document).ready(function () {
    var id = 1; ///$('input[name="task-id"]').val();
    var table = $('#table_task_opo').dataTable({
        "scrollX": true,
        "paging": true,
        "searching": true,
        "scrollY": "100%",
        "scrollCollapse": true,
        /*"rowCallback": function( row, data ) {
         if ( $.inArray(data.DT_RowId, selected) !== -1 ) {
         $(row).addClass('selected');
         }
         },*/
        "createdRow": function (row, data, dataIndex) {

            /*   if (data['status'] === "В работе") {
             $(row).addClass('active');
             }
             if (data['status'] === "Выполнено") {
             $(row).addClass('success');
             }
             if (data['status'] === "Не выполнено") {
             $(row).addClass('danger');
             }*/
            /*  $(row).find('td').eq(5).append('<ul class="list-inline"><li>' +
             '<a href="#" id="edit_subtask" onclick= ajax_task(' + data['id'] + ',this)>' +
             '<span class="glyphicon glyphicon-pencil"></a>' +
             '</li><li>' +
             '<a id="delete_subtask" onclick= ajax_task(' + data['id'] + ',this) >' +
             '<span class="glyphicon glyphicon-trash"></a>' +
             '</li></ul>');*/
        },
        ajax: {
            url: "/app/views/register/opo/funcopo.php",
            data: {"datatable": "1", "id": id},
            dataType: "json",
            cache: false,
            contentType: "application/json; charset=utf-8",
            dataSrc: function (json) {
                return json.opo;
            },
        },
        columnDefs: [
            {"title": "№", "targets": [0], "className": "text-center", "data": "id"},
            {"title": "Имя", "targets": [1], "className": "text-center", "data": "name"},
            {"title": "Класс опасности", "targets": [2], "className": "text-center", "data": "hazard_class"},
            {"title": "Рег. №", "targets": [3], "className": "text-center", "data": "reg"},
            {"title": "Статус", "targets": [4], "className": "text-center", "data": "status"},
            {"title": "Отв. за экспуатацию", "targets": [5], "className": "text-center", "data": "resp_operat"},
            {
                "title": "тв. за производственный контроль",
                "targets": [6],
                "className": "text-center",
                "data": "resp_prod_contr"
            },

            {"title": "Числ. сотр-ов", "targets": [7], "className": "text-center", "data": "worker"},
            {
                "title": "Дата очеред. страх-ия",
                "targets": [8],
                "className": "text-center",
                "data": "d_n_insurance"
            },
            {
                "title": "Дата пред. страх-ия",
                "targets": [9],
                "className": "text-center",
                "data": "d_p_insurance"
            },
            {
                "title": "Наименования СК",
                "targets": [10],
                "className": "text-center",
                "data": "name_sk"
            },
            {
                "title": "Дата очеред. гидр. исп-ия",
                "targets": [11],
                "className": "text-center",
                "data": "d_n_hydra_test"
            },
            {
                "title": "Дата очеред. тех. осв-ия",
                "targets": [12],
                "className": "text-center",
                "data": "d_n_tech_inspec"
            },
            {
                "title": "Дата пред. тех. осв-ия",
                "targets": [13],
                "className": "text-center",
                "data": "d_p_tech_inspec"
            },
            {"title": "Площадка", "targets": [14], "orderable": false, "data": "platform"},
        ],
        language: {
            "lengthMenu": "Выводить по _MENU_ записей",
            "zeroRecords": "Ни одной записи не найдено",
            "info": "Показано страниц _PAGE_ из _PAGES_",
            "infoEmpty": "No records available",
            "infoFiltered": "(отфильтровано из _MAX_ записей)",
            "search": "Поиск:",
            "paginate": {
                "first": "Первая",
                "last": "Последняя",
                "next": "Следующая",
                "previous": "Предыдущая"
            }
        }
    });

    /*    $("#create_subtask").on('click', function () {
     var id = $('input[name="task-id"]').val();
     ajax_task(id, this);
     });*/

    $("form").on("submit", function (event) {
        event.preventDefault();
        console.log(getFormData($(this)));
        var formName = event.target.id;
        console.log(formName);
       var data1 = getFormData($(this));

        data1["opo-action"] = formName;
       /* if (formName = "subtask") {
            var execut = $('select[name="subtask-executors"]').val();
            data1["subtask-executors"] = execut.join(',');
        }*/
        //data1["task-description"] = $("#" + formName + " textarea[name$='zadacha']").summernote('code');
        $.ajax({
            type: 'post',
            dataType: "text",
            contentType: "application/x-www-form-urlencoded;charset=utf-8",
            data: {edit: data1},
            url: "/app/views/register/opo/funcopo.php",
            cache: false,
            success: function (dataout) {
                console.log(dataout);
                /*    var task_table = $('#datatable-task').dataTable();
                 task_table.api().ajax.reload();
                 if (formName = "subtask") {
                 $('#createsubtask').modal('hide');
                 autoHideNotify('success', 'top right', 'Задача ', 'Сохранено успешно');
                 }*/
            },
            error: function () {
                $.notify({'type':'error', 'message':'Произошла ошибка!'});
            }
        })
    });
})
function getFormData($form) {
    var unindexed_array = $form.serializeArray();
    var indexed_array = {};

    $.map(unindexed_array, function (n, i) {
        indexed_array[n['name']] = n['value'];
    });

    return indexed_array;
}
/* <th>Наименование</th>
 <th>Класс<br>опасности</th>
 <th>Рег. №</th>
 <th>Статус</th>
 <th>Отв. за экспуатацию</th>
 <th>Отв. за производственный контроль</th>
 <th>Численность сотрудников</th>
 <th>Дата очеред. страх-ия</th>
 <th>Дата пред. страх-ия</th>
 <th>Наименования СК</th>
 <th>Дата очеред. гидр. исп-ия</th>
 <th>Дата пред. гидр. исп-ия</th>
 <th>Дата очеред. тех. осв-ия</th>
 <th>Дата пред. тех. осв-ия</th>
 <th>Площадка</th>


 <th>Наименование</th>
 <th>Тип/марка/модель</th>
 <th>Срок Экспл.</th>
 <th>Рег. №</th>
 <th>Инв. №</th>
 <th>Назначение</th>
 <th>ОПО</th>
 <th>Статус</th>
 <th>Дата очеред. исп-ия</th>
 <th>Дата пред. исп-ия</th>*/
