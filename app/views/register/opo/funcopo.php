<? include_once $_SERVER['DOCUMENT_ROOT'] . '/assets/functions.php';?>
<?php
function opo_all($id)
{
    $db = DB::Connection();
    $result = $db->query("SELECT * FROM opo ");
    $arr = ["opo" => []];
    while ($row = $result->fetch_assoc()) {
        $arr['opo'][] = [
            "id" => $row['id'],
            "name" => $row['name'],
            "status" => $row['status'],
            "hazard_class"=>$row['hazard_class'],
            "reg"=>$row['reg'],
            "resp_operat"=>$row['resp_operat'],
            "resp_prod_contr"=>$row['resp_prod_contr'],
            "worker"=>$row['worker'],
            "d_n_insurance"=>$row['d_n_insurance'],
            "d_p_insurance"=>$row['d_p_insurance'],
            "name_sk"=>$row['name_sk'],
            "d_n_hydra_test"=>$row['d_n_hydra_test'],
            "d_p_hydra_test"=>$row['d_p_hydra_test'],
            "d_n_tech_inspec"=>$row['d_n_tech_inspec'],
            "d_p_tech_inspec"=>$row['d_p_tech_inspec'],
            "platform"=>$row['platform'],
        ];
    }
    return $arr;
}
if (isset($_REQUEST['datatable'])) {
    //  var_dump($_GET['id']);
    //  $pid = 13;
    /*  $t = ["data" =>
          [
              [

                  "id" => "1",
                  "name" => "Подзадача_1",
                  "status" => "В роботе",
                  "date" => "28.01.1984",
                  "subtask" => "Пушкин А.А.",
                  "position2" => ""
              ],
              [

                  "id" => "2",
                  "name" => "Подзадача_2",
                  "status" => "В роботе",
                  "date" => "28.01.1984",
                  "subtask" => "Пушкин А.А.",
                  "position2" => ""
              ]
          ]
      ];*/

    echo json_encode(opo_all($_GET['id']));
}
if (isset($_REQUEST['edit'])) {
    $db = Db::Connection();
    $query = "";
    $where = "";
    $data = [];
    foreach ($_REQUEST['edit'] as $key => $value) {
        $key = preg_replace('/(\w+)?opo-/', "", $key);
        if ($key == "name" or $key == "description" or $key == "authority") {
            $value = '"' . htmlspecialchars(trim($value)) . '"';
        }
        if ($key == "deadline" or $key == "start_noty" or $key == "repeat" or $key == "repeat_task") {
            $value = '"' . date("Y.m.d", strtotime($value)) . '"';
        }
        if ($key == "director" or $key == "executors") {
            $value = '"' . $value . '"';
        }
        if ($key != "id" and $key != "action") {
            $query .= $key . "=" . $value . ",";
        }
        if ($key == "id") {
            $where .= " where id=" . $value;
        }
    }
    $query = trim($query, ',') . $where;
   // $result = $db->query("update tasks set {$query}");
    var_dump($query);
    //var_dump($db->affected_rows);
}
?>