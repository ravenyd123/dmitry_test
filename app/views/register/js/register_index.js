$(document).ready(function () {

    var table = $('#datatable').DataTable({
        fnRowCallback: function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            // Bold the grade for all 'A' grade browsers
            // console.log(  );
            $(nRow).attr('data-id', $(nRow).find('input[type="checkbox"]').attr('data-nd'));
            $(nRow).attr('data-company', $(nRow).find('input[type="checkbox"]').attr('data-company'));
        },
        drawCallback: function (settings) {
            $('#datatable tbody input[type="checkbox"]').change(function (event) {
                checkRowAmount();
            })
            $('#register .select2').select2();
        },
        initComplete: function () {
            // console.log(this);
            this.api().column(3).every(function () {
                var column = this;
                var select = $('<select class="form-control input-sm "><option value="">Любой статус документа</option></select>')
                    .appendTo($('#datatable-filter'))
                    .on('change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );

                        column
                            .search(val ? '^' + val + '$' : '', true, false)
                            .draw();
                    });

                column.data().unique().sort().each(function (d, j) {
                    select.append('<option value="' + d + '">' + d + '</option>')
                });
            });
            this.api().column(4).every(function () {
                var column = this;
                var select = $('<select class="form-control input-sm "><option value="">Все группы документов</option></select>')
                    .appendTo($('#datatable-filter'))
                    .on('change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );
                        column
                            .search(val ? val : '', true, false)
                            .draw();
                    });

                $.each(docsGroup, function (d, j) {
                    select.append('<option value="' + j + '">' + j + '</option>')
                });
            });
            this.api().column(5).every(function () {
                var column = this;
                var select = $('<select class="form-control input-sm "><option value="">Тип документа</option></select>')
                    .appendTo($('#datatable-filter'))
                    .on('change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );
                        column
                            .search(val ? '^' + val + '$' : '', true, false)
                            .draw();
                    });

                column.data().unique().sort().each(function (d, j) {
                    select.append('<option value="' + d + '">' + d + '</option>')
                });
            });
            if (company_coockie == 'all') {
                this.api().column(6).every(function () {
                    var column = this;
                    var select = $('<select class="form-control input-sm "><option value="">Предприятии</option></select>')
                        .appendTo($('#datatable-filter'))
                        .on('change', function () {
                            var val = $.fn.dataTable.util.escapeRegex(
                                $(this).val()
                            );
                            column
                                .search(val ? val : '', true, false)
                                .draw();
                        });

                    $.each(company, function (d, j) {
                        select.append('<option value="' + j + '">' + j + '</option>')
                    });
                });
            }
            /*this.api().column(5).every( function () {
             var column = this;
             var select = $('<select class="form-control input-sm m-l-10"><option value="">Все компании</option></select>')
             .appendTo( $('#datatable-filter') )
             .on( 'change', function () {
             var val = $.fn.dataTable.util.escapeRegex(
             $(this).val()
             );

             column
             .search( val ? '^'+val+'$' : '', true, false )
             .draw();
             } );

             column.data().unique().sort().each( function ( d, j ) {
             select.append( '<option value="'+d+'">'+d+'</option>' )
             } );
             } );*/
        },
        "deferRender": true,
        "ajax": "/register/ajax",
        "lengthMenu": [[50, 100, 250], [50, 100, 250]],
        /*"columnDefs": [ {
         "searchable": false,
         "orderable": false,
         "className": "firstColumn",
         "targets": 0
         } ],*/
        //"order": [[ 0, 'asc' ]],
        "ordering": false,
        "orderClasses": false,
        "language": {
            "lengthMenu": "Выводить по _MENU_ записей",
            "zeroRecords": "Ни одной записи не найдено",
            "info": "Показано страниц _PAGE_ из _PAGES_. Записей: _TOTAL_",
            "infoEmpty": "No records available",
            "infoFiltered": "(отфильтровано из _MAX_ записей)",
            "search": "Поиск:",
            "paginate": {
                "first": "Первая",
                "last": "Последняя",
                "next": "Следующая",
                "previous": "Предыдущая"
            },
        },
    });
    // Добавление записи
    $('*[data-action="add-data"]').click(function () {
  // Меняем заголовок
        $('#editRegisterModal .modal-header h4').text('Добавить документ');
        $('#editRegisterModal input[name="register-action"]').val('add');
        $('#editRegisterModal textarea[name="register-comment"]').val('');
        $('#editRegisterModal input[name="register-nd"]').val('');
        $('#editRegisterModal input[name="register-symbol"]').val('');
        // Чистим селекты
        $('select[name="register-group"]').select2().val('').trigger('change');
        $('select[name="register-statusdoc"]').select2().val('').trigger('change');
        $('select[name="register-type-docum"]').select2().val("docum").trigger('change');

    })
// Конвертим HtmlSpecialChars обратно
    function decodeHtml(text) {
        return text
            .replace(/&amp;/g, '&')
            .replace(/&lt;/, '<')
            .replace(/&gt;/, '>')
            .replace(/&quot;/g, '"')
            .replace(/&#039;/g, "'");
    }

    // Редактирование записи
    $('*[data-action="edit-data"]').click(function () {

        if (checkRowAmount() != 1) {
            $.notify({'type':'error', 'message':'Можно выбрать только одну запись!'});

            return false;
        }
        ;
        $('#editRegisterModal .modal-header h4').text('Редактировать документ');
        var value = $('#datatable tbody input[type="checkbox"]:checked').closest("tr").attr('data-id');

        var json = getNodeByField('register', 'nd', value);

        $('#editRegisterModal input[name="register-action"]').val('edit');

        $('#editRegisterModal textarea[name="register-comment"]').val(json.data[0]['comment']);
        $('#editRegisterModal input[name="register-nd"]').val(json.data[0]['nd']);
        var decode_symbol = decodeHtml(json.data[0]['symbol']);
        $('#editRegisterModal input[name="register-symbol"]').val(decode_symbol);

        // Группа документов
        var group = json['data'][0]['group'].split(',');
        $('select[name="register-group"]').val(group);
        $('select[name="register-group"]').trigger('change');

        var base_name = json['data'][0]['base_name'];
        $('select[name="register-type-docum"]').val(base_name).trigger('change');

        // Статус документа
        var statusdoc = json['data'][0]['status'];
        $('select[name="register-statusdoc"]').val(statusdoc);

        $('select[name="register-statusdoc"]').trigger('change');

        $('input[name^="company"]').each(function (index, item) {

            if (json.data[0]['company'].indexOf($(item).val()) != -1) {
                $(item).prop('checked', true);
            } else {
                $(item).prop('checked', false);
            }
            ;

        })

    })

    // Удаление записи
    $('*[data-action="remove-data"]').click(function () {

        if (checkRowAmount() < 1) {
            $.notify({'type':'error', 'message':'Необходимо выбрать хотя бы одну запись!'});
            return false;
        }
        ;

        confirm('info', 'top right', 'Документ(ы) и его требования будут удалены безвозвратно');

        $(document).on('click', '.notifyjs-metro-base .no', function () {
            $(this).trigger('notify-hide');
        });

        $(document).on('click', '.notifyjs-metro-base .yes', function () {

            $(this).trigger('notify-hide');

            var table = "register";
            var field = "nd";
            var removeId = [];

            $('#datatable tbody input[type="checkbox"]:checked').each(function (index) {
                removeId.push($(this).closest("tr").attr('data-id'));
            })

            var attr = 'table=' + table + '&field=' + field + '&value=' + removeId + '&action=delete';

            $.ajax({
                type: 'POST',
                data: attr,
                url: "/assets/functions.php",
                cache: false,
                complete: function (msg) {

                    if (msg) {
                        var table = $('#datatable').DataTable();
                        table.ajax.reload(null, false);
                        $.notify({'type':'success', 'message':'Документ удачно удален!'});
                    }
                }
            })

        });

    })
    $('select[name="register-type-docum"]').on('change', function (e) {
        if (e.target.value == "ext") {
            $('#nd').closest('div.form-group').addClass('hidden');
        } else {
            $('#nd').closest('div.form-group').removeClass('hidden');
        }
        ;
    })
    // Сохранение записи
    $('.modal form').submit(function (event) {
        event.preventDefault();

        var nd_old = $('#datatable tbody input[type="checkbox"]:checked').parents('tr').attr('data-id');
        var nd = $('input[name="register-nd"]').val();
        var symbol = $('input[name="register-symbol"]').val();
        var group = $('select[name="register-group"]').val();
        // var status = $('#register-statusdoc option:selected').text();
        var status = $('select[name="register-statusdoc"]').val();
        var comment = $('textarea[name="register-comment"]').val();
        var base_name = $('select[name="register-type-docum"]').val();
        var action = $('input[name="register-action"]').val() + 'Register';
        // компания
        var company = [];

        $('input[name^="company"]').each(function (index, value) {
            if (value.checked) {
                company.push($(value).val());
            }
            ;
        })

        company = company.join(',');
        if (base_name == 'ext') {
            nd = 1
        }
        ;
        if (nd == '' || symbol == '' || comment == '' || group.length == 0 || status == '') {

            if (nd == '') {
                $('input[name="register-nd"]').closest('.form-group').addClass('has-error');
                $('input[name="register-nd"]').closest('.form-group').find('.help-block').text('Поле обязательно для заполнения');
            }
            ;

            if (symbol == '') {
                $('input[name="register-symbol"]').closest('.form-group').addClass('has-error');
                $('input[name="register-symbol"]').closest('.form-group').find('.help-block').text('Поле обязательно для заполнения');
            }
            ;
            if (group.length == 0) {
                $('select[name="register-group"]').closest('.form-group').addClass('has-error');
                $('select[name="register-group"]').closest('.form-group').find('.help-block').text('Поле обязательно для заполнения');
            }
            ;
            if (comment == '') {
                $('textarea[name="register-comment"]').closest('.form-group').addClass('has-error');
                $('textarea[name="register-comment"]').closest('.form-group').find('.help-block').text('Поле обязательно для заполнения');
            }
            ;
            if (status == '') {
                $('select[name="register-statusdoc"]').closest('.form-group').addClass('has-error');
                $('select[name="register-statusdoc"]').closest('.form-group').find('.help-block').text('Поле обязательно для заполнения');
            }
            ;

            return false;

        }

        $.ajax({
            type: 'POST',
            data: {
                'nd_old': nd_old,
                'nd': nd,
                'symbol': symbol,
                'comment': comment,
                'action': action,
                'group': group,
                'company': company,
                'status': status,
                'base_name': base_name
            },
            url: "/assets/functions.php",
            cache: false,
            complete: function (msg) {
                if (msg) {
                    $("#editRegisterModal").modal('hide');

                    table.ajax.reload();
                    $.notify({'type':'success', 'message':'Документ успешно сохранен'});
                }
            }
        });

    })

    // Локализация datepicker
    // $(function () {
    //     $("#datepicker").datepicker({
    //         dateFormat: 'dd.mm.yy',
    //         language: 'ru'
    //     });
    //     $.datepicker.setDefaults($.datepicker.regional["ru"]);
    // });
})


// Скачать реестр в xls
$('*[data-action="dload-register"]').click(function () {
    $.notify({'type':'success', 'message':'Скачивание реестра документов началось. Это может занять некоторое время...'});
    window.open('/app/views/register/dload_register.php', '_self');

});