<? 
/* Выгрузка реестра */
/* Версия 1.0 */

include_once $_SERVER['DOCUMENT_ROOT'].'/assets/functions.php';// Подключаем функции
include_once $_SERVER['DOCUMENT_ROOT'].'/vendor/phpoffice/phpexcel/Classes/PHPExcel.php'; // Подключаем библиотеку PHPExcel
$date_now = date ('d.m.Y'); // Дата в нормальном формате dd.mm.yy
$date_1day = date ('d.m.Y', strtotime($Date. ' - 1 days'));
$company = $_COOKIE['company'];
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="Реестр нормативных документов '.company[$company].' актуализированный на '.$date_1day.' (Источник - СУНКТ Well Compliance).xlsx"'); // Придумываем будущее имя файла
header('Cache-Control: max-age=0');

$fio_my = getUserName($_COOKIE['login']); // Свое ФИО

$query = $db->query("SELECT `nd`,`symbol`,`type`,`date`,`status`,`comment`,`group`, `create_date`
							 FROM `register` 
							 WHERE `company` LIKE '%".$company."%' ");

/* Подготавливаем будущую таблицу (Стили, формат, печать, рамка при печати и так далее) */
$phpexcel = new PHPExcel(); // Создаём объект PHPExcel
$page = $phpexcel->setActiveSheetIndex(0); // Делаем активной первую страницу и получаем её
$page = $phpexcel->getActiveSheet()->mergeCells('A1:K1'); // Обьединяем ячейки в первой строчке	
$page = $phpexcel->getActiveSheet()->freezePane('A3'); // Закрепляем первую строчку, чтобы не двигалась
$page->setCellValue('A1', "Реестр нормативных документов ".company[$company]." актуализированный на ".$date_1day." / Источник: СУНКТ Well Compliance / Реестр выгрузил: ".$fio_my);
$page->setTitle("Реестр от $date_now"); // Ставим заголовок на странице
// $page->getHeaderFooter()->setOddHeader('&CТД ТИНКО: прайс-лист'); // Устанавливаем хейдер только для печати.
$page->getPageMargins()->setTop(0); //  отступ сверху
$page->getPageMargins()->setRight(0); // отступ справа
$page->getPageMargins()->setLeft(0.6); // отступ слева
$page->getPageMargins()->setBottom(0);// отступ снизу
$page->getPageMargins()->setFooter(0); // Нижний колонтитуль
$page->getPageMargins()->setHeader(0.2); // Верхний колонтитуль
$page->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE); // Альбомная ориентация
$page->getPageSetup()->SetPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4); // Размер листа при печати
$page->getPageSetup()->setFitToWidth(1);
$page->getPageSetup()->setFitToHeight(0);
$page->getHeaderFooter()->setOddFooter('&L&B'.$page->getTitle().'&RСтраница &P из &N'); // устанавливаем подвал только при печати
// $page->getPageSetup()->setPrintArea('A:K'); // Область печати
$phpexcel->getActiveSheet(0)->setAutoFilter('A2:H2'); // Создание автофильтра

/* Установка стиля для оглавления */
$arHeadStyle = array('font' => array('size'  => 11,
									 'name'  => 'Calibri',
									 'bold' => 'true'),
					 'fill' => array ('type' => PHPExcel_Style_Fill::FILL_SOLID,
									  'rotation' => 0,
									  'color' => array('rgb' => "9BC2E6")),  // Заливка ячеек определенным цветом
					 'borders' => array ('allborders'   => array('style' => PHPExcel_Style_Border::BORDER_THIN )), // Установка рамок (тонких)
				     'alignment' => array ('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,   // Выравнивание текста по верт. и гор. в оглавлении
										   'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
										   'wrap' => true));  // Перенос строк
$BodyStyle = array('font' => array('size'  => 11,
								   'name'  => 'Calibri'),
				   'alignment' => array ('wrap' => true));  // Перенос строк

$BodyStyle1 = array('font' => array('size'  => 11,
								    'name'  => 'Calibri',
									'color' => array('rgb' => 'FF0000')),
				   'alignment' => array ('wrap' => true));  // Перенос строк
$BodyStyle2 = array('font' => array('size'  => 11,
								    'name'  => 'Calibri',
									'color' => array('rgb' => 'E26B0A')),
				   'alignment' => array ('wrap' => true));  // Перенос строк

$contents = array (
				array ("cell"=>"A2",
					   "content"=>"№",
					   "width"=>"8"),
				array ("cell"=>"B2",
					   "content"=>"Наименование документа",
					   "width"=>"53"),
				array ("cell"=>"C2",
					   "content"=>"Тип",
					   "width"=>"16"),
				array ("cell"=>"D2",
					   "content"=>"Дата действия",
					   "width"=>"13"),
				array ("cell"=>"E2",
					   "content"=>"Статус",
					   "width"=>"17"),
				array ("cell"=>"F2",
					   "content"=>"Примечание",
					   "width"=>"31"),
				array ("cell"=>"G2",
					   "content"=>"Отрасль",
					   "width"=>"17"),
				array ("cell"=>"H2",
					   "content"=>"Дата изменения",
					   "width"=>"15")
			);
$mass_var = array('&lt;','br','&gt;');
// var_dump ($contents);
foreach ($contents as $value) {
	$page->setCellValue($value['cell'], $value['content']); // Вставляем оглавление
	$page->getStyle($value['cell'])->applyFromArray($arHeadStyle); // Вставляем стили для ячеек оглавления
	$words_cell = preg_replace('/[0-9]+/', '', $value['cell']); // Убираем цифры из координаты ячейки
	$page->getColumnDimension($words_cell)->setWidth($value['width']); // Устанавливаем ширину столбцов
}
foreach ($query as $key => $data_value) {
	$data_value['symbol'] = strip_tags(str_replace($mass_var,'', $data_value['symbol']));
	$data_value['comment'] = strip_tags(str_replace($mass_var,'', $data_value['comment']));
	$indent = $key + 3; // 3 строки отступаем
	$number = $key + 1; // №
	if ($data_value['type']=="null") {$data_value['type']="";} // Заменяем null в Тип на ''
	if ($data_value['date']=="null") {$data_value['date']="";} // Заменяем null в Дате на ''
	$page->setCellValue("A".$indent, $number); // Вставляем №
	$page->setCellValue("B".$indent, $data_value['symbol']); // Наименование
	/* Вставляем гиперссылки */
	$page->getCell("B".$indent)-> getHyperlink()->setUrl('kodeks://link/d?nd='.$data_value['nd']); // ссылка
	$page->getCell("B".$indent)-> getHyperlink()->setTooltip('Открыть ссылку'); // Всплывающая пожсказка
	$page->getStyle("B".$indent)-> getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

	$page->setCellValue("C".$indent, $data_value['type']); // Тип
	$page->setCellValue("D".$indent, $data_value['date']); // Дата действия
	$page->setCellValue("E".$indent, $data_value['status']); // Статус
	$page->setCellValue("F".$indent, $data_value['comment']); // Примечание
	$page->setCellValue("G".$indent, $data_value['group']); // Отрасль
	$page->setCellValue("H".$indent, $data_value['create_date']); // Дата последних измнений
	$page->getStyle("A".$indent.":H".$indent)->applyFromArray($BodyStyle);
	if ($data_value['status']=="Статус: Недействующий (неактуальный)") {
		$page->getStyle("A".$indent.":H".$indent)->applyFromArray($BodyStyle1);
	}
	if (($data_value['status']=="Статус: Не вступил в силу или действие приостановлено") OR ($data_value['status']=="Статус: Применение в качестве национального стандарта РФ прекращено"))
		$page->getStyle("A".$indent.":H".$indent)->applyFromArray($BodyStyle2); 
	}

/* Начинаем готовиться к записи информации в xlsx-файл */
	$objWriter = PHPExcel_IOFactory::createWriter($phpexcel, 'Excel2007');
	$objWriter->save('php://output');
exit();
?>