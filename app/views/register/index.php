<? include_once $_SERVER['DOCUMENT_ROOT'].'/app/views/header.php'; ?>
<? include_once $_SERVER['DOCUMENT_ROOT'].'/app/views/template.php'; ?>

<!--Main Content -->
<section id="register" class="content">

	<!-- Page Content -->

	<div class="wraper container-fluid">
		<div class="page-title">
			<h3 class="title">Реестр документов<div><small>Составлен на основании статьи 212 ТК РФ</small></div></h3>
		</div>

		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title pull-left m-t-10">Актуализирован по состоянию на: <? echo date("d.m.Y", strtotime("yesterday"))."\n";?></h3>
						<div class="top_nav">

							<? if ( in_array("C", $access['register']) ){ ?>
								<a href="#" data-target="#editRegisterModal" data-toggle="modal">
									<button class="btn btn-success" data-action="add-data" data-toggle="tooltip" title="Добавить документ"><i class="ion-plus"></i></button>
								</a><br>
							<? }?>
							<? if ( in_array("U", $access['register']) ){ ?>
								<a href="#" data-target="#editRegisterModal" data-toggle="modal">
									<button class="btn btn-default" data-action="edit-data" data-toggle="tooltip" title="Редактировать документ"><i class="ion-edit"></i></button>
								</a><br>
							<? }?>
							<? if ( in_array("D", $access['register']) ){ ?>
								<button class="btn btn-default" data-action="remove-data" data-toggle="tooltip" title="Удалить документ"><i class="ion-trash-a"></i></button><br>
							<? }?>
								<button class="btn btn-success" data-action="dload-register" data-toggle="tooltip" title="Скачать реестр"><i class="fa fa-table"></i></button>
							



						</div>
						<div class="clearfix"></div>
					</div>
					<div class="panel-body">

						<div class="row">
							<div class="col-md-12">
								<div class="m-b-15" id="datatable-filter">

			                    </div>
							</div>	
						</div>

						<div class="row">
							<div class="col-md-12 col-sm-12 col-xs-12">
								<table id="datatable" class="table table-hover table-striped table-bordered">
									<thead>
										<tr>
											<th class="text-center"><input type="checkbox" name="select_all" value="1" id="select-all"></th>
											<th>№</th>
											<th>Обозначение документа</th>
											<th>Статус документа</th>
											<th>Группы документа</th>
                                            <th>Тип</th>
                                           <? if($_COOKIE['company']=='all'){?> <th>Компании</th><?}?>
										</tr>
									</thead>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>
		<!-- End Row -->

	</div>

</section>
<script>
    var company_coockie = "<?=$_COOKIE['company']?>";
    var company= JSON.parse('<?= json_encode(company) ?>');
    var docsGroup= JSON.parse('<?= json_encode(docsGroup) ?>');

</script>
<script type="text/javascript" src="/app/views/register/js/register_index.js"></script>
