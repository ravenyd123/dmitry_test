<? include_once $_SERVER['DOCUMENT_ROOT'] . '/app/views/header.php'; ?>
<? include_once $_SERVER['DOCUMENT_ROOT'] . '/app/views/template.php'; ?>

<?php include_once 'modal.php'; ?>
    
    <!--при добавление библиотек появляется баг-->
        <link rel="stylesheet" type="text/css" href="/node_modules/lightbox2/dist/css/lightbox.min.css"/>

        <script type="text/javascript" src="/node_modules/lightbox2/dist/js/lightbox.min.js"></script>
    <!--Main Content -->
    <section class="content">

        <!-- Page Content -->

        <div class="wraper container-fluid">
            <div class="row">
                <div class="col-md-12 col-sx-12">
                    <div class="panel panel-default w-100">
                        <div class="panel-heading">
                            <h3 class="panel-title pull-left m-t-10">Обратная связь с пользователями</h3>
                            
                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-body">

                            <div class="row m-b-15">
                                
                                <div class="" id="datatable-filter">
                                    
                                    <div class="col-md-2 inic">
                                        <label>Инициатор</label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <table id="datatable" class="table table-hover table-striped">
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <!-- End Row -->

        </div>

    </section>
    <style>
        .selected {
            background-color: #d9edf7 !important;
        }
    </style>
    <script>

        var data_recurs =<?php echo json_encode(recursion(true))?>;
    </script>
    <script type="text/javascript" src="/assets/admina/js/de_datetime.js"></script>
    <script type="text/javascript" src="/app/views/feedback/js/feedback.js"></script>
    <script src="/node_modules/chart.js/dist/Chart.min.js"></script>

<? include 'footer.php'; ?>