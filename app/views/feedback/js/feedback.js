// Datatables
    var table = $('#datatable').dataTable({
        lengthChange: false,
        //stateSave: true,
        buttons: ['colvis'],
        "aaSorting": [[1, 'desc']],
        // "sScrollX": "100%",
        "scrollCollapse": true,
        "paging": true,
        "processing": true,
        ajax: {
            type: 'POST',
            dataType: "json",
            url: "app/views/feedback/models/feedback.php",
            data: {"action": "all"},
            cache: false,
            dataSrc: function (json) {
                return json.data
            },
        },
        columnDefs: [
            {
                "title": "№",
                "targets": [0],
                "className": "text-center",
                "data": "id",
                "orderable": false, 
                "width": "1%"
            },
            {
                "title": "",
                "targets": [1],
                "className": "text-center",
                "orderable": false,
                "defaultContent": '', "width": "10%"
            },
            {
                "title": "Пользователь",
                "targets": [2],
                "className": "text-center",
                "data": "createDate",
                "type": "de_datetime",
                "width": "1%"
            },
            {
                "title": "E-mail",
                "targets": [3],
                "className": "text-center",
                "data": "dateAdoption",
                "type": "de_datetime",
                "width": "1%",
                "visible": false
            },
            {
                "title": "Телефон",
                "targets": [4],
                "className": "text-center",
                "data": "dateValidation",
                "type": "de_datetime",
                "width": "1%",
                "visible": false
            },
            {   "title": "Последнее сообщение",
                "targets": [5],
                "className": "text-center", 
                "data": "platform", 
                "width": "5%"
            }
        ],
        "initComplete": function () {
           
            /* Фильтр по инициатору */
            this.api().column(2).every(function () {
                var column = this;
                var select = $('<select class="sel_filter"><option value="">Все</option></select>')
                    .appendTo($('#datatable-filter div.user'))
                    .on('change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );
                        column
                            .search(val ? '^' + val + '$' : '', true, false)
                            .draw();
                    })

                column.data().unique().sort().each(function (d, j) {
                    select.append('<option value="' + d + '">' + d + '</option>')
                });
            });
            

            $('select.sel_filter').select2();
        },
        "createdRow": function (row, data, dataIndex) {
            $(row).find("td").eq(1).append('<span>');
            if (data['id_self'] > 0) {
                $(row).find("td").eq(1).append('<li title="По нарушению создан календарь" class="glyphicon glyphicon-calendar" aria-hidden="true"></li>');
            }
            if (data['photo'] > 0) {
                $(row).find("td").eq(1).append('<li title="В нарушении присутствует фотография" class="glyphicon glyphicon-picture" aria-hidden="true"></li>');
            }
            if (data['typeinput'] == 2) {
                $(row).find("td").eq(1).append('<li title="Нарушение загружено из файла" class="glyphicon glyphicon-open-file" aria-hidden="true"></li>');
            }
            if (data['typeinput'] == 1) {
                $(row).find("td").eq(1).append('<li title="Нарушение загружено из мобильного устройства" class="glyphicon glyphicon-phone" aria-hidden="true"></li>');
            }
             if (data['adjustment'] == 1) {
                $(row).find("td").eq(1).append('<li title="Требуется корректировка!" class=" text-danger glyphicon glyphicon-pencil" aria-hidden="true"></li>');
            }
            $(row).find("td").eq(1).append('</span>');
            $(row).attr("data-id", data['id']);
            $(row).attr("adjustment", data['adjustment']);

        },
        "language": {
            "processing": "Производиться загрузка данных",
            "lengthMenu": "Выводить по _MENU_ записей",
            "zeroRecords": "Ни одной записи не найдено",
            "info": "Показано страниц _PAGE_ из _PAGES_",
            "infoEmpty": "No records available",
            "infoFiltered": "(отфильтровано из _MAX_ записей)",
            "search": "Поиск:",
            "paginate": {
                "first": "Первая",
                "last": "Последняя",
                "next": "Следующая",
                "previous": "Предыдущая"
            },
        }
    }).api()
