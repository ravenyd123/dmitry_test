<?php

class register extends Controller{

    public function __construct() {

    }

    public function index() {
		$this->render('register/index');
    }

    public function reload() {
		$this->render('register/reload');	
    }

    public function test() {
        $this->render('register/test'); 
    }

    public function parse() {
        $this->render('register/parse'); 
    }

    public function ajax() {
        $this->render('register/ajax'); 
    }
    public function opo() {
        $this->render('register/opo/index');
    }
    public function funcopo() {
        $this->render('register/opo/funcopo');
    }

}

?>