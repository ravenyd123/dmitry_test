<?php

class nearmiss extends Controller
{

    public function __construct()
    {

    }

    public function index()
    {
        $this->render('nearmiss/index');
    }
    public function ajax()
    {
        $this->render('nearmiss/models/ajax');
    }
    public function mobile()
    {
        $this->render('nearmiss/models/write');
    }
    public function upload() {
        $this->render('reports/upload');
    }
    public function prolog1(){
        $this->render('nearmiss/templates/dload_progressReport');
    }
    public function prolog2(){
        $this->render('nearmiss/templates/dload_stats');
    }
    public function prolog3(){
        $this->render('nearmiss/templates/dload_register');
    }
    public function prolog4(){
        $this->render('nearmiss/templates/dload_stats_iniciator');
    }
}

?>