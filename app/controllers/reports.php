<?php

class reports extends Controller{

    public function __construct() {

    }

    public function index() {
        header("Location: /reports/all");
    }

    public function add() {
        $this->render('reports/add');
    }

    public function matrix() {
		$this->render('reports/matrix');
    }


    public function all() {
        $this->render('reports/all');
    }

    public function calendar() {
        $this->render('reports/calendar');
    }

    public function download_evaluation_excel() {
        $this->render('reports/download_evaluation_excel');
    }

    public function load_evaluation_excel() {
        $this->render('reports/load_evaluation_excel');
    }
    public function upload() {
        $this->render('reports/upload');
    }

}

?>