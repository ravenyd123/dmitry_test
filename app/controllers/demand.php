<?php

class demand extends Controller{

    public function __construct() {

    }

    public function index() {
		$this->render('demand/index');
    }
    public function index2() {
		$this->render('demand/index_new');
    }

    public function editor() {
        $this->render('demand/editor');
    }

//    public function editor_new() {
//        $this->render('demand/editor_new');
//    }

    public function update() {
        $this->render('demand/update');
    }

    public function fix_connected() {
        $this->render('demand/fix_connected');
    }

    public function fix_ehs() {
        $this->render('demand/fix_ehs');
    }

    public function ajax() {
        $this->render('demand/ajax');
    }
    public function ajax2() {
        $this->render('demand/ajax2');
    }

}

?>