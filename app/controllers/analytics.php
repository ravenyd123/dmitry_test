<?php

class Analytics extends Controller{

    public function __construct() {
		
    }

    public function index() {
		$this->render('analytics/index');
    }

    public function report() {
        $this->render('analytics/report/index');
    }
    public function get() {
        $this->render('analytics/ajax');
    }
}

?>