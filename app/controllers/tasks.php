<?php

class tasks extends Controller{

    public function __construct() {

    }

	public function index() {
		$this->render('tasks/index');	
	}
    public function task_new() {
        $this->render('tasks/task_new');
    }
    public function upload() {
        $this->render('tasks/upload');
    }
}

?>