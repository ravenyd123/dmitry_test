<?php

class logout extends Controller{

    public function __construct() {
		setcookie("session", "", time() - 60*60*24*30);
		setcookie("company", "", time() - 60*60*24*30);
		setcookie("id", "", time() - 60*60*24*30);
		setcookie("login", "", time() - 60*60*24*30);
		setcookie("platform", "", time() - 60*60*24*30);
		header('Location: login');
    }
}

?>