<?php
class Controller {

	public function __construct() {

		# Разбираем URL
		$url = $_GET['url'];
		$url = rtrim($url, '/');
		$url = explode('/', $url);

		# Редирект с главной страницы на рабочий стол
		if ($url[0] == '') {
			header("Location: /tasks"); exit();
		}

		$file = $_SERVER['DOCUMENT_ROOT'].'/app/controllers/'.$url[0].'.php';

		if (file_exists($file)) {
			require $file;
		} else{
			require $_SERVER['DOCUMENT_ROOT'].'/app/controllers/error.php';
			$controller = new Errors();
			return false;
		}

		$controller = new $url[0];
		$url1 = $url[1];


		if(isset($url1)) {
			if (method_exists($controller,$url1)) {
				$controller->$url1();
			}else{
				require $_SERVER['DOCUMENT_ROOT'].'/app/controllers/error.php';
				$controller = new Errors();
				return false;
			}
		}else{
			$controller->index();
		}

	}

	public function render($name) {
		require $_SERVER['DOCUMENT_ROOT'].'/app/views/'.$name.'.php';
	}

}

?>