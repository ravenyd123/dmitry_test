<?php

// типы задач
define('type_tasks',get_enum_values('tasks','type'));
define('type_tasks_new',Get_all_type_t());
define('type_list',['quick'=>'Собственная оценка','indef'=>'N/A']);
//переводчик типов
define('translit_type',["self-evaluation"=>"Самооценка","audit"=>"Аудит","product_control"=>"Производственный контроль","state_inspection"=>"'Проверка гос. органов"]);
// типы статус в задачах
define('type_status',get_enum_values('tasks','status'));
define('type_docum',["docum"=>"Основной","ext"=>"Внешний"]);
// типы статус в реестре
define('register_status',[
'Прочие',
'Применение в качестве национального стандарта РФ прекращено',
'Недействующий',
'Не вступил в силу',
'Действующий. Отменен в части',
'Действующий',
'undefined']);
// типы статус в демонде
define('demand_status',[
    'На утверждении',
    'Действующий',
    'Недействующий',
    'Неприменимые',
  ]);
define('status_completed',["",
    "<span style=' background-color: rgb(28, 168, 221);'>Более 90%</span>",
    "<span style=' background-color: rgb(0, 255, 51);'>От 71% до 90%</span>",
    "<span style=' background-color: rgb(255, 255, 51);'>От 40% до 70%</span>",
    "<span style=' background-color: rgb(255, 51, 0);'>Менее 40%</span>",
    "<span style=''>N/A</span>"] );
// путь для подлючение к тех эксперту

define("Sourse_conn",myHost()."/nd?nd=");///http://suntd.company-dis.ru:5000/catalog02
//путь НОМЕ
define("Sourse_Нome",myHost());
// Источник (техэксперт)
define('Sourse_app',"СУНКТ \"Well Compliance\"");
//виды работ
define('GetAllvid',GetAllvid());
//виды областей в реестре
define('docsGroup',['Не выбрано','Пожарная безопасность','ГО и ЧС','Охрана труда', 'ООС', 'Промышленная безопасность', 'ЛНД','Энергобезопасность','Кап.строительство']);
//виды областей в требовании
define('docsGroupDemand', ['Нет', 'Охрана труда','Пожарная безопасность','Промышленная безопасность','Энергобезопасность','ООС','ГО и ЧС','Кап.строительство']);
// площадки у юзера
define('platforms_user',GetPlatformUser());
//все площадки
define('platforms',GetAllPlatforms());
//юзеры моей платформы
define('user_in_platforms',getUserNameInPlatform());
//юзеры все
define('user_all',GetAllUser());
//Компании передела взять зависимость к площадкам
define('company',[
    'all'=>'Все',
    'pepsico' => 'PepsiCo, Inc',
    'gazprom' => 'ООО «Газпром георесурс»']
);

// Надзорные органы
if ( strpos($_SERVER['REQUEST_URI'], 'tasks') !== false ) {

    define ('authority' ,
        ['Нет',
        'Ростехнадзор',
        'Министерство чрезвычайных ситуаций',
        'ГО ЧС',
        'Росприроднадзор',
        'Фонд социального страхования',
        'Государственная инспекция по труду',
        'Роснедра',
        'Министерство экологии и природопользования',
        'Роспотребнадзор',
        'Центр гигиены и эпидемиологии',
        'Санитарно эпидемиологическая станция',
        'Аудитор',
        'Прокуратора',
        'Управление федеральной миграционной службы',
        'Государственный автодорожный надзор',
        'Администрация субъекта РФ',
        'Руководство',
        'Другое'
    ]);
}