<?php

/**
 * Created by PhpStorm.
 * User: ilya
 * Date: 07.08.2019
 * Time: 14:11
 */
class DB
{ private static $mysqli;
    private static $pdo;
    private static $User = "root";
    private static $Host = "localhost";
    private static $Password = "Sis1801911777911777911777!@#$%^&*";
    private static $table = "test";
    private static $port = "3306";
    private static $charset = 'utf8';


    private function __construct(){}

    private function __clone(){}

    private function __wakeup(){}

    public static function Connection()
    {
        if (!is_object(self::$mysqli)) self::$mysqli = new mysqli(self::$Host, self::$User, self::$Password, self::$table);
        return self::$mysqli;
    }
    public static function Connectionpdo()
    {
        $dsn = "mysql:host=" . self::$Host . ";dbname=" . self::$table . ";port=" . self::$port . ";charset=" . self::$charset;
        $opt = [
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            PDO::ATTR_EMULATE_PREPARES => false,

        ];
        if (!is_object(self::$pdo)) self::$pdo = new PDO($dsn, self::$User, self::$Password, $opt);;
        return self::$pdo;
    }

}
class Sphinx
{
    public $Mysql_host = "127.0.0.1";

    public function Connection()
    {
        $link = new mysqli($this->Mysql_host, '', '', '', '9306');
        return $link;
    }

}
//class DbPdo
//{
//    private const user = "root";
//    private const host = "127.0.0.1";
//    private const port = "3307";
//    private const password = "sis1801emp";
//    private const db = "Matrix_test";
//    private const charset = 'utf8';
//
//    public static function Connection()
//    {
//        $dsn = "mysql:host=" . self::host . ";dbname=" . self::db . ";port=" . self::port . ";charset=" . self::charset;
//        $opt = [
//            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
//            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
//            PDO::ATTR_EMULATE_PREPARES => false,
//
//        ];
//        return new PDO($dsn, self::user, self::password, $opt);
//    }
//
//}
