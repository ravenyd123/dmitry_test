<? if (strpos($_SERVER['REQUEST_URI'], 'matrix') !== false) { ?>
    <!-- ВСПЛЫВАЮЩЕЕ ОКНО(ОТЧЕТ ПО САМООЦЕНКЕ) -->
    <div class="modal fade" id="selfModal" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="row">
                        <!-- <div class="col-md-offset-3 col-md-3">
            <p><b>Аудит № </b><span id="audit"><?= $_GET['id']; ?></span><br></p>
          </div> -->
                        <div class="col-md-6 col-sm-6">
                            <p><b>Требование № </b><span id="row"></span><br></p>
                        </div>
                        <div class="col-md-offset-3 col-md-3 col-sm-3">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                    </div>

                </div>
                <div class="modal-body">

                    <div class="row">
                        <? if ($_GET['type'] == 'calendar' || $_GET['type'] == 'self-evaluation') { ?>
                            <div class="col-md-6 col-sm-6">
                                <div class="form-group">
                                    <label>Плановый срок<sup>*</sup></label>
                                    <input type="text" class="form-control datepicker" required
                                    name="date-plan"
                                           data-date-format="dd.mm.yyyy" autocomplete="off">
                                    <div class="help-block"></div>
                                </div>
                            </div>
                        <? } ?>

                        <? if ($_GET['type'] == 'calendar') { ?>
                            <div class="col-md-6 col-sm-6">
                                <div class="form-group">
                                    <label>Фактический срок<sup>*</sup></label>
                                    <input type="text" class="form-control datepicker" name="fact-date"
                                           data-date-format="dd.mm.yyyy" autocomplete="off">
                                    <div class="help-block"></div>
                                </div>
                            </div>
                        <? } ?>

                    </div>

                    <div class="row">
                        <div class="col-md-6 col-sm-6">
                            <div class="form-group">
                                <label>Статус<sup>*</sup></label>
                                <select class="select2" name='status' autocomplete="off">
                                    <option value="0" class=fivet>&nbsp;</option>
                                    <option value="5" class=fivet>N/A</option>
                                    <option value="1" class=onet>более 90%</option>
                                    <option value="2" class=twot>от 71 до 90%</option>
                                    <option value="3" class=threet>от 41 до 70%</option>
                                    <option value="4" class=fourt>менее 40%</option>
                                </select>
                                <div class="help-block"></div>
                            </div>
                        </div>

                        <? if ($_GET['type'] == 'calendar' || $_GET['type'] == 'self-evaluation') { ?>
                            <div class="col-md-6 col-sm-6">
                                <div class="form-group">
                                    <label for="fio">ФИО ответственного<sup>*</sup></label>
                                    <select class="select2" name='fio' autocomplete="off">
                                        <option value=" ">&nbsp;</option>
                                        <? foreach (getUserNameInPlatform(true, true) as $key => $value) { ?>
                                            <option value="<?= $key ?>"><?= $value ?></option>
                                        <? } ?>
                                    </select>

                                    <!-- <input type="text" class="form-control" id="fio" name="fio"> -->
                                    <div class="help-block"></div>
                                </div>
                            </div>
                        <? } ?>
                    </div>

                    <? if ($_GET['type'] == 'calendar') { ?>
                        <div class="row">
                            <div class="col-md-6 col-sm-6">
                                <div class="form-group">
                                    <label>Утвердить мероприятие</label>
                                    <select class="select2" name='agreed' autocomplete="off">
                                        <option value="">Не утвердить</option>
                                        <option value="1">Утвердить</option>
                                    </select>
                                    <div class="help-block"></div>
                                </div>
                            </div>
                        </div>
                    <? } ?>

                    <p><b>Необходимые мероприятия</b><br><span id="demand"></span></p>

                    <? if ($_GET['type'] != 'calendar') { ?>
                        <div class="form-group">
                            <label>
                                Выявленные несоответствия<sup>*</sup>
                            </label>
                            <textarea class="form-control" rows="5" name="actions"></textarea>
                            <div class="help-block"></div>
                        </div>
                    <? } ?>

                    <? if ($_GET['type'] == 'calendar') { ?>
                        <div class="form-group">
                            <label>
                                Мероприятия по устранению<sup>*</sup>
                            </label>
                            <textarea class="form-control" rows="5" name="recommendations"></textarea>
                            <div class="help-block"></div>
                        </div>
                    <? } ?>

                    <? if ($_GET['type'] == 'calendar') { ?>
                        <div class="form-group cause">
                            <label>
                                Причина невыполнения<sup>*</sup>
                            </label>
                            <textarea class="form-control" rows="5" name="reason-failure"></textarea>
                            <div class="help-block"></div>
                        </div>
                    <? } ?>
                    <div class="form-group">
                        <label>
                            Файлы
                        </label>
                        <form enctype="multipart/form-data">
                            <div class="task_file_loading">
                                <input id="input-ru" class="" name="fileToUpload[]" type="file" multiple>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="text-right">
                        <input type="button" class="btn btn-default" id="cancel" value="Отменить"/>
                        <input type="button" class="btn btn-primary" id="save" value="Сохранить"/>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ОБНОВЛЕНИЕ ТРЕБОВАНИЙ(ОТЧЕТ ПО САМООЦЕНКЕ) -->
    <div class="modal fade" id="updateMatrix" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Доступны обновления</h4>
                </div>
                <div class="modal-body">
                    <p>Часть требований, используемых в данной самооценке, устарели и информация по ним стала
                        неактуальной.
                        Пожалуйста обновите требования!</p>
                </div>
                <div class="modal-footer">
                    <div class="text-right">
                        <input type="button" class="btn btn-default" data-dismiss="modal" value="Продолжить"/>
                        <input type="button" class="btn btn-primary" onclick="updateMatrix(<?= $_GET['id']; ?>);"
                               value="Обновить требования"/>
                    </div>
                </div>
            </div>
        </div>
    </div>
<? } ?>
    <!--Добавить пользователей-->
<? if (strpos($_SERVER['REQUEST_URI'], 'settings') !== false) { ?>

    <div class="modal fade" id="edit-user"  data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Информация о пользователе:</h4>
                </div>
                <div class="modal-body">

                    <form class="edit-user" novalidate method="POST">

                        <input type="hidden" name="operation" value="add">

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Логин:<sup>*</sup></label>
                                    <input type="text" required class="form-control" disabled name="login">
                                    <div class="help-block"></div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Фамилия:<sup>*</sup></label>
                                    <input type="text" class="form-control" required name="surname">
                                    <div class="help-block"></div>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Имя:<sup>*</sup></label>
                                    <input type="text" class="form-control" required name="name">
                                    <div class="help-block"></div>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Отчество:</label>
                                    <input type="text" class="form-control" name="father_name">
                                    <div class="help-block"></div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Email:<sup>*</sup></label>
                                    <input type="text" class="form-control" required name="email">
                                    <div class="help-block"></div>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Телефон:<sup>*</sup></label>
                                    <input type="text" class="form-control" required name="phone">
                                    <div class="help-block"></div>
                                </div>
                            </div>
                        <? $divisions = getDivision();?>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Подразделение:<sup>*</sup></label>
                                    <select id="division" class="select2 select2-offscreen"
                                    data-placeholder="Выберите Подразделение пользователя"  name="division" title="Выберите Подразделение пользователя">
                                    <? foreach ($divisions as $key=>$divdata) {
                                     ?> <option value="<?=$divdata['id']?>"><?=$divdata['name_division']?></option>   

                                   <? } ?>  
                                    </select>
                                    <div class="help-block"></div>
                                </div>
                            </div>

                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <a href="#" data-action="reset-password">Сбросить пароль</a>
                                </div>
                            </div>
                        </div>

                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a href="#platforms" data-toggle="tab" aria-expanded="true">
                                    <span class="visible-xs"><i class="fa fa-home"></i></span>
                                    <span class="hidden-xs">Площадки</span>
                                </a>
                            </li>
                            <li class="">
                                <a href="#access" data-toggle="tab" aria-expanded="false">
                                    <span class="visible-xs"><i class="fa fa-user"></i></span>
                                    <span class="hidden-xs">Права доступа</span>
                                </a>
                            </li>
                        </ul>


                </div>

                </form>

                <div class="tab-content">

                    <div class="tab-pane active" id="platforms">

                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-group">
                                    <!--   <label>Площадки:<sup>*</sup></label> -->
                                    <div id="platform_tree"></div>

                                    <div class="help-block"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="access">
                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-group">
                                    <!-- <label>Права доступа:<sup>*</sup></label> -->
                                    <div id="access">

                                        <?
                                        foreach ($rools as $key => $value) { ?>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label><?= $value ?>:<sup>*</sup></label>
                                                    <select name="<?= $key ?>" autocomplete="off" multiple
                                                            class="select2"
                                                            data-placeholder="Установите права доступа...">
                                                        <option value="C" >Создание</option>
                                                        <option value="R" >Чтение</option>
                                                        <option value="U" >Редактирование</option>
                                                        <option value="D" >Удаление</option>
                                                </div>
                                                </select>
                                                <div class="help-block"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <? } ?>
                                </div>
                                <div class="help-block"></div>
                            </div>
                        </div>
                    </div>


                </div>

                <div class="modal-footer">
                    <div class="text-right">
                        <input type="button" class="btn btn-default" data-dismiss="modal" value="Отменить"/>
                        <input type="button" class="btn btn-primary" onclick="$('.edit-user').submit();"
                               value="Сохранить"/>
                    </div>
                </div>


            </div>


        </div>
    </div>
    </div>

<? } ?>


<? if (strpos($_SERVER['REQUEST_URI'], 'analytics') !== false) { ?>
    <!-- ВСПЛЫВАЮЩЕЕ ОКНО(ГРАФИК:ПЕРЕЧИСЛЕНИЕ ВИДОВ РАБОТ) -->
    <div class="modal fade" id="worksModal">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Укажите виды работ</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <ul id="show_hide" class="header-filter">
                                <li>
                                    <a href="#" onclick="showAll(this);">Выбрать все</a>
                                </li>
                                <li>
                                    <a href="#" onclick="hideAll(this);">Снять все</a>
                                </li>
                            </ul>
                        </div>
                        <div id="chart-labels">

                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <div class="text-right">
                        <!-- <input type="button" class="btn btn-default" data-dismiss="modal" value="Отменить" /> -->
                        <input type="button" class="btn btn-primary" data-dismiss="modal" id="save"
                               value="Подтвердить"/>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- ВСПЛЫВАЮЩЕЕ ОКНО(ГРАФИК:ПЕРЕЧИСЛЕНИЕ ПРЕДПРИЯТИЙ) -->
    <div class="modal fade" id="factoryModal">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Укажите предприятия</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <ul id="show_hide" class="header-filter">
                                <li>
                                    <a href="#" onclick="showAll(this);">Выбрать все</a>
                                </li>
                                <li>
                                    <a href="#" onclick="hideAll(this);">Снять все</a>
                                </li>
                            </ul>
                        </div>
                        <div id="chart-factory">

                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <div class="row">
                        <div class="col-md-6">
                            <p class="text-danger text-left m-t-5 m-b-0 hide"><i
                                        class="ion-information-circled"></i>
                                Укажите не менее 3 предприятий</p>
                        </div>
                        <div class="col-md-6 text-right">
                            <input type="button" class="btn btn-primary" data-dismiss="modal" value="Подтвердить"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- ВСПЛЫВАЮЩЕЕ ОКНО(ГРАФИК:СТОЛБЧАТАЯ ДИАГРАММА) -->
    <div class="modal fade" id="chartWorksModal">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" id="detail_title">Укажите виды работ</h4>
                </div>
                <div class="modal-body">
                    <div id="detail_company">
                        <div id="years"></div>
                        <div id="pdf"></div>
                        <table id="detail_table">
                            <tbody></tbody>
                        </table>
                        <div id="detail_delimiter"></div>
                    </div>

                </div>
                <!-- <div class="modal-footer"></div> -->
            </div>
        </div>
    </div>

    <!-- ВСПЛЫВАЮЩЕЕ ОКНО(ГРАФИК:РАДАР) -->
    <div class="modal fade" id="chartRadarModal">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body">

                    <div id="detail_screen">
                        <p class="years"></p>
                        <div id="table">
                            <table>
                                <tbody></tbody>
                            </table>
                            <div id="delimiter"></div>
                        </div>
                    </div>

                </div>
                <!-- <div class="modal-footer"></div> -->
            </div>
        </div>
    </div>
<? } ?>


    <!--Всплывающее окно суб задач-->

<?
$tt = explode('?', $_SERVER['REQUEST_URI']);
preg_match('/\w+$/m', $tt[0], $match);
if ($match[0] == 'tasks') { ?>
    <div class="modal fade" id="task_create" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog modal-md">
            <form id="task_create" novalidate method="POST">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <strong>Создать задачу</strong>
                        <input type="hidden" name="task_create_status" value="1">
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="task_create_name"><sup>*</sup> Тема:</label>
                            <input autocomplete="off" type="text" id="task_create_name" name="task_create_name" class="form-control "
                                   value="<?= $row['name'] ?>">
                        </div>

                        <div class="form-group">
                            <label for="task_create_type"><sup>*</sup> Тип задачи:</label>
                            <select id="task_create_type" class="select2" autocomplete="off" name="task_create_type">
                                <?
                                foreach (type_tasks_new as $key => $value) {
                                    if (($key != 1) AND ($key != 7))  { ?>
                                        <option value="<?= $key ?>"><?= $value ?></option>
                                    <? }
                                } ?>
                            </select>
                        </div>
                        <div class="all_platform">
                        </div>
                        <div class="form-group ">
                            <label>Надзорный орган:</label>
                            <select name="task_create_authority" autocomplete="off" required class="select2"
                                    data-placeholder="Выберите надзорный орган...">
                                <?
                                foreach (authority as $key => $value) { ?>
                                    <option value="<?= $value ?>" <?php if ($key == 0) {
                                        echo "selected";
                                    } ?>><?= $value ?></option>
                                <? } ?>
                            </select>
                            <div class="help-block"></div>
                        </div>
                        <div class="form-group hidden">
                            <label for="pkplatform">Подразделении:</label>
                            <select class="select2" id="pkplatform" autocomplete="off" data-placeholder="Выберите..."
                                    disabled>
                                <option selected></option>
                            </select>
                            <div class="help-block"></div>
                        </div>

                        <div class="form-group">
                            <label for="task_create_description">Описание задачи:</label>
                            <textarea id="task_create_description" name="task_create_description" rows="4"></textarea>
                            <!--<div class="task_create_description">
                                Текст
                            </div>-->
                        </div>
                        <div class="form-group">
                            <label for="task_create_deadline" class="control-label "><sup>*</sup> Срок
                                исполнения:</label>

                            <input id="task_create_deadline" type="text" class="form-control datepicker" required
                                   autocomplete="off"
                                   name="task_create_deadline"
                                   value="">

                        </div>
                        <div class="form-group">
                            <label for="task_create_responsible"><sup>*</sup> Ответственный:</label>
                            <select id="task_create_responsible" autocomplete="off" name="task_create_responsible"
                                    required
                                    class="select2 "
                                    data-placeholder="Выберите ответственного...">
                                <option value="">Выберите ответственного</option>
                                <?
                                foreach (user_in_platforms as $key => $value) { ?>
                                    <option value="<?= $key ?>"><?= $value ?></option>
                                <? } ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="task_options">Опции:</label>
                            <?
                            foreach (type_list as $key => $value) { ?>
                                <div class="checkbox-inline p-0">
                                    <label class="cr-styled ">
                                        <input type="checkbox" value="1" name=task_create_<?= $key ?>>
                                        <i class="fa"></i><?= $value ?></label>
                                </div>
                            <? } ?>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <div class="text-right">
                            <input type="button" class="btn btn-default" data-dismiss="modal" value="Отменить"/>
                            <input type="submit" class="btn btn-primary" value="Далее"/>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
<? } ?>
<? if ($match[0] == 'task_new') { ?>
    <div class="modal fade modal2" data-backdrop="static" id="subtasks" data-keyboard="false">
        <div class="modal-dialog modal-lg">
            <form id="edit_subtask" novalidate method="POST">
                <input type="hidden" value="" name="subtask_id"/>
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <ol class="list-inline">
                            <li style="width: 50%">
                                <div class="form-group">
                                    <input id="subtask_name" type="text" name="subtask_name" class="form-control"
                                           autocomplete="off"
                                           value="" placeholder="Наименование подзадачи">
                                </div>
                            </li>
                            <li>
                                <a href="#">
                                    <h5>
                                        <span class="glyphicon glyphicon-pencil"></span>
                                    </h5>
                                </a>
                            </li>
                        </ol>

                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-7">
                                <div class="task_header">
                                    <h5>
                                        <a data-target="#detail_subtask" data-toggle="collapse"><strong>Детали
                                                задачи</strong></a>
                                    </h5>
                                </div>
                                <div id="detail_subtask" class="collapse in">
                                    <div class="all_platform">
                                    </div>

                                    <div class="form-group">
                                        <label for="subtask_job" class="control-label"><sup>*</sup> Виды работ: </label><label
                                                class="text-right"><input type="checkbox" id="checkbox_select_all">
                                            Выделить все</label>
                                        <select id="subtask_job" name="subtask_job" autocomplete="off" required multiple
                                                class="select2">
                                        </select>
                                    </div>
                                </div>
                                <div class="task_header">
                                    <h5>
                                        <a data-target="#textarea_subtask" data-toggle="collapse">
                                            <strong>Описание задачи:</strong>
                                        </a>
                                        <ul class="list-inline" style="float:right">
                                            <li>
                                                <a href="#" onclick="text_editor('text_sub_zadacha')"><span
                                                            class="glyphicon glyphicon-pencil"></span></a>
                                            </li>
                                        </ul>
                                    </h5>
                                </div>

                                <div id="textarea_subtask" class=" collapse in">
                                    <textarea class="text_sub_zadacha form-control" name="subtask_description" rows="5"
                                              disabled>
                                    </textarea>
                                </div>

                                <div class="task_header">
                                    <h5><a data-target="#file_subtask" data-toggle="collapse"><strong>Вложенные
                                                файлы:</strong></a>
                                    </h5>
                                </div>
                                <div id="file_subtask" class="collapse in">
                                    <p>файлы</p>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="task_header">
                                    <h5><a data-target="#date-subtask" data-toggle="collapse">
                                            <strong>Дата:</strong>
                                        </a>
                                    </h5>
                                </div>
                                <div id="date-subtask" class="collapse in">
                                    <dl>
                                        <dt><strong><sup>*</sup>Срок исполнения:</strong></dt>
                                        <dd>
                                            <div class="form-group">
                                                <input type="text" class="form-control datepicker" autocomplete="off"
                                                        required
                                                       name="subtask_deadline" value="">
                                            </div>
                                        </dd>
                                        <dt><strong>Напоминать начиная с:</strong></dt>
                                        <dd>
                                            <div class="form-group">
                                                <input type="text" class="form-control datepicker" required
                                                       autocomplete="off"
                                                       name="subtask_startnoty" id="subtask_startnoty">
                                                <div>
                                                    <a class="m-r-10" href="javascript:void(0);"
                                                       onclick="setFromDate('7','subtask_startnoty','day',true,'subtask_deadline')">
                                                        <small>за неделю</small>
                                                    </a>
                                                    <a class="m-r-10" href="javascript:void(0);"
                                                       onclick="setFromDate('14','subtask_startnoty','day',true,'subtask_deadline')">
                                                        <small>за 2 недели</small>
                                                    </a>
                                                    <a class="m-r-10" href="javascript:void(0);"
                                                       onclick="setFromDate('1','subtask_startnoty','mounth',true,'subtask_deadline')">
                                                        <small>за 1 месяц</small>
                                                    </a>
                                                    <a class="" href="javascript:void(0);"
                                                       onclick="setFromDate('3','subtask_startnoty','mounth',true,'subtask_deadline')">
                                                        <small>за 3 месяца</small>
                                                    </a>
                                                </div>
                                            </div>
                                        </dd>
                                    </dl>

                                </div>
                                <div class="task_header">
                                    <h5>
                                        <a data-target="#people-subtask" data-toggle="collapse">
                                            <strong>Люди:</strong>
                                        </a>
                                    </h5>
                                </div>
                                <div id="people-subtask" class="collapse in">
                                    <div class="form-group">
                                        <label for="subtask_executors" class="control-label"><sup>*</sup>
                                            Исполнитель:</label>
                                        <select id="subtask_executors" autocomplete="off" name="subtask_executors"
                                                required class="select2"
                                                data-placeholder="Выберите ответственного...">
                                            <option value="">Выберите Исполнителя</option>
                                            <?
                                            foreach (user_in_platforms as $key => $value) { ?>
                                                <option value="<?= $key ?>" <?= $key == $row['responsible'] ? "selected" : "" ?> ><?= $value ?></option>
                                            <? } ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="subtask_subexecutors" class="control-label">Соисполнитель:</label>
                                        <select id="subtask_subexecutors" autocomplete="off" name="subtask_subexecutors"
                                                required multiple
                                                class="select2"
                                                data-placeholder="Выберите ответственного...">
                                            <option value="">Выберите Соисполнителя</option>
                                            <?
                                            foreach (user_in_platforms as $key => $value) { ?>
                                                <option value="<?= $key ?>" <?= $key == $row['responsible'] ? "selected" : "" ?> ><?= $value ?></option>
                                            <? } ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="text-right">
                            <input type="button" class="btn btn-default" data-dismiss="modal" value="Отменить"/>
                            <input id="save_subtask" type="submit" class="btn btn-primary" value="Сохранить"/>
                        </div>

                    </div>
                </div>
            </form>
        </div>
    </div>
<? } ?>
    <!--СПЛЫВАЮЩЕЕ ОКНО(opo)-->
<? preg_match('/\w+$/m', $_SERVER['REQUEST_URI'], $match);
if ($match[0] === "opo") {
    ?>
    <div class="modal fade" data-backdrop="static" id="reg_opo" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title text-left">Добавить новый объект</h4>
                </div>

                <form id="opo" method="POST">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">//ок
                                <div class="form-group form-inline">
                                    <label for="opo-reg">Регистрационный номер:</label>
                                    <input class="form-control" type="text" name="opo-reg" id="opo-reg">
                                    <div class="help-block"></div>
                                </div>
                            </div>
                            <div class="col-md-12">//ок
                                <div class="form-group">
                                    <label for="opo-name">Наименование</label>
                                    <textarea class="form-control vresize" id="opo-name" name="opo-name" rows="2"
                                              style="resize: vertical"></textarea>
                                    <div class="help-block"></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="opo-model">Тип:</label>
                                    <select name="opo-model" autocomplete="off" id="opo-model" class="form-control">
                                        <option value="1" selected>?????????</option>
                                    </select></div>
                            </div>
                            <div class="col-md-6">//ок
                                <div class="form-group">
                                    <label for="opo-worker">Кол-во сотрудн. на ОПО:</label>
                                    <input type="text" name="opo-worker" id="opo-worker" class="form-control" value="5">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Год ввода<sup>*</sup></label>//ок
                                    <input type="text" class="form-control datepicker" name="opo-commissioning"
                                           data-date-format="dd.mm.yyyy">
                                    <div class="help-block"></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">//ок
                                    <label>Дата перерегистрации<sup>*</sup></label>
                                    <input type="text" class="form-control datepicker" name="opo-re_registration"
                                           data-date-format="dd.mm.yyyy">
                                    <div class="help-block"></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group ">//ок
                                    <label for="opo-responsible">Ответственный:</label>
                                    <select name="opo-responsible" autocomplete="off" id="opo-responsible"
                                            class="form-control">
                                        <option value="1" selected>1</option>
                                    </select></div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group ">//ок
                                    <label for="opo-exploitation">Эксплуатация:</label>
                                    <select name="opo-exploitation" autocomplete="off" id="opo-exploitation"
                                            class="form-control">
                                        <option value="1" selected>1</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">//ок
                                    <label>Дата очеред. гидравл. испыт.:<sup>*</sup></label>
                                    <input type="text" class="form-control datepicker" name="opo-d_n_hydra_test"
                                           data-date-format="dd.mm.yyyy">
                                    <div class="help-block"></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">///????
                                    <label>Периодичность<sup>*</sup></label>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <select name="opo-people" autocomplete="off" id="opo-people"
                                                    class="form-control">
                                                <option value="1" selected>1</option>
                                            </select>
                                        </div>
                                        <div class="col-md-6">
                                            <select name="opo-people" autocomplete="off" id="opo-people"
                                                    class="form-control">
                                                <option value="1" selected>1</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="help-block"></div>
                                </div>
                            </div>
                            <div class="col-md-12 delimiter">
                                <p>Отметки о предыдущих испытаниях</p>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">//ок
                                    <label>Дата гидравлического испытания:<sup>*</sup></label>
                                    <input type="text" class="form-control datepicker" name="opo-d_p_hydra_test"
                                           data-date-format="dd.mm.yyyy">
                                    <div class="help-block"></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="opo-people">Статус:</label>
                                    <select name="opo-people" autocomplete="off" id="opo-people" class="form-control">
                                        <option value="1" selected>Выполнено</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">//ok
                                    <label>Дата очеред. техн. освидет-ния:<sup>*</sup></label>
                                    <input type="text" class="form-control datepicker" name="opo-d_n_tech_inspec"
                                           data-date-format="dd.mm.yyyy">
                                    <div class="help-block"></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Периодичность<sup>*</sup></label>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <select name="opo-p" autocomplete="off" id="opo-p" class="form-control">
                                                <option value="1" selected>1</option>
                                            </select>
                                        </div>
                                        <div class="col-md-6">
                                            <select name="opo-p" autocomplete="off" id="opo-p" class="form-control">
                                                <option value="1" selected>1</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="help-block"></div>
                                </div>
                            </div>
                            <div class="col-md-12 delimiter">
                                <p>Отметки о предыдущих освидетельствованиях</p>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">//ок
                                    <label>Дата техн. освидет-ния:<sup>*</sup></label>
                                    <input type="text" class="form-control datepicker" name="opo-d_p_tech_inspec"
                                           data-date-format="dd.mm.yyyy">
                                    <div class="help-block"></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group ">
                                    <label for="opo-people">Статус:</label>
                                    <select name="opo-p" autocomplete="off" id="opo-people" class="form-control">
                                        <option value="1" selected>Выполнено</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                </form>
                <div class="modal-footer">
                    <div class="text-right">
                        <input type="button" class="btn btn-default" data-dismiss="modal" value="Отменить"/>
                        <input type="submit" class="btn btn-primary" value="Сохранить"/>
                    </div>
                </div>

            </div>
        </div>
    </div>
    </div>
<? } ?>

    <!--    ВСПЛЫВАЮЩЕЕ ОКНО(РЕДАКТИРОВАНИЕ ДОКУМЕНТА В РЕЕСТРЕ)-->
<? preg_match('/\w+$/m', $_SERVER['REQUEST_URI'], $match);
if ($match[0] === "register") { ?>

    <div class="modal fade" id="editRegisterModal" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Редактировать документ:</h4>
                </div>
                <form class="edit-task" method="POST">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">

                                <input type="hidden" value="" name="register-action"/>

                                <div class="form-group">
                                    <label for="comment">Тип<sup>*</sup></label>
                                    <select id="register-type-docum" name="register-type-docum" autocomplete="off"
                                            required class="select2"
                                            placeholder="Выберите тип документа">
                                        <?
                                        foreach (type_docum as $key => $value) { ?>
                                            <option value="<?= $key; ?>"><?= $value; ?></option>
                                        <? } ?>
                                    </select>
                                    <div class="help-block"></div>
                                </div>

                                <div class="form-group">
                                    <label for="demand-name">Идентификатор документа в системе<sup>*</sup></label>
                                    <input type="text" class="form-control" id="nd" name="register-nd" placeholder="">
                                    <div class="help-block"></div>
                                </div>

                                <div class="form-group">
                                    <label for="demand-name">Обозначение документа:<sup>*</sup></label>
                                    <input type="text" class="form-control" id="register-symbol" name="register-symbol"
                                           placeholder="">
                                    <div class="help-block"></div>
                                </div>

                                <div class="form-group">
                                    <label for="comment">Примечание к документу</label>
                                    <textarea class="form-control" rows="4" id="register-comment"
                                              name="register-comment"></textarea>
                                    <div class="help-block"></div>
                                </div>

                                <div class="form-group">
                                    <label for="comment">Статус документа<sup>*</sup></label>
                                    <select id="register-statusdoc" name="register-statusdoc" autocomplete="off"
                                             class="select2"
                                            placeholder="Выберите статус документа">
                                        <?
                                        foreach (register_status as $key => $value) { ?>
                                            <option value="<?= $value ?>"><?= $value ?></option>
                                        <? } ?>
                                    </select>
                                    <div class="help-block"></div>
                                </div>

                                <div class="form-group">
                                    <label>Группы документа:<sup>*</sup></label>
                                    <select name="register-group" autocomplete="off"  multiple class="select2"
                                            placeholder="Выберите группу документов">
                                        <? foreach (docsGroup as $key => $value) { ?>
                                            <option value="<?= $value; ?>"><?= $value; ?></option>
                                        <? } ?>
                                    </select>
                                    <div class="help-block"></div>
                                </div>

                                <div class="form-group">
                                    <label>Используется на предприятии:<sup>*</sup></label>
                                    <? foreach (company as $key => $value) { ?>
                                        <div class="checkbox">
                                            <label class="cr-styled">
                                                <input type="checkbox" name="company-<?= $key ?>" value="<?= $key ?>"><i
                                                        class="fa"></i><?= $value ?></label>
                                        </div>
                                    <? } ?>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="text-right">
                            <input type="button" class="btn btn-default" data-dismiss="modal" value="Отменить"/>
                            <input type="submit" class="btn btn-primary" value="Сохранить"/>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
<? } ?>

    <!-- ВСПЛЫВАЮЩЕЕ ОКНО(РЕДАКТОР ТРЕБОВАНИЙ) -->
<? if (strpos($_SERVER['REQUEST_URI'], 'demand') !== false) { ?>
    <div class="modal fade" id="demandModal" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Добавить требования</h4>
                </div>
                <form class="edit-demand" method="POST">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">

                                <input type="hidden" value="" name="demand-id"/>

                                <div class="form-group">
                                    <label for="demand-name">Нормативный документ:<sup>*</sup></label>
                                    <input type="text" class="form-control" name="demand-document" placeholder="">
                                    <div class="help-block"></div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="demand-paragraph-name">Пункт документа:<sup>*</sup></label>
                                            <input type="text" class="form-control" name="demand-paragraph-name">
                                            <div class="help-block"></div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="demand-paragraph-link">Ссылка на пункт документа:</label>
                                            <input type="text" class="form-control" name="demand-paragraph-link"
                                                   placeholder="9004157&prevdoc=901827804&point=mark=000000000007DE0K8">
                                            <div class="help-block"></div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div id="demand-status" class="col-md-6">
                                        <div class="form-group">
                                            <label>Статус документа:<sup>*</sup></label>
                                            <select class="form-control" autocomplete="off" name='demand-status'>
                                                <? foreach (demand_status as $key => $value) { ?>
                                                    <option value="<?= $value; ?>"><?= $value; ?></option>
                                                <? } ?>
                                            </select>
                                            <div class="help-block"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Выберите отрасль:<sup>*</sup></label>
                                            <select class="select2" autocomplete="off" multiple="multiple" required
                                                    name="demand-industry[]">
                                                <? foreach (docsGroupDemand as $key => $value) { ?>
                                                    <option value="<?= $value; ?>"><?= $value; ?></option>
                                                <? } ?>
                                            </select>
                                            <div class="help-block"></div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="demand-demand">Необходимые мероприятия:<sup>*</sup></label>
                                    <textarea class="wysihtml5 form-control" rows="5" name="demand-demand"></textarea>
                                    <div class="help-block"></div>
                                </div>

                                <div class="row">
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>Критичность:<sup>*</sup></label>
                                            <select class="form-control" autocomplete="off" name='demand-criticality1'>
                                                <option value="0" disabled selected>OHSAS</option>
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                            </select>
                                            <div class="help-block"></div>
                                        </div>
                                    </div>

                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label></label>
                                            <select class="form-control m-t-5" autocomplete="off"
                                                    name='demand-criticality2'>
                                                <option value="0" disabled selected>Влияние</option>
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                            </select>
                                            <div class="help-block"></div>
                                        </div>
                                    </div>

                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label></label>
                                            <select class="form-control m-t-5" autocomplete="off"
                                                    name='demand-criticality3'>
                                                <option value="0" disabled selected>КоАП</option>
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                            </select>
                                            <div class="help-block"></div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="cr-styled m-t-30">
                                                <input type="checkbox" name="demand-privilege">
                                                <i class="fa"></i> Привелигированное требование </label>
                                        </div>
                                    </div>
                                </div>

                               



                                <div id="chains2" class="row">

                                    <div class="chain">
                                            <hr>
                                         
                                        <div class="col-md-7">
                                            <div class="form-group">
                                                <label>Группа4:</label>
                                               <!-- <input type="text" class="form-control demand-group" autocomplete="on" name="demand-group-0"> -->
                                               <select id="el1">
                                                  
                                               </select>
                                                <div class="help-block"></div>
                                            </div>
                                        </div>

                                        <div class="col-md-1">
                                            <div class="form-group m-t-30">
                                                <a href="javascript:void(0);" class="remove-chunk"
                                                   onclick="removeChain();"><i class="fa ion-close text-danger"></i></a>
                                                <div class="help-block"></div>
                                            </div>
                                        </div>

                                        <div class="col-md-7">
                                            <div class="form-group">
                                                <label>Основной процесс:</label>
                                               <!-- <input class="form-control demand-main-process" autocomplete="on"
                                                        name='demand-main-process-0'> -->
                                                    <select id="el2"></select>
                                                <div class="help-block"></div>
                                            </div>
                                        </div>
                                        <div class="col-md-7">
                                            <div class="form-group">
                                                <label>Виды работ:</label>
                                                <!-- <input class="form-control demand-job" autocomplete="on"
                                                        name='demand-job-0'> -->
                                                    <select id="el3" class=""></select>
                                                <div class="help-block"></div>
                                            </div>
                                        </div>
                                        <div class="col-md-7">
                                            <div class="form-group">
                                                <label>Обеспечение:</label>
                                               <!-- <input class="form-control demand-provision" autocomplete="on"
                                                        name='demand-provision-0'> -->
                                                        <select id="el4"></select>
                                                    
                                                <div class="help-block"></div>
                                            </div>
                                        </div>
                                       
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                        


                                
                                <div class="row">
                                    <input type="hidden" value="" name="demand-connected"/>

                                    <div class="col-md-12 text-center">
                                        <a href="javascript:void(0);" class="add-chunk text-info" onclick="addChain();">Добавить
                                            еще одну связку</a>
                                    </div>
                                </div>


                                <div class="row">
                                    <div class="col-md-7">
                                        <div class="form-group">
                                            <label>Вид работы дочернии:</label>
                                            <select class="select2 form-control demand-job-child" autocomplete="off"
                                                    name='demand-job_child'>
                                                <option value="0">Не выбранно</option>
                                            </select>
                                            <div class="help-block"></div>
                                        </div>
                                    </div>
                                </div>

                                <hr>

                                <div class="form-group">
                                    <label for="demand-event">Тип требования по срокам:<sup>*</sup></label>
                                    <textarea class="form-control" rows="5" name="demand-event"></textarea>
                                    <div class="help-block"></div>
                                </div>

                                <div class="form-group">
                                    <label for="demand-source">Источник инфомации:<sup>*</sup></label>
                                    <input type="text" class="form-control" name="demand-source"
                                           value='<?= Sourse_app ?>'>
                                    <div class="help-block"></div>
                                </div>


                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="text-right">
                            <input type="button" class="btn btn-default" data-dismiss="modal" value="Отменить"/>
                            <input type="submit" class="btn btn-primary" value="Сохранить"/>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
<? } ?>

<? if (strpos($_SERVER['REQUEST_URI'], 'demand') !== false) { ?>
    <!-- ВСПЛЫВАЮЩЕЕ ОКНО(РЕДАКТОР СВЯЗЕЙ) -->
    <div class="modal fade" id="editJOB">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Редактор видов работ</h4>
                </div>
                <form id="job-list" method="POST" action="">

                    <? include $_SERVER['DOCUMENT_ROOT'] . "/app/views/demand/ajax-job.php"; ?>

                </form>
                <div class="modal-footer">

                </div>
            </div>
        </div>
    </div>
<? } ?>
    <!--Добавить список для цехов-->
<? if (strpos($_SERVER['REQUEST_URI'], 'demand') !== false) { ?>

    <div class="modal fade" id="add-listdata" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <!--                    <input type="hidden" value="" name="list_actions" data-action=""/>-->
                    <h4 class="modal-title">Создание списка</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <label class="col-sm-2 control-label">Тип задач</label>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group" id="list_type_demand">

                                <? foreach (type_list as $key => $value) {
                                    if ($key) {
                                        ?>
                                        <div class="checkbox-inline <?= $key == 1 ? "m-l-10" : "" ?>">
                                            <label class="cr-styled">
                                                <input type="checkbox" name="check" checked="" value="<?= $key ?>">
                                                <i class="fa"></i><?= $value ?> </label>
                                        </div>
                                    <? }
                                } ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <label class="col-sm-2 control-label"> Плошадки</label>
                            <div class="form-group">
                                <div id="platform_tree_list"></div>
                                <div class="help-block"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="text-right">
                        <input type="button" class="btn btn-default" data-dismiss="modal" value="Отменить"/>
                        <input type="button" id="save_list" class="btn btn-primary" value="Сохранить"/>
                    </div>
                </div>
            </div>
        </div>
    </div>

<? } ?>

<? if (strpos($_SERVER['REQUEST_URI'], 'demand') !== false) { ?>
    <!-- ВСПЛЫВАЮЩЕЕ ОКНО(РЕДАКТОР СВЯЗЕЙ) -->
    <div class="modal fade" id="editEHS">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Редактор EHS</h4>
                </div>
                <form id="ehs-list" method="POST" action="">

                    <? include $_SERVER['DOCUMENT_ROOT'] . "/app/views/demand/ajax-ehs.php"; ?>

                </form>
                <div class="modal-footer">

                </div>
            </div>
        </div>
    </div>
<? } ?>

<? if (strpos($_SERVER['REQUEST_URI'], 'reports') !== false) { ?>
    <!-- ВСПЛЫВАЮЩЕЕ ОКНО(РЕДАКТОР ТРЕБОВАНИЙ) -->
    <div class="modal fade" id="editReport">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Подробная информация</h4>
                </div>
                <form class="edit-report" method="POST">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">

                                <input type="hidden" value="" name="report-id"/>

                                <div class="form-group">
                                    <label>Аналитика:</label>
                                    <canvas id="report-chart" class="m-0-a" width="400" height="170"></canvas>
                                </div>

                                <div class="form-group">
                                    <label>Единый статус соответствия:</label>
                                    <select class="select2" autocomplete="off" name="single-tsatus"
                                            onchange="setSingleStatus(event);">
                                        <option value="" selected>Выберите статус соответствия</option>
                                        <option value="0">Не заполнено</option>
                                        <option value="5">N/A</option>
                                        <option value="1">более 90%</option>
                                        <option value="2">от 71 до 90%</option>
                                        <option value="3">от 41 до 70%</option>
                                        <option value="4">менее 40%</option>
                                    </select>
                                    <div class="help-block"></div>
                                </div>

                            </div>
                        </div>


                        <div class="modal-footer">
                            <div class="text-right">
                                <input type="button" class="btn btn-default" data-dismiss="modal" value="Отменить"/>
                                <input type="button" class="btn btn-primary" onclick="editEvaluation();"
                                       value="Сохранить"/>
                            </div>
                        </div>


                    </div>

                </form>
            </div>
        </div>
    </div>
<? } ?>