<?
include_once $_SERVER['DOCUMENT_ROOT'] . '/vendor/autoload.php';
include_once $_SERVER['DOCUMENT_ROOT'] . '/assets/DB.php';
include_once $_SERVER['DOCUMENT_ROOT'] . '/assets/all_const.php';
include_once $_SERVER['DOCUMENT_ROOT'] . '/assets/mail.php';


$db = DB::Connection();
//class Db1
//{
//    private const Mysql_user = "root";
//    private const Mysql_host = "127.0.0.1";
//    private const Mysql_password = "sis1801emp";
//    private const Mysql_DB = "Matrix_test";
//
//    public static function Connection()
//    {
//        $link = new mysqli(self::Mysql_host, self::Mysql_user, self::Mysql_password, self::Mysql_DB);
//        return $link;
//    }
//
//}
/* ---------- ОБЩИЕ ФУНКЦИИ ---------- */

//$docsGroup = ['Не выбрано','Пожарная безопасность', 'Охрана труда', 'Экология', 'Промышленная безопасность', 'ЛНД Газпром'];

# Собираем массив прав доступа пользователя

# Проверяем сессию и лицензию пользователя
if (!isset($_COOKIE['session'])) {
    if (strpos($_SERVER['REQUEST_URI'], 'login') == false) {
        header('Location: /login');
        exit();
    }
} else {
//    $userdata = mysqli_fetch_array($db->query( "SELECT * FROM users WHERE id = '" . intval($_COOKIE['id']) . "' LIMIT 1"));


    $userdata = $db->query("SELECT * FROM users WHERE id = '" . intval($_COOKIE['id']) . "' LIMIT 1")->fetch_array();

    if (($userdata['hash'] !== $_COOKIE['session']) or ($userdata['id'] !== $_COOKIE['id'])) {
        setcookie('id', '', time() - 60 * 24 * 30 * 12, '/');
        setcookie('session', '', time() - 60 * 24 * 30 * 12, '/');
        header('Location: /login');
        exit();
    }
    if ($userdata['company'] !== $_COOKIE['company']) {
        setcookie('company', $userdata['company'], time() + 3600 * 24, "/");
    }

    if ($userdata['platform'] !== $_COOKIE['platform']) {
        setcookie('platform', $userdata['platform'], time() + 3600 * 24, "/");
    }

}

# Получаем роль пользователя и статус
if (isset($_COOKIE['session'])) {
    $query = $db->query("SELECT users.level, users.access, users.status FROM `users`
							  WHERE users.login = '" . $_COOKIE['login'] . "' LIMIT 1")->fetch_array();
    $role = $query['level'];
    if ((($query['status'] == 2) OR ($query['status'] == 1)) AND ($_SERVER['REQUEST_URI'] <> "/login")) {
        header('Location: /login');
        exit();
    }
    $access = json_decode($query['access'], true);
}
$rools = [
    'tasks' => 'Рабочий стол',
    'reports' => 'Чек-листы',
    'calendar' => 'Календарь',
    'matrix' => 'Матрица',
    'analytics' => 'Аналитика',
    'register' => 'Реестр документов',
    'help' => 'Помощь'
];

if ($role == 'admin') {
    $rools['editor'] = 'Редактор стандартов';
    $rools['settings'] = 'Настройки';
    $rools['register'] = 'Реестр документов';
    $rools['demands'] = 'Редактор требований';
    // $rools['options'] = 'Редактор опций';
}

# Валидация данных, полученных из форм
function sanitizeString($var)
{
    $var = stripslashes($var);
    $var = htmlspecialchars($var);
    $var = strip_tags($var);
    $var = trim($var);
    return $var;
}

# Генерация случайной строки
function generateCode($length = 6)
{
    $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPRQSTUVWXYZ0123456789";
    $code = "";
    $clen = strlen($chars);
    while (strlen($code) <= $length) {
        $code .= $chars[mt_rand(0, $clen)];
    }
    return $code;
}

# Сортировка массива по ключу
function cmp($a, $b)
{
    if ($a == $b) {
        return 0;
    }
    return ($a < $b) ? -1 : 1;
}

# Получение списка пользоваталей компании
function getUsersFromCompany()
{
    $db = DB::Connection();

    $query_users = $db->query("SELECT id, login, name, surname, `father_name`
         FROM `users` 
         WHERE company='" . $_COOKIE['company'] . "'");
    return $query_users;
}


# Получение значения поля по значению другого поля
function getNodeByField($table, $field, $value)
{
    $db = DB::Connection();
    $result = $db->query("SELECT * FROM `$table` WHERE `$field` IN (" . $value . ")");

    while ($row = $result->fetch_assoc()) {
        $object[] = $row;
    }
    return $object;
}

# Получение значения поля по значению другого поля
function getFieldbyField($table, $field, $value, $needle)
{
    $db = DB::Connection();

    $result = $db->query("SELECT * FROM `$table` WHERE `$field`='" . $value . "'")->fetch_array();
    return $result[$needle];
}

# Получение максимального значения поля
function getMaxFieldValue($table, $field)
{
    $db = DB::Connection();
    $result = $db->query("SELECT MAX(`$field`) AS 'maxValue' FROM `$table` ");
    $row = $result->fetch_assoc();

    return intval($row['maxValue']);
}

# Получение значения поля по значению другого поля
if ($_POST['action'] == 'getFieldbyField') {
    $table = sanitizeString($_POST['table']);
    $field = sanitizeString($_POST['field']);
    $value = sanitizeString($_POST['value']);
    $needle = sanitizeString($_POST['needle']);

//    $result = mysqli_fetch_array($db->query( "SELECT * FROM `$table` WHERE `$field`='$value' "));

    $result = $db->query("SELECT * FROM `$table` WHERE `$field`='$value'")->fetch_array();

    echo $result[$needle];
}

# Получение записи(ей)
if ($_POST['action'] == 'get') {
    $table = $_POST['table'];
    $field = $_POST['field'];
    $value = $_POST['value'];
    $result = $db->query("SELECT * FROM `$table` WHERE `$field`='$value' ");
    while ($row = $result->fetch_assoc()) {
        $object['data'][] = $row;
    }

    echo json_encode($object, JSON_UNESCAPED_UNICODE);
}

# Удаление записи
if ($_POST['action'] == 'delete') {
    $table = sanitizeString($_POST['table']);
    $field = sanitizeString($_POST['field']);
    $value = sanitizeString($_POST['value']);

    $result = $db->query("DELETE FROM `$table` WHERE `$field` IN ($value)");
    echo $result;
}

# Получение максимального значения поля
if ($_POST['action'] == 'getMaxFieldValue') {
    $table = sanitizeString($_POST['table']);
    $field = sanitizeString($_POST['field']);
    //echo "SELECT MAX(`$field`) AS 'maxValue' FROM `$table` ";
    $result = $db->query("SELECT MAX(`$field`) AS 'maxValue' FROM `$table` ");
    while ($row = $result->fetch_assoc()) {
        echo intval($row['maxValue']);
    }
}


/* ---------- EHS и JOB ---------- */


function getEhs()
{
    $db = DB::Connection();
    $ehs = $db->query("SELECT * FROM `ehs` GROUP BY `ehs_numbers` ");

    while ($row = $ehs->fetch_array()) {
        $data[$row['id']]['id'] = $row['id'];
        $data[$row['id']]['Parent_id_job'] = $row['Parent_id_job'];
        $data[$row['id']]['ehs_numbers'] = $row['ehs_numbers'];
        $data[$row['id']]['ehs_name'] = $row['ehs_name'];

    }

    return $data;
}

# Получение Получение видов работ для NM
function getJobForNM()
{
    $db = DB::Connection();
    $ar = [];
    $jobs = $db->query("SELECT `id`, `type` FROM `types_jobs` WHERE `type_n` = 1 ORDER BY `type`");
    foreach ($jobs as $key => $value) {
        $ar[$key]['id'] = $value['id'];
        $ar[$key]['type'] = $value['type'];
    }
    // var_dump("gtgtgt");
    // var_dump($arr);
    return $ar;
}

# Получение видов работ без опасных условий или действий
function getTypeJobForNM()
{
    $db = DB::Connection();
    $ar = [];
    $jobs = $db->query("SELECT `id`, `type` FROM `types_jobs` WHERE `type_n` is NULL");
    foreach ($jobs as $key => $value) {
        $ar[$key]['id'] = $value['id'];
        $ar[$key]['type'] = $value['type'];
    }
    // var_dump("gtgtgt");
    // var_dump($arr);
    return $ar;
}


# Получение  названия вида работ по id self-evaluation
function getJobNameBySelfId($id)
{
    $db = DB::Connection();
//    $job_name = mysqli_fetch_assoc($db->query( "SELECT `type` FROM
//												 `types_jobs` WHERE `id` =
//												(SELECT `job` FROM `collector`
//												 WHERE `self-evaluation`= '" . $id . "' LIMIT 1)"));
    $job_name = $db->query("SELECT `type` FROM `types_jobs` WHERE `id` =
												(SELECT `job` FROM `collector`
												 WHERE `self-evaluation`= '" . $id . "' LIMIT 1)")->fetch_assoc();

    return $job_name['type'];
}

# Получение EHS по id
function getEhsById($id)
{
    $db = DB::Connection();
//    $ehs = mysqli_fetch_array($db->query( "SELECT * FROM ehs WHERE id = '" . intval($id) . "' LIMIT 1"));
    $ehs = $db->query("SELECT * FROM ehs WHERE id = '" . intval($id) . "' LIMIT 1")->fetch_array();
    return $ehs['ehs_numbers'];
}

# Получение id EHS по Наименованию
function getEhsByName($number)
{
    $db = DB::Connection();
    $ehs = $db->query("SELECT * FROM ehs WHERE ehs_numbers = '" . $number . "' LIMIT 1")->fetch_array();
    return $ehs['id'];
}

# Получение id EHS по Наименованию и Виду работ
function getEhsByFilter($number, $job)
{
    $db = DB::Connection();
    $ehs = $db->query("SELECT `id` FROM ehs WHERE ehs_numbers = '" . $number . "' AND Parent_id_job IN ('" . $job . "') LIMIT 1")->fetch_array();
    return $ehs['id'];
}

# Получение вида работы по id
function getJobById($id)
{
    $db = DB::Connection();
    $job = $db->query("SELECT * FROM types_jobs WHERE id = '" . intval($id) . "' LIMIT 1")->fetch_array();
    return $job['type'];
}

# Получение id вида работы по наименованию
function getJobByName($name)
{
    $db = DB::Connection();
    $job = $db->query("SELECT * FROM types_jobs WHERE type = '" . $name . "' LIMIT 1")->fetch_array();
    return $job['id'];
}

# Собираем используемые виды работ в строку
function getJobString($arr)
{

    foreach ($arr as $key => $value) {
        $jobs[] = $value;
    }

    $jobs = implode("','", $jobs);

    return $jobs;
}

# Конвертируем виды работ в строку
function jobToString($string)
{

    $string = explode(",", $string);
    $string = implode("','", $string);
    $string = trim($string, ',');

    return $string;
}

# Тест для проверки магических EHS
function getSimilarEHSid($id)
{
    $string = '';
    $db = DB::Connection();
    $number = getEhsById($id);

    $ehs = $db->query("SELECT * FROM `ehs` WHERE `ehs_numbers` = '" . $number . "'");
    while ($row = $ehs->fetch_array()) {
        $string .= $row['id'] . ",";
    }
    $string = trim($string, ',');
    return $string;
}

# Получение EHS
if ($_POST['action'] == 'getEHS') {
    $EHSInfo = $db->query("SELECT DISTINCT `ehs_numbers` FROM `ehs` ");
    while ($row = $EHSInfo->fetch_array()) {
        $ehs[] = $row['ehs_numbers'];
    }
    echo json_encode($ehs, JSON_UNESCAPED_UNICODE);
}

# Получение JOB по EHS
if ($_POST['action'] == 'getJOBbyEHS') {
    $dataInfo = $db->query(
        "SELECT t.id,t.type 
		 FROM `types_jobs` t
		 WHERE t.`id` IN (
			SELECT `Parent_id_job`
			FROM `ehs`
			WHERE `ehs_numbers` = '" . $_POST['ehs'] . "' 
		 )ORDER BY t.`type` ASC");
    while ($row = $dataInfo->fetch_array()) {
        $data[$row['id']] = $row['type'];
    }
    sort($data, SORT_STRING);
    echo json_encode($data, JSON_UNESCAPED_UNICODE);
}


# Получение всех JOB
if ($_POST['action'] == 'getJOB') {
    $dataInfo = $db->query("SELECT * FROM `types_jobs`");
    while ($row = $dataInfo->fetch_array()) {
        $data[$row['id']] = $row['type'];
    }
    echo json_encode($data, JSON_UNESCAPED_UNICODE);
}


/* ---------- ПОЛЬЗОВАТЕЛИ ---------- */

# Проверка платформ у пользователей 
function checkUsersPlafrom()
{
    $db = DB::Connection();
    $users = getUsers();

    foreach ($users as $user) {

        $platforms = explode(',', $user['platform']);
        $newUserPlatforms = '';

        foreach ($platforms as $platform) {

            $exist = $db->query("SELECT * FROM `platform` WHERE `id` = " . $platform);

            if ($exist->num_rows > 0) {

                $newUserPlatforms .= $platform . ",";

            }

        }

        $db->query("UPDATE `users` SET `platform` = '" . trim($newUserPlatforms, ',') . "' WHERE `login`='" . $user['login'] . "'");

    }

}

# Отправка пароля


# Массовый сброс пароля пользователей


//# Добавление/редактирование пользователя
//if ($_POST['action'] == 'updateUsers') {
//
//    $data = explode('&', $_POST['data']);
//
//    // Преобразуем в массив для работы
//    foreach ($data as $key => $value) {
//
//        $parts = explode('=', $value);
//
//        $conditions[$parts[0]] = urldecode($parts[1]);
//
//        if ($parts[0] == 'platform') {
//            $platform .= $parts[1] . ",";
//        }
//
//    }
//
//    $conditions['access'] = $_POST['access'];
//
//    $operation = $conditions['operation'];
//    unset($conditions['operation']);
//
//    // Проверяем есть ли такой Логин
//    $user = $db->query( "SELECT `id` FROM `users` WHERE `login` = '" . $conditions['login'] . "'");
//
//    if ($operation == 'add') {
//
//        $email = $db->query( "SELECT `id` FROM `users` WHERE `email` = '" . $conditions['email'] . "'");
//
//        // Проверяем есть ли уже такой логин
//        if (mysqli_num_rows($user) > 0) {
//            header('HTTP/1.1 403 Forbidden');
//            $status['type'] = 'error';
//            $status['message'] = 'Операция не выполнена';
//            $status['message2'] = 'Данный логин уже используется в системе';
//            print_r(json_encode($status, JSON_UNESCAPED_UNICODE));
//            return;
//        }
//
//        // Проверяем есть ли уже такой email
//        if (mysqli_num_rows($email) > 0) {
//            header('HTTP/1.1 403 Forbidden');
//            $status['type'] = 'error';
//            $status['message'] = 'Операция не выполнена';
//            $status['message2'] = 'Данный email уже используется в системе';
//            print_r(json_encode($status, JSON_UNESCAPED_UNICODE));
//            return;
//        }
//        // Проверяет существует ли домен у такого email
//        $domen_of_mail = substr($conditions['email'], strrpos($conditions['email'], "@") + 1);
//        if (checkdnsrr($domen_of_mail, "MX") == false) {
//            header('HTTP/1.1 403 Forbidden');
//            $status['type'] = 'error';
//            $status['message'] = 'Операция не выполнена';
//            $status['message2'] = 'Не правильный email. ' . $domen_of_mail . ' не существует';
//            print_r(json_encode($status, JSON_UNESCAPED_UNICODE));
//            return;
//        }
//    }
//
//    $conditions['platform'] = trim($platform, ',');
//    $conditions['company'] = $_COOKIE['company'];
//    // $conditions['status'] = 3;
//    // $conditions['level'] = "user";
//    setcookie("platform", $conditions['platform'], time() + 3600 * 24, "/");
//
//    foreach ($conditions as $key => $value) {
//        $string .= " `" . $key . "`='" . $value . "',";
//    }
//
//    $string = trim($string, ',');
//
//    if (mysqli_num_rows($user) == 0) {
//
//        $password = substr(uniqid(), 0, 7);
//        $hash = md5(md5($password));
//        $string .= ", `password`='" . $hash . "'";
//        $q = "INSERT INTO `users` SET `status`=3, `level`='auditor', " . $string;
//        $query = $db->query( $q);
//
//        sendPassword($conditions['name'], $conditions['login'], $conditions['email'], $password, 'create');
//        sendPassword($conditions['name'], $conditions['login'], 'info@sunkt.ru', $password, 'create');
//
//    } else {
//        $query = $db->query( "UPDATE `users` SET " . $string . " WHERE `login`='" . $conditions['login'] . "'");
//    }
//    if ($query == true) {
//        // Успешное выполнение
//        $status['type'] = 'success';
//        $status['message'] = 'Операция выполнена';
//        $status['message2'] = 'Информация пользователя изменена';
//        print_r(json_encode($status, JSON_UNESCAPED_UNICODE));
//        return;
//        var_dump($query);
//    }
//}
//
//# Удаление пользователей
//if ($_POST['action'] == 'removeUsers') {
//    $idArr = $_POST['ids'];
//    $db->query( "DELETE FROM `users` WHERE `id` IN ($idArr)");
//}
//
//# Получение пользователей
//if ($_POST['action'] == 'getUsers') {
//    $userInfo = $db->query( "SELECT * FROM `users` WHERE `company`= '" . $_COOKIE['company'] . "'");
//    while ($uRow = mysqli_fetch_array($userInfo)) {
//        $fio[] = $uRow['surname'] . ' ' . $uRow['name'] . ' ' . $uRow['father_name'];
//    }
//    echo json_encode($fio, JSON_UNESCAPED_UNICODE);
//}

# Права доступа в системе
function getUsersGroup()
{
    $db = DB::Connection();
    $query = $db->query("SELECT * FROM `users_group`");

    while ($row = $query->fetch_assoc()) {
        $data[$row['name']] = $row['translation'];
    }

    return $data;
}

# Получение всех платформ компании
function getPlatforms()
{
    $db = DB::Connection();
    $query = $db->query("SELECT * FROM `platform` WHERE `company`= '" . $_COOKIE['company'] . "'");

    while ($row = $query->fetch_assoc()) {
        $data[$row['id']]['id'] = $row['id'];
        $data[$row['id']]['name'] = $row['name'];
        $data[$row['id']]['settings'] = $row['settings'];
        $data[$row['id']]['company'] = $row['company'];
        $data[$row['id']]['criticality'] = $row['criticality'];
    }

    return $data;
}

# Получение названия платформы по ее ID
function getPlatformsNames($platform)
{
    $db = DB::Connection();
    $platform_name = $db->query("SELECT `name` FROM `platform` WHERE `id` = '" . $platform . "'")->fetch_assoc();
    return $platform_name;
}

# Получение пользователей
function getUsers($initials = false)
{
    $db = DB::Connection();
    $userInfo = $db->query("SELECT u.* , d.name_division, t.name_type
                            FROM `users` AS u
                            LEFT JOIN `divisions` d ON u.division = d.id
                            LEFT JOIN `type_division` t ON d.`type_divisions` = t.id
                            WHERE `company`= '" . $_COOKIE['company'] . "' ORDER BY `login` ASC");

    while ($uRow = $userInfo->fetch_array()) {

        if ($initials == true) {
            $name = mb_substr($uRow['name'], 0, 1, 'UTF-8');
            $fatherName = mb_substr($uRow['father_name'], 0, 1, 'UTF-8');
            $data[$uRow['id']]['name'] = $uRow['surname'] . ' ' . $name . '. ' . $fatherName . '.';
        } else {
            $data[$uRow['id']]['name'] = $uRow['surname'] . ' ' . $uRow['name'] . ' ' . $uRow['father_name'];
        }
        $data[$uRow['id']]['id'] = $uRow['id'];
        $data[$uRow['id']]['login'] = $uRow['login'];
        $data[$uRow['id']]['first_name'] = $uRow['name'];
        $data[$uRow['id']]['surname'] = $uRow['surname'];
        $data[$uRow['id']]['father_name'] = $uRow['father_name'];
        $data[$uRow['id']]['email'] = $uRow['email'];
        $data[$uRow['id']]['phone'] = $uRow['phone'];
        $data[$uRow['id']]['level'] = $uRow['level'];
        $data[$uRow['id']]['city'] = $uRow['city'];
        $data[$uRow['id']]['company'] = $uRow['company'];
        $data[$uRow['id']]['platform'] = $uRow['platform'];
        $data[$uRow['id']]['status'] = $uRow['status'];
        $data[$uRow['id']]['name_division'] = $uRow['name_division'];
        $data[$uRow['id']]['name_type'] = $uRow['name_type'];
    }

    return $data;
}

# Получение ФИО пользователя
function getUserName($login)
{
    $db = DB::Connection();
    $userInfo = $db->query("SELECT * FROM `users` WHERE `login`='" . $login . "'");
    while ($uRow = $userInfo->fetch_array()) {
        $data['name'] = $uRow['surname'] . ' ' . $uRow['name'] . ' ' . $uRow['father_name'];
        // $data['login'] = $uRow['login'];
    }
    return $data['name'];
}

# Получение краткое ФИО пользователя по его id
function getSmallFioFromId($id_user)
{
    $db = DB::Connection();
    $fio_user = $db->query("SELECT name, surname, father_name FROM `users` WHERE `id`='" . $id_user . "' LIMIT 1")->fetch_assoc();
    if ($fio_user['surname'] <> '' AND $fio_user['name'] <> '' AND $fio_user['father_name'] <> '') {
        $small_fio = $fio_user['surname'] . ' ' . mb_substr($fio_user['name'], 0, 1, "utf-8") . '.' . mb_substr($fio_user['father_name'], 0, 1, "utf-8") . '.';
    }
    if ($fio_user['surname'] <> '' AND $fio_user['name'] <> '' AND $fio_user['father_name'] == '') {
        $small_fio = $fio_user['surname'] . ' ' . mb_substr($fio_user['name'], 0, 1, "utf-8") . '.';
    }
    return $small_fio;
}


function getUserid($id)
{
    $db = DB::Connection();
    $userInfo = $db->query("SELECT * FROM `users` WHERE `id`='" . $id . "'");
    while ($uRow = $userInfo->fetch_array()) {
        $data['name'] = $uRow['surname'] . ' ' . $uRow['name'] . ' ' . $uRow['father_name'];
        // $data['login'] = $uRow['login'];
    }
    return $data['name'];
}


# Получение ответственного
function getResponse($data)
{
    $conditions = '';
    $db = DB::Connection();
    $data = explode(',', $data);

    foreach ($data as $key => $value) {
        $conditions .= "'" . $value . "',";
    }

    $conditions = trim($conditions, ',');

    // return $conditions;
    // return $fio;

    $userInfo = $db->query("SELECT * FROM `users` WHERE `login` IN (" . $conditions . ")");
    while ($uRow = $userInfo->fetch_array()) {
        $fio[] = $uRow['surname'] . ' ' . $uRow['name'] . ' ' . $uRow['father_name'];
    }

    return $fio[0];

    // foreach ($fio as $key => $value) {
    // 	echo "<div>".$value."</div>";
    // }
}

# Получение пользоватей по платформе
function getUsersByPlatform()
{
    $data = '';
    $db = DB::Connection();
    $result = $db->query("SELECT user.login FROM `users` AS user
	                               WHERE user.platform = (
          	                          SELECT `platform` FROM `users` WHERE `login` = '" . $_COOKIE['login'] . "'
	                               )");

    while ($row = mysqli_fetch_assoc($result)) {
        $data .= "'" . $row['login'] . "',";
    }

    $data = trim($data, ',');

    return $data;
}

# Получение пользоватей по платформе, используя ID платформы
function getUsersByPlatformFromId($platform_id)
{
    $db = DB::Connection();

    $result = $db->query("SELECT login, name, surname, father_name FROM `users` WHERE `platform` LIKE '%" . $platform_id . "%' AND `company` = '" . $_COOKIE['company'] . "' ");

    return $result;
}

# Счетчик заходов пользователя
function visitCount($id, $visit)
{
    $db = DB::Connection();
    $db->query("UPDATE users SET count='" . $visit . "' WHERE id='" . $id . "'") or die("MySQL Error: " . mysqli_error());
}


# Комментарии
function comments($id_task)
{
    $db = DB::Connection();
    $json_comments = $db->query("SELECT `comments`
		  FROM `tasks` 
		  WHERE id='" . $id_task . "' LIMIT 1")->fetch_assoc();
    $comments = json_decode($json_comments['comments']);
    return $comments;
}


/* ---------- РЕДАКТОР ТРЕБОВАНИЙ ---------- */


# Проверяем критичность требования
function demandCriticalLevel($amount)
{

    if ($amount >= 8 && $amount <= 9) {
        $level[] = 1;
        $level[] = 2;
        return $level;
    } else if ($amount >= 5 && $amount < 8) {
        $level[] = 2;
        $level[] = 3;
        return $level;
    } else if ($amount < 5) {
        $level[] = 4;
    }

    return $level;
}

# Цвет статуса в редакторе требований
function demandStatusColor($status)
{
    $status = str_replace("Статус:", "", $status);
    $status = trim($status);

    if (strpos($status, 'Недействующее') !== false) {
        echo "text-danger";
    } elseif (strpos($status, 'Действующее') !== false) {
        echo "text-success";
    } elseif (strpos($status, 'Проект') !== false) {
        echo "text-muted";
    } elseif (strpos($status, 'На утверждении') !== false) {
        echo "text-muted";
    } else {
        echo "text-success";
    }
}

# Добавление/редактирование требования
if ($_POST['action'] == 'addDemand' || $_POST['action'] == 'editDemand') {

    $demand['document'] = getDocumentBySymbol($_POST['demand-document']);
    $demand['paragraph-name'] = sanitizeString($_POST['demand-paragraph-name']);
    $demand['paragraph-link'] = sanitizeString($_POST['demand-paragraph-link']);
    $demand['demand'] = $_POST['demand-demand'];
    $demand['user'] = $_COOKIE['login'];
    $demand['criticality1'] = sanitizeString($_POST['demand-criticality1']);
    $demand['criticality2'] = sanitizeString($_POST['demand-criticality2']);
    $demand['criticality3'] = sanitizeString($_POST['demand-criticality3']);
    $demand['industry'] = sanitizeString($_POST['demand-industry']);
    $demand['job_child'] = sanitizeString($_POST['demand-job_child']);

    // Если требование добавляется берем максимальное значение connected + 1
    if ($_POST['action'] == 'addDemand') {
        $demand['connected'] = getMaxFieldValue('demands', 'connected') + 1;
    }
    // Если требование редактируется оставляем его без изменнения
    if ($_POST['action'] == 'editDemand') {
        $demand['connected'] = $_POST['demand-connected'];
    }

    if (sanitizeString($_POST['demand-privilege']) == 'on') {
        $demand['privilege'] = 1;
    }

    if (isset($_POST['demand-status'])) {
        $demand['status'] = sanitizeString($_POST['demand-status']);
    } else {
        $demand['status'] = 'На утверждении';
    }


    $demand['event'] = sanitizeString($_POST['demand-event']);
    $demand['source'] = $_POST['demand-source'];
    $now = new DateTime();
    $demand['date'] = $now->getTimestamp();

    foreach ($demand as $key => $value) {
        $recordPart1 .= " `" . $key . "`='" . $value . "',";
    }

    for ($i = 0; $i < 20; $i++) {

        if (isset($_POST['demand-ehs-' . $i])) {

            $currentNumber = $_POST['demand-ehs-' . $i];
            $currentJob = getJobByName(sanitizeString($_POST['demand-job-' . $i]));

            $recordPart2 = '';
            $recordPart2 .= " `ehs`='" . getEhsByFilter($currentNumber, $currentJob) . "',";
            $recordPart2 .= " `job`='" . getJobByName(sanitizeString($_POST['demand-job-' . $i])) . "'";
            $record[$i] = $recordPart1 . $recordPart2;

        } else {

            break;

        }

    }

    if ($_POST['action'] == 'addDemand') {

        foreach ($record as $key => $value) {
            $query = $db->query("INSERT INTO `demands` SET " . $value);

            //$lastInsertedPeopleId = mysqli_insert_id($db);
            //var_dump($lastInsertedPeopleId);
            //$db->query( "INSERT INTO `changes_demands` SET ");
        }

    }

    if ($_POST['action'] == 'editDemand') {

        $db->query("DELETE FROM `demands` WHERE `connected` =" . $_POST['demand-connected']);

        foreach ($record as $key => $value) {
            $query = $db->query("INSERT INTO `demands` SET " . $value);
        }

        // Если цепочка требований была отредактирована просто удаляем её из коллектора(далее она зальется сама)
        $db->query("DELETE FROM `collector` WHERE `connected` = " . $_POST['demand-connected']);
    }

}

# Получаем buch по ID отчета

function getBunchOfId($id)
{
    $db = DB::Connection();
    $number_bunch = $db->query("SELECT `bunch` FROM `reports` WHERE `id` IN ('" . $id . "')")->fetch_assoc();
    return $number_bunch['bunch'];
}

# Находим все площадки компании
function getCompanyPlatforms()
{
    $db = DB::Connection();
    $query = $db->query("SELECT `id` FROM platform WHERE `company` = '" . $_COOKIE['company'] . "'");

    while ($row = $query->fetch_assoc()) {
        $platforms[] = $row['id'];
    }

    return $platforms;

}

# Обновление коллектора
function updateCollector($connected = false)
{
    $db = DB::Connection();
    // Редактируем требования в коллекторе по идентификатуру связки
    if ($connected != false) {

    }

    // Находим в справочнике записи с новым `connected` (нет в коллекторе)
    $new = $db->query("SELECT * FROM `demands` WHERE
							   `status` = 'Действующий' AND
		                       `connected` NOT IN 
		                       (SELECT DISTINCT `connected` FROM `collector`)");


    // Определяем кол-во чек-листов
    $reports = $db->query("SELECT COUNT(*), `bunch`,`platform`,`creator`,`date`
	                           	   FROM `reports` 
	                               WHERE `platform` IN (" . implode(',', getCompanyPlatforms()) . ")
	                           	   GROUP BY `bunch`");

    // Для каждой группы чек-листов
    while ($rRow = $reports->fetch_assoc()) {

        // Для каждого требования из цепочки
        mysqli_data_seek($new, 0);
        while ($cRow = mysqli_fetch_assoc($new)) {

            // Определяем идентификатор чек-листа в который будет добавлено требование
            $reportId = getReportIdbyJob($cRow['job'], $rRow['bunch'], $rRow['platform'], $rRow['creator'], $rRow['date']);

            // Добавляем запись в коллектор
            $query = "INSERT INTO `collector` SET
			`ehs`='" . $cRow['ehs'] . "', 
			`job`='" . $cRow['job'] . "', 
			`demand`='" . $cRow['demand'] . "', 
			`event`='" . $cRow['event'] . "', 
			`paragraph-name`='" . $cRow['paragraph-name'] . "', 
			`paragraph-link`='" . $cRow['paragraph-link'] . "', 
			`source`='" . $cRow['source'] . "', 
			`document`='" . $cRow['document'] . "', 
			`connected`='" . $cRow['connected'] . "', 
			`privilege`='0', 
			`criticality1`='" . $cRow['criticality1'] . "', 
			`criticality2`='" . $cRow['criticality2'] . "', 
			`criticality3`='" . $cRow['criticality3'] . "', 
			`demand-id`='" . $cRow['id'] . "', 
			`user`='" . $_COOKIE['login'] . "', 
			`date`='" . $cRow['date'] . "', 
			`self-evaluation`=$reportId, 
			`calendar`= NULL,
			`platform`= '" . $rRow['platform'] . "' ";

            $result = $db->query($query);
            var_dump($result);
        }

    }
}

# Определяем идентификатор чек-листа в который будет добавлено требование
function getReportIdbyJob($job, $bunch, $platform, $creator, $date)
{
    $db = DB::Connection();
    $reports = $db->query("SELECT `job`, `self-evaluation`
		                           FROM `collector`
                                   WHERE `job` = $job
                                   AND `self-evaluation` IN
                                   (
										SELECT `id` from `reports`
										WHERE `bunch` = $bunch
                                   )
		                           GROUP BY `self-evaluation`");

    // Если вид работы старый(уже есть в списке)
    if ($reports->num_rows > 0) {
        if ($row = $reports->fetch_assoc()) {
            return $row['self-evaluation'];
        }
    }
    // Если вид работы новый - добавляем новый отчет
    if ($reports->num_rows == 0) {

        $reportId = getMaxFieldValue('reports', 'id') + 1;

        // Добавляем запись в таблицу отчетов
        $db->query("INSERT INTO `reports` SET 
			                `id`='" . $reportId . "',
			                `bunch`='" . $bunch . "',
			                `status`='0',
			                `creator`='" . $creator . "',
			                `platform`='" . $platform . "',
			                `date` = " . $date);

        return $reportId;

    }
}

# Удаление требований
if ($_POST['action'] == 'removeDemand') {
    $idArr = $_POST['ids'];
    $connectedArr = $db->query("SELECT `connected` from `demands` WHERE `id` IN ($idArr)");
    while ($row = $connectedArr->fetch_array()) {
        $db->query("DELETE FROM `demands` WHERE `connected` = " . $row['connected']);
        $db->query("DELETE FROM `collector` WHERE `connected` = " . $row['connected']);
    }
}

# Подтверждение требований с одинаковым ID связок
if ($_POST['action'] == 'approveDemand') {
    $idArr = $_POST['ids'];
    $query = "UPDATE `demands` t1 SET t1.status = 'Действующий' 
		                WHERE t1.connected IN (
		  	            	SELECT DISTINCT t2.connected FROM (SELECT `id`,`connected` FROM `demands`) t2 WHERE t2.id IN (" . $idArr . ")
		                )";
    $result = $db->query($query);
    // Обновление коллектора
    updateCollector();
}


/* ---------- ЧЕК ЛИСТЫ ---------- */

function getReportProgress($status)
{

    $data = 0 / 8 * 100;

    return $data;

}

function getReportType($type) //переделать сделаьб общую для всех
{

    switch ($type) {
        case 'self-evaluation':
            $data = 'Самооценка';
            break;
        case 'audit':
            $data = 'Аудит';
            break;
        case 'government':
            $data = 'Проверка гос. органов';
            break;
        case 'state_inspection':
            $data = 'Проверка Гос/органов';
            break;
        case 'product_control':
            $data = 'Производственный контроль';
            break;
    }

    return $data;

}

function getReportsByPlatform()
{
    $db = DB::Connection();
    // return "SELECT * FROM `reports` WHERE `platform` IN (".$_COOKIE['platform'].") GROUP BY `bunch`";

    $result = $db->query("SELECT * FROM `reports` WHERE `platform` IN (" . $_COOKIE['platform'] . ") GROUP BY `bunch`");

    while ($row = $result->fetch_assoc()) {
        $data[] = $row;
    }

    return $data;

}

function getBunchReports($bunch)
{
    $reports_string = '';
    $db = DB::Connection();
    $result = $db->query("SELECT `id` FROM `reports` WHERE `bunch` = " . $bunch);

    while ($row = $result->fetch_assoc()) {
        $reports_string .= $row['id'] . ',';
    }

    $data = trim($reports_string, ',');
    return $data;

}

function getBunchDemands($id_self)
{
    $db = DB::Connection();
    $result = $db->query("SELECT `id` FROM `collector` WHERE `self-evaluation` = " . $id_self);
    while ($row = $result->fetch_assoc()) {
        $data[] = $row['id']; // Список ID  нужных нам
    }
    return $data;
}

function getSettingsSelf($id_self)
{
    $db = DB::Connection();
    $settings_self = $db->query("SELECT `date`,`type`,`platform` FROM `reports` WHERE
                                  `id` IN (" . trim($id_self, ',') . ") ");
    $data = $settings_self->fetch_assoc();
    return $data;
}

function getPlatformFromBunch($bunch)
{
    $db = DB::Connection();
    $bunch = $db->query("SELECT `platform` FROM `reports` WHERE
                                  `bunch` = {$bunch}");
    $data = $bunch->fetch_assoc();
    return $data;
}

/* Фильтр. Получаем id требований, в которых есть actions. $id_demands_str - список всех id Требований */
function FilterDemands($id_demands_str)
{
    $db = DB::Connection();
    $data_query = $db->query("SELECT `id`, `job`, `actions`, `recommendations`, `fio`, `current-status-compliance`, `date-plan`
								  FROM `collector` 
								  WHERE `id` 
								  IN (" . trim($id_demands_str, ',') . ") AND `actions` <> ''"); // Берем нужные нам данные из базы
    return $data_query;
}


# Закрытие/открытие отчета
if ($_POST['action'] == 'closeEvaluation') {

    $result = $db->query("SELECT `id`, `status` FROM `reports` WHERE `bunch` = " . $_POST['bunch']);

    // echo "SELECT `id`, `status` FROM `reports` WHERE `bunch` = ".$_POST['bunch'];

    if (mysqli_num_rows($result) == 0) {
        header('HTTP/1.1 500 Forbidden');
        $status['type'] = 'error';
        $status['message'] = 'Ошибка';
        $status['message2'] = 'Данного отчета не существует';
        print_r(json_encode($status, JSON_UNESCAPED_UNICODE));
        return;
    }

    // $reports = getBunchReports( $_POST['bunch']);
    $row = $result->fetch_assoc();

    // var_dump($row);

    # Проверяем первую запись
    if ($row['status'] == 0) {
        $new_status = 1;
        /*$db->query( "INSERT INTO `collector`
                             SELECT * FROM `collector_archive`
                             WHERE `self-evaluation` IN (".$reports.")");*/
    } elseif ($row['status'] == 1) {
        $new_status = 0;
    }

    // echo "UPDATE `reports` SET `status` = ".$new_status." WHERE `bunch` = ".$_POST['bunch'];

    $db->query("UPDATE `reports` SET `status` = " . $new_status . " WHERE `bunch` = " . $_POST['bunch']);

    $status['type'] = 'success';
    $status['message'] = 'Операция успешно выполнена';
    $status['message2'] = 'Статус отчета изменен';
    print_r(json_encode($status, JSON_UNESCAPED_UNICODE));

}

# Статус чек-листа
function getReportStatus($status)
{

    switch ($status) {
        case 0:
            $status = "В работе";
            break;
        case 1:
            $status = "Закрыт";
            break;
        case 2:
            $status = "В архиве";
            break;
    }

    return $status;

}

# Идентификаторы активных отчетов
function getActiveReport()
{
    $data = '';
    $db = DB::Connection();
    $result = $db->query("SELECT `id` FROM `reports` WHERE `status` = '1' ");

    while ($row = $result->fetch_assoc()) {
        $data .= "'" . $row['id'] . "',";
    }

    $data = trim($data, ',');

    return $data;
}

# Проверяем критичность требования
function isCritical()
{
}

function getJobsNamesString($ids)
{
    $db = DB::Connection();
    $string = '';
    $jobs = $db->query("SELECT DISTINCT types_jobs.`type` FROM `collector` 
	                            INNER JOIN `types_jobs`
	                            ON types_jobs.id = collector.job
	                            WHERE collector.`self-evaluation` IN (" . $ids . ")");
    while ($row = $jobs->fetch_assoc()) {
        $string .= $row['type'] . ",";
    }
    $string = trim($string, ',');
    return $string;
}

// вытягиваем дату создания существующих отчетов
function dateReportCreate($id_audit)
{
    $db = DB::Connection();
    $date_report = $db->query("SELECT `date` FROM reports
	 												WHERE `id` = '" . intval($id_audit) . "'")->fetch_assoc();


    $date_audit = date("d.m.Y", $date_report['date']);
    return $date_audit;
}

# Создание чек-листов
if ($_POST['action'] == 'collector') {

    // $db->query( "TRUNCATE TABLE `reports`");
    // $db->query( "TRUNCATE TABLE `collector`");

    // Проверяем есть уже ли по данной платформе открытый отчет
    $platform = $db->query("SELECT * FROM `reports` 
		                             WHERE `platform` = (" . $_POST['platform'] . ") AND
		                             `type` = '" . $_POST['type'] . "' AND
									 `status` = 0
		                             ");

    if (mysqli_num_rows($platform) > 0) {

        $status['type'] = 'error';
        $status['message'] = 'По данной платформе есть открытый отчет';
        $status['message2'] = 'Нельзя создавать несколько групп отчетов в одном отчетном периоде';
        print_r(json_encode($status, JSON_UNESCAPED_UNICODE));
        return;

    }

    // Получаем максимальный идентификатор группы отчетов и увеличиваем его на 1
    $reportGroupId = getMaxFieldValue('reports', 'bunch') + 1;

    // Время создания отчета
    $now = strtotime("now");


    // Флаг для определения отчета
    $oldJob = '';

//// Зачем по ehs если нам нужно вид работ и возможно область

    // Собираем массив требований из справочника по ehs
    // Не забудь про 137 записей, которые находяться в справочнике с неправильным EHS
    $demands = $db->query("SELECT * FROM `demands` 
		                            WHERE `ehs` IN (SELECT `id` FROM `ehs`) AND
		                            `document` IN (" . implode(',', getCompanyDocuments()) . ")
                                    ORDER BY `job`
		                            ");

    // $status['type'] = 'debug';
    // $status['object'] = "SELECT * FROM `demands` WHERE `ehs` NOT IN (".$ehs.") ";
    // print_r( json_encode($status,JSON_UNESCAPED_UNICODE) );
    // return;

    // exit;
    /*
    массив виды работ job_vid=[1,2,3,4,5,6];
    foreach (job_vid as $key=>$value){

    }
    $query="
    INSERT INTO collector(
    ehs,
    job,
    demand,
    event,
    `paragraph-name`,
    `paragraph-link`,
    source,
    document,
    connected,
    privilege,
    criticality1,
    criticality2,
    criticality3,
    `demand-id`,
    `self-evaluation`,
    date,
    platform,
    user,
    actual,
    calendar)
    SELECT
           e.ehs_numbers,
           d.job,
           d.demand,
           d.event,
           d.`paragraph-name`,
           d.`paragraph-link`,
           d.source,
           d.document,
           d.connected,
           d.privilege,
           d.criticality1,
           d.criticality2,
           d.criticality3,
           d.id,
    $reportId,
    d.date,
    '".$_POST['platform']."',
    '".$_COOKIE['login']."',
    1,
    NULL
    FROM demands d JOIN ehs e
      ON d.ehs=e.id
      WHERE d.job IN (1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22)"
    */
    if ($demands->num_rows > 0) {

        while ($row = mysqli_fetch_assoc($demands)) {

            // Если новый вид работ, создаем новый отчет
            if ($oldJob != $row['job']) {

                // Получаем максимальный идентификатор самооценки и увеличиваем его на 1
                $reportId = getMaxFieldValue('reports', 'id') + 1;

                // Добавляем запись в таблицу отчетов
                $db->query("INSERT INTO `reports` SET 
					                `id`='" . $reportId . "',
					                `bunch`='" . $reportGroupId . "',
					                `status`='0',
					                `creator`='" . $_COOKIE['login'] . "',
					                `platform`='" . $_POST['platform'] . "',
					                `type`='" . $_POST['type'] . "',
					                `date` = " . $now);

            }

            $full[$row['id']]['ehs-name'] = getFieldbyField('ehs', 'id', $row['ehs'], 'ehs_name');
            $full[$row['id']]['ehs'] = $row['ehs'];
            $full[$row['id']]['job'] = $row['job'];
            $full[$row['id']]['demand'] = $row['demand'];
            $full[$row['id']]['event'] = $row['event'];
            $full[$row['id']]['paragraph-name'] = $row['paragraph-name'];
            $full[$row['id']]['paragraph-link'] = $row['paragraph-link'];
            $full[$row['id']]['source'] = $row['source'];
            $full[$row['id']]['document'] = $row['document'];
            $full[$row['id']]['connected'] = $row['connected'];
            $full[$row['id']]['privilege'] = $row['privilege'];
            $full[$row['id']]['criticality1'] = $row['criticality1'];
            $full[$row['id']]['criticality2'] = $row['criticality2'];
            $full[$row['id']]['criticality3'] = $row['criticality3'];
            $full[$row['id']]['date'] = $row['date'];
            $full[$row['id']]['demand-id'] = $row['id'];
            $full[$row['id']]['self-evaluation'] = $reportId;

            $oldJob = $row['job'];

        }

        // Добавляем записи в коллектор
        foreach ($full as $key => $value) {

            $db->query("INSERT INTO `collector` SET 
			`ehs`='" . $full[$key]['ehs'] . "', 
			`job`='" . $full[$key]['job'] . "', 
			`demand`='" . $full[$key]['demand'] . "', 
			`event`='" . $full[$key]['event'] . "', 
			`paragraph-name`='" . $full[$key]['paragraph-name'] . "', 
			`paragraph-link`='" . $full[$key]['paragraph-link'] . "', 
			`source`='" . $full[$key]['source'] . "', 
			`document`='" . $full[$key]['document'] . "', 
			`connected`='" . $full[$key]['connected'] . "', 
			`privilege`='" . $full[$key]['privilege'] . "', 
			`criticality1`='" . $full[$key]['criticality1'] . "', 
			`criticality2`='" . $full[$key]['criticality2'] . "', 
			`criticality3`='" . $full[$key]['criticality3'] . "', 
			`demand-id`='" . $full[$key]['demand-id'] . "', 
			`self-evaluation`='" . $full[$key]['self-evaluation'] . "', 
			`date`='" . $full[$key]['date'] . "', 
			`platform`='" . $_POST['platform'] . "', 
			`user`='" . $_COOKIE['login'] . "', 
			`actual`='1', 
			`calendar`= NULL
			");

        }

        $status['type'] = 'success';
        $status['message'] = 'Успех';
        print_r(json_encode($status, JSON_UNESCAPED_UNICODE));
        return;

    }
}

# Обновление требований в чек-листе
if ($_POST['action'] == 'updateMatrix') {

    // Выбираем все неактуальные записи по определенной самооценке
    $collector = $db->query("SELECT * FROM `collector`
		                              WHERE `self-evaluation`='" . $_POST['id'] . "' 
		                              AND `actual`=0 ");

    while ($cRow = $collector->fetch_assoc()) {

        // Выбираем все требования из справочника по критериям
        $demand = $db->query("SELECT * FROM `demands`
			                           WHERE `ehs`='" . $cRow['ehs'] . "'
									   AND `job`='" . $cRow['job'] . "'
									   AND `connected`='" . $cRow['connected'] . "'"
        );

        while ($dRow = $demand->fetch_assoc()) {
            $db->query("UPDATE `collector` 
				                 SET `demand`='" . $dRow['demand'] . "',
				                 `event`='" . $dRow['event'] . "',
				                 `paragraph-name`='" . $dRow['paragraph-name'] . "',
				                 `paragraph-link`='" . $dRow['paragraph-link'] . "',
				                 `source`='" . $dRow['source'] . "',
				                 `document`='" . $dRow['document'] . "',
				                 `privilege`='" . $dRow['privilege'] . "',
				                 `criticality1`='" . $dRow['criticality1'] . "',
				                 `criticality2`='" . $dRow['criticality2'] . "',
				                 `criticality3`='" . $dRow['criticality3'] . "',
				                 `date`='" . $dRow['date'] . "',
				                 `actual`='1',
				                 `fio`='',
				                 `date-plan`=NULL,
				                 `actions`='',
				                 `status-compliance`='0'
				                 WHERE `id`='" . $cRow['id'] . "'");
            // echo $dRow['id'];
        }

    }
}

# Удаление чек-листов
if ($_POST['action'] == 'removeBunch') {
    $id = intval($_POST['id']);
    $db->query("DELETE FROM `collector` WHERE `self-evaluation` IN (
	                     SELECT reports.id FROM `reports` WHERE `bunch` = {$id}
	                     )");

    $db->query("DELETE FROM `reports` WHERE `bunch` = {$id}");

    $db->query("UPDATE tasks t SET t.nreport='0' WHERE t.nreport={$id}");

    $status['type'] = 'success';
    print_r(json_encode($status, JSON_UNESCAPED_UNICODE));
    return;
}


#Устанавливаем ответственного за отчет
if ($_POST['action'] == 'setResponsible') {

    $db->query("UPDATE `reports` SET `responsible` = '" . $_POST['responsible'] . "' WHERE `id`=" . $_POST['report']);

    $tema = 'Вас выбрали ответственным за группу в системе Well Compliance';
    $mess = '
	<html>
	<head>
	 <title>Вас выбрали ответственным за группу в системе Well Compliance</title>
	</head>
	<body>
	<p><b>Здравствуйте, ' . getFieldbyField('users', 'login', $_POST['responsible'], 'name') . '!</b></p>
	<p>Вас выбрали ответственным за группу вида работ на платформе мониторинга в системе Well Compliance.</p>
	<p>Перейдите по <a href="' . Sourse_Home . '/reports/all">ссылке</a>, чтобы узнать подробную информацию</p>
	</body>
	</html>
	';

    $to = getFieldbyField('users', 'login', $_POST['responsible'], 'email');

    Notification::sendMail($to, $tema, $mess);

    $status['type'] = 'success';
    $status['message'] = 'Запись успешно обновлена';
    $status['message2'] = 'Нужно придумать какой нибудь пояснительный текст';
    print_r(json_encode($status, JSON_UNESCAPED_UNICODE));
    return;

}

# Удаление директории
function rrmdir($src)
{
    $dir = opendir($src);
    while (false !== ($file = readdir($dir))) {
        if (($file != '.') && ($file != '..')) {
            $full = $src . '/' . $file;
            if (is_dir($full)) {
                rrmdir($full);
            } else {
                unlink($full);
            }
        }
    }
    closedir($dir);
    rmdir($src);
}

# Редактирование матрицы
if (isset($_POST['matrixData'])) {

    $object = json_decode($_POST['matrixData'], TRUE);

    // echo "<pre>";
    // var_dump($object);
    // echo "</pre>";

    // exit;

    // Переопределяем даты
    if ($object['date-plan'] != "") {
        $object['date-plan'] = strtotime($object['date-plan']);
    } else {
        $object['date-plan'] = 0;
    }

    if ($object['fact-date'] != "") {
        $object['fact-date'] = strtotime($object['fact-date']);
    } else {
        $object['fact-date'] = 0;
    }
    $all = GetDistinctUsersfromBunch($object['id']);

    if (($object['fact-date'] == "") AND ($object['current-status-compliance'] == 1)) {
        $object['fact-date'] = strtotime("now");
    }
    // if (in_array($object['fio'],$all)){
    //     echo "yes";
    //    }else{
//    $tema = 'Вас выбрали ответственным за группу в системе Well Compliance';
//    $mess = '
//	<html>
//	<head>
//	 <title>Вас назначили ответственным за устранения в системе Well Compliance</title>
//	</head>
//	<body>
//	<p><b>Здравствуйте, {username} !</b></p>
//	<p>Вас назначили ответственным за устранения несоответсвии требованиям в системе Well Compliance.</p>
//	<p>Перейдите по <a href="' . Sourse_Нome . '/reports/matrix?type=calendar&user={fio}' . '>ссылке</a>, чтобы узнать подробную информацию</p>
//	</body>
//	</html>
//	';

    $to = getFieldbyField('users', 'login', $object['fio'], 'email');
    $replacements = [
        $to =>
            [
                '{username}' => getFieldbyField('users', 'login', $object['fio'], 'surname'),
                '{id}' => getFieldbyField('users', 'login', $object['fio'], 'id'),
            ]
    ];

    sendmail_executor($replacements);
//    Notification::sendMail($to, $tema, $mess);

    $status['type'] = 'success';
    $status['message'] = 'Запись успешно обновлена';
    $status['message2'] = 'Нужно придумать какой нибудь пояснительный текст';
    //  print_r( json_encode($status,JSON_UNESCAPED_UNICODE) );
    // }
    foreach ($object as $key => $value) {

        $params .= " `" . $key . "`='" . $value . "',";
    }

    $params = trim($params, ',');
    $query = "UPDATE `collector` SET " . $params . " WHERE `id`=" . $object['id'];
    // $db->query("UPDATE `collector` SET " . $params . " WHERE `id`=" . $object['id']);
    $result = $db->query($query);
    if ($result) {
        if ($object['current-status-compliance'] == 1) {
            $query = "UPDATE `nearmiss` SET `status`= 4, `flag`=1, recommendations = '" . $object['recommendations'] . "' WHERE `id_self` = " . $object['id'];
            //TODO  sendMailSoglas("ravenyd123@mail.ru", $object['id']);
            // sendMailSoglas("viktor.korovkin@mail.ru");
        } else {
            $query = "UPDATE `nearmiss` SET  `flag`=1, recommendations = '" . $object['recommendations'] . "' WHERE `id_self` = " . $object['id'];
        }
        $result = $db->query($query);
    }
}

# Редактирование матрицы
if ($_POST['action'] == 'editMatrix') {

    $id = $_POST['id'];
    $key = $_POST['key'];
    $value = $_POST['value'];

    if (preg_match('/(\d{2}\.\d{2}\.\d{4})/', $value)) {
        $value = strtotime($value);
    }

    // echo "UPDATE `collector` SET `".$key."` = '".$value."' WHERE `id`=".$id;

    $db->query("UPDATE `collector` SET `" . $key . "` = '" . $value . "' WHERE `id`=" . $id);

    $status['type'] = 'success';
    $status['message'] = 'Запись успешно обновлена';
    $status['message2'] = 'Нужно придумать какой нибудь пояснительный текст';
    print_r(json_encode($status, JSON_UNESCAPED_UNICODE));
    return;

}

# Массовое проставление статуса соответствия
if ($_POST['action'] == 'setSingleStatus') {

    $evaluation = $_POST['evaluation'];
    $status = $_POST['status'];

    $db->query("UPDATE `collector` SET `status-compliance` = '" . $status . "' WHERE `self-evaluation` = " . $evaluation);

}

# Количество требований в группе чек-листов(с учетом критичности)
function getDemandsAmount()
{

}


/* ---------- ОРГАНАЙЗЕР ---------- */

# Взять задачи, где ответственный $user
function getUserTask($user)
{
    $db = DB::Connection();
    $query = $db->query("SELECT `name`, `director` FROM `tasks` WHERE `responsible` IN '%" . $_COOKIE['company'] . "%'");

    while ($row = $query->fetch_assoc()) {
        $nd[] = $row['nd'];
    }

    // var_dump($nd);
    return $nd;

}

# Удаление задачи
// Проверка подзадач по параметрам для удаления 
function taskVerification($id)
{
    $no_del = 0;
    $id_list = '';
    $db = DB::Connection();
    $query = $db->query("SELECT `id`, `pid`, `status`, `nreport`
                                 FROM `tasks`
                                 WHERE `pid` = {$id}");
    if ($query->num_rows > 0) {
        foreach ($query as $value) {

            if (($value['status'] == 'Новая') AND ($value['nreport'] == 0)) {
                taskVerification($value['id']);
            } else {
                $no_del++;
                unset($id_list);
            }
            $id_list .= "," . $value['id'];

        }
    } else {
        return $id_list;
    }
    return $id_list;
}


//ф-ция удаления тасков
function delTasksFromIds($id_del)
{
    $db = DB::Connection();
    $query_del = $db->query("UPDATE `tasks` SET `archive` = 1  WHERE `id` IN ($id_del)");
    $db->query("UPDATE `attachments` SET `archive` = 1  WHERE `id` IN ($id_del)");
    return $query_del;
}

if ($_POST['action'] == 'removeTask') {

    $idArr = $_POST['ids'];
    $myid = $_POST['myid'];
    // $myid=56;
    // $idArr="399,400,401,402,403,405,392";
    $id_array = explode(",", $idArr);
    $id_array_tsk = [];
    $query = $db->query("SELECT `id` FROM `tasks` WHERE `status`='Новая' AND `nreport`=0 AND `director`=" . $myid . " AND  `id` IN (" . $idArr . ")");

    if (mysqli_num_rows($query) > 0) {
        foreach ($query as $value) {
            $id_array_tsk = $value['id'];
            $id_list = taskVerification($value['id']);
            if (empty($id_list)) {
                $id_list .= "," . $value['id'];
                $nb_del++;
            } else {
                $no_del++;
            }
        }
    } else {
        $no_del++;
    }
    $id_list = trim($id_list, ",");
    //var_dump($id_list);

    $status_del = delTasksFromIds($id_list);


    if ((isset($no_del)) AND (isset($nb_del))) {
        echo json_encode(array(
            "message" => "Не все задачи были удалены. Удалено задач: " . $nb_del . " Не удалось удалить: " . $no_del,
            "type" => "warning"
        ));
        // echo "Не все задачи были удалены. Удалено задач ".$nb_del." Не удалось удалить".$nb_no_del++;
    } else {
        if (isset($no_del) OR $status_del == false) {
            echo json_encode(array(
                "message" => "Задачи не были удалены.",
                "type" => "error"
            ));
        }
        if (isset($nb_del)) {
            echo json_encode(array(
                "message" => "Выбранные задачи были успешно удалены",
                "type" => "success"
            ));
        }

    }
}

# Добавление задачи
if ($_POST['action'] == 'updateTask') {

    $data = explode('&', str_replace('task-', '', $_POST['data']));

    // Преобразуем в массив для работы
    foreach ($data as $key => $value) {

        $parts = explode('=', $value);

        $conditions[$parts[0]] = $parts[1];

        if ($parts[0] == 'executors') {
            $executors .= $parts[1] . ",";
        }

    }

    // Проверяем есть ли такое задание
    $task = $db->query("SELECT `id` FROM `tasks` WHERE `id` = " . $conditions['id']);
    if (mysqli_num_rows($task) == 0) {
        $now = new DateTime();
        $conditions['date'] = $now->getTimestamp();
    } else {
        $conditions['status'] = 'В работе';
    }

    if ($conditions['id'] == '') {
        $conditions['id'] = getMaxFieldValue('tasks', 'id') + 1;
    }

    // Даты
    $conditions['deadline'] = strtotime($conditions['deadline']);
    $conditions['start-noty'] = strtotime($conditions['start-noty']);

    // Соисполнители
    $conditions['executors'] = trim($executors, ',');

    // Убираем комментарии
    unset($conditions['comment']);

    // Статус задачи
    if (!isset($conditions['status'])) {
        $conditions['status'] = 'Новая';
    }

    // Чек листы
    if (isset($_POST['subtask'])) {
        $conditions['subtask'] = json_encode($_POST['subtask'], JSON_UNESCAPED_UNICODE);
        $conditions['subtask'] = addslashes($conditions['subtask']);
    }

    foreach ($conditions as $key => $value) {
        $string .= " `" . $key . "`='" . $value . "',";
    }

    $string = trim($string, ',');

    // Записываем задание в базу
    if (mysqli_num_rows($task) == 0) {
        //echo "INSERT INTO `tasks` SET ".$string;
        $query = $db->query("INSERT INTO `tasks` SET " . $string);
    } else {

        $query = $db->query("UPDATE `tasks` SET " . $string . " WHERE `id`=" . $conditions['id']);
    }

    // Кому
    $to = $conditions["director"] . ',' . $conditions["responsible"] . ',' . $conditions["executors"];
    $to = trim($to, ',');
    $to = explode(',', $to);
    $to = array_unique($to);

    foreach ($to as $key => $value) {

        if ($conditions['status'] == 'Новая') {
            $tema = 'Создана новая задача в системе Well Compliance';
            $mess = '
			<html>
			<head>
			 <title>Создана новая задача в системе Well Compliance</title>
			</head>
			<body>
			<p><b>Здравствуйте, ' . getFieldbyField('users', 'login', $value, 'name') . '!</b></p>
			<p>У вас была создана задача "' . $conditions["name"] . '" в системе Well Compliance.</p>
			<p>Перейдите по <a href="' . Sourse_Нome . '">ссылке</a>, чтобы узнать подробную информацию</p>
			</body>
			</html>
			';
        }

        if ($conditions['status'] != 'Новая') {
            $tema = 'изменение в задаче в системе Well Compliance'; // Буква И не отоброжается в теме письма. Заменил на ее код.
            $mess = '
			<html>
			<head>
			 <title>Изменение в задаче в системе Well Compliance</title>
			</head>
			<body>
			<p><b>Здравствуйте, ' . getFieldbyField('users', 'login', $value, 'name') . '!</b></p>
			<p>Задача "' . $conditions["name"] . '" была изменена в системе Well Compliance.</p>
			<p>Перейдите по <a href="' . Sourse_Нome . '">ссылке</a>, чтобы узнать подробную информацию</p>
			</body>
			</html>
			';
        }

        $to[$key] = getFieldbyField('users', 'login', $value, 'email');

        Notification::sendMail($to[$key], $tema, $mess);

    }

}

# Изменение статуса
if ($_POST['action'] == 'changeTaskStatus') {
    $id = sanitizeString($_POST['task-id']);
    $status = sanitizeString($_POST['task-status']);
    $conditions = " `status`='" . $status . "'";
    if ($status == 'Выполнено') {
        $now = new DateTime();
        $conditions .= ", `finished` = " . $now->getTimestamp() . ", `closer` = '" . $_COOKIE['login'] . "' ";
    }
    echo $conditions;
    $query = $db->query("UPDATE `tasks` SET " . $conditions . " WHERE `id`='$id' ");
}

# Добавление комментария к задаче
if ($_POST['action'] == 'addComment') {
    $id = $_POST['id'];
    $text = $_POST['text'];

    // Проверяем есть ли такое задание
    $task = $db->query("SELECT `comments` FROM `tasks` WHERE `id` = " . $id);

    if (mysqli_num_rows($task) == 0) {

        $status['type'] = 'error';
        $status['message'] = 'Ошибка';
        $status['message2'] = 'Данного задания не существует';
        print_r(json_encode($status, JSON_UNESCAPED_UNICODE));
        return;

    } else {

        $comments = $task->fetch_array();
        $now = new DateTime();

        // Если первая запись
        if (empty($comments['comments'])) {

            $newComment = array(
                0 => array(
                    "user" => $_COOKIE['login'],
                    "date" => $now->getTimestamp(),
                    "text" => $text
                )
            );

            $comments = json_encode($newComment, JSON_UNESCAPED_UNICODE);

        } else {

            $newComment = array(
                "user" => $_COOKIE['login'],
                "date" => $now->getTimestamp(),
                "text" => $text
            );


            $comments = json_decode($comments['comments'], true);
            array_push($comments, $newComment);
            $comments = json_encode($comments, JSON_UNESCAPED_UNICODE);

        }

        $db->query("UPDATE `tasks` SET `comments`='" . $comments . "' WHERE `id`='$id' ");

        $status['type'] = 'success';
        $status['message'] = 'Операция выполнена';
        $status['message2'] = 'Добавлен пользовательский комментарий к задаче';
        print_r(json_encode($status, JSON_UNESCAPED_UNICODE));
        return;

    }
}


/* ---------- РЕДАКТОР СТАНДАРТОВ ---------- */


# Редактирование связей ehs и видов работ
if ($_POST['action'] == 'setRelations') {

    $ehs_number = sanitizeString($_POST['ehs_numbers']);
    $ehs_name = sanitizeString($_POST['ehs_name']);
    $job_type = sanitizeString($_POST['type']);
    $job_id = getFieldbyField('types_jobs', 'type', $_POST['type'], 'id');

    // Если данного вида работы не существует
    if (!isset($job_id)) {
        $status['type'] = 'error';
        $status['message'] = 'Данный вид работы не существует';
        print_r(json_encode($status, JSON_UNESCAPED_UNICODE));
        return;
    }

    // Данная запись уже есть в таблице EHS
    $exist = $db->query("SELECT * FROM `ehs` WHERE 
		     					 `ehs_numbers`='" . $ehs_number . "' AND 
		     					 `Parent_id_job`='" . $job_id . "'");

    if (mysqli_num_rows($exist) > 0) {
        $status['type'] = 'error';
        $status['message'] = 'Данная связка уже существует';
        print_r(json_encode($status, JSON_UNESCAPED_UNICODE));
        return;
    }

    // Если связки не существует добавляем новую
    $db->query("INSERT INTO `ehs` 
		                 SET `Parent_id_job`='" . $job_id . "',
                         `ehs_numbers`='" . $ehs_number . "',
                         `ehs_name`='" . $ehs_name . "'
                         ");
    $status['type'] = 'success';
    $status['message'] = 'Связка успешно добавлена';
    print_r(json_encode($status, JSON_UNESCAPED_UNICODE));
    return;
}

# Удаление связей ehs и видов работ
if ($_POST['action'] == 'removeRelations') {
    $number = sanitizeString($_POST['ehs_numbers']);
    $name = sanitizeString($_POST['ehs_name']);
    $type = sanitizeString($_POST['type']);
    $ehsId = sanitizeString($_POST['ehs_id']);
    $jobId = sanitizeString($_POST['job_id']);

    // echo "<pre>";
    // 	var_dump($_POST);
    // echo "</pre>";

    // echo $ehsId;
    //echo "DELETE FROM `ehs` WHERE `id` = $ehsId";
    $db->query("DELETE FROM `ehs` WHERE `id` = $ehsId");
}

# Сохранение вида работы
if ($_POST['action'] == 'saveJob' || $_POST['action'] == 'removeJob') {

    $id = $_POST['id'];
    if (isset($_POST['type'])) {
        $type = $_POST['type'];
    }

    if ($_POST['action'] == 'saveJob') {

        // Проверяем есть ли данный вид работ в базе
        $job = $db->query("SELECT * FROM `types_jobs` WHERE `type` ='" . $type . "'");

        if (mysqli_num_rows($job) > 0) {
            $status['type'] = 'error';
            $status['message'] = 'Данный вид работы уже существует';
            print_r(json_encode($status, JSON_UNESCAPED_UNICODE));
            return;
        }

        if ($id != '') {
            $db->query("UPDATE `types_jobs` SET `type`='$type' WHERE `id`='$id' ");
            $status['type'] = 'success';
            $status['message'] = 'Вид работ упешно обновлен';
            print_r(json_encode($status, JSON_UNESCAPED_UNICODE));
            return;
        }

        if ($id == '') {
            $db->query("INSERT INTO `types_jobs` SET `type`='$type' ");
            $status['type'] = 'success';
            $status['message'] = 'Новый вид работ упешно добавлен';
            print_r(json_encode($status, JSON_UNESCAPED_UNICODE));
            return;
        }

    }

    if ($_POST['action'] == 'removeJob') {
        // Если обновляем запись
        if (isset($_POST['id'])) {
            $db->query("DELETE FROM `types_jobs` WHERE `id`='$id' ");
            $status['type'] = 'success';
            $status['message'] = 'Вид работ упешно удален';
            print_r(json_encode($status, JSON_UNESCAPED_UNICODE));
            return;
        }
    }
}

# Сохранение/Удаление объекта
if ($_POST['action'] == 'saveObj' || $_POST['action'] == 'removeObj') {

    $data = json_decode($_POST['data'], true);

    if (isset($_POST['id'])) {
        $id = $_POST['id'];
    }

    $name = $data['name'];
    $table = $data['table'];

    unset($data['table']);

    foreach ($data as $key => $value) {
        $string .= " `" . $key . "`='" . $value . "',";
    }

    $string = trim($string, ',');

    // print_r( json_encode($string,JSON_UNESCAPED_UNICODE) );

    // exit;

    if ($_POST['action'] == 'saveObj') {

        // Проверяем есть ли данная запись в базе
        $node = $db->query("SELECT * FROM `" . $table . "` WHERE `name` ='" . $data['name'] . "'");

        if (mysqli_num_rows($node) > 0) {
            header('HTTP/1.1 500 Forbidden');
            $status['type'] = 'error';
            $status['message'] = 'Операция не выполнена';
            $status['message2'] = 'Данная запись уже присутствует в базе';
            print_r(json_encode($status, JSON_UNESCAPED_UNICODE));
            return;
        }

        if ($id != '') {
            $db->query("UPDATE `" . $table . "` SET " . $string . " WHERE `id`='$id' ");
            $status['type'] = 'success';
            $status['message'] = 'Операция успешно выполнена';
            $status['message2'] = 'Запись упешно обновлена';
            print_r(json_encode($status, JSON_UNESCAPED_UNICODE));
            return;
        }

        if ($id == '') {
            $db->query("INSERT INTO `" . $table . "` SET " . $string);
            $status['type'] = 'success';
            $status['message'] = 'Операция успешно выполнена';
            $status['message2'] = 'Новая запись упешно добавлена';
            print_r(json_encode($status, JSON_UNESCAPED_UNICODE));
            return;
        }

    }

    if ($_POST['action'] == 'removeObj') {

        $db->query("DELETE FROM `" . $table . "` WHERE `id`='$id' ");
        checkUsersPlafrom();

        $status['type'] = 'delete';
        $status['message'] = 'Операция успешно выполнена';
        $status['message2'] = 'Запись удалена';
        print_r(json_encode($status, JSON_UNESCAPED_UNICODE));
        return;

    }
}


/* ---------- РЕЕСТР ---------- */


# Находим все площадки компании ///Проверить и удалить
function getCompanyDocuments()
{
    $db = DB::Connection();
    $query = $db->query("SELECT DISTINCT `nd` FROM `register` WHERE `company` LIKE '%" . $_COOKIE['company'] . "%'");

    while ($row = $query->fetch_assoc()) {
        $nd[] = $row['nd'];
    }

    // var_dump($nd);
    return $nd;

}

# Цвет статуса в регистре
function registerStatusColor($status)
{
    $status = str_replace("Статус:", "", $status);
    $status = trim($status);

    if (strpos($status, 'Недействующий') !== false) {
        return "text-danger";
    } elseif (strpos($status, 'Применение в качестве национального стандарта РФ прекращено') !== false) {
        return "text-danger";
    } elseif (strpos($status, 'Действующий') !== false) {
        return "text-info";
    } elseif (strpos($status, 'Проект') !== false) {
        return "text-muted";
    } else {
        return "text-warning";
    }
}


# Добавление и редактирование записи в реестре
if ($_POST['action'] == 'editRegister' || $_POST['action'] == 'addRegister') {


    $nd_old = $_POST['nd_old'];
    $nd = $_POST['nd'];
    $symbol = htmlspecialchars($_POST['symbol']);
    $comment = sanitizeString($_POST['comment']);
    $group = implode(",", $_POST['group']);
    $company = sanitizeString($_POST['company']);
    $status = $_POST['status'];
    $base_name = htmlspecialchars($_POST['base_name']);

    // echo "<pre>";
    // 	var_dump($group);
    // echo "</pre>";

    // exit();

    if ($_POST['action'] == 'editRegister') {
        //var_dump($status);
        $result = $db->query("UPDATE `register` SET 
			                         `nd`=$nd, 
			                         `symbol`='$symbol', 
			                         `comment`='$comment',
			                         `group`='$group',
			                         `company`='$company',
			                         `status`='$status',
			                          `base_name`='$base_name'
			                          WHERE `nd`='$nd_old' ");
        echo $result;
    }

    if ($_POST['action'] == 'addRegister') {
        $query = $db->query("SELECT `nd` FROM `register` WHERE `nd`='$nd' ") or die ("Проблема при подключении к БД");

        if (mysqli_num_rows($query) == 0) {
            $query = "";
            $query = "INSERT INTO `register` SET ";
            if ($base_name != 'ext') {
                $query .= "`nd`=$nd,";
            }
            $query .= "`status`='$status', 
				       `symbol`='$symbol', 
				       `group`='$group', 
				       `company`='$company', 
				       `comment`='$comment',
				       `base_name`='$base_name'
				                ";
            $result = $db->query($query);
            if ($result) {
                $new_id = $db->insert_id;
                $query = "UPDATE `register` SET nd='$new_id' WHERE id='$new_id'";
                $result = $db->query($query);
            }
        } else {
            $query = "UPDATE `register` SET
			                    `nd`=$nd, 
			                    `symbol`='$symbol', 
			                    `comment`='$comment', 
			                    `group`='$group', 
			                    `company`='$company', 
			                    `status`='$status',
			                    `base_name`='$base_name'
				                 WHERE `nd`='$nd_old'";
            $result = $db->query($query);
        }
        echo $result;
    }
}

# Получение актуальных документов
if ($_POST['action'] == 'getDocuments') {
    $query = "SELECT DISTINCT `symbol` FROM `register` WHERE `status` LIKE '%Действующий%' AND 
		                                  `company` LIKE '%" . $_COOKIE['company'] . "%'";
    $documentsInfo = $db->query($query);

    while ($row = $documentsInfo->fetch_array()) {
        $documents[] = $row['symbol'];
    }
    echo json_encode($documents, JSON_UNESCAPED_UNICODE);
}

# Проверяем существование документа
if ($_POST['action'] == 'checkDocument') {

    $document = $db->query("SELECT * FROM register WHERE symbol = '" . $_POST['symbol'] . "' LIMIT 1");

    if (mysqli_num_rows($document) > 0) {
        $status['type'] = 'success';
        $status['message'] = 'Документ есть в системе';
        print_r(json_encode($status, JSON_UNESCAPED_UNICODE));
        return;
    } else {
        $status['type'] = 'error';
        $status['message'] = 'Документа нет в системе';
        print_r(json_encode($status, JSON_UNESCAPED_UNICODE));
        return;
    }
}

# Получение документа по nd
function getDocumentByNd($id)
{
    $db = DB::Connection();
    $document = $db->query("SELECT * FROM register WHERE nd = '" . intval($id) . "' LIMIT 1")->fetch_array();
    return $document['symbol'];
}

# Получение nd документа по symbol
function getDocumentBySymbol($symbol)
{
    $db = DB::Connection();
    $document = $db->query("SELECT * FROM register WHERE symbol = '" . $symbol . "' LIMIT 1")->fetch_array();
    return $document['nd'];
}

# Проверка активности документа по nd
function getDocumentActivityByNd($id)
{
    $db = DB::Connection();
    $document = $db->query("SELECT * FROM register WHERE nd = '" . intval($id) . "' LIMIT 1")->fetch_array();
    return $document['status'];
}

# Получаем кто создал отчет по id Отчета #
function getCreatorById($id_audit)
{
    $db = DB::Connection();
    $document = $db->query("SELECT creator FROM reports WHERE id = '" . intval($id_audit) . "' LIMIT 1")->fetch_array();
    return $document['creator'];
}


# Обрезка статуса в регистрах
function cropStatus($status)
{

    $status = str_replace("Статус:", "", $status);
    $status = trim($status);

    return $status;
}

# Если документа не существует удаляем требования и записи в коллекторе
if ($_POST['action'] == 'checkDocumentExistence') {

    $result = $db->query("DELETE FROM `demands` WHERE `document` NOT IN (SELECT DISTINCT `nd` FROM `register` )");
    $result = $db->query("DELETE FROM `collector` WHERE `document` NOT IN (SELECT DISTINCT `nd` FROM `register` )");
    var_dump($result);
}


/* ---------- НАСТРОЙКИ ПРЕДРИЯТИЯ ---------- */


# Изменение настроек предприятия
if ($_POST['action'] == 'saveSettings') {

    $id = $_POST['platform'];
    $criticality = $_POST['criticality'];

    $db->query("UPDATE `platform` SET `criticality`='$criticality' WHERE `id`=$id ");
}

# Получение настроек платформы
function getPlatformSettings($platform)
{
    $db = DB::Connection();
    // Выбираем настройки предприятия
    $settings = $db->query("SELECT platform.id, platform.name FROM `platform` AS platform
	                 WHERE platform.id = '" . $platform . "'
	                 ");
    $settingsRow = $settings->fetch_assoc();
    $settings = $settingsRow['settings'];
    return json_decode($settings, true);
}


/* ---------- НОТИФИКАЦИЯ ---------- */


$notification = new Notification;


//class

/* обьединение многомерного массивы */
function super_unique($array)
{
    $result = array_map("unserialize", array_unique(array_map("serialize", $array)));

    foreach ($result as $key => $value) {
        if (is_array($value)) {
            $result[$key] = super_unique($value);
        }
    }

    return $result;
}

/*корзина*/
/*function trash($id_audit,$delete=false){
    $db=DB::Connection();
    $db->query("update audit_person set del = 1 WHERE id='".$id_audit."'");
    if ($delete==true){
     $db->query("DELETE LOW_PRIORITY QUICK FROM audit_person WHERE id='".$id_audit."' and del=1");
     $db->query("DELETE LOW_PRIORITY QUICK FROM Matrix.audit WHERE Matrix.audit.id_personal='".$id_audit."'");
    }
}*/
////////////////////////////////////////////////////////////////////////


function GetAllPlatforms()/// получить все платформы ['1'=>'Имя платформы']
{
    $data = [];
    $db = DB::Connection();
    $result1 = $db->query("SELECT p.id,p.name FROM platform p ");
    while ($row = $result1->fetch_assoc()) {
        $data[$row['id']] = $row['name'];
    }
    return $data;
}

function GetOnlyMyGroupPlatform($iduser)
{
    $firstPlatformUser = GetPlatformsByFirstLevel($iduser);
    $platformList = GetAllFirstPlatform($firstPlatformUser);
//    $platforms = GetArrayPlatfornByMyFirst($platformList);
    $platforms = GetArrayPlatfornByMyFirst_new($platformList);
    return $platforms;
}


function GetArrayPlatfornByMyFirst($listplatform)/// получить массив id и названия площадок при моей главной площаке
{
    $data = [];
    $db = DB::Connection();
    $result1 = $db->query("SELECT id, name FROM platform WHERE id IN ({$listplatform})");
    while ($row = $result1->fetch_assoc()) {
        $data[$row['id']] = $row['name'];
    }
    return $data;
}

function GetArrayPlatfornByMyFirst_new($listplatform)
{
    $data = [];
    $db = DB::Connection();
    $result1 = $db->query("WITH RECURSIVE plat as (
      SELECT p.id,p.name,p.company,p.parent,p.id as groppid  
      FROM platform as p WHERE id in ($listplatform) and company ='pepsico'
      UNION 
      SELECT p.id,p.name,p.company,p.parent,pl.groppid FROM platform as p,plat as pl WHERE p.id=pl.parent)
    SELECT id,GROUP_CONCAT(name ORDER by id ASC SEPARATOR ' /') as name from plat GROUP BY groppid");
    while ($row = $result1->fetch_assoc()) {
        $data[$row['id']] = $row['name'];
    }

    return $data;
}

/* Главная площадка пользователя */
function GetPlatformsByFirstLevel($id)
{
    $db = DB::Connection();
    $query = "SELECT `platform` FROM `users` WHERE id = {$id}";
    $result = $db->query($query)->fetch_assoc();
    // $platforms = $result['platform'];
    //preg_match("~^.+?,\\s*(.+)~ui",$platforms,$m);
    $pl = explode(",", $result['platform']);
    $platforms = intval($pl['0']);
    //  $platforms = ($m[1]);

    $idparrent = GetFirstDB($platforms);

    if ($idparrent == 0) {
        $idFirstPlatform = $platforms;
    } else {
        $idparrent2 = GetFirstDB($idparrent);
        if ($idparrent2 == 0) {
            $idFirstPlatform = $idparrent;
        } else {
            $idparrent3 = GetFirstDB($idparrent2);
            if ($idparrent3 == 0) {
                $idFirstPlatform = $idparrent2;
            } else {
                $idFirstPlatform = $idparrent3;
            }
        }
    }
    return $idFirstPlatform;
}

/* Запрос в базу для фунции выше */
function GetFirstDB($id)
{
    $db = DB::Connection();
    $query = "SELECT `parent` FROM platform WHERE id = {$id}";
    $result = $db->query($query)->fetch_assoc();
    return $result['parent'];
}

/* Все площажки и цеха у главной площадки пользователя */
function GetAllFirstPlatform($id)
{
    $db = DB::Connection();
    $query = "SELECT `id` FROM platform WHERE parent = {$id}";
    $result = $db->query($query);
    $listplatforms = $id;
    foreach ($result as $key => $value) {
        $listplatforms .= "," . $value['id'];
        $query = "SELECT `id` FROM platform WHERE parent = {$value['id']}";
        $result2 = $db->query($query);
        foreach ($result2 as $key2 => $value2) {
            $listplatforms .= "," . $value2['id'];
            $query = "SELECT `id` FROM platform WHERE parent = {$value2['id']}";
            $result3 = $db->query($query);
            foreach ($result3 as $key3 => $value3) {
                $listplatforms .= "," . $value3['id'];
            }
        }
    }

    return $listplatforms;


}


function Delete_Platform_in_User($idm)
{
    $result2 = false;
    $db = DB::Connection();
    $string = Platform_One_User()[0];
    if (count(explode(",", $string)) > 1) {
        $param = preg_replace('/(,?' . $idm . ')|(' . $idm . ',?)/', "", $string, 1);// '/(,?'.$idm.')|('.$idm.',?)/'
        $result2 = $db->query("UPDATE users SET platform='" . $param . "' WHERE id='" . $_COOKIE['id'] . "'");
    }
    return $result2;
}

function Platform_One_User()
{
    $db = DB::Connection();
    $result = $db->query("SELECT platform FROM users WHERE id='" . $_COOKIE['id'] . "' AND company='" . $_COOKIE['company'] . "'")->fetch_row();
    return $result;
}

function Platform_All_Minus_User()//можно удалить
{
    $all = GetAllPlatforms();
    $user = explode(",", Platform_One_User()[0]);
    foreach ($user as $t) {
        unset($all[intval($t)]);
    }
    return $all;
}

//////////////////////////////////////////////////
function user_header()
{
    $result = one_user($_COOKIE['id']);
    if ($result['father_name'] == "") {
        $str = $result['surname'] . " " . mb_substr($result['name'], 0, 1, 'UTF-8') . ".";
    } else {
        $str = $result['surname'] . " " . mb_substr($result['name'], 0, 1, 'UTF-8') . ". " . mb_substr($result['father_name'], 0, 1, 'UTF-8') . ".";
    }
    echo mb_convert_case($str, MB_CASE_TITLE, "UTF-8");
}


function one_user($id)
{
    $db = DB::Connection();
    $result = $db->query("SELECT * FROM users WHERE id='" . $id . "' LIMIT 1")->fetch_assoc();
    return $result;
}

///////////////////////////// отчет
function job_view_all($bunch, $platform, $vid_rabot = 0)
{
    $resultArray = [];
    $db = DB::Connection();
    $query = "SELECT DISTINCT
collector.job,
types_jobs.type,
collector.`self-evaluation`
FROM reports
  INNER JOIN collector
    ON reports.id = collector.`self-evaluation`
  INNER JOIN types_jobs
    ON types_jobs.id = collector.job
  WHERE reports.bunch ='" . intval($bunch) . "'AND collector.platform='" . intval($platform) . "' ";
    if ($vid_rabot != 0) {
        $query .= "and collector.job='" . intval($vid_rabot) . "'";
    }
    $result = $db->query($query);
    while ($row = $result->fetch_assoc()) {
        array_push($resultArray, $row);
    }
    return $resultArray;
}

;
function all_data($job, $self_id, $platform)
{

    $resultArray = [];
    $db = DB::Connection();
    $query = "SELECT t.id,t.`paragraph-link`,t.demand ,((t.criticality1+t.criticality2+t.criticality3)*10)AS krit,e.ehs_numbers,t.`status-compliance`,t.`actions`,r.symbol,t.`paragraph-name`
  FROM collector AS t 
  INNER JOIN register AS r
   ON r.nd = t.document 
  INNER JOIN ehs AS e 
    ON e.id = t.ehs 
WHERE job='" . intval($job) . "'
 AND t.`self-evaluation`='" . intval($self_id) . "'
 AND t.`status-compliance`<>5 
 AND t.`status-compliance`<>0 
 AND t.platform='" . intval($platform) . "'";
//    echo $query;
    $result = $db->query($query);
    while ($row = $result->fetch_assoc()) {
        array_push($resultArray, $row);
    }
    return $resultArray;
}

;
function sum_vid()
{
    $data = [];
    $data1 = [];
    $platf_bunch = get_bunch();
    foreach ($platf_bunch as $platform => $bunch) {

        foreach (job_view_all($bunch, $platform) as $value) {
            $summakrit = 0;
            $summaself = 0;
            foreach (all_data($value['job'], $value['self-evaluation'], $platform) as $key1 => $value1) {
                $soo1 = 1;
                $soo = 0;
                switch ($value1['status-compliance-audit']) {
                    case 5:
                        $soo = 0;
                        $soo1 = 0;
                        break;
                    case 0:
                        $soo = 0;
                        $soo1 = 0;
                        break;
                    case 1:
                        $soo = 1;
                        break;
                    case 2:
                        $soo = 0.75;
                        break;
                    case 3:
                        $soo = 0.5;
                        break;
                    case 4:
                        $soo = 0;
                }
                $summakrit += ($value1['krit'] * $soo1);
                $summaself += ($value1['krit'] * $soo);
            }
            $data[$value['job']] = [
                "krit" => $summakrit,
                "self" => $summaself
            ];

        }
        $data1[$platform] = $data;
    }

    return $data1;

}

function get_bunch()
{
    $resultArray = [];
    $db = DB::Connection();
    $result = $db->query("SELECT DISTINCT r.bunch,r.platform  FROM reports r");
    while ($row = $result->fetch_assoc()) {
        $resultArray[$row['platform']] = $row['bunch'];
    }
    return $resultArray;
}

function new1()
{
    $db = DB::Connection();
    $data1 = [];
    $platform_bunch = get_bunch();
    foreach ($platform_bunch as $platform => $bunch) {
        $result = NULL;
        $result = $db->query("SELECT c.job,SUM(sum_krit(c.`status-compliance-audit`,c.criticality1, c.criticality2, c.criticality3))AS self, SUM(((c.criticality1+c.criticality2+c.criticality3)*10))AS krit FROM collector c 
WHERE  c.platform=" . $platform . " AND c.`status-compliance-audit`<>5 AND c.`status-compliance-audit`<>0 GROUP BY c.job;");
        while ($row = $result->fetch_assoc()) {
            $data[$row['job']] = ["self" => $row['self'], "krit" => $row['krit']];
        }
        $data1[$platform] = $data;
        $data = [];
    }
    return $data1;
}


function GetAllvid($key = false)/// получение всех видов работ
{
    $data = [];
    $db = DB::Connection();
    $result1 = $db->query("SELECT
  tj.id,
  tj.type
FROM types_jobs tj  ");
    while ($row = $result1->fetch_assoc()) {
        if ($key) {
            $data[] = $row['id'];
        } else {
            $data[$row['id']] = $row['type'];
        }
    }
    return $data;
}

function GetAllvidForNM($key = false)/// получение всех видов работ
{
    $data = [];
    $db = DB::Connection();
    $result1 = $db->query("SELECT
  tj.id,
  tj.type
FROM types_jobs tj WHERE `type_n` = 1 ORDER BY `type`");
    while ($row = $result1->fetch_assoc()) {
        if ($key) {
            $data[] = $row['id'];
        } else {
            $data[$row['id']] = $row['type'];
        }
    }
    return $data;
}

function randomHex()
{
    $chars = 'ABCDEF0123456789';
    $color = '#';
    for ($i = 0; $i < 6; $i++) {
        $color .= $chars[rand(0, strlen($chars) - 1)];
    }
    return $color;
}

function GetAllvid_randomcolor()
{
    $data = [];
    $db = DB::Connection();
    $result1 = $db->query("SELECT
  tj.id,
  tj.type
FROM types_jobs tj");
    while ($row = $result1->fetch_assoc()) {
        $data[$row['type']] = randomHex();
    }
    return $data;
}

//Аналитика////
function report_all()
{
    $works = GetAllvid();
    $places = GetPlatformUser();
    $data1 = [];
    $bunch = 0;
    $platform = 0;
    $year = 0;
    $i = 0;
    $db = DB::Connection();
    $query = "SELECT
  reports.bunch,
  c.job,
  SUM(sum_krit(c.`status-compliance`,c.criticality1, c.criticality2, c.criticality3))AS audit,
  SUM(((c.criticality1+c.criticality2+c.criticality3)*10))AS krit,
  reports.platform,
  reports.date
  FROM collector c
  INNER JOIN reports
    ON c.`self-evaluation` = reports.id
  WHERE reports.platform IN (" . $_COOKIE["platform"] . ")
   AND c.`status-compliance`<>0 AND c.`status-compliance`<>5
 GROUP BY  reports.bunch,c.job ";
    $result1 = $db->query($query);
    while ($row = $result1->fetch_assoc()) {
        $row['krit'] = empty($row['krit']) ? 1 : $row['krit'];
        $row['audit'] = empty($row['audit']) ? 1 : $row['audit'];
        $row['self'] = empty($row['self']) ? 1 : $row['self'];
        if ($bunch <> $row['bunch'] or $platform <> $row['platform'] or date("Y", $row['date']) <> $year) {
            $bunch = $row['bunch'];
            $platform = $row['platform'];
            $year = date("Y", $row['date']);

            $data1[] = ["bunch" => $bunch, "platform" => $places[$platform], "id_platform" => $platform,
                "id_years" => [
                    $year => [
                        "percentage_audit" => 0,
                        "percentage_self" => 0,
                        "job" => [$row['job'] => [
                            "audit" => $row['audit'],
                            "self" => $row['self'],
                            "krit" => $row['krit'],
                            "rus" => $works[$row['job']],
                            "auditproc" => round(($row['audit'] / $row['krit']) * 100, 2),
                            "selfproc" => round(($row['self'] / $row['krit']) * 100, 2)
                        ]
                        ]
                    ]

                ]
            ];
            $i++;
        } else {
            $data1[$i - 1]["id_years"][$year]["job"] += [$row['job'] => [
                "audit" => $row['audit'],
                "self" => $row['self'],
                "krit" => $row['krit'],
                "rus" => $works[$row['job']],
                "auditproc" => round(($row['audit'] / $row['krit']) * 100, 2),
                "selfproc" => round(($row['self'] / $row['krit']) * 100, 2)
            ]
            ];
        }
    }
    return $data1;
}

function report_all_sum()
{
    $data3 = report_all();
    foreach ($data3 as $key => &$value) {
        foreach ($value["id_years"] as $year => $value1) {
            $sumaudit = 0;
            $sumkolaudit = 0;
            $sumkolself = 0;
            $sumself = 0;
            foreach ($value1["job"] as $value2) {
                if ($value2['auditproc'] != 0) {
                    $sumkolaudit++;
                    $sumaudit += $value2["auditproc"];
                }
                if ($value2['selfproc'] != 0) {
                    $sumkolself++;
                    $sumself += $value2["selfproc"];
                }
            }
            if ($sumkolaudit != 0) {
                $data3[$key]["id_years"][$year]['percentage_audit'] = round($sumaudit / $sumkolaudit, 2);
            };
            if ($sumkolself != 0) {
                $data3[$key]["id_years"][$year]['percentage_self'] = round($sumself / $sumkolself, 2);
            };
        }
    }
    return $data3;
}

///робота c деревьяви с двумя уровнями устарел
function tree_to_json($company)
{
    $n = [];
    $db = DB::Connection();
    $query = "SELECT * FROM platform p WHERE p.company IN ('" . $company . "') ORDER BY p.parent";
    //  echo $query;
    $result = $db->query($query);
    while ($val = $result->fetch_assoc()) {
        if ($val['parent']) {
            for ($i = 0; $i < count($n); $i++) {
                if ($n[$i]['id'] == $val['parent']) {
                    $n[$i]['children'][] =
                        ['id' => $val['id'],
                            'text' => $val['name']];
                }
            }
        } else {
            $n[] = ['id' => $val['id'],
                'text' => $val['name'],
                'children' => []
            ];
        }
    }
    return json_encode($n);
}

///робота c деревьяви с 3 уровнями
//function recursion($cookies = false)
//{
//    $datatree1 = [];
//    $db = DB::Connection();
//    $query = "SELECT * FROM platform p WHERE p.company IN ('{$_COOKIE['company']}')";
//    if ($cookies) {
//        $query .= "and p.id IN({$_COOKIE['platform']})";
//    }
//    $query .= " ORDER BY p.parent";
//    $result = $db->query($query);
//    $data = $result->fetch_all(MYSQLI_ASSOC);
//    $datatree = search_of_tree($data);
//    if ($cookies) {
//        foreach ($datatree as $key => $val) {
//            $datatree1[$val['id']] = $datatree[$key];
//        }
//        unset($datatree);
//        $datatree=$datatree1;
//    }
//
//    return $datatree;
//}

function recursion($cookies = false, $onle_tree = false)
{
    $i = 0;
    $j = 0;
    $datatree = [];
    $datatree1 = [];
    $children = [];//исправить
    $children1 = [];//
    $db = DB::Connection();
    $query = "SELECT * FROM platform p WHERE p.company IN ('{$_COOKIE['company']}')";
    if ($cookies) {
        $query .= "and p.id IN({$_COOKIE['platform']})";
    }
    //$query .= " ORDER BY p.parent";
    $query .= " ORDER BY p.lvl asc";
    $result = $db->query($query);
    $data = $result->fetch_all(MYSQLI_ASSOC);

    if ($cookies) {
        $minlvl = $data[0]['lvl'];
        if ($minlvl != 0) {
            foreach ($data as $ldata) {
                if ($minlvl == $ldata['lvl']) {

                    $datatree[$ldata['id']] = ['id' => $ldata['id'],
                        'text' => $ldata['name'],
                        //'icon' => "fa fa-bank",
                        "type" => "level" . $ldata['lvl'],
                        "lvl" => $ldata['lvl'],
                        'children' => []
                    ];
                    $datatree[$ldata['id']]['children'] = search_of_tree($data, $ldata['id'], 0, true);
                }
            }
        } else {
            $datatree = search_of_tree($data);
        }

        if ($onle_tree) {
            foreach ($datatree as $key => $val) {

                $datatree1[$i] = $val;
                foreach ($val['children'] as $key1 => $t) {

                    $children[] = $t;
                    foreach ($t['children'] as $t2) {
                        $children1[] = $t2;
                    }
                    $children[$j]['children'] = $children1;
                    $j++;
                    $children1 = [];

                }

                $datatree1[$i]['children'] = $children;
                $i++;
                $children = [];
                $j = 0;
            }
        } else {
            foreach ($datatree as $key => $val) {
                $datatree1[$val['id']] = $val;
            }
        }


        unset($datatree);
        $datatree = $datatree1;

    } else {
        $datatree = search_of_tree_to_settings($data);
    }
    return $datatree;
}

///робота c деревьяви с 3 уровнями
//function search_of_tree($data, $pid = 0, $level = 0)
//{///проблемма с нулем
//    static $n_row = [];
//    foreach ($data as $row) {
//        if ($row['parent'] == $pid) {
//            //echo $row['parent'];
//            $index = count($n_row) - 1;
//
//            switch ($level) {
//                case 0 :
//                    $n_row[] = ['id' => $row['id'],
//                        'text' => $row['name'],
//                        //'icon' => "fa fa-bank",
//                        "type" => "level0",
//                        'children' => []
//                    ];
//                    break;
//                case 1:
//                    $n_row[$index]['children'][] =
//                        [
//                            'id' => $row['id'],
//                            'text' => $row['name'],
//                            "type" => "level1",
//                            // 'icon' => "fa fa-briefcase",
//                            'children' => []
//                        ];
//
//                    break;
//                case 2:
//                    $n_row[$index]['children'][count($n_row[$index]['children']) - 1]['children'][] =
//                        [
//                            'id' => $row['id'],
//                            'text' => $row['name'],
//                            //  'icon' => "fa  fa-cube",
//                            "type" => "level2"
//                        ];
//                    break;
//            }
//            search_of_tree($data, $row['id'], $level + 1);
//        }
//    }
//    return $n_row;
//}
function search_of_tree($data, $pid = 0, $level = 0, $clear_static = false)
{
    static $n_row = null;
    if ($clear_static) {
        $n_row = null;
    }
    foreach ($data as $row) {

        if ($row['parent'] == $pid) {

            $index = count($n_row) - 1;

            switch ($level) {
                case 0 :
                    If ($pid == 0) {
                        $n_row[] = ['id' => $row['id'],
                            'text' => $row['name'],
                            //'icon' => "fa fa-bank",
                            "type" => "level" . $row['lvl'],
                            "lvl" => $row['lvl'],
                            'children' => []
                        ];
                    } else {
                        $n_row[$row['id']] = ['id' => $row['id'],
                            'text' => $row['name'],
                            //'icon' => "fa fa-bank",
                            "type" => "level" . $row['lvl'],
                            "lvl" => $row['lvl'],
                            'children' => []
                        ];
                    }
                    break;
                case 1:
                    $n_row[$index]['children'][$row['id']] =
                        [
                            'id' => $row['id'],
                            'text' => $row['name'],
                            "type" => "level" . $row['lvl'],
                            "lvl" => $row['lvl'],
                            // 'icon' => "fa fa-briefcase",
                            'children' => []
                        ];

                    break;
                case 2:
                    $n_row[$index]['children'][$pid]['children'][$row['id']] =
                        [
                            'id' => $row['id'],
                            'text' => $row['name'],
                            //  'icon' => "fa  fa-cube",
                            "type" => "level" . $row['lvl'],
                            "lvl" => $row['lvl']
                        ];
                    break;
            }
            search_of_tree($data, $row['id'], $level + 1);
        }
    }
    return $n_row;
}

function search_of_tree_to_settings($data, $pid = 0, $level = 0)
{
    static $n_row;
    foreach ($data as $row) {

        if ($row['parent'] == $pid) {

            $index = count($n_row) - 1;

            switch ($level) {
                case 0 :
                    $n_row[] = ['id' => $row['id'],
                        'text' => $row['name'],
                        //'icon' => "fa fa-bank",
                        "type" => "level" . $row['lvl'],
                        "lvl" => $row['lvl'],
                        'children' => []
                    ];

                    break;
                case 1:
                    $n_row[$index]['children'][] =
                        [
                            'id' => $row['id'],
                            'text' => $row['name'],
                            "type" => "level" . $row['lvl'],
                            "lvl" => $row['lvl'],
                            // 'icon' => "fa fa-briefcase",
                            'children' => []
                        ];
                    break;
                case 2:
                    $n_row[$index]['children'][count($n_row[$index]['children']) - 1]['children'][] =
                        [
                            'id' => $row['id'],
                            'text' => $row['name'],
                            //  'icon' => "fa  fa-cube",
                            "type" => "level" . $row['lvl'],
                            "lvl" => $row['lvl']
                        ];

                    break;
            }

            search_of_tree_to_settings($data, $row['id'], $level + 1);
        }
    }
    return $n_row;
}

////для загрузки и выгрузки файлов в чек листах
/*function allfiles_on_bunch($bunch)
{
    $db = DB::Connection();
    $result = $db->query("SELECT a.id AS 'key', a.f_name AS caption, a.f_path AS downloadUrl ,'app/views/reports/upload.php?delete' AS url , a.f_size AS size FROM attachments a WHERE a.f_bunch in ('" . $bunch . "')
");
    $allfiles = $result->fetch_all(MYSQLI_ASSOC);
    return  $allfiles;
}*/
function allfiles_on_bunch()
{
    $resultArray = [];

    $db = DB::Connection();
    $result = $db->query("SELECT id, t_id, name, path, size FROM attachments WHERE t_id IN (SELECT distinct r.bunch FROM reports r WHERE r.platform IN ({$_COOKIE["platform"]})) ORDER BY t_id");
    while ($row = $result->fetch_assoc()) {
        $resultArray[$row['t_id']][] = [
            'key' => $row['id'],
            'caption' => $row['name'],
            'downloadUrl' => $row['path'],
            'url' => 'upload?delete',
            'size' => $row['size']
        ];

    }
    return $resultArray;
}

;
function allfiles_count_bunch()
{
    $resultArray = [];
    $db = DB::Connection();
    $result = $db->query("SELECT  a.f_bunch,COUNT(a.id) AS sum FROM attachments a GROUP BY a.f_bunch");
    while ($row = $result->fetch_assoc()) {
        $resultArray[$row['f_bunch']] = $row['sum'];
    }
    return $resultArray;
}

///////// task Возвращает пользователей Исполнитлей которые присутствуют у пользователя по площадкам
//добавлено full_name -полные имена без сокращений , login меняет c id = full_name на login(имя id) = full_name
function getUserNameInPlatform($full_name = false, $login = false)
{
    $resultArray = [];
    $db = DB::Connection();
    $query = "SELECT u.id,u.login,";
    if ($full_name) {
        $query .= "CONCAT(u.surname,' ',u.name,' ',u.father_name) AS fullname ";
    } else {
        $query .= "CONCAT(u.surname,' ',SUBSTR(u.name,1,1),'.',SUBSTR(u.father_name,1,1),'.') AS fullname ";
    }
    $query .= "from users u WHERE u.platform REGEXP (SELECT REPLACE(u.platform,',','|') FROM users u WHERE u.login='{$_COOKIE["login"]}')";
    $userInfo = $db->query($query);
    if ($userInfo) {
        while ($uRow = $userInfo->fetch_assoc()) {
            if ($login) {
                $resultArray[$uRow['login']] = $uRow['fullname'];
            } else {
                $resultArray[$uRow['id']] = $uRow['fullname'];
            }
        }
    }
    return $resultArray;
}

function recursive_platf($platform)
{ //для get_user_platform
    $db = DB::Connection();
    $recursive = "CALL recursive('{$platform}')";
    $allplat = $platform;
    $recuv_data = $db->query($recursive);
    if ($recuv_data->num_rows > 0) {
        $dat = [];
        foreach ($recuv_data as $row) {
            $dat[] = $row['id'];
        }
        $allplat = implode($dat, '|');
    }
    return $allplat;
}

function getNameCheck()
{
    $db = DB::Connection();
    $task = $db->query("SELECT `id`, `name`, `type`, `comment` FROM `name_type_task`");
    return $task;
}

///  поиск юзеров по площадкам
function Get_User_Platform($platform, $full_name = false, $login = false)
{
    $db = DB::Connection();
    $platform = intval($platform);
    $resultArray = [];

    $allplat = recursive_platf($platform);

    $query = "SELECT u.id,u.login,";
    if ($full_name) {
        $query .= "CONCAT(u.surname,' ',u.name,' ',u.father_name) AS fullname ";
    } else {
        $query .= "CONCAT(u.surname,' ',SUBSTR(u.name,1,1),'.',SUBSTR(u.father_name,1,1),'.') AS fullname ";
    }
    $query .= "from users u WHERE u.platform REGEXP('{$allplat}') and u.company='{$_COOKIE['company']}'";

    $info = $db->query($query);
    if ($info->num_rows > 0) {
        while ($uRow = $info->fetch_assoc()) {
            if ($login) {
                $resultArray[$uRow['login']] = $uRow['fullname'];
            } else {
                $resultArray[$uRow['id']] = $uRow['fullname'];
            }
        }
    }

    return $resultArray;
}


///  поиск юзеров по площадкам
function GetAllUser()
{
    $db = DB::Connection();
    $platform = intval($platform);
    $resultArray = [];

    $allplat = recursive_platf($platform);

    $query = "SELECT u.id,u.login,";
    if ($full_name) {
        $query .= "CONCAT(u.surname,' ',u.name,' ',u.father_name) AS fullname ";
    } else {
        $query .= "CONCAT(u.surname,' ',SUBSTR(u.name,1,1),'.',SUBSTR(u.father_name,1,1),'.') AS fullname ";
    }
    $query .= "from users u";

    $info = $db->query($query);
    if ($info->num_rows > 0) {
        while ($uRow = $info->fetch_assoc()) {
            // var_dump($resultArray[$uRow['login']]);
            if ($login) {
                $resultArray[$uRow['login']] = $uRow['fullname'];
            } else {
                $resultArray[$uRow['id']] = $uRow['fullname'];
            }
        }
    }

    return $resultArray;
}


function Get_all_type_t($old_name = false)
{
    $data = [];
    $db = DB::Connection();
    $result1 = $db->query("SELECT
  tj.id,
  tj.name,
  tj.type
FROM name_type_task tj ");
    while ($row = $result1->fetch_assoc()) {
        If ($old_name) {
            $data[$row['id']] = $row['type'];
        } else {
            $data[$row['id']] = $row['name'];
        }

    }
    return $data;
}

function GetPlatformUser($row = false)
{
    /* получить все платформы только юзера
    row=true формат [индех(id площадки)=название]
    row=false формат [1=название]*/

    $platform = GetAllPlatforms();//получить все площ
    $data = [];
    $result = Platform_One_User();//получить все площ только этого юзера
    $t = explode(",", $result[0]);
    foreach ($t as $tm) {
        if ($row) {
            array_push($data, $platform[$tm]);
        } else {
            $platform[$tm] != '' ? $data[$tm] = $platform[$tm] : $tm = '';
        }
    };
    return $data;
}


//function getUserNameInPlatform2()
//{
//    $resultArray = [];
//    $db = DB::Connection();
//    $query = "SELECT u.login,CONCAT(u.surname,' ',u.name,' ',u.father_name) AS fullname from users u WHERE u.platform REGEXP (SELECT REPLACE(u.platform,',','|') FROM users u WHERE u.login='{$_COOKIE["login"]}')";
//    $userInfo = $db->query($query);
//    while ($uRow = $userInfo->fetch_assoc()) {
//        $resultArray[$uRow['login']] = $uRow['fullname'];
//    }
//    return $resultArray;
//}

function GetDistinctUsersfromBunch($id) //получение users по группе самоценнок //// переработать
{
    $resultArray = [];
    $db = DB::Connection();
    $query = "SELECT DISTINCT c.fio from collector c 
  JOIN reports r ON c.`self-evaluation`=r.id 
  WHERE r.bunch IN (SELECT r.bunch from collector c
  JOIN reports r ON r.id=c.`self-evaluation` 
  WHERE c.id='{$id}') AND c.fio !=' ' ;";
    $result = $db->query($query);
    if ($result->num_rows != 0) {
        while ($row = $result->fetch_row()) {
            array_push($resultArray, $row[0]);
        }
        return $resultArray;
    } else {
        return [];
    }
}

///устарел
function vid_for_task($pid, $dontid = false, $only = false)
{
    $db = DB::Connection();
    $resultArray = [];
    $query = "SELECT GROUP_CONCAT(t.job) as job FROM tasks t WHERE t.pid in({$pid})";
    $result = $db->query($query);
    If ($result->num_rows > 0) { //null
        $tt = $result->fetch_row();
        if ($tt[0] == null) {
            $tt[0] = 0;
        }
        if ($only) {
            $query_job = "SELECT id,type FROM types_jobs tj WHERE tj.id IN ({$tt[0]})";
        } else {
            if ($dontid != false and gettype($dontid) == "integer") {
                $query .= " and id <> {$dontid} ";
            }
            $query_job = "SELECT id,type FROM types_jobs tj WHERE tj.id NOT IN ({$tt[0]})";
        }
        $result1 = $db->query($query_job);
        while ($row = $result1->fetch_assoc()) {
            $resultArray[$row['id']] = $row['type'];
        }
        return $resultArray;
    }
}

function vid_fast_self($platform)
{
    $result_Array = [];
    $platform = intval($platform);
    $q = "SELECT d.job,tj.type FROM demand_pl dp LEFT JOIN demands d ON dp.demand = d.id left JOIN types_jobs tj ON tj.id=d.job  WHERE JSON_SEARCH(JSON_EXTRACT(dp.settings,'$.quick'),'one','{$platform}')IS NOT NULL";

    $db = DB::Connection();

    $result = $db->query($q);
    if ($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {
            $result_Array[$row['job']] = $row['type'];
        }
    }
    return $result_Array;
}

function subtask_plans($pid, $pk = false)
{
    $result_Array = [];
    $db = DB::Connection();
    if ($pk and gettype($pid) == "array") {//выбрать все job для ПкиПд с основных задач
        $pid = implode(',', $pid);
    }
    $query = "SELECT GROUP_CONCAT(t.job) as job, t.platform FROM tasks t WHERE t.pid in({$pid}) GROUP BY t.platform";//выбрать все подзадачи в текущей задачи
    //$query = "SELECT GROUP_CONCAT(t.job) as job FROM tasks t WHERE t.pid in({$pid})";//выбрать все подзадачи в текущей задачи

    $result = $db->query($query);
    If ($result->num_rows > 0) { //null
        while ($row = $result->fetch_assoc()) {
            $result_Array[$row['platform']] = $row['job'];
        }
    }
    //return $tt[0] == null ? $tt[0] = 0 : $tt[0];
    return $result_Array;
}

function plans_diff($osn_pid, $sub_pid)// разница видов работ нужно переделать
{
    if (gettype($osn_pid) == "array" and gettype($sub_pid) == "integer") {
        $osn = subtask_plans($osn_pid, true);
        $pid = subtask_plans($sub_pid);
    } else {
        return false;
    }
    if (count($pid) != 0) {
        foreach ($pid as $pl => $jobs) {
            $t = explode(",", $jobs);
            $t1 = explode(",", $osn[$pl]);
            $diff = array_diff($t1, $t);
            if (count($diff) == 0) {
                unset($osn[$pl]);
            } else {
                $osn[$pl] = implode(",", $diff);
            }
        };
    }
    return $osn;
}

function plans_text($job, $only = false) ///вид в текст
{
    $db = DB::Connection();
    $resultArray = [];
    $query_job = "SELECT id,type FROM types_jobs tj WHERE tj.id";
    if ($only) {

        $query_job .= " IN ({$job})";
    } else {
        $query_job .= " NOT IN ({$job})";
    }
    $result1 = $db->query($query_job);
    while ($row = $result1->fetch_assoc()) {
        $resultArray[$row['id']] = $row['type'];
    }
    return $resultArray;
}

///////////////////////////////////
function task_fields($id)
{
    $id = intval($id);
    $db = DB::Connection();
    $result = $db->query("select id,
       name,
       description,
       DATE_FORMAT(t.deadline,'%d.%m.%Y') as deadline,
DATE_FORMAT(t.finished,'%d.%m.%Y') as finished,
       closer,
       authority,
       priority,
       director,
       responsible,
       executors,
       STATUS+0 as status,
DATE_FORMAT(t.startnoty,'%d.%m.%Y') as startnoty,
       `whom-noty`,
       comments,
       subtask,
DATE_FORMAT(t.repeattask,'%d.%m.%Y') as repeattask ,type,agreement,subexecutors,job,
       platform from tasks t WHERE t.id={$id} limit 1");
    return $result->fetch_all(MYSQLI_ASSOC)[0];
}


// добавление нулей
function numberFormat($digit, $width)
{
    while (strlen($digit) < $width)
        $digit = '0' . $digit;
    return $digit;
}

//// почта
function getUsers_for_mail()
{
    $resultArray = [];
    $db = DB::Connection();
    $query = "SELECT u.id,u.login,u.name,u.surname,u.father_name,CONCAT(u.surname,' ',u.name,' ',u.father_name) AS fullname,u.email from users u WHERE u.platform REGEXP (SELECT REPLACE(u.platform,',','|') FROM users u WHERE u.login='{$_COOKIE["login"]}')";
    $userInfo = $db->query($query);
    while ($uRow = $userInfo->fetch_assoc()) {
        $resultArray[$uRow['id']] = ["full_name" => $uRow['fullname'],
            "user_name" => $uRow['name'],
            "user_surname" => $uRow['surname'],
            "user_father_name" => $uRow['father_name'],
            "login" => $uRow['login'],
            "mail" => $uRow['email']
        ];
    }
    return $resultArray;
}


function get_enum_values($table, $field) // преобразование enum in array
{
    $db = DB::Connection();

    $type = $db->query("SHOW COLUMNS FROM {$table} WHERE Field = '{$field}'")->fetch_row();
    preg_match("/^enum\(\'(.*)\'\)$/", $type[1], $matches);

    $enum = explode("','", $matches[1]);
    $array = [];

    for ($i = 0; $i < count($enum); $i++) {
        $array[$i + 1] = $enum[$i];
    }
    return $array;
}

function get_task($id, $pid = false)
{
    $data = [];
    $id = intval($id);
    $db = DB::Connection();
    $query = "SELECT GROUP_CONCAT(t.job) AS job,t.nreport AS bunch FROM tasks t ";
    If ($pid) {
        $query .= "WHERE pid={$id} GROUP BY bunch";
    } else {
        $query .= "WHERE id={$id} GROUP BY bunch";
    }
    $result = $db->query($query);

    foreach ($result as $row) {
        $row['job']=trim($row['job'],',');
        $data[] = "SELECT  r.id FROM reports r WHERE r.bunch = {$row['bunch']} AND r.job IN({$row['job']})";
    }
    return $data;
}

//////поиск

function getNameTask($type)
{
    $db = DB::Connection();
    $query = "SELECT `name` FROM `name_type_task` WHERE `type` = '{$type}'";
    $result = $db->query($query)->fetch_assoc();
    $nameTask = $result['name'];
    return $nameTask;
}

function getDivision()
{
    $db = DB::Connection();
    $query = "SELECT id, `name_division` FROM `divisions` GROUP BY `name_division`";
    $result = $db->query($query);
    return $result;
}

function getSmallFioByFull($surname, $name, $fathername)
{
    if ($surname <> '' AND $name <> '' AND $fathername <> '') {
        $small_fio = $surname . ' ' . mb_substr($name, 0, 1, "utf-8") . '.' . mb_substr($fathername, 0, 1, "utf-8") . '.';
    }
    if ($surname <> '' AND $name <> '' AND $fathername == '') {
        $small_fio = $surname . ' ' . mb_substr($name, 0, 1, "utf-8") . '.';
    } else if
    ($surname <> '' AND $name == '' AND $fathername == ''
    ) {
        $small_fio = $surname;
    }
    return $small_fio;
}

/* Получаем хост сервера для all_const.php*/
function myHost()
{
    $host = $_SERVER['HTTP_HOST'];
    $protocol = $_SERVER['PROTOCOL'] = isset($_SERVER['HTTPS']) && !empty($_SERVER['HTTPS']) ? 'https' : 'http';
    $fullhost = $protocol . "://" . $host;
    return $fullhost;
}

/* Получаем список действия инициатора в массиве */
function getIniciatorAction()
{
    $data = [];
    $db = DB::Connection();
    $result = $db->query("SELECT `id`, `action_name` FROM `nearmiss_initiator_actions`");
    foreach ($result as $key => $value) {
        $data[$key]['id'] = $value['id'];
        $data[$key]['action_name'] = $value['action_name'];
    }
    return $data;
}

////////// Заменить или удалить потом. Яковлев 04.06.2020. Для Неамисов
function getUserNameInPlatform2($full_name = false, $login = false)
{
    $resultArray = [];
    $db = DB::Connection();
    $query = "SELECT u.id,u.login,";
    if ($full_name) {
        $query .= "CONCAT(u.surname,' ',u.name,' ',u.father_name) AS fullname ";
    } else {
        $query .= "CONCAT(u.surname,' ',SUBSTR(u.name,1,1),'.',SUBSTR(u.father_name,1,1),'.') AS fullname ";
    }
    $query .= "from users u WHERE u.rols='Executor'";
    $userInfo = $db->query($query);
    if ($userInfo) {
        while ($uRow = $userInfo->fetch_assoc()) {
            if ($login) {
                $resultArray[$uRow['login']] = $uRow['fullname'];
            } else {
                $resultArray[$uRow['id']] = $uRow['fullname'];
            }
        }
    }
    return $resultArray;
}

?>